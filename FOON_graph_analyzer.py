###############################################################################################################################
# 						FOON: Graph Analyzer (FOON_graph_analyzer) 								#
# 				--------------------------------------------------------------------						#
# 			Written by David Paulius (davidpaulius@usf.edu) of RPAL (University of South Florida) 				#
#			Maintained by Md Sadman Sakib (mdsadman@usf.edu) of RPAL (University of South Florida)				#
#		   	    with special thanks to Kelvin Dong Sheng Pei and Sanjeeth Bhat for assistance				#
###############################################################################################################################

# NOTE: If using this program and/or annotations provided by our lab, please kindly cite or refer to the following papers:
# 	* Paulius et al. 2016 - https://ieeexplore.ieee.org/abstract/document/7759413/
#							OR
#	* Paulius et al. 2018 - https://ieeexplore.ieee.org/abstract/document/8460200/

from __future__ import print_function
from configparser import ConfigParser
from builtins import input
import collections
import getopt
import itertools as IT
import os
import sys
import time
import ast

import tqdm

last_updated = '24th August, 2021'

# NOTE: you MUST have the accompanying 'FOON_classes.py'!
# -- you should probably find this file in the same repository as you found this script.
try:
    import FOON_classes as FOON
except ImportError:
    print(" -- ERROR: Missing 'FOON_classes.py' file! Make sure you have downloaded the accompanying class file!")
    print("\t-- Download here: https://bitbucket.org/davidpaulius/foon_api/src/master/")
    exit()
# end

# NOTE: Sadman recently introduced config files; here, we check config file for any flags (if one is present):

config = None
try:
    config_file = 'config.ini'
    config = ConfigParser()
    config.read_file(open(config_file))
except FileNotFoundError:
    pass
else:
    print(" -- Loaded configuration file 'config.ini' !")
# end

###############################################################################################################################

# NOTE: The following variables are typically referenced as global variables in inner functions or references:
FOON_node_count = 0  # -- total number of nodes (w.r.t level 3 of hierarchy):

# NOTE: list of all functional units for each hierarchy level:
#  -- hierarchy levels, FOON-EXP and FOON-GEN discussed in greater detail in Paulius et. al 2018
FOON_lvl1 = []
FOON_lvl2 = []
FOON_lvl3 = []

# NOTE: list of all nodes used for the functional unit lists above for each hierarchy level:
nodes_lvl1 = []
nodes_lvl2 = []
nodes_lvl3 = []

# NOTE: The following are dictionary structures used to map:
#  -- output objects to functional units:
FOON_outputsToUnits_lvl1 = {}
FOON_outputsToUnits_lvl2 = {}
FOON_outputsToUnits_lvl3 = {}
#  -- object to functional units (for both inputs or outputs) :
FOON_objectsToUnits_lvl1 = {}
FOON_objectsToUnits_lvl2 = {}
FOON_objectsToUnits_lvl3 = {}
#  -- functional units to other units (determined by existence of overlapping input and output objects) :
FOON_functionalUnitMap_lvl1 = {}
FOON_functionalUnitMap_lvl2 = {}
FOON_functionalUnitMap_lvl3 = {}
#  -- motion nodes from the lists above to the functional units they are part of:
motionsToFunctionalUnits_lvl1 = {}
motionsToFunctionalUnits_lvl2 = {}
motionsToFunctionalUnits_lvl3 = {}

# NOTE: list collecting all of these lists and dictionaries:
# -- list of lists of functional units at all levels
FOON_functionalUnits = [FOON_lvl1, FOON_lvl2, FOON_lvl3]
# -- list of lists of nodes at all levels
FOON_nodes = [nodes_lvl1, nodes_lvl2, nodes_lvl3]
# -- list of all dictionaries mapping output objects to their respective functional units:
FOON_outputsToUnits = [FOON_outputsToUnits_lvl1,
                       FOON_outputsToUnits_lvl2, FOON_outputsToUnits_lvl3]
# -- list of all dictionaries mapping ALL objects to their respective functional units:
FOON_objectsToUnits = [FOON_objectsToUnits_lvl1,
                       FOON_objectsToUnits_lvl2, FOON_objectsToUnits_lvl3]
# -- list of all dictionaries mapping functional units to all other functional units with overlapping objects (inputs or outputs):
FOON_functionalUnitMap = [FOON_functionalUnitMap_lvl1,
                          FOON_functionalUnitMap_lvl2, FOON_functionalUnitMap_lvl3]
# -- list of all dictionaries mapping motion nodes to their respective functional units:
motionsToFunctionalUnits = [motionsToFunctionalUnits_lvl1,
                            motionsToFunctionalUnits_lvl2, motionsToFunctionalUnits_lvl3]

# NOTE: dictionary objects used for one-mode projection (graph of ONLY object nodes):
FOON_oneModeProjection_lvl1 = {}
FOON_oneModeProjection_lvl2 = {}
FOON_oneModeProjection_lvl3 = {}
# -- list of objects that are used in one-mode projection of object nodes:
objects_oneModeProjection_lvl1 = []
objects_oneModeProjection_lvl2 = []
objects_oneModeProjection_lvl3 = []

FOON_oneModeProjection = [FOON_oneModeProjection_lvl1,
                          FOON_oneModeProjection_lvl2, FOON_oneModeProjection_lvl3]
objects_oneModeProjection = [objects_oneModeProjection_lvl1,
                             objects_oneModeProjection_lvl2, objects_oneModeProjection_lvl3]

# NOTE: dictionary mapping labels to IDs for objects, motions, and states:
FOON_objectLabels = {}
FOON_motionLabels = {}
FOON_stateLabels = {}

# NOTE: storing the sense id for objects (based on WordNet) or to denote Concept-Net sense -- these were verified semi-automatically using parser!
object_senses = {}

# NOTE: the following are dictionaries used for mapping categories to object labels (for generalization of FOON):
category_index = {}
object_categories = {}

# NOTE: these lists are used for the generalization of FOON:
# -- Two "generalized" versions of FOON:
# 	1. FOON-EXP - expanded version of FOON that uses WordNet/Concept-Net similarities to create new units;
#		-- this would use the regular FOON lists from above since we perform expansion and read the new file created
# 	2. FOON-GEN - compressed version of FOON that uses object categories
# -- please refer to Paulius et al. 2018 for more explanation on these approaches.
FOON_GEN = []
nodes_GEN = []
FOON_object_map_GEN = {}
flag_EXP_complete = False
flag_GEN_complete = False

# NOTE: this dictionary is used to store the similarity value between pairs of objects:
object_similarity_index = {}

verbose = False  # -- change this if you would like more output to the console

# NOTE: name of the file name used for this FOON analyzer script:
# -- change this if you want to set default universal FOON file to FIXED file
file_name = None

# NOTE: maximum depth to search for task tree:
depth = 25  # -- can be changed to larger number if diameter of FOON is large

# NOTE: flags used for _buildInternalMaps function:
flag_buildFunctionalUnitMap = False
flag_buildObjectToUnitMap = False
flag_mapsReady = False

FOON_video_source = []

# NOTE: global variable to store the location of the Concept-Net word embedding model (numberbatch);
#	you can download or find out more about this model here: https://github.com/commonsense/Concept-Net-numberbatch
#  -- override this with an actual path if you want to skip the prompts.
path_to_ConceptNet = None

# NOTE: version 2 means that we have performed functional unit chain compression into "combinational" functional units / hubs.
# -- see function *_findFunctionalUnitClusters* for more details
FOON_lvl1_ver2 = []
FOON_lvl2_ver2 = []
FOON_lvl3_ver2 = []
FOON_unitClusters = [FOON_lvl1_ver2, FOON_lvl2_ver2, FOON_lvl3_ver2]

###############################################################################################################################

# NOTE: loading of internal dictionaries that are used for the searching process:


def _buildInternalMaps():
    # NOTE: this function builds dictionary structures that will be used for the searching algorithms:
    print('\n -- [FOON] : Building internal dictionaries..')

    # -- make sure everything is cleared and then initialized:
    _resetMaps()

    # -- build mapping between output objects to the units that make them:
    _buildOutputsToUnitMap()

    # -- build mapping between functional units whose outputs overlap with another's inputs:
    _buildFunctionalUnitMap()

    # -- building mapping between all objects and the functional units they appear in:
    _buildObjectToUnitMap()
# enddef


def _buildOutputsToUnitMap():
    # NOTE: create a mapping between all objects and related units based on outputs:
    _buildOutputsToUnitMap_lvl1()
    print('  -- Level 1: Output object map complete..')
    _buildOutputsToUnitMap_lvl2()
    print('  -- Level 2: Output object map complete..')
    _buildOutputsToUnitMap_lvl3()
    print('  -- Level 3: Output object map complete..')
# enddef


def _buildOutputsToUnitMap_lvl1():
    global FOON_outputsToUnits_lvl1, nodes_lvl1, FOON_lvl1
    for _input in nodes_lvl1:
        if isinstance(_input, FOON.Object):
            procedures = []
            for _U in FOON_lvl1:
                for _output in _U.getOutputList():
                    if _input.equals_functions[0](_output):
                        procedures.append(_U)
                        break
                    # endif
                # endfor
            # endfor
            FOON_outputsToUnits_lvl1[_input] = procedures
        # endif
    # endfor
# enddef


def _buildOutputsToUnitMap_lvl2():
    global FOON_outputsToUnits_lvl2, nodes_lvl2, FOON_lvl2
    for _input in nodes_lvl2:
        if isinstance(_input, FOON.Object):
            procedures = []
            for _U in FOON_lvl2:
                for _output in _U.getOutputList():
                    if _input.equals_functions[1](_output):
                        procedures.append(_U)
                        break
                    # endif
                # endfor
            # endfor
            FOON_outputsToUnits_lvl2[_input] = procedures
        # endif
    # endfor
# enddef


def _buildOutputsToUnitMap_lvl3():
    global FOON_outputsToUnits_lvl3, nodes_lvl3, FOON_lvl3
    for _input in nodes_lvl3:
        if isinstance(_input, FOON.Object):
            procedures = []
            for _U in FOON_lvl3:
                for _output in _U.getOutputList():
                    if _input.equals_functions[2](_output):
                        procedures.append(_U)
                        break
                    # endif
                # endfor
            # endfor
            FOON_outputsToUnits_lvl3[_input] = procedures
        # endif
    # endfor
# enddef


def _buildObjectToUnitMap():
    # NOTE: create a mapping between all objects and related units:
    global flag_buildObjectToUnitMap

    if flag_buildObjectToUnitMap is True:
        _buildObjectToUnitMap_lvl1()
        print('  -- Level 1: Object map complete..')
        _buildObjectToUnitMap_lvl2()
        print('  -- Level 2: Object map complete..')
        _buildObjectToUnitMap_lvl3()
        print('  -- Level 3: Object map complete..')
# enddef


def _buildObjectToUnitMap_lvl1():
    global FOON_objectsToUnits_lvl1
    for N in nodes_lvl1:
        if isinstance(N, FOON.Object):
            procedures = []
            for _U in FOON_outputsToUnits_lvl1[N]:
                procedures.append(_U)
            for _U in FOON_lvl1:
                for _input in _U.getInputList():
                    if _input.equals_functions[0](N):
                        procedures.append(_U)
                        break
                    # endif
                # endfor
            # endfor
            FOON_objectsToUnits_lvl1[N] = list(set(procedures))
        # endif
    # endfor
# enddef


def _buildObjectToUnitMap_lvl2():
    global FOON_objectsToUnits_lvl2
    for N in nodes_lvl2:
        if isinstance(N, FOON.Object):
            procedures = []
            for _U in FOON_outputsToUnits_lvl2[N]:
                procedures.append(_U)
            for _U in FOON_lvl2:
                for _input in _U.getInputList():
                    if _input.equals_functions[1](N):
                        procedures.append(_U)
                        break
                    # endif
                # endfor
            # endfor
            FOON_objectsToUnits_lvl2[N] = list(set(procedures))
        # endif
    # endfor
# enddef


def _buildObjectToUnitMap_lvl3():
    global FOON_objectsToUnits_lvl3
    for N in nodes_lvl3:
        if isinstance(N, FOON.Object):
            procedures = []
            for _U in FOON_outputsToUnits_lvl3[N]:
                procedures.append(_U)
            # endfor
            for _U in FOON_lvl3:
                for _input in _U.getInputList():
                    if _input.equals_functions[2](N):
                        procedures.append(_U)
                        break
                    # endif
                # endfor
            # endfor
            FOON_objectsToUnits_lvl3[N] = list(set(procedures))
        # endif
    # endfor
# enddef


def _buildFunctionalUnitMap():
    # NOTE: create a mapping between functional units to show
    # 	which ones are connected to one another.
    global flag_buildFunctionalUnitMap
    if flag_buildFunctionalUnitMap is True:
        _buildUnitToUnitMap_lvl1()
        print('  -- Level 1: Functional unit map complete..')
        _buildUnitToUnitMap_lvl2()
        print('  -- Level 2: Functional unit map complete..')
        _buildUnitToUnitMap_lvl3()
        print('  -- Level 3: Functional unit map complete..')
# enddef


def _buildUnitToUnitMap_lvl1():
    global FOON_functionalUnitMap_lvl1
    for _FU in FOON_lvl1:
        prerequisite_units = []
        for _input in _FU.getInputList():
            # -- we already collected the units that create every single input object in FOON:
            candidates = FOON_outputsToUnits_lvl1.get(_input, [])

            for C in candidates:
                if FOON_lvl1.index(C) not in prerequisite_units:
                    prerequisite_units.append(FOON_lvl1.index(C))
        # endfor
        FOON_functionalUnitMap_lvl1[FOON_lvl1.index(_FU)] = prerequisite_units
    # endfor
# enddef


def _buildUnitToUnitMap_lvl2():
    global FOON_functionalUnitMap_lvl2
    for _FU in FOON_lvl2:
        prerequisite_units = []
        for _input in _FU.getInputList():
            # -- we already collected the units that create every single input object in FOON:
            candidates = FOON_outputsToUnits_lvl2.get(_input, [])

            for C in candidates:
                if FOON_lvl2.index(C) not in prerequisite_units:
                    prerequisite_units.append(FOON_lvl2.index(C))
        # endfor
        FOON_functionalUnitMap_lvl2[FOON_lvl2.index(_FU)] = prerequisite_units
    # endfor
# enddef


def _buildUnitToUnitMap_lvl3():
    global FOON_functionalUnitMap_lvl3
    for _FU in FOON_lvl3:
        prerequisite_units = []
        for _input in _FU.getInputList():
            # -- we already collected the units that create every single input object in FOON:
            candidates = FOON_outputsToUnits_lvl3.get(_input, [])
            for C in candidates:
                if FOON_lvl3.index(C) not in prerequisite_units:
                    prerequisite_units.append(FOON_lvl3.index(C))
        # endfor
        FOON_functionalUnitMap_lvl3[FOON_lvl3.index(_FU)] = prerequisite_units
    # endfor
# enddef


def _printObjectToUnitMap(hierarchy_level):
    if hierarchy_level == 1:
        objectMap = FOON_outputsToUnits_lvl1
    elif hierarchy_level == 2:
        objectMap = FOON_outputsToUnits_lvl2
    elif hierarchy_level == 3:
        objectMap = FOON_outputsToUnits_lvl3
    else:
        return

    for _key in objectMap:
        _key.print_functions[hierarchy_level-1]()
        for _FU in objectMap[_key]:
            print("{")
            _FU.print_functions[hierarchy_level-1]()
            print("}\n")
        # endfor
    # endfor
# enddef


def _printFunctionalUnitMap():
    for _key in FOON_functionalUnitMap_lvl3:
        print("SOURCE:")
        _key.print_functions[2]()
        print("\nTARGET(S):")
        for _FU in FOON_functionalUnitMap_lvl3[_key]:
            print("{")
            _FU.print_functions[2]()
            print("}\n")
        # endfor
    # endfor
# enddef


def _readIndexFiles(FOON_objectLabels, FOON_motionLabels, FOON_stateLabels):
    print('\n-- Reading index files..')

    # NOTE: first, try to open combined .JSON file (contains all labels for objects, states, and motions):

    try:
        import json
        FOON_index = json.load(open('FOON_index.json', 'r'))

    except FileNotFoundError:
        print(" -- WARNING: Combined index file 'FOON_index.json' not found in current directory!")
        print("\t-- Using legacy text files instead!")

    else:
        for O in FOON_index['objects']:
            FOON_objectLabels[O] = int(FOON_index['objects'][O]['id'])
            if 'sense' in FOON_index['objects'][O]:
                object_senses[O] = int(FOON_index['objects'][O]['sense']) if str(
                    FOON_index['objects'][O]['sense']).isdigit() else FOON_index['objects'][O]['sense']
            else:
                object_senses[O] = 1

        for S in FOON_index['states']:
            FOON_stateLabels[S] = int(FOON_index['states'][S]['id'])

        for M in FOON_index['motions']:
            FOON_motionLabels[M] = int(FOON_index['motions'][M]['id'])

        return
    # end

    # NOTE: if we get here, that means that we do not have the .JSON file, so just use regular text files:
    try:
        _file = open('FOON-object_index.txt', 'r')
    except FileNotFoundError:
        print(" -- WARNING: File 'FOON-object_index.txt' not found in current directory!")
    else:
        items = _file.read().splitlines()
        for L in items:
            if L.startswith("//"):
                continue
            _parts = L.split("\t")
            FOON_objectLabels[_parts[1]] = int(_parts[0])
            if len(_parts) > 2:
                object_senses[_parts[1]] = int(_parts[2]) if str(
                    _parts[2]).isdigit() else _parts[2]
            else:
                object_senses[_parts[1]] = 1
        # endfor
        _file.close()
    # end

    try:
        _file = open('FOON-motion_index.txt', 'r')
    except FileNotFoundError:
        print(" -- WARNING: File 'FOON-motion_index.txt' not found in current directory!")
    else:
        items = _file.read().splitlines()
        for L in items:
            if L.startswith("//"):
                continue
            _parts = L.split("\t")
            FOON_motionLabels[_parts[1]] = int(_parts[0])
        # endfor
        _file.close()
    # end

    try:
        _file = open('FOON-state_index.txt', 'r')
    except FileNotFoundError:
        print(" -- WARNING: File 'FOON-state_index.txt' not found in current directory!")
    else:
        items = _file.read().splitlines()
        for L in items:
            if L.startswith("//"):
                continue
            _parts = L.split("\t")
            FOON_stateLabels[_parts[1]] = int(_parts[0])
        # endfor
        _file.close()
    # end
# enddef

###############################################################################################################################

# NOTE: network centrality algorithms for analysis:


def _buildOneModeProjections(hierarchy_level=None):
    # NOTE: purpose of the function is to create one-mode projection of FOON at all levels:
    if not hierarchy_level:
        # -- just build them all...
        _buildOneModeProjection_lvl1()
        _buildOneModeProjection_lvl2()
        _buildOneModeProjection_lvl3()
    elif hierarchy_level == 1:
        _buildOneModeProjection_lvl1()
    elif hierarchy_level == 2:
        _buildOneModeProjection_lvl2()
    elif hierarchy_level == 3:
        _buildOneModeProjection_lvl3()
    else:
        return
# enddef


def _buildOneModeProjection_lvl1():
    global objects_oneModeProjection_lvl1, FOON_oneModeProjection_lvl1
    for _node in objects_oneModeProjection_lvl1:
        source = objects_oneModeProjection_lvl1.index(_node)
        dest = set()
        for _motion in _node.getNeighbourList():
            for _output in _motion.getNeighbourList():
                dest.add(objects_oneModeProjection_lvl1.index(_output))
            # endfor
        # endfor
        FOON_oneModeProjection_lvl1[source] = dest
    # endfor
# enddef


def _buildOneModeProjection_lvl2():
    global objects_oneModeProjection_lvl2, FOON_oneModeProjection_lvl2
    for _node in objects_oneModeProjection_lvl2:
        source = objects_oneModeProjection_lvl2.index(_node)
        dest = set()
        for _motion in _node.getNeighbourList():
            for _output in _motion.getNeighbourList():
                dest.add(objects_oneModeProjection_lvl2.index(_output))
            # endfor
        # endfor
        FOON_oneModeProjection_lvl2[source] = dest
    # endfor
# enddef


def _buildOneModeProjection_lvl3():
    global objects_oneModeProjection_lvl3, FOON_oneModeProjection_lvl3
    for _node in objects_oneModeProjection_lvl3:
        source = objects_oneModeProjection_lvl3.index(_node)
        dest = set()
        for _motion in _node.getNeighbourList():
            for _output in _motion.getNeighbourList():
                dest.add(objects_oneModeProjection_lvl3.index(_output))
            # endfor
        # endfor
        FOON_oneModeProjection_lvl3[source] = dest
    # endfor
# enddef


def _calculateCentrality(hierarchy_level):
    # NOTE: Refer to "Networks: An Introduction" by Mark Newman (more info: https://dl.acm.org/doi/book/10.5555/1809753)
    # 	for an excellent overview of this algorithm and many other neat graph theory tricks and concepts.

    global file_name, verbose
    try:
        # -- if you don't have NumPy.. then you're gonna have a bad time.
        import numpy as np
    except ImportError:
        print(" -- ERROR: NumPy not found! Please install NumPy to use this function!")
        return

    # -- first, we need to get the one-mode projection so that we can interpret the results after:
    objectList = None
    searchMap = None
    if hierarchy_level == 1:
        objectList = objects_oneModeProjection_lvl1
        searchMap = FOON_oneModeProjection_lvl1
    elif hierarchy_level == 2:
        objectList = objects_oneModeProjection_lvl2
        searchMap = FOON_oneModeProjection_lvl2
    else:
        objectList = objects_oneModeProjection_lvl3
        searchMap = FOON_oneModeProjection_lvl3

    # -- get adjacency matrix for one-mode projection of FOON:
    oneModeMatrix = _populateAdjacencyMatrix(hierarchy_level)

    # -- determining the dimensions of the adjacency matrix:
    num_elements = oneModeMatrix.shape[1]

    if verbose:
        # -- saving the adjacency matrix to a file for verification:
        np.savetxt("adjacency_matrix.csv", oneModeMatrix, delimiter=",")

    # -- calculate eigenvalues for each object node in FOON:
    eigenvalues, eigenvectors = np.linalg.eig(oneModeMatrix)
    max_index = np.argmax(eigenvalues)
    max_eigenvalue = eigenvalues[max_index]

    max_eigen = 0
    _file = open(os.path.splitext(file_name)[
                 0] + "_eigenvector_lvl" + str(hierarchy_level) + ".txt", 'w')
    for E in range(num_elements):
        _file.write(objectList[E].getObjectLabel(
        ) + '_' + str(objectList[E].getStatesList()) + "\t" + str(np.real(eigenvectors[max_index][E])) + '\n')
        if eigenvectors[max_index][E] > eigenvectors[max_index][max_eigen]:
            max_eigen = E
    _file.close()

    print('\n -- [NET-CENT] : Object with largest eigenvector centrality value (value=' +
          str(np.real(eigenvectors[max_index][max_eigen])) + ') is :')
    objectList[max_eigen].print_functions[hierarchy_level-1]()

    # -- necessary values for Katz centrality computation:
    # NOTE: recommended that it is less than 1/K^1, so 0.25 was added to arbitrarily meet this requirement.
    alpha = 1 / (max_eigenvalue + 0.25)
    B = np.ones((num_elements, 1))
    I = np.eye(num_elements)
    A = np.subtract(I, np.multiply(oneModeMatrix, alpha))

    # -- calculate Katz centrality, which simplifies to Ax = B (as per 7.10 in Newman):
    X = np.linalg.solve(A, B)
    max_katz = 0
    _file = open(os.path.splitext(file_name)[
                 0] + "_katz_lvl" + str(hierarchy_level) + ".txt", 'w')
    for E in range(num_elements):
        _file.write(objectList[E].getObjectLabel(
        ) + '_' + str(objectList[E].getStatesList()) + "\t" + str(np.real(X.item(E))) + '\n')
        if X[E] > X[max_katz]:
            max_katz = E
    _file.close()

    # NOTE: Katz centrality : pick the node with the largest computed value in X:
    print('\n -- [NET-CENT] : Object with largest Katz centrality value (katz=' +
          str(np.real(X[max_katz])) + ') is :')
    objectList[max_katz].print_functions[hierarchy_level-1]()

    # -- calculate degree centrality:
    _file = open(os.path.splitext(file_name)[
                 0] + "_degree_lvl" + str(hierarchy_level) + ".txt", 'w')
    max_value = 0
    for E in range(len(objectList)):
        _file.write(objectList[E].getObjectLabel(
        ) + '_' + str(objectList[E].getStatesList()) + "\t" + str(len(searchMap[E])) + '\n')
        if len(searchMap[E]) > max_value:
            max_value = len(searchMap[E])
    _file.close()

    max_deg = []
    for x in range(len(objectList)):
        if len(searchMap[x]) == max_value:
            max_deg.append(x)

    print('\n -- [NET-CENT] : Object(s) with largest degree centrality value (n_neighbours = ' +
          str(max_value) + ') are :')
    for x in max_deg:
        objectList[x].print_functions[hierarchy_level-1]()
        print("------------------")
# enddef


def _populateAdjacencyMatrix(hierarchy_level):
    # NOTE: Refer to "Networks: An Introduction" by Mark Newman (more info: https://dl.acm.org/doi/book/10.5555/1809753)
    # 	for an excellent overview of this algorithm and many other neat graph theory tricks and concepts.

    global verbose
    searchMap = None

    try:
        import numpy as np
    except ImportError:
        print(" -- ERROR: NumPy not found! Please install NumPy to use this function!")
        return

    if hierarchy_level == 1:
        searchMap = FOON_oneModeProjection_lvl1
    elif hierarchy_level == 2:
        searchMap = FOON_oneModeProjection_lvl2
    elif hierarchy_level == 3:
        searchMap = FOON_oneModeProjection_lvl3
    else:
        return

    if not FOON_oneModeProjection_lvl3:
        _buildOneModeProjections(hierarchy_level=hierarchy_level)

    # -- create an adjacency matrix of size N x N, where N is number of nodes (i.e. both object and motion) :
    oneModeMatrix = np.eye((len(searchMap)))
    for src, tgt in searchMap.items():
        for x in tgt:
            oneModeMatrix[src][x] = 1

    if verbose:
        print(
            ' -- [NET-CENT] Adjacency matrix for one-mode projection is as follows:')
        print(oneModeMatrix)

    return oneModeMatrix
# enddef

###############################################################################################################################

# NOTE: functions used in loading FOON graph from parsing of files:


def _checkIfFUExists(U, H):
    if H == 1:
        if not FOON_lvl1:
            return False
        for _F in FOON_lvl1:
            if _F.equals_lvl1(U):
                return True
        return False
    if H == 2:
        if not FOON_lvl2:
            return False
        for _F in FOON_lvl2:
            if _F.equals_lvl2(U):
                return True
        return False
    if H == 3:
        if not FOON_lvl3:
            return False
        for _F in FOON_lvl3:
            if _F.equals_lvl3(U):
                return True
        return False
    else:
        pass
# enddef


def _checkIfNodeExists(O, H):
    objectExisting = -1
    if H == 1:
        for N in nodes_lvl1:
            if isinstance(N, FOON.Object) and N.equals_functions[H-1](O):
                objectExisting = nodes_lvl1.index(N)
    elif H == 2:
        for N in nodes_lvl2:
            if isinstance(N, FOON.Object) and N.equals_functions[H-1](O):
                objectExisting = nodes_lvl2.index(N)
    elif H == 3:
        for N in nodes_lvl3:
            if isinstance(N, FOON.Object) and N.equals_functions[H-1](O):
                objectExisting = nodes_lvl3.index(N)
    else:
        pass

    return objectExisting
# enddef


def _constructFOON(file=None):
    # NOTE: entry point function to load a FOON subgraph file, which may either be a .TXT, .PKL or .JSON file:
    global file_name
    if not file:
        file = file_name
    else:
        file_name = file

    print("\n -- [FOON] : Reading file '" + str(file) + "'..")

    if '.txt' in file.lower():
        _constructFOON_as_txt(file)
        FOON.print_old_style = True
    elif '.json' in file.lower():
        # -- give a .JSON file, which is typically larger than the average text file, but it structures things neatly:
        _constructFOON_as_JSON(file)
        FOON.print_old_style = False
    elif '.pkl' in file.lower():
        # WARNING: ONLY USE .PKL FILE THAT HAS BEEN PROCESSED ALREADY!!
        # -- users can load a .PKL file containing the structures and references for an already processed universal FOON:
        _loadFOON_pkl(file)
    else:
        print(' -- WARNING: Wrong file type or format!')
        print("  -- Skipping: '" + str(file) + "' ...")
# enddef


def _constructFOON_as_txt(file=None):
    # NOTE: this is the .TXT file variant of the FOON subgraph loading function:

    # NOTE: 'FOON_node_count' indicates the number of nodes (i.e. both object AND motion nodes) exist in a universal FOON.
    # -- this number is based on the hierarchy level 3.
    global FOON_node_count

    global FOON_video_source, verbose

    global FOON_lvl1, FOON_lvl2, FOON_lvl3, nodes_lvl1, nodes_lvl2, nodes_lvl3

    # -- objects used to contain the split strings
    stateParts, objectParts, motionParts = [], [], []

    # -- isInput - flag used to switch between adding to input or output nodes list for each functional unit
    isInput = True

    # -- newObject - this stores an object that is in the process of being read; this is important since we can have multiple AND variable states.
    newObject = None

    # -- objects which will hold the functional unit being read:
    newFU_lvl1, newFU_lvl2, newFU_lvl3 = FOON.FunctionalUnit(
    ), FOON.FunctionalUnit(), FOON.FunctionalUnit()

    _file = open(file, 'r')
    items = _file.read().splitlines()

    line_count = 0

    for line in tqdm.tqdm(items, desc='Reading file line '):
        line_count += 1

        # -- checking flag for verbose (print-outs):
        if verbose:
            print('line ' + str(line_count) + ' - ' + line)

        try:

            if line.startswith("# Source:"):
                line = line.split('\t')
                FOON_video_source.append(line[1].strip('\n'))

            elif line.startswith("//"):
                if newObject:
                    # -- the last output object is not added right away since we need to see if we do not know how many states we are expecting:
                    _addObjectToFOON(
                        newObject, isInput, objectParts[2], newFU_lvl3, newFU_lvl2, newFU_lvl1)

                if newFU_lvl3.isEmpty():
                    # -- this means that we did not add anything to this functional unit object, so just continue:
                    continue

                # -- we are adding a new FU, so start from scratch..
                if _checkIfFUExists(newFU_lvl3, 3) == False:
                    # NOTE: no matter what, we add new motion nodes; we will have multiple instances everywhere.
                    nodes_lvl3.append(newFU_lvl3.getMotion())
                    FOON_lvl3.append(newFU_lvl3)
                    motionsToFunctionalUnits_lvl3[nodes_lvl3.index(
                        newFU_lvl3.getMotion())] = newFU_lvl3
                    # -- we only keep track of the total number of nodes in the LVL3 FOON.
                    FOON_node_count += 1

                if _checkIfFUExists(newFU_lvl2, 2) == False:
                    nodes_lvl2.append(newFU_lvl2.getMotion())
                    FOON_lvl2.append(newFU_lvl2)
                    motionsToFunctionalUnits_lvl2[nodes_lvl2.index(
                        newFU_lvl2.getMotion())] = newFU_lvl2

                if _checkIfFUExists(newFU_lvl1, 1) == False:
                    nodes_lvl1.append(newFU_lvl1.getMotion())
                    FOON_lvl1.append(newFU_lvl1)
                    motionsToFunctionalUnits_lvl1[nodes_lvl1.index(
                        newFU_lvl1.getMotion())] = newFU_lvl1

                # -- create an entirely new FU object to proceed with reading new units.
                newFU_lvl1, newFU_lvl2, newFU_lvl3 = FOON.FunctionalUnit(
                ), FOON.FunctionalUnit(), FOON.FunctionalUnit()

                # -- this is the end of a FU so we will now be adding input nodes; set flag to TRUE.
                isInput = True
                newObject = None

            elif line.startswith("O"):
                # -- we have an Object already in consideration which we were appending states to:
                if newObject:
                    _addObjectToFOON(
                        newObject, isInput, objectParts[2], newFU_lvl3, newFU_lvl2, newFU_lvl1)
                # -- this is an Object node, so we probably should read the next line one time
                # -- get the Object identifier by splitting first instance of O
                objectParts = line.split("O")
                objectParts = objectParts[1].split("\t")
                newObject = FOON.Object(objectID=int(
                    objectParts[0]), objectLabel=objectParts[1])

                # -- checking if an object is marked as intended goal of a subgraph file:
                if '!' in line:
                    newObject.setAsGoal()

            elif line.startswith("S"):
                # -- get the Object's state identifier by splitting first instance of S
                stateParts = line.split("S")
                stateParts = stateParts[1].split("\t")
                stateParts = list(filter(None, stateParts))

                # -- check if this object is a container:
                relative_object = None
                list_ingredients = []
                if len(stateParts) > 2:
                    if '{' in stateParts[2]:
                        # NOTE: all ingredients are enclosed in curly brackets:

                        ingredients = [stateParts[2]]
                        ingredients = ingredients[0].split("{")
                        ingredients = ingredients[1].split("}")
                        # -- we then need to make sure that there are ingredients to be read.
                        if len(ingredients) > 0:
                            ingredients = ingredients[0].split(",")
                            for I in ingredients:
                                list_ingredients.append(I)
                            list_ingredients.sort()
                    elif '[' in stateParts[2]:
                        # NOTE: a geometrically-related object will be enclosed in square brackets:

                        # -- mention of geometrically-relative object:
                        relater = [stateParts[2]]
                        relater = relater[0].split("[")
                        relater = relater[1].split("]")
                        relative_object = relater[0]
                    else:
                        pass

                newObject.addNewState(
                    [int(stateParts[0]), stateParts[1], relative_object])

                if list_ingredients:
                    newObject.setIngredients(list_ingredients)

            elif line.startswith("M"):
                if newObject:
                    _addObjectToFOON(
                        newObject, isInput, objectParts[2], newFU_lvl3, newFU_lvl2, newFU_lvl1)
                newObject = None

                # -- We are adding a Motion node, so very easy to deal with, as follows:
                motionParts = line.split("M")				# -- get the Motion number...
                # ... and get the Motion label
                motionParts = list(filter(None, motionParts[1].split("\t")))

                # Functional Unit - Level 3:
                # -- create new Motion based on what was read:
                newMotion = FOON.Motion(motionID=int(
                    motionParts[0]), motionLabel=motionParts[1])

                for T in newFU_lvl3.getInputList():
                    # -- make the connection from Object(s) to Motion
                    T.addNeighbour(newMotion)

                newFU_lvl3.setMotion(newMotion)

                start_time, end_time = None, None
                success_rate, weighted_entity = None, None

                # -- check if we have the old or new format of the times:
                if '<' in line or '>' in line:
                    # -- we will have the format <XXX,YYY> or <Assumed>:
                    motionTimes = motionParts[2].split(
                        '<')[1].split('>')[0].split(',')
                    if len(motionTimes) > 1:
                        start_time = motionTimes[0]
                        end_time = motionTimes[1]

                    # -- this is to check for the new version of FOON with robot/human difficulties:
                    if len(motionParts) > 3:
                        weighted_entity = motionParts[3]
                        success_rate = motionParts[4]
                        # -- this will indicate whether motion is done by robot or human
                    # endif
                else:
                    # -- we will have regular tab-separated format:
                    start_time = motionParts[2]
                    end_time = motionParts[3]

                    # -- this is to check for the new version of FOON with robot/human difficulties:
                    if len(motionParts) > 4:
                        # -- this will indicate whether motion is done by robot or human
                        weighted_entity = motionParts[4]
                        success_rate = motionParts[5]
                    # endif
                # endif

                if start_time and end_time:
                    newFU_lvl3.setTimes(start_time, end_time)

                if success_rate:
                    newFU_lvl3.setSuccessRate(float(success_rate))

                if weighted_entity:
                    newFU_lvl3.setIndication(weighted_entity)

                # Functional Unit - Level 2:
                newMotion = FOON.Motion(motionID=int(
                    motionParts[0]), motionLabel=motionParts[1])

                for T in newFU_lvl2.getInputList():
                    T.addNeighbour(newMotion)

                newFU_lvl2.setMotion(newMotion)
                if start_time and end_time:
                    newFU_lvl2.setTimes(start_time, end_time)

                if success_rate:
                    newFU_lvl2.setSuccessRate(float(success_rate))

                if weighted_entity:
                    newFU_lvl2.setIndication(weighted_entity)

                # Functional Unit - Level 1:
                newMotion = FOON.Motion(motionID=int(
                    motionParts[0]), motionLabel=motionParts[1])

                for T in newFU_lvl1.getInputList():
                    T.addNeighbour(newMotion)

                newFU_lvl1.setMotion(newMotion)
                if start_time and end_time:
                    newFU_lvl1.setTimes(start_time, end_time)

                if success_rate:
                    newFU_lvl1.setSuccessRate(float(success_rate))

                if weighted_entity:
                    newFU_lvl1.setIndication(weighted_entity)

                isInput = False  # -- we will now switch over to adding output nodes since we have seen a motion node
            else:
                pass

            # endif

        except Exception:
            sys.exit('\t-- ERROR: line ' + str(line_count) + ' - ' + line)

        # endtry

    # endfor

    _file.close() 	# -- Don't forget to close the file once we are done!
# enddef


def _constructFOON_as_JSON(file=None):
    # NOTE: this is the .JSON file variant of the FOON loading function:

    # -- if you don't have this for some reason.. you're gonna have a bad time.
    try:
        import json
    except ImportError:
        return

    # -- 'FOON_node_count' gives an indication of the number of object AND motion nodes are in FOON.
    global FOON_node_count

    global FOON_video_source, verbose

    global FOON_lvl1, FOON_lvl2, FOON_lvl3, nodes_lvl1, nodes_lvl2, nodes_lvl3

    # -- objects which will hold the functional unit being read:
    newFU_lvl1, newFU_lvl2, newFU_lvl3 = FOON.FunctionalUnit(
    ), FOON.FunctionalUnit(), FOON.FunctionalUnit()

    _file = open(file, 'r')
    _json = json.load(_file)
    for func_unit in _json['functional_units']:

        # -- checking flag for verbose (print-outs):
        if verbose:
            print(func_unit)

        for _input in func_unit['input_nodes']:
            # -- level 3 version:
            newObject = FOON.Object(objectID=int(
                _input['object_id']), objectLabel=_input['object_label'])
            for S in _input['object_states']:
                if 'relative_object' in S:
                    newObject.addNewState(
                        [int(S['state_id']), S['state_label'], S['relative_object']])
                else:
                    newObject.addNewState(
                        [int(S['state_id']), S['state_label'], None])

            for I in _input['ingredients']:
                # NOTE: in the new JSON format, it is easy to assign objects to objects as ingredients; in this way, object nodes could possibly be recursive.

                # NOTE: to use this, set 'flag_recursive_objects' to True
                # -- HOWEVER, you must have the files annotated with this format!
                if FOON.flag_recursive_objects:
                    newIngredient = FOON.Object(objectID=int(
                        I['object_id']), objectLabel=I['object_label'])
                    for S in I['object_states']:
                        if 'relative_object' in S:
                            newObject.addNewState(
                                [int(S['state_id']), S['state_label'], S['relative_object']])
                        else:
                            newObject.addNewState(
                                [int(S['state_id']), S['state_label'], None])

                    # -- objects contained within objects may already be used in other ways, so it is important to note if
                    #	they already exist in FOON.
                    newIngredient = _addIngredientToFOON(newIngredient)
                    newObject.addIngredient(newIngredient)
                else:
                    # -- if not, just stick to object-label-only ingredients as done before
                    newObject.addIngredient(I)

            _addObjectToFOON(
                newObject, True, _input['object_in_motion'], newFU_lvl3, newFU_lvl2, newFU_lvl1)

        # NOTE: reading motion node information:
        # -- level 1 version:
        newMotion = FOON.Motion(motionID=int(
            func_unit['motion_node']['motion_id']), motionLabel=func_unit['motion_node']['motion_label'])
        for T in newFU_lvl3.getInputList():
            # -- make the connection from Object(s) to Motion
            T.addNeighbour(newMotion)
        newFU_lvl3.setMotion(newMotion)
        newFU_lvl3.setTimes(
            func_unit['motion_node']['start_time'], func_unit['motion_node']['end_time'])

        # -- this is to check for the new version of FOON with robot/human difficulties:
        if func_unit['motion_node'].get('weight_success', None):
            # -- this will indicate whether motion is done by robot or human
            newFU_lvl3.setIndication(func_unit['motion_node']['robot_type'])
            newFU_lvl3.setSuccessRate(
                float(func_unit['motion_node']['weight_success']))

        # -- level 2 version:
        newMotion = FOON.Motion(motionID=int(
            func_unit['motion_node']['motion_id']), motionLabel=func_unit['motion_node']['motion_label'])
        for T in newFU_lvl2.getInputList():
            # -- make the connection from Object(s) to Motion
            T.addNeighbour(newMotion)
        newFU_lvl2.setMotion(newMotion)
        newFU_lvl2.setTimes(
            func_unit['motion_node']['start_time'], func_unit['motion_node']['end_time'])

        if func_unit['motion_node'].get('weight_success', None):
            # -- this will indicate whether motion is done by robot or human
            newFU_lvl2.setIndication(func_unit['motion_node']['robot_type'])
            newFU_lvl2.setSuccessRate(
                float(func_unit['motion_node']['weight_success']))

        # -- level 1 version:
        newMotion = FOON.Motion(motionID=int(
            func_unit['motion_node']['motion_id']), motionLabel=func_unit['motion_node']['motion_label'])
        for T in newFU_lvl1.getInputList():
            # -- make the connection from Object(s) to Motion
            T.addNeighbour(newMotion)
        newFU_lvl1.setMotion(newMotion)
        newFU_lvl1.setTimes(
            func_unit['motion_node']['start_time'], func_unit['motion_node']['end_time'])

        if func_unit['motion_node'].get('weight_success', None):
            # -- this will indicate whether motion is done by robot or human
            newFU_lvl1.setIndication(func_unit['motion_node']['robot_type'])
            newFU_lvl1.setSuccessRate(
                float(func_unit['motion_node']['weight_success']))

        for _output in func_unit['output_nodes']:
            # -- level 3 version:
            newObject = FOON.Object(objectID=int(
                _output['object_id']), objectLabel=_output['object_label'])
            for S in _output['object_states']:
                if 'relative_object' in S:
                    newObject.addNewState(
                        [int(S['state_id']), S['state_label'], S['relative_object']])
                else:
                    newObject.addNewState(
                        [int(S['state_id']), S['state_label'], None])

            for I in _output['ingredients']:
                # NOTE: in the new JSON format, it is easy to assign objects to objects as ingredients; in this way, object nodes could possibly be recursive.

                # NOTE: to use this, set 'flag_recursive_objects' to True
                # -- HOWEVER, you must have the files annotated with this format!
                if FOON.flag_recursive_objects is True:
                    newIngredient = FOON.Object(objectID=int(
                        I['object_id']), objectLabel=I['object_label'])
                    for S in I['object_states']:
                        if 'relative_object' in S:
                            newObject.addNewState(
                                [int(S['state_id']), S['state_label'], S['relative_object']])
                        else:
                            newObject.addNewState(
                                [int(S['state_id']), S['state_label'], None])

                    # -- objects contained within objects may already be used in other ways, so it is important to note if
                    #	they already exist in FOON.
                    newIngredient = _addIngredientToFOON(newIngredient)
                    newObject.addIngredient(newIngredient)
                else:
                    # -- if not, just stick to object-label-only ingredients as done before
                    newObject.addIngredient(I)

            _addObjectToFOON(
                newObject, False, _output['object_in_motion'], newFU_lvl3, newFU_lvl2, newFU_lvl1)

        if _checkIfFUExists(newFU_lvl3, 3) == False:
            # NOTE: no matter what, we add new motion nodes; we will have multiple instances everywhere.
            nodes_lvl3.append(newFU_lvl3.getMotion())
            FOON_lvl3.append(newFU_lvl3)
            motionsToFunctionalUnits_lvl3[nodes_lvl3.index(
                newFU_lvl3.getMotion())] = newFU_lvl3

            # -- we only keep track of the total number of nodes in the LVL3 FOON.
            FOON_node_count += 1

        if _checkIfFUExists(newFU_lvl2, 2) == False:
            nodes_lvl2.append(newFU_lvl2.getMotion())
            FOON_lvl2.append(newFU_lvl2)
            motionsToFunctionalUnits_lvl2[nodes_lvl2.index(
                newFU_lvl2.getMotion())] = newFU_lvl2

        if _checkIfFUExists(newFU_lvl1, 1) == False:
            nodes_lvl1.append(newFU_lvl1.getMotion())
            FOON_lvl1.append(newFU_lvl1)
            motionsToFunctionalUnits_lvl1[nodes_lvl1.index(
                newFU_lvl1.getMotion())] = newFU_lvl1

        # -- create an entirely new FU object to proceed with reading new units.
        newFU_lvl1, newFU_lvl2, newFU_lvl3 = FOON.FunctionalUnit(
        ), FOON.FunctionalUnit(), FOON.FunctionalUnit()

    # endfor

    _file.close() 	# -- Don't forget to close the file once we are done!
# enddef


def _identifyKitchenItems(file='FOON-input_only_nodes.txt'):
    # NOTE: kitchen items refer to nodes that are either:
    #	1) input only nodes (by default);
    #	2) nodes that will not be expanded in the search process.

    try:
        _file = open(file, 'r')
    except Exception:
        # -- this means that no file exists, so just default to creating a file with purely input nodes:
        _createFile_InputOnly()
        _file = open('FOON-input_only_nodes.txt', 'r')
    # endtry

    # -- split the file into lines and then do parsing as we would for a regular FOON graph file:
    items = _file.read().splitlines()
    kitchenItem = None
    kitchen = []

    for line in items:
        if line.startswith("O"):
            # -- we have an Object already in consideration which we were appending states to:
            if kitchenItem:
                kitchen.append(kitchenItem)
            # -- this is an Object node, so we probably should read the next line one time
            # -- get the Object identifier by splitting first instance of O
            objectParts = line.split("O")
            objectParts = objectParts[1].split("\t")

            # -- create a new object which is equal to the kitchenItem and add it to the list:
            kitchenItem = FOON.Object(objectID=int(
                objectParts[0]), objectLabel=objectParts[1])

        elif line.startswith("S"):
            # -- get the Object's state identifier by splitting first instance of S
            stateParts = line.split("S")
            stateParts = stateParts[1].split("\t")
            stateParts = list(filter(None, stateParts))

            # -- check if this object is a container or has geometrically-relative object:
            relative_object = None
            list_ingredients = []
            if len(stateParts) > 2:
                if '{' in stateParts[2]:
                    # NOTE: all ingredients are enclosed in curly brackets:
                    ingredients = [stateParts[2]]
                    ingredients = ingredients[0].split("{")
                    ingredients = ingredients[1].split("}")

                    # -- we then need to make sure that there are ingredients to be read.
                    if len(ingredients) > 0:
                        ingredients = ingredients[0].split(",")
                        for I in ingredients:
                            list_ingredients.append(I)
                elif '[' in stateParts[2]:
                    # NOTE: a geometrically-relative object (relater - not a good name) is enclosed in square brackets:

                    # -- mention of geometrically-relative object:
                    relater = [stateParts[2]]
                    relater = relater[0].split("[")
                    relater = relater[1].split("]")
                    relative_object = relater[0]
                else:
                    print(' -- WARNING: possibly incorrect or unexpected extra entry in line ' +
                          str(items.index(line)) + '? > ' + str(stateParts[2]))
                    pass

            kitchenItem.addNewState(
                [int(stateParts[0]), stateParts[1], relative_object])

            if list_ingredients:
                kitchenItem.setIngredients(list_ingredients)

        else:
            pass
    # endfor

    # -- append the last object that would not have been saved in the last iteration:
    kitchen.append(kitchenItem)

    _file.close()  # -- Don't forget to close the file once we are done!

    return kitchen
# enddef


def _addIngredientToFOON(newIngredient):
    # -- function to check if ingredients (which can exist as individual nodes) already exist within FOON:
    global FOON_node_count

    objectExisting, objectIndex = _checkIfNodeExists(newIngredient, 3), -1
    if objectExisting == -1:
        # -- just add new object to the list of all nodes
        nodes_lvl3.append(newIngredient)
        objectIndex = FOON_node_count
        objects_oneModeProjection_lvl3.append(nodes_lvl3[objectIndex])
        FOON_node_count += 1
    else:
        objectIndex = objectExisting
    return nodes_lvl3[objectIndex]
# enddef


def _addObjectToFOON(newObject, isInput, D, newFU_lvl3, newFU_lvl2, newFU_lvl1):
    # -- check if object already exists within the list so as to avoid duplicates
    global FOON_node_count, nodes_lvl1, nodes_lvl2, nodes_lvl3

    objectExisting = _checkIfNodeExists(newObject, 3)
    if objectExisting == -1:
        # -- just add new object to the list of all nodes
        nodes_lvl3.append(newObject)
        objectIndex = FOON_node_count
        objects_oneModeProjection_lvl3.append(nodes_lvl3[objectIndex])
        FOON_node_count += 1
    else:
        objectIndex = objectExisting

    if isInput == True:
        # -- this Object will be an input node to the FU:
        newFU_lvl3.addObjectNode(
            objectNode=nodes_lvl3[objectIndex], is_input=True, objectInMotion=int(D))
    else:
        # -- add the Objects as output nodes to the FU:
        newFU_lvl3.addObjectNode(
            objectNode=nodes_lvl3[objectIndex], is_input=False, objectInMotion=int(D))
        # -- make the connection from Motion to Object
        newFU_lvl3.getMotion().addNeighbour(nodes_lvl3[objectIndex])

    object_ID = newObject.getObjectType()
    object_label = newObject.getObjectLabel()

    # NOTE: Creating level 2 version of this node:
    newObject_lvl2 = FOON.Object(objectID=object_ID, objectLabel=object_label)

    for I in newObject.getStatesList():
        newObject_lvl2.addNewState([int(I[0]), str(I[1]), I[2]])

    objectExisting = _checkIfNodeExists(newObject_lvl2, 2)

    # -- check if object already exists within the list so as to avoid duplicates
    if objectExisting == -1:
        # -- just add new object to the list of all nodes
        objectIndex = len(nodes_lvl2)
        nodes_lvl2.append(newObject_lvl2)
        objects_oneModeProjection_lvl2.append(nodes_lvl2[objectIndex])
    else:
        objectIndex = objectExisting

    if isInput == True:
        newFU_lvl2.addObjectNode(
            nodes_lvl2[objectIndex], is_input=True, objectInMotion=int(D))
    else:
        newFU_lvl2.addObjectNode(
            nodes_lvl2[objectIndex], is_input=False, objectInMotion=int(D))
        newFU_lvl2.getMotion().addNeighbour(nodes_lvl2[objectIndex])

    # NOTE: Creating level 1 version of this node:
    newObject_lvl1 = FOON.Object(objectID=object_ID, objectLabel=object_label)

    objectExisting = _checkIfNodeExists(newObject_lvl1, 1)

    # -- check if object already exists within the list so as to avoid duplicates
    if objectExisting == -1:
        objectIndex = len(nodes_lvl1)
        nodes_lvl1.append(newObject_lvl1)
        objects_oneModeProjection_lvl1.append(nodes_lvl1[objectIndex])
    else:
        objectIndex = objectExisting

    if isInput == True:
        newFU_lvl1.addObjectNode(
            nodes_lvl1[objectIndex], is_input=True, objectInMotion=int(D))
    else:
        newFU_lvl1.addObjectNode(
            nodes_lvl1[objectIndex], is_input=False, objectInMotion=int(D))
        newFU_lvl1.getMotion().addNeighbour(nodes_lvl1[objectIndex])
# enddef


def _resetFOON(reload=False):
    # NOTE: this function simply resets all lists and structures to empty:

    global FOON_lvl1, FOON_lvl2, FOON_lvl3, FOON_functionalUnits
    FOON_lvl1 = []
    FOON_lvl2 = []
    FOON_lvl3 = []
    FOON_functionalUnits = [FOON_lvl1, FOON_lvl2, FOON_lvl3]

    global nodes_lvl1, nodes_lvl2, nodes_lvl3, FOON_nodes
    nodes_lvl1 = []
    nodes_lvl2 = []
    nodes_lvl3 = []

    FOON_nodes = [nodes_lvl1, nodes_lvl2, nodes_lvl3]

    global FOON_node_count
    FOON_node_count = 0

    global FOON_video_source
    FOON_video_source = []

    global file_name

    if reload:
        _constructFOON(file_name)
        _buildInternalMaps()
# enddef


def _resetMaps():
    global FOON_functionalUnitMap_lvl1, FOON_outputsToUnits_lvl1
    FOON_functionalUnitMap_lvl1, FOON_outputsToUnits_lvl1 = {}, {}

    global FOON_functionalUnitMap_lvl2, FOON_outputsToUnits_lvl2
    FOON_functionalUnitMap_lvl2, FOON_outputsToUnits_lvl2 = {}, {}

    global FOON_functionalUnitMap_lvl3, FOON_outputsToUnits_lvl3
    FOON_functionalUnitMap_lvl3, FOON_outputsToUnits_lvl3 = {}, {}

    global FOON_outputsToUnits
    FOON_outputsToUnits = [FOON_outputsToUnits_lvl1,
                           FOON_outputsToUnits_lvl2, FOON_outputsToUnits_lvl3]

    global motionsToFunctionalUnits_lvl1, motionsToFunctionalUnits_lvl2, motionsToFunctionalUnits_lvl3, motionsToFunctionalUnits
    motionsToFunctionalUnits_lvl1 = {}
    motionsToFunctionalUnits_lvl2 = {}
    motionsToFunctionalUnits_lvl3 = {}

    motionsToFunctionalUnits = [motionsToFunctionalUnits_lvl1,
                                motionsToFunctionalUnits_lvl2, motionsToFunctionalUnits_lvl3]
# enddef


def _findFunctionalUnitClusters(hierarchy_level=None):
    # NOTE: the purpose of this function is to find chains of functional units which can be compressed into (what we coin as)
    #	"combinational" functional units, which act as a major hub for when ingredients are being added into a container or structure.
    # -- this will be vital when it comes to the *task tree adaptation* problem, which we are currently exploring for deriving truly novel sequences.

    global FOON_functionalUnits, FOON_unitClusters, FOON_nodes, FOON_outputsToUnits

    # -- identify motions that suggest transfer or movement of objects from one point to another:
    transfer_motion_labels = ['pick-and-place', 'place',
                              'pour', 'sprinkle', 'add', 'scoop', 'grind']

    # -- for a certain level of FOON, we need to iterate through the units to find any that could be condensed into a single, larger hub unit.
    for level in range(len(FOON_functionalUnits)):
        # -- if we only want to load a specific level of FOON, then we skip the other levels:
        if hierarchy_level and hierarchy_level != level + 1:
            continue

        current_FOON = FOON_functionalUnits[level]

        clustered_units = []
        clustered_unit_map = {}

        # -- we start with the second unit, and we evaluate whether it can merge it with the previous unit (i.e. the first),
        # 	and we keep exploring and comparing every n-th unit to its (n-1)-th unit till the end.
        # -- or should we start at the end and then bubble all the way to the top?
        for index in range(len(current_FOON) - 1,  0, -1):

            print('do')
            related_units = []

            for _input in current_FOON[index].getInputNodes():
                if _input.hasIngredients() and _input in FOON_outputsToUnits[level]:
                    for N in FOON_outputsToUnits[level][_input]:
                        related_units.append(N)

            for _unit in related_units:
                _unit.print_functions[level]()
                input()
                if current_FOON[index].getMotionNode().getMotionLabel() in transfer_motion_labels \
                        and _unit.getMotionNode().getMotionLabel() in transfer_motion_labels:
                    print('re')

                    # -- check if there is overlap of objects in the input of N-th unit and the output of the (N-1)-th unit:
                    overlapping_nodes = list(
                        set(current_FOON[index].getInputNodes()) & set(_unit.getOutputNodes()))

                    if overlapping_nodes:
                        print('mi')
                        truly_overlapping = False
                        containers = []

                        for O in overlapping_nodes:
                            O.print_functions[level]()
                            print(len(O.getIngredients()))
                            print(O.getIngredients())

                        print()

                        # -- check the nodes' ingredients and see if they truly differ by at least one ingredient:
                        for O in overlapping_nodes:
                            for _input in _unit.getInputNodes():
                                # -- only compare objects that have the same object type/ID:
                                if _input.getObjectType() != O.getObjectType():
                                    continue

                                _input.print_functions[level]()
                                print(_input.getIngredients())
                                print(O.getIngredients())

                                if abs(len(_input.getIngredients()) - len(O.getIngredients())) == 1:
                                    print('fa')
                                    truly_overlapping = True
                                    # -- take note of the containers that suggest overlapping:
                                    containers.append(
                                        FOON_nodes[level].index(O))
                                    containers.append(
                                        FOON_nodes[level].index(_input))
                                    break
                                # endif
                            if truly_overlapping:
                                break
                            # endif

                            # endfor
                        # endfor

                        if truly_overlapping:
                            print('so')
                            # -- let us create the combinational functional unit:
                            new_functional_unit = FOON.FunctionalUnit()
                            new_functional_unit.setMotion(
                                FOON.Motion(motionID=-1, motionLabel='add*'))

                            # -- check if the current functional unit already maps to a cluster:
                            current_FU = clustered_unit_map[current_FOON[index]
                                                            ] if current_FOON[index] in clustered_unit_map else current_FOON[index]
                            prior_FU = clustered_unit_map[_unit] if _unit in clustered_unit_map else _unit

                            # -- from the PRIOR functional unit, we need to take ALL of the input nodes and we take ALL BUT the container
                            # 	of the output nodes (i.e., the overlapping node that is present in the CURRENT functional unit):
                            for N in range(prior_FU.getNumberOfInputs()):
                                _input = prior_FU.getInputNodes()[N]
                                new_functional_unit.addObjectNode(objectNode=_input, is_input=True,
                                                                  objectInMotion=prior_FU.getMotionDescriptor(N, is_input=True))

                            for N in range(prior_FU.getNumberOfOutputs()):
                                _output = prior_FU.getOutputNodes()[N]
                                if FOON_nodes[level].index(_output) not in containers:
                                    new_functional_unit.addObjectNode(objectNode=_output, is_input=False,
                                                                      objectInMotion=prior_FU.getMotionDescriptor(N, is_input=False))

                            # -- from the CURRENT functional unit, we need to take ALL of the output nodes and we take ALL BUT the container
                            # 	of the intput nodes (i.e., the overlapping node that is present in the PRIOR functional unit):
                            for N in range(current_FU.getNumberOfInputs()):
                                _input = current_FU.getInputNodes()[N]
                                if FOON_nodes[level].index(_input) not in containers:
                                    new_functional_unit.addObjectNode(objectNode=_input, is_input=True,
                                                                      objectInMotion=current_FU.getMotionDescriptor(N, is_input=True))

                            for N in range(current_FU.getNumberOfOutputs()):
                                _output = current_FU.getOutputNodes()[N]
                                new_functional_unit.addObjectNode(objectNode=_output, is_input=False,
                                                                  objectInMotion=current_FU.getMotionDescriptor(N, is_input=False))

                            # -- review all functional units that have been previously mapped to a clustered unit:
                            for key in clustered_unit_map:
                                if clustered_unit_map[key] == current_FU:
                                    clustered_unit_map[key] = new_functional_unit

                            clustered_unit_map[current_FOON[index]
                                               ] = new_functional_unit
                            clustered_unit_map[_unit] = new_functional_unit

                        # endif
                    # endif
                # endif
            # endfor
        # endfor

        # -- now we review all the changes we've made:
        for FU in current_FOON:
            if FU in clustered_unit_map:
                new_unit = clustered_unit_map[FU]
                if new_unit not in clustered_units:
                    clustered_units.append(new_unit)
            else:
                clustered_units.append(FU)

            # endif
        # endfor

        input(len(clustered_units))
        for FU in clustered_units:
            FU.print_functions[level]()
            print('//')

        FOON_unitClusters[level] = clustered_units
    # endfor
# enddef

###############################################################################################################################

# NOTE: the following are retrieval related functions:
# -- task tree retrieval -> find a single task tree / path that satisfies making a specific goal node:
# -- path tree retrieval -> find all possible paths that satisfy making a specific goal node:


def _createFile_InputOnly():
    inputs_only = _findNodes_InputOnly()
    _file = open('FOON-input_only_nodes.txt', 'w')
    for _object in inputs_only:
        _file.write(_object.getObjectText() + "\n")
    # endfor
    _file.close()  # -- Don't forget to close the file once we are done!
# enddef


def _createFile_OutputOnly():
    outputs_only = _findNodes_OutputOnly()
    _file = open('FOON-output_only_nodes.txt', 'w')
    for _object in outputs_only:
        _file.write(_object.getObjectText() + "\n")
    # endfor
    _file.close()  # -- Don't forget to close the file once we are done!
# enddef


def _findNodes_InputOnly():
    global verbose
    inputs_only = []
    if verbose:
        print("\n-- Default input nodes:")

    # -- checking for starting nodes based on lack of functional units that contain them as output nodes:
    for _node in FOON_outputsToUnits_lvl3:
        if _isStartingNode(_node):
            inputs_only.append(_node)
            if verbose:
                _node.print_functions[2]()
        # end
    # endfor

    return inputs_only
# enddef


def _isStartingNode(O):
    # NOTE: simple function to evaluate whether a certain node is a starting node:

    # -- one surefire way: check the output node to functional unit map;
    # 	a node that is a starting node will be mapped to an empty list of functional units:
    procedures = FOON_outputsToUnits_lvl3.get(O)
    if not procedures:
        return True

    # -- another possibility: what if this node has starting node states?
    #	(e.g. 'empty', 'off', 'clean')
    count_initial_states = 0
    for S in range(len(O.getStatesList())):
        if O.getStateLabel(S) in ['empty', 'off', 'off (ready)', 'clean', 'whole', 'unpeeled', 'clove']:
            count_initial_states += 1

    # --  a starting node will only have the preceding starting/initial states:
    if count_initial_states == len(O.getStatesList()):
        return True

    # -- if it passes these conditions, then we can say that it is not a starting node:
    return False
# enddef


def _findNodes_OutputOnly():
    global verbose
    outputs_only = []
    if verbose:
        print("\n-- Default output nodes:")

    for _node in FOON_outputsToUnits_lvl3:
        procedures = FOON_outputsToUnits_lvl3.get(_node)
        if procedures:
            outputs_only.append(_node)
            if verbose:
                _node.print_functions[2]()
    # endfor
    return outputs_only
# enddef


def _prepareTaskTreeRetrieval(choice=1):
    if choice != 1:
        object_id = int(input(" -- Enter the OBJECT type to be searched: > "))
        state_ids = ast.literal_eval(input(
            " -- Enter the OBJECT state(s) in the form of a list (E.G. '[1,33,7]'): > "))

    else:

        global FOON_objectLabels, FOON_stateLabels

        # -- using existing string distance metrics from either 'jellyfish' or 'NLTK' to find closest object/state labels;
        #	this will make it easier for people to initialize a search via name rather than knowing or providing integer IDs
        try:
            from jellyfish import jaro_winkler_similarity
        except ImportError:
            print(
                "WARNING: Module 'jellyfish' is not installed! Now trying to import from 'NLTK'...")
        try:
            from nltk.metrics.distance import jaro_winkler_similarity
        except ImportError:
            print("WARNING: Module 'NLTK' is also not installed! Please install one of these to use this function.")
            return

        # -- Prompting for the object name:
        option, selection = 0, None
        while option == 0:
            search_results = []

            _response = input(
                ' -- Please type the name of an OBJECT you would like for the goal object: > ')
            for key in FOON_objectLabels:
                if jaro_winkler_similarity(_response, key) > 0.75:
                    search_results.append([key, FOON_objectLabels[key]])

            # -- sort results in alphabetical order:
            search_results.sort()

            if len(search_results) > 0:
                print(' -- Closest object labels are as follows:')
                for res in search_results:
                    print('   ' + str(search_results.index(res) + 1) +
                          ' - ' + str(res[0]))
                option = int(input(
                    '   Enter INTEGER preceding the suitable label to select it or enter 0 (zero) to start over : > '))
                if option > 0:
                    selection = search_results[option-1]
            else:
                print(" -- No objects found that are close enough to '" +
                      _response + "'!")
        # end

        # -- object ID will be the second element of the selection result:
        object_id = selection[1]

        # -- Prompting for the object's state name:
        option, selection = 0, []
        while option == 0:
            search_results = []

            _response = input(
                ' -- Please type the name or phrase for an object STATE you would like for the goal object: > ')
            for key in FOON_stateLabels:
                if jaro_winkler_similarity(_response, key) > 0.65:
                    search_results.append([key, FOON_objectLabels[key]])

            # -- sort results in alphabetical order:
            search_results.sort()

            if len(search_results) > 0:
                print(' -- Closest state labels are as follows:')
                for res in search_results:
                    print('   ' + str(search_results.index(res) + 1) +
                          ' - ' + str(res[0]))
                option = int(input(
                    '   Enter INTEGER preceding the suitable label to select it or enter 0 (zero) to start over : > '))
                if option > 0:
                    selection.append(search_results[option-1])
            else:
                print(" -- No state found that is close enough to '" +
                      _response + "'!")

            print(' -- Repeating search (in case of multiple states)...')
        # end

        # -- just as with objects, state IDs will be the second element in each sub-array:
        state_ids = [state[1] for state in selection]

    return object_id, state_ids
# enddef


def _taskTreeRetrieval_greedy(goalType, goalState, hierarchy_level=3, environment=None):
    # NOTE: this is the vanilla task tree algorithm from Paulius et al. 2016.
    # -- this algorithm simply finds the first instance of functional units that meets the goal node requirement.
    # -- this algorithm requires a list of ingredients that are found in the robot's environment (provided as a file).

    global verbose

    # -- sometimes we may define a specific kitchen; other times, we just stick to default:
    if not environment:
        file = input(
            '  -- Please provide the name of the file with KITCHEN ITEMS (press ENTER ONLY to use default): > ')
        if file == '':
            # -- use default 'FOON-input_only_nodes.txt'
            environment = _identifyKitchenItems()
        else:
            # -- use a different text file with kitchen items
            environment = _identifyKitchenItems(file)
        # endif
    # endif

    # -- create a temporary goal node object which is used to find the exact target in FOON:
    goalNode = FOON.Object(objectID=int(goalType), objectLabel=None)
    for x in goalState:
        goalNode.addNewState([int(x), None, []])
    # endif

    if hierarchy_level == 1:
        searchNodes = nodes_lvl1
        searchMap = FOON_outputsToUnits_lvl1
    elif hierarchy_level == 2:
        searchNodes = nodes_lvl2
        searchMap = FOON_outputsToUnits_lvl2
    elif hierarchy_level == 3:
        searchNodes = nodes_lvl3
        searchMap = FOON_outputsToUnits_lvl3
    else:
        return

    search_level = hierarchy_level
    if search_level == 3:
        search_level = 2  # -- we only consider level 2 nodes if we do not have exact candidate

    # -- we will identify different target goal nodes that have the same object/state combination:
    goals = []

    # NOTE: refer to the search_level description for a run-down of the intuiton here:
    index = -1
    for T in searchNodes:
        if isinstance(T, FOON.Object):
            if T.equals_lvl1(goalNode) and T.isSameStates_ID_only(goalNode):
                index = searchNodes.index(T)
                goals.append(searchNodes[index])

    if index == -1:
        print("Item " + goalNode.getObjectKey(hierarchy_level) +
              " has not been found in network!")
        return False
    # endif

    if verbose:
        print("\n-- Candidate goal nodes are as follows:")
        for G in goals:
            G.print_functions[hierarchy_level-1]()

    if verbose:
        print("\n-- Proceeding with task tree retrieval...\n")

    # -- startNodes - list of object nodes which we have available to the algorithm:
    startNodes = []
    for T in environment:
        index = -1
        for N in searchNodes:
            if isinstance(N, FOON.Object):
                if T.equals_lvl1(N) and T.isSameStates_ID_only(N):
                    index = searchNodes.index(N)
                    break
        if index != -1:
            # -- this means that the object exists in FOON; if not..
            startNodes.append(searchNodes[index])
        else:
            # .. we can try to find a substitute for this object:
            possible_substitutes = _findObjectSubstitute(T)

            for P in possible_substitutes:
                index = -1
                for N in searchNodes:
                    if P.equals_functions[hierarchy_level-1](N):
                        index = searchNodes.index(N)
                        break
                # endfor

                if index > -1:
                    # -- this means that the object exists in FOON; if not..
                    startNodes.append(searchNodes[index])
                # endif
            # endfor
        # endif
    # endfor

    for G in goals:
        print(" -- Attempting to search for object:")
        G.print_functions[hierarchy_level-1]()
        print(" -----------------------------------")

        if G in startNodes:
            print(' -- WARNING: This object already exists in the kitchen!')
            continue

        # What structures do we need in record keeping?
        #	-- a FIFO list of all nodes we need to search (a queue)
        #	-- a list that keeps track of what we have seen
        #	-- a list of all items we have/know how to make in the present case (i.e. the kitchen list)
        itemsToSearch = collections.deque()
        subgoals = {}
        kitchen = []

        goalNode = G  # this is the actual goal node which is in the network.

        # -- Add the object we wish to search for to the two lists created above:
        itemsToSearch.append(goalNode)

        # -- generating new kitchen list per object searched (since we may satisfy new subgoals):
        for T in startNodes:
            kitchen.append(T)
        # endfor

        candidates = [] 	# -- structure to keep track of all candidate functional units in FOON
        task_tree = []		# -- tree with all functional units needed to create the goal based on the kitchen items
        # -- list of ALL possible functional units that can be used for making the item.
        all_path_trees = []

        # -- maximum number of times you can "see" the original goal node.
        global depth
        max_iterations = 0
        # -- flag that is used to stop searching at goal node G that satisfies retrieval.
        endSearch = False

        while itemsToSearch:
            # -- Remove the item we are trying to make from the queue of items we need to learn how to make
            tempObject = itemsToSearch.popleft()

            if verbose:
                print('\n -- Object removed from queue:')
                tempObject.print_functions[hierarchy_level-1]()

            # -- sort-of a time-out for the search if it does not produce an answer dictated by the amount of time it takes.
            if tempObject.equals_functions[hierarchy_level-1](goalNode):
                max_iterations += 1
                print('\n -- max_iterations = ' + str(max_iterations))

            if tempObject in startNodes:
                continue

            # just the worst possible way of doing this, but will do for now.
            if max_iterations > depth:
                if verbose:
                    print("-- Objects that were found to be missing:")
                    print("  -- Starting Nodes:")
                    for S in startNodes:
                        S.print_functions[hierarchy_level-1]()
                    print("  -- Nodes remaining in queue:")
                    for S in itemsToSearch:
                        S.print_functions[hierarchy_level-1]()
                endSearch = True
                break

            if goalNode in kitchen:
                break

            if tempObject in kitchen:
                # -- just proceed to next iteration, as we already know how to make current item!
                continue

            # -- we need to then identify possible candidate functional units;
            #	candidates are those that contain this current object (i.e. tempObject) in its outputs
            num_candidates = 0
            candidates = list(searchMap.get(tempObject))
            num_candidates = len(candidates)
            all_path_trees += candidates

            if candidates == False:
                # -- this means that there is NO solution to making an object,
                #	and so we just need to add it as something we still need to learn how to make.
                startNodes.append(tempObject)
                continue

            numObjectsAdded = 0

            while candidates:
                # -- remove the first candidate functional unit we found
                candidate_FU = candidates.pop()
                count = 0	 			# -- variable to count the number of inputs that we have in the kitchen

                for T in candidate_FU.getInputList():
                    flag = False
                    if T in kitchen:
                        flag = True

                    if flag == False:
                        # -- if an item is not in the "objects found" list, then we add it to the list of items we then need to explore and find out how to make.
                        if T in itemsToSearch:
                            flag = True

                        if flag == False:
                            if tempObject in subgoals:
                                mini_goals = subgoals[tempObject]
                                if T not in mini_goals:
                                    mini_goals.append(T)
                            else:
                                subgoals[tempObject] = [T]
                            itemsToSearch.append(T)
                            numObjectsAdded += 1
                            if verbose:
                                print('\n -- Object added to queue:')
                                T.print_functions[hierarchy_level-1]()
                    else:
                        # -- since this is in the kitchen, we need to account for this:
                        count += 1

                num_candidates -= 1  # -- one less candidate to consider..

                if count == candidate_FU.getNumberOfInputs() and count > 0:
                    # UPDATE: before marking an object as solved, we need to remove other objects in the queue that are not necessary anymore:
                    mini_goals = subgoals.pop(tempObject, None)
                    if mini_goals:
                        for M in mini_goals:
                            itemsToSearch = collections.deque(
                                (filter((M).__ne__, itemsToSearch)))

                    # We will have all items needed to make something;
                    #	add that item to the "kitchen", as we consider it already made.
                    found = False
                    if tempObject in kitchen:
                        found = True

                    if found == False:
                        kitchen.append(tempObject)

                    for T in candidate_FU.getOutputList():
                        #	add that item to the "kitchen", as we consider it already made.
                        found = False
                        if T in kitchen:
                            found = True

                        if found == False:
                            kitchen.append(T)
                    # endfor

                    found = False
                    if candidate_FU in task_tree:
                        found = True

                    if not found:
                        task_tree.append(candidate_FU)

                    # -- since we found a suitable FU, we must remove all other traces of other candidates:
                    for x in range(numObjectsAdded):
                        # -- remove the last items that were added to queue that no longer should be considered!
                        itemsToSearch.popleft()

                    for x in range(num_candidates):
                        # -- remove all functional units that can make an item - we take the first one that works!
                        candidates.pop()
                else:
                    # -- if a solution has not been found yet, add the object back to queue.
                    found = False
                    if tempObject in itemsToSearch:
                        found = True

                    if not found:
                        itemsToSearch.append(tempObject)

        if endSearch:
            print(" -- Task tree could not be found!")
            return False

        if not task_tree:
            print(" -- No functional units were found! Search has timed out!")

        else:
            # -- saving task tree sequence to file..
            # 	1. saving .TXT file version:
            file_name = "FOON_task-tree-for-" + \
                goalNode.getObjectKey(hierarchy_level) + \
                "_lvl" + str(hierarchy_level) + ".txt"
            _file = open(file_name, 'w')

            print("-- Length of task tree found: " + str(len(task_tree)))
            for FU in task_tree:
                # -- just write all functional units that were put into the list:
                _file.write(FU.getFunctionalUnitText())
            # endfor
            _file.close()

            # 	2. saving .JSON file version:
            try:
                import json
            except ImportError:
                pass
            else:
                subgraph_units = {}
                subgraph_units['functional_units'] = []

                file_name = "FOON_task-tree-for-" + \
                    goalNode.getObjectKey(
                        hierarchy_level) + "_lvl" + str(hierarchy_level) + ".json"
                _file = open(file_name, 'w')
                for FU in task_tree:
                    subgraph_units['functional_units'].append(
                        FU.getFunctionalUnitJSON())
                json.dump(subgraph_units, _file, indent=7)
                _file.close()
            # endtry

            print(" -- Task tree sequence has been saved in file : " + file_name)
            return task_tree

    # -- after exhausting through all possible goal nodes, we just end the search:
    return False
# enddef


def _taskTreeRetrieval_weighted(all_path_trees, goalType, goalState, hierarchy_level):
    from copy import copy

    goalNode = FOON.Object(objectID=int(goalType), objectLabel=None)
    for x in goalState:
        goalNode.addNewState([int(x), None, []])

    folder = "path_trees_to_" + goalNode.getObjectKey(hierarchy_level) + "/"
    if not os.path.exists(folder):
        os.makedirs(folder)

    # -- this is the total number of human-assisted motions possible.
    max_depth = 0

    for T in all_path_trees:
        M = len(T)

        # -- this is to keep track of the maximum length M possible in all task trees:
        if M > max_depth:
            max_depth = M
    # endfor

    optimal_paths = []

    # Step 4: Determine the highest success rate paths from human-assisted steps 0 to M.
    for M in range(max_depth + 1):
        # NOTE: our goal is to find a path with the highest success rate out of all possible paths for every value of M:
        candidate_paths = []

        for T in all_path_trees:
            if len(T) < M:
                # -- this means that we are checking for a certain value of M that exceeds the entire length of this path.
                continue

            # -- sort each path first to determine the M lowest success rate FUs.
            path_sorted = sorted(T)

            temp_path = []
            # -- the FUs in T (path) are unsorted, while they are sorted in another list called 'path_sorted'.
            for FU in T:
                found = False
                for x in range(M):
                    # -- if FU in sorted path is same as FU in unsorted path...
                    if path_sorted[x].equals_functions[hierarchy_level-1](FU):
                        found = True  # ..mark that FU as True!
                    # endif
                # endfor

                # -- make a copy of this functional unit so that we only have to change a few of its attributes:
                temp_FU = copy(FU)

                if found == True:
                    # -- swap for human step
                    temp_FU.setIndication("Human")
                    temp_FU.setSuccessRate(1.0)
                # endif

                temp_path.append(temp_FU)
            # endfor

            candidate_paths.append(temp_path)
        # endfor

        # -- print all success rates for each value of M :
        print('BEST PATH TREES FOR VALUES OF M:\n--------------------------------')
        max_SR = -1.0
        index = 0
        path_count = 0
        for C in candidate_paths:
            total_success_rate = 1.0

            # -- compute the total success rate of this path:
            for FU in C:
                total_success_rate = total_success_rate * \
                    FU.getSuccessRate() if FU.getSuccessRate() else -1.0
            # endfor

            print("M = " + str(M) + " : new success rate for path #" +
                  str(path_count) + " -- " + str(total_success_rate))
            if max_SR < total_success_rate:
                max_SR = total_success_rate
                index = candidate_paths.index(C)
            # endif
            path_count += 1  # -- increment count before each path
        # endfor
        print('--------------------------------')

        _file = open(folder+"FOON_M=" + str(M) + "-path_#" + str(index + 1) + "_to-" +
                     goalNode.getObjectKey(hierarchy_level) + "_lvl" + str(hierarchy_level) + ".txt", 'w')
        for FU in candidate_paths[index]:
            _file.write(FU.getInputsForFile())
            _file.write(FU.getMotionForFile())
            _file.write(FU.getOutputsForFile())
            _file.write("//\n")
        # endfor
        _file.close()

        optimal_paths.append(candidate_paths[index])
    # endfor

    return optimal_paths
# enddef


def _taskTreeRetrieval_optimal(goalType, goalState, hierarchy_level=3, environment=None):
    global verbose

    # -- sometimes we may define a specific kitchen; other times, we just stick to default:
    if not environment:
        file = input(
            '  -- Please provide the name of the file with KITCHEN ITEMS (press ENTER ONLY to use default): > ')
        if file == '':
            # -- use default 'FOON-input_only_nodes.txt'
            environment = _identifyKitchenItems()
        else:
            # -- use a different text file with kitchen items
            environment = _identifyKitchenItems(file)
        # endif
    # endif

    # -- create a temporary goal node object which is used to find the exact target in FOON:
    goalNode = FOON.Object(objectID=int(goalType), objectLabel=None)
    for x in goalState:
        goalNode.addNewState([int(x), None, []])
    # endif

    if hierarchy_level == 1:
        searchNodes = nodes_lvl1
    elif hierarchy_level == 2:
        searchNodes = nodes_lvl2
    elif hierarchy_level == 3:
        searchNodes = nodes_lvl3
    else:
        return

    search_level = hierarchy_level
    if search_level == 3:
        search_level = 2  # -- we only consider level 2 nodes if we do not have exact candidate

    # -- we will identify different target goal nodes that have the same object/state combination:
    goals = []

    # NOTE: refer to the search_level description for a run-down of the intuiton here:
    index = -1
    for T in searchNodes:
        if isinstance(T, FOON.Object):
            if T.equals_lvl1(goalNode) and T.isSameStates_ID_only(goalNode):
                index = searchNodes.index(T)
                goals.append(searchNodes[index])

    if index == -1:
        print("Item " + goalNode.getObjectKey(hierarchy_level) +
              " has not been found in network!")
        return False
    # endif

    if verbose:
        print("\n-- Candidate goal nodes are as follows:")
        for G in goals:
            G.print_functions[hierarchy_level-1]()

    if verbose:
        print("\n-- Proceeding with task tree retrieval...\n")

    startNodes = []
    for T in environment:
        index = -1
        for N in searchNodes:
            if isinstance(N, FOON.Object):
                if T.equals_lvl1(N) and T.isSameStates_ID_only(N):
                    index = searchNodes.index(N)
                    break
        if index > -1:
            # -- this means that the object exists in FOON; if not..
            startNodes.append(searchNodes[index])
        else:
            # .. we can try to find a substitute for this object:
            possible_substitutes = _findObjectSubstitute(T)

            for P in possible_substitutes:
                index = -1
                for N in searchNodes:
                    if P.equals_functions[hierarchy_level-1](N):
                        index = searchNodes.index(N)
                        break
                # endfor

                if index > -1:
                    # -- this means that the object exists in FOON; if not..
                    startNodes.append(searchNodes[index])
                # endif
            # endfor
        # endif
    # endfor

    # NOTE: this function will first identify the "best" path, which is based on the availability of objects:

    # -- first, find all paths that reach the provided goal:
    all_paths = _findAllPaths(goalType, goalState, hierarchy_level)
    best_path_count, best_path = 0, 0

    for T in all_paths:
        # -- note all objects required or referred to in the path tree T:
        objects_required = []
        for FU in T:
            for _input in FU.getInputList():
                if _input in objects_required:
                    continue
                objects_required.append(_input)
            for _output in FU.getOutputList():
                if _output in objects_required:
                    continue
                objects_required.append(_output)
        # endfor

        count = 0
        for O in objects_required:
            if O in startNodes:
                count += 1

        # -- keep note of the best path with most overlap:
        if best_path_count < count:
            best_path = all_paths.index(T)
            best_path_count = count
    # endfor

    # TODO: more tests..
    return all_paths[best_path]
# enddef


def _pathTreeRetrieval(goalType, goalState, hierarchy_level=3, skip_prompts=False):
    # -- first, identify all possible paths in the network:
    all_path_trees = _findAllPaths(goalType, goalState, hierarchy_level)
    all_path_trees.sort(key=len)

    if not skip_prompts:
        _response = input(' -- Print length of all paths? [Y/N] > ')
        if _response == 'y' or _response == 'Y':
            for path in all_path_trees:
                total_success_rate = 1.0
                for FU in path:
                    total_success_rate = total_success_rate * \
                        (FU.getSuccessRate() if FU.getSuccessRate() else -1.0)
                print("length of path #" + str(all_path_trees.index(path)) + "\t:\t" + str(
                    len(path)) + ' - ' + (str(total_success_rate) if total_success_rate >= 0 else ''))

    # -- we can also find the average length of a path and keep those that are below the average:
    #avg_path_length = 0
    # for x in all_path_trees:
    #	avg_path_length += len(x)
    #avg_path_length /= len(all_path_trees)

    # -- then, let's only keep the upper X-% of shortest length paths, so we can discard nonsensical paths:
    shortest_paths = all_path_trees[:int(0.05 * len(all_path_trees)) + 1]

    if not skip_prompts:
        _response = input(
            '\n -- Save preliminary path trees to disk? [Y/N] (default : Y) > ')
        if _response is not 'N' and _response is not 'n':
            # -- prompt user for whether to save all the paths or to only save the best X-% (if there are that many):
            _response = input('  -- Save ALL or upper 5%? [1/2] > ')
            paths_to_print = all_path_trees

            if _response == '2':
                paths_to_print = shortest_paths
            # endif

            folder = "path_trees_to_O" + \
                str(goalType) + "S" + str(goalState) + "/"
            if not os.path.exists(folder):
                os.makedirs(folder)

            for task_tree in tqdm.tqdm(paths_to_print):
                # -- produce all files to the different paths to verify that the M swapping is working correctly:

                path_count = paths_to_print.index(task_tree)

                # Remember, we save as two file types now:
                # -- 1) Save TXT version:
                _file = open(folder + "FOON_path_#" + str(path_count) + "_to_O" + str(
                    goalType) + "S" + str(goalState) + "_lvl" + str(hierarchy_level) + ".txt", 'w')
                for FU in task_tree:
                    _file.write(FU.getFunctionalUnitText())
                _file.close()

                # -- 2) Save JSON version:
                try:
                    import json
                except ImportError:
                    pass
                else:
                    subgraph_units = {}
                    subgraph_units['functional_units'] = []

                    _file = open(folder + "FOON_path_#" + str(path_count) + "_to_O" + str(
                        goalType) + "S" + str(goalState) + "_lvl" + str(hierarchy_level) + ".json", 'w')
                    for FU in task_tree:
                        subgraph_units['functional_units'].append(
                            FU.getFunctionalUnitJSON())
                    json.dump(subgraph_units, _file, indent=7)
                    _file.close()
                # endtry
            # endfor
        # endif

    if not skip_prompts:
        # -- although we ask the user if we should print ALL or upper-X%, let's only work on the upper-X%:
        _response = input(
            '\n -- Proceed with human-cobot task tree planning? [Y/N] > ')
        if _response == 'Y' or _response == 'y':
            return _taskTreeRetrieval_weighted(shortest_paths, goalType, goalState, hierarchy_level)

    return shortest_paths
# enddef


def _findAllPaths_old(goalType, goalState, hierarchy_level=3):
    global FOON_functionalUnits, FOON_nodes, FOON_outputsToUnits

    # -- note the level we are searching at, selecting the right FOON:
    goalNode = FOON.Object(objectID=int(goalType), objectLabel=None)
    for x in goalState:
        goalNode.addNewState([int(x), None, []])

    searchNodes = None
    searchFOON = None
    searchMap = None

    if hierarchy_level == 1 or hierarchy_level == 2 or hierarchy_level == 3:
        searchNodes = FOON_nodes[hierarchy_level-1]
        searchFOON = FOON_functionalUnits[hierarchy_level-1]
        searchMap = FOON_outputsToUnits[hierarchy_level-1]
    else:
        print(' -- ERROR: provided invalid hierarchy level as argument to function! Must be between 1 and 3 (inclusive).')
        return

    # -- first part: check if the object actually exists in the network:

    # NOTE: when searching for the procedure to make a single item, we need to negate the ingredients usage.
    #	-- when searching level 3, we default to level 2 to find all object/state combinations of a goal node:
    search_level = hierarchy_level
    if search_level == 3:
        search_level = 2

    # -- list of ALL possible functional units that can be used for making the item:
    global verbose
    all_path_trees = []

    print(' -- Searching for goal node candidates in FOON (level ' +
          str(hierarchy_level) + '...')

    goal_index = []
    for T in searchNodes:
        if isinstance(T, FOON.Object):
            if T.equals_lvl1(goalNode) and T.isSameStates_ID_only(goalNode):
                goal_index.append(searchNodes.index(T))
                print('  -- Added goal node:')
                T.print_functions[hierarchy_level-1]()

    if not goal_index:
        print(" -- Item " + str(goalNode.getObjectKey(hierarchy_level)) +
              " has not been found in network!")
        return all_path_trees
    # endif

    # NOTE: Necessary lists needed to keep track of the tree we are creating.
    #	-- 'tree_stack' is used for creating new children using combination of FUs that make the parent node's input nodes (see below)
    # 	-- 'root_nodes' contains the original root nodes for the trees we are growing.
    root_nodes = collections.deque()

    # -- map the goal node to its respective root node:
    goal_to_root = {}

    # STEP 1: Find all root nodes (i.e. all FUs that make the original goal node) to different trees for each goal node:
    for G in goal_index:
        # -- this is the actual goal node which is in the network:
        goalNode = searchNodes[G]

        for FU in searchFOON:
            # -- searching for all functional units with our goal as output
            for N in FU.getOutputList():
                if N.equals_functions[search_level-1](goalNode):
                    # -- create roots for path trees:
                    node = FOON.TreeNode()
                    node.setUnitsToTreeNode([searchFOON.index(FU)])
                    root_nodes.append(node)

                    # -- note mapping of the root to its intended goal node:
                    goal_to_root[node] = goalNode
                # endif
            # endfor
        # endfor
    # endfor

    for _root in root_nodes:
        start_time = time.time()

        print('\n  -- Starting root #' + str(root_nodes.index(_root)) + '...')

        # -- initialize the tree stack here for each root node:
        tree_stack = collections.deque()
        tree_stack.append(_root)

        # -- create a set that enforces uniqueness of certain tree nodes (based on contained units):
        tree_set = {}

        # NOTE: we can possibly have many object nodes that are suitable as the goal node;
        #	we will go through each of these candidate targets:
        goalNode = goal_to_root[_root]

        # NOTE: relevant ingredients are ingredients that pertain to the creation of the goal node:
        relevant_ingredients = goalNode.getIngredients() if goalNode.hasIngredients() else []
        for _input in searchFOON[_root.getUnitsInTreeNode()[0]].getInputNodes():
            if _input.hasIngredients():
                relevant_ingredients = list(
                    set(relevant_ingredients) | set(_input.getIngredients()))

        # -- keep track of items that have already been seen using this set:
        items_seen = set()

        # Step 2: Create a tree of functional units that can be used for exploring all possible paths with DFS:
        while tree_stack:

            head = tree_stack.popleft()

            if verbose:
                print("tree node:")
                print(" -children : " + str(head.getChildren()))
                print(" -contained : " + str((head.getUnitsInTreeNode())))

            if verbose:
                for FU in head.getUnitsInTreeNode():
                    print(str(searchFOON.index(FU)))

            permutations = set()

            # # -- keep track of items that have already been seen using this set:
            items_seen = head.getItemsSeen()

            # -- pre-build ancestors list such that each TreeNode contains it for quick and easy reference:
            ancestors = set(head.getUnitsInTreeNode()
                            ) | set(head.getAncestors())

            # -- search for all functional units that create the COMBINATION of input nodes in the root:
            for _FU in head.getUnitsInTreeNode():
                for _input in searchFOON[_FU].getInputList():

                    temp_object_id = searchNodes.index(_input)
                    if temp_object_id in items_seen:
                        continue

                    items_seen.add(temp_object_id)

                    # -- for all inputs for the root FU node..
                    try:
                        candidates = [searchFOON.index(
                            I) for I in searchMap[_input]]
                    except KeyError:
                        continue

                    # -- making sure we only explore functional units relevant to the goal:
                    if relevant_ingredients:
                        # _input.print_functions[hierarchy_level-1]()

                        # -- make a temporary list containing units deemed relevant:
                        revised_candidates = []
                        for C in candidates:
                            # -- identify all ingredients used by outputs for each candidate:
                            ingredients_required = []
                            for _output in searchFOON[C].getOutputNodes():
                                for I in _output.getIngredients():
                                    ingredients_required.append(I)
                                # endif
                            # endif

                            # -- make sure we only have unique instances of ingredients:
                            ingredients_required = list(
                                set(ingredients_required))
                            num_ingredients_required = len(
                                ingredients_required)
                            num_ingredients_left = len(
                                list(set(ingredients_required) - set(relevant_ingredients)))

                            # print(relevant_ingredients)
                            # print(ingredients_required)

                            if not ingredients_required:
                                revised_candidates.append(C)
                            elif num_ingredients_left / num_ingredients_required <= 0.25 and num_ingredients_left < num_ingredients_required:
                                revised_candidates.append(C)

                                #relevant_ingredients = list(set(relevant_ingredients) | set(ingredients_required))
                                # print('yes')
                                # searchFOON[C].print_functions[hierarchy_level-1]()
                            else:
                                # print('no')
                                ancestors.add(C)
                                pass
                            # input()
                        # endfor

                        candidates = revised_candidates
                    # endif

                    temp_list = frozenset(set(candidates) - ancestors)

                    if verbose:
                        input()
                        print(
                            ' -- current list of unseen functional units: ' + str(temp_list))

                    if temp_list:
                        permutations.add(temp_list)

                    # #endfor
                # endfor
            # endfor

            # -- remove all empty sets that cause incorrect calculations!
            # -- this happens when we come across a 'leaf' FU with NO functional units that create the inputs for this tree node.
            permutations = list(filter(None, permutations))
            if not permutations:
                continue

            # --  we use a set() to keep UNIQUE copies of children:
            new_children = set()

            # -- NOTE: itertools.product() is a function that is used for calculating all sets
            # 	(the number of ways of selecting items in order) of a given set of items.
            # -- For example: given the sets [1, 2] and [2, 3], the total number of Cartesian sets would be:
            #		[1, 2], [1, 3], [2, 2], [2, 3]
            for P in IT.product(*permutations):
                # Interesting note: set((1,2)) == set((2,1)) will be True, so this is why we don't have to worry!

                # -- add the tuples to the set, as it will only keep unique Cartesian sets:
                new_children.add(frozenset(P))
            # endfor

            new_children = list(new_children)
            # -- pruning certain sets:
            # new_children.sort(key=len)
            # new_children = set(new_children[:int( 0.01 * len(new_children)) + 1])

            for C in new_children:
                # -- flag reflecting if tree node has been explored before (i.e. it has been created) or not:
                is_new_node = False

                # NOTE: C will be a frozen set object, making it immutable:
                if C not in tree_set:
                    tree_set[C] = FOON.TreeNode()
                    is_new_node = True
                # else:
                    # print('this combination already explored')
                # end

                # -- with this child, we need to set its attributes and parents accordingly:
                child = tree_set[C]

                # child = FOON.TreeNode(); is_new_node = True

                # -- get the units already contained in this tree node (if any exist)
                # 	and merge them with the new set C:
                child.setUnitsToTreeNode(
                    list(set(child.getUnitsInTreeNode()) | C))
                child.setItemsSeen(child.getItemsSeen() | items_seen)

                # -- merge child's ancestors (if any) with ancestors from its parent:
                child.setAncestors(ancestors | child.getAncestors())

                # -- set parents and children here; check if we won't be making duplicates here:
                head.addChild(child)

                child.addParent(head)

                # -- only change this child's level if it did not exist before (i.e. has default value of 0):
                if child.getLevel() == 0:
                    child.setLevel(int(head.getLevel()) + 1)

                if verbose:
                    child.printTreeNode(searchFOON)
                # endif

                if is_new_node:
                    # -- add new TreeNode to the deque if we just added a completely new tree node:
                    tree_stack.appendleft(child)

            # endfor

            if verbose:
                input("Press ENTER to continue...")

        # endwhile

        end_time = time.time()

        print('   -- Time taken to explore root #' + str(root_nodes.index(_root)
                                                         ) + ' : ' + (str(end_time - start_time)) + ' s..')
        print('   -- Total objects seen in exploration : ' +
              str(len(items_seen)) + " / " + str(len(searchMap)))

    # endfor

    if verbose:
        print('\n -- printing entire path tree:\n')
        for R in root_nodes:
            FOON.TreeNode.printPathTree(R)
            print()

    # Step 3: Save all paths from all possible root nodes as found in Step 1 to a list.
    for R in root_nodes:
        for path in FOON.TreeNode.makeList(R):
            # -- everything will be saved as reverse order, so switch it around.
            path.reverse()

            # -- save all task trees found by algorithm to this list:
            task_tree = [searchFOON[FU] for FU in path]
            print(len(task_tree))
            all_path_trees.append(task_tree)
        # endfor
    # endfor

    print('\n  -- Total number of task trees found : ' +
          str(len(all_path_trees)) + '\n')

    return all_path_trees
# enddef


def _findAllPaths_2(goalType, goalState, hierarchy_level=3):
    global FOON_functionalUnits, FOON_nodes, FOON_outputsToUnits

    # -- note the level we are searching at, selecting the right FOON:
    goalNode = FOON.Object(objectID=int(goalType), objectLabel=None)
    for x in goalState:
        goalNode.addNewState([int(x), None, []])

    searchNodes = None
    searchFOON = None
    searchMap = None

    if hierarchy_level == 1 or hierarchy_level == 2 or hierarchy_level == 3:
        searchNodes = FOON_nodes[hierarchy_level-1]
        searchFOON = FOON_functionalUnits[hierarchy_level-1]
        searchMap = FOON_outputsToUnits[hierarchy_level-1]
    else:
        print(' -- ERROR: provided invalid hierarchy level as argument to function! Must be between 1 and 3 (inclusive).')
        return

    # -- first part: check if the object actually exists in the network:

    # NOTE: when searching for the procedure to make a single item, we need to negate the ingredients usage.
    #	-- when searching level 3, we default to level 2 to find all object/state combinations of a goal node:
    search_level = hierarchy_level
    if search_level == 3:
        search_level = 2

    # -- list of ALL possible functional units that can be used for making the item:
    global verbose
    all_path_trees = []

    index = -1
    for T in searchNodes:
        if isinstance(T, FOON.Object):
            if T.equals_lvl1(goalNode) and T.isSameStates_ID_only(goalNode):
                index = searchNodes.index(T)
                print('-- Added goal node:')
                T.print_functions[hierarchy_level-1]()

    if index == -1:
        print("Item " + str(goalNode.getObjectKey(hierarchy_level)) +
              " has not been found in network!")
        return all_path_trees
    # endif

    # NOTE: Necessary lists needed to keep track of the tree we are creating.
    #	-- 'tree_stack' is used for creating new children using combination of FUs that make the parent node's input nodes (see below)
    # 	-- 'root_nodes' contains the original root nodes for the trees we are growing.
    root_nodes = collections.deque()

    # -- this is the actual goal node which is in the network:
    goalNode = searchNodes[index]

    # Step 1: Find all root nodes (i.e. all FUs that make the original goal node) to different trees:
    for FU in searchFOON:
        # -- searching for all functional units with our goal as output
        for N in FU.getOutputList():
            if N.equals_functions[search_level-1](goalNode):
                root_nodes.append(searchFOON.index(FU))
        # endfor
    # endfor

    solutions = []

    for _root in root_nodes:
        start_time = time.time()

        print('\n  -- Starting root #' + str(root_nodes.index(_root)) + '...')

        # -- initialize the tree stack here for each root node:
        tree_stack = collections.deque()
        tree_stack.append(_root)

        # -- keep a list of all preliminary paths that we modify and build throughout the search:
        paths = collections.deque()
        paths.append([_root])

        # -- keep track of all items that have been evaluated for each path in the works:
        items_seen = {}
        items_seen[0] = set({index})

        while tree_stack:
            print(len(tree_stack))
            head = tree_stack.popleft()

            permutations = set()

            inputs_evaluated = list()

            for _input in searchFOON[head].getInputList():
                # -- for all inputs for the root FU node..
                candidates = None
                try:
                    candidates = [searchFOON.index(I)
                                  for I in searchMap[_input]]
                except KeyError:
                    pass
                # endtry

                if candidates:
                    temp_list = frozenset(candidates)
                    inputs_evaluated.append(searchNodes.index(_input))
                    permutations.add(temp_list)

            if not permutations:
                continue

            new_children = set()
            for P in IT.product(*permutations):
                # Interesting note: set((1,2)) == set((2,1)) will be True, so this is why we don't have to worry!

                # -- add the tuples to the set, as it will only keep unique Cartesian sets:
                new_children.add(frozenset(P))
            # endfor

            # -- convert set of sets into list of lists for ease of use:
            new_children = [list(S) for S in new_children]

            # -- we will be making duplicates of existing paths (if they are relevant to this present head)
            #	and add them to new_paths temporarily.
            new_paths = []
            new_items_seen = {}

            changes = False

            candidates_for_stack = set()

            print(len(paths))

            for old_path in paths:

                if head in old_path:
                    # -- this means that a certain path has some relation to the head we are currently on:

                    for _set in new_children:
                        # --  a set is one of the computed Cartesian products from above:

                        # -- we take the record of what items have already been solved for a particular path:
                        temp_path = list(old_path)
                        temp_items_seen = items_seen[paths.index(
                            old_path)].copy()

                        # -- we need to gatekeep to make sure that we are not exploring something we have already solved:
                        for y in range(len(_set)):
                            # -- for each item in the Cartesian product...

                            if inputs_evaluated[y] in temp_items_seen or _set[y] in temp_path:
                                # ... check if the object for the candidate unit already was solved:
                                # -- if the input object has already been seen, then no need to try and add a functional unit
                                # 	to the path that solves it again:
                                pass
                            else:
                                # searchNodes[inputs_evaluated[y]].print_functions[hierarchy_level-1]()
                                # -- we note if changes were made such that we will
                                # 	override the old paths list with new one:
                                changes = True

                                # -- append changes to the path and items seen lists:
                                temp_path.append(_set[y])
                                temp_items_seen.add(inputs_evaluated[y])

                                candidates_for_stack.add(_set[y])
                                # tree_stack.append(_set[y])
                            # endif
                        # endfor

                        print(temp_path)
                        print(temp_items_seen)

                        new_paths.append(temp_path)
                        new_items_seen[len(new_paths)-1] = temp_items_seen
                    # endfor
                else:
                    new_paths.append(old_path)
                    new_items_seen[len(new_paths) -
                                   1] = items_seen[paths.index(old_path)]

                # endif

            # endfor

            input(len(new_paths) == len(new_items_seen))

            if changes:
                # -- we override the list of paths with the new set as well as the items seen for each path:
                paths = new_paths
                print(len(paths))
                paths = set([frozenset(x) for x in paths])
                paths = [list(x) for x in paths]
                print(len(paths))

                print(len(paths))
                items_seen = new_items_seen

                # -- we only add certain elements to the tree stack if we need to further explore
                # 	(i.e. we added new things to paths)
                for C in candidates_for_stack:
                    tree_stack.append(C)

        paths = set([frozenset(x.sort()) for x in paths])
        paths = [list(x) for x in paths]

        for P in paths:
            print(P)
            solutions.append(P)

        print(len(solutions))

        end_time = time.time()

        print('   -- Time taken to explore root #' + str(root_nodes.index(_root)
                                                         ) + ' : ' + (str(end_time - start_time)) + ' s..')

    for S in solutions:
        # -- everything will be saved as reverse order, so switch it around.
        S.reverse()

        # -- save all task trees found by algorithm to this list:
        task_tree = [searchFOON[FU] for FU in S]
        all_path_trees.append(task_tree)

    return all_path_trees
# enddef


def _findAllPaths(goalType, goalState, hierarchy_level=3, check_ingredients=True, max_height=sys.maxsize, max_children=sys.maxsize, end_early=False, objects_to_keep=[]):
    '''
    Finds all path trees to make a given goal, which is an object in FOON.

    Parameters:
            goalType (int): An integer referring to the goal object's ID
            goalState (list): An list of integers referring to the goal object's state ID(s)
            hierarchy_level (int): The hierarchy level to be considered when searching (default: 3)
            check_ingredients (bool): Flag to determine whether to check for ingredient context for searching (default: True)
            max_height (int): The maximum allowed height that a path tree node can have; a restrained path tree will be faster to build, but it may not be complete (default: sys.maxsize)
            max_children (int): The maximum allowed children that a path tree node can have; the smaller, the better, but less likely to get good results (default: sys.maxsize)
            downsize (bool) : Flag to end search when a single path has ended (i.e., a tree node has no children) (default: False)
            objects_to_keep (list) : List of object labels to keep when propagating path tree; anything not in this list is removed from functional units.

    Returns:
            all_path_trees (list): A list of all path trees found; if goal node is not found in FOON, it will be empty.
    '''

    # NOTE: this is a greedy version of the path tree retrieval; what makes it greedy is that we set a limit to the
    #	number of children created by each node of the tree, which is reflected by the variable 'max_children'.

    print('\n -- PATH_TREE: Commencing path tree retrieval with the following parameters:')
    print('    -> hierarchy_level = ' + str(hierarchy_level))
    if check_ingredients:
        print('    -> checking for ingredient context? = YES')
    else:
        print('    -> checking for ingredient context? = NO')
    if max_children < sys.maxsize:
        print('    -> maximum children allowed = ' + str(max_children))
    if objects_to_keep:
        print('    -> performing task tree generation pruning? = YES')

    global FOON_functionalUnits, FOON_nodes, FOON_outputsToUnits, verbose

    # NOTE: Three major steps to this algorithm:
    #	1. Identify all possible root nodes (roots are the functional units that IMMEDIATELY produce the given goal);
    #	2. Build and propagate a path tree that starts from each root that fulfills requirements to make the goal;
    #	3. Explore all roots using DFS to acquire each unique path that create the goal node.

    # -- note the level we are searching at, selecting the right FOON:
    goalNode = FOON.Object(objectID=int(goalType), objectLabel=None)
    for x in goalState:
        goalNode.addNewState([int(x), None, []])

    # -- assign the right FOON structures to these variables:
    searchNodes = None
    searchFOON = None
    searchMap = None
    if hierarchy_level == 1 or hierarchy_level == 2 or hierarchy_level == 3:
        searchNodes = FOON_nodes[hierarchy_level-1]
        searchFOON = FOON_functionalUnits[hierarchy_level-1]
        searchMap = FOON_outputsToUnits[hierarchy_level-1]
    else:
        print(' -- ERROR: provided invalid hierarchy level as argument to function! Must be between 1 and 3 (inclusive).')
        return
    # endif

    # NOTE: when searching for the procedure to make a single item, we need to negate the ingredients usage.
    #	-- when searching level 3, we default to level 2 to find all object/state combinations of a goal node:
    search_level = hierarchy_level
    if search_level == 3:
        search_level = 2

    # -- list of ALL possible functional units that can be used for making the item:
    all_path_trees = []

    print('\n -- PATH_TREE: Searching for goal node candidates in FOON (level ' +
          str(hierarchy_level) + ')...')

    # -- first part: check if the object actually exists in the network:
    goal_index = []
    for T in searchNodes:
        if isinstance(T, FOON.Object):
            if T.equals_lvl1(goalNode) and T.isSameStates_ID_only(goalNode):
                goal_index.append(searchNodes.index(T))
                print('  -- Goal node # ' + str(len(goal_index)) + ':')
                T.print_functions[hierarchy_level-1]()
                print()
            # endif
        # endif
    # endfor

    if not goal_index:
        print(" -- Item " + str(goalNode.getObjectKey(hierarchy_level)) +
              " has not been found in network!")
        return all_path_trees
    # endif

    # NOTE: Necessary lists needed to keep track of the tree we are creating.
    #	-- 'tree_stack' is used for creating new children using combination of FUs that make the parent node's input nodes (see below)
    # 	-- 'root_nodes' contains the original root nodes for the trees we are growing.
    root_nodes = collections.deque()

    # -- map the goal node to its respective root node:
    goal_to_root = {}

    # NOTE: if we are using this for task tree generation, then we may want to do something different..
    #	this means that we would want to do some preliminary pruning of objects and branches that are not necessary.
    # -- in the regular path tree retrieval, to be more memory-efficient, we only consider the functional unit's by their
    # 	index values when storing units within path tree nodes
    # -- in this case, we will use a dictionary that maps pruned units to their original unit index such that we do not
    # 	need to modify this function greatly:
    mapping_to_pruned_units = {}

    # STEP 1: Find all root nodes (i.e. all FUs that make the original goal node) to different trees for each goal node:
    for G in goal_index:
        # -- this is the actual goal node which is in the network:
        goalNode = searchNodes[G]

        for FU in searchFOON:
            # -- searching for all functional units with our goal as output
            for N in FU.getOutputList():
                if N.equals_functions[search_level-1](goalNode):
                    # -- create roots for path trees:
                    node = FOON.TreeNode()
                    node.setUnitsToTreeNode([searchFOON.index(FU)])
                    root_nodes.append(node)

                    # -- note mapping of the root to its intended goal node:
                    goal_to_root[node] = goalNode

                    # NOTE: functional unit pruning:
                    if objects_to_keep:
                        # -- we will use a mapping between the functional unit to a pruned unit:
                        temp_FU, count = FOON.FunctionalUnit(), 0
                        temp_FU.setMotionNode(FOON.Motion(motionID=FU.getMotion(
                        ).getMotionType(), motionLabel=FU.getMotion().getMotionLabel()))

                        # -- main idea here: remove any input nodes in all functional units that are not relevant to what we need:
                        for _input in FU.getInputNodes():
                            if _input.getObjectLabel() in objects_to_keep:
                                temp_FU.addObjectNode(
                                    _input, True, FU.getMotionDescriptor(count, is_input=True))
                            # endif
                            count += 1
                        # endfor

                        count = 0
                        for _output in FU.getOutputNodes():
                            if _output.getObjectLabel() in objects_to_keep:
                                temp_FU.addObjectNode(
                                    _output, False, FU.getMotionDescriptor(count, is_input=False))
                            # endif
                            count += 1
                        # endfor

                        if searchFOON.index(FU) not in mapping_to_pruned_units:
                            mapping_to_pruned_units[searchFOON.index(
                                FU)] = temp_FU
                        # endif
                    # endif
                    # NOTE: end of functional unit pruning part.

                # endif
            # endfor
        # endfor
    # endfor

    print(' -- PATH_TREE: Beginning search...')

    response = input(
        '  -- Which object would you like to set as target? (Press ENTER for no preference) > ')
    if response:
        root_nodes = collections.deque([root_nodes[int(response)-1]])

    for _root in root_nodes:
        start_time = time.time()

        print('  -- Starting root #' + str(root_nodes.index(_root)) + '...')

        # -- initialize the tree stack here for each root node:
        tree_stack = collections.deque()
        tree_stack.append(_root)

        # -- create a set that enforces uniqueness of certain tree nodes (based on contained units):
        tree_set = {}

        # NOTE: we can possibly have many object nodes that are suitable as the goal node;
        #	we will go through each of these candidate targets:
        goalNode = goal_to_root[_root]

        # -- keep track of items that have already been seen using this set:
        items_seen = set()

        # NOTE: relevant ingredients are ingredients that pertain to the creation of the goal node:
        relevant_ingredients = goalNode.getIngredients() if goalNode.hasIngredients() else []

        # there should only be one, but just writing it in this way.
        for _unit in _root.getUnitsInTreeNode():
            if not objects_to_keep:
                # NOTE: here, we are just performing regular path tree retrieval (NO PRUNING).

                # -- we check the inputs of the functional unit to see if any different ingredients are being used:
                for _input in searchFOON[_unit].getInputNodes():
                    if _input.hasIngredients():
                        relevant_ingredients = list(
                            set(relevant_ingredients) | set(_input.getIngredients()))
                    # endif
                # endfor

                # -- we can also immediately mark off the outputs for this root's functional unit as being seen,
                #	since this functional unit will already be "executed":
                for _output in searchFOON[_unit].getOutputNodes():
                    # -- get the object's index and add to list of items seen:
                    temp_object_id = searchNodes.index(_output)
                    items_seen.add(temp_object_id)
                # endfor

            else:
                # NOTE: here, we are just pruning while performing path tree retrieval:

                for _input in mapping_to_pruned_units[_unit].getInputNodes():
                    if _input.hasIngredients():
                        relevant_ingredients = list(
                            set(relevant_ingredients) | set(_input.getIngredients()))
                    # endif
                # endfor

                for _output in mapping_to_pruned_units[_unit].getOutputNodes():
                    # -- get the object's index and add to list of items seen:
                    temp_object_id = searchNodes.index(_output)
                    items_seen.add(temp_object_id)
                # endfor

            # endif
        # endfor

        # Step 2: Create a tree of functional units that can be used for exploring all possible paths with DFS:
        while tree_stack:

            # NOTE: pop left from deque -> FIFO -> breadth-first ordering!
            head = tree_stack.popleft()

            # -- check if the popped tree node exceeds our given path tree height/depth limit:
            if head.getLevel() > max_height:
                continue

            print(head.getLevel())

            if verbose:
                print("tree node:")
                print(" -children : " + str(head.getChildren()))
                print(" -contained : " + str((head.getUnitsInTreeNode())))

            if verbose:
                for FU in head.getUnitsInTreeNode():
                    print(str(searchFOON.index(FU)))

            # NOTE: permutations
            permutations = set()

            # # -- keep track of items that have already been seen using this set:
            items_seen = head.getItemsSeen()

            # -- pre-build ancestors list such that each TreeNode contains it for quick and easy reference:
            ancestors = set(head.getUnitsInTreeNode()
                            ) | set(head.getAncestors())

            # -- search for all functional units that create the COMBINATION of input nodes in the root:
            for _FU in head.getUnitsInTreeNode():

                precondition_unit = mapping_to_pruned_units[_FU] if objects_to_keep else searchFOON[_FU]

                for _input in precondition_unit.getInputList():

                    # -- check if this node is considered to be a starting node:
                    if _isStartingNode(_input):
                        if verbose:
                            _input.print_functions[2]()
                        continue

                    # -- check if we have already seen or explored this object before:
                    temp_object_id = searchNodes.index(_input)
                    if temp_object_id in items_seen:
                        continue

                    items_seen.add(temp_object_id)

                    # -- for all inputs for the root FU node..
                    try:
                        candidates = [searchFOON.index(
                            I) for I in searchMap[_input]]
                    except KeyError:
                        continue

                    # -- making sure we only explore functional units relevant to the goal:
                    if relevant_ingredients and check_ingredients:
                        # _input.print_functions[hierarchy_level-1]()

                        # -- make a temporary list containing units deemed relevant:
                        revised_candidates = []
                        for C in candidates:
                            # -- identify all ingredients used by outputs for each candidate:
                            ingredients_required = []
                            for _output in searchFOON[C].getOutputNodes():
                                # -- we check all output nodes in the candidate functional unit C and we take note of their ingredients:
                                for I in _output.getIngredients():
                                    ingredients_required.append(I)
                                # endif
                            # endif

                            # -- make sure we only have unique instances of ingredients:
                            ingredients_required = list(
                                set(ingredients_required))
                            num_ingredients_required = len(
                                ingredients_required)

                            # -- ingredients left - ingredients in the required set that have not been matched to the relevant ingredients for goal:
                            num_ingredients_left = len(
                                list(set(ingredients_required) - set(relevant_ingredients)))

                            print(ingredients_required)
                            print(relevant_ingredients)
                            input((set(ingredients_required) -
                                  set(relevant_ingredients)))

                            if not ingredients_required:
                                # -- if there are no ingredients required at all, then we don't have any context to worry about:
                                revised_candidates.append(C)

                            elif num_ingredients_left / num_ingredients_required <= 0.25 and num_ingredients_left < num_ingredients_required:
                                # -- this means that there was some overlap between C's required ingredients and the goal-relevant ingredients;
                                #	we can either base this relevance on some threshold (here, it is suggests that we must cover 75% of relevant ingredients) #
                                #	or we look for an exact match (which may not always be ideal)
                                revised_candidates.append(C)

                                # -- not sure if it's a good idea to combine the non-overlapping ingredients with the goal-relevant ingredients:
                                #relevant_ingredients = list(set(relevant_ingredients) | set(ingredients_required))

                            else:
                                # -- this means we did not meet the required overlap threshold; however, we should still treat it as if we visited this functional unit.
                                ancestors.add(C)

                            # endif
                        # endfor

                        # -- now that we evaluated the candidates for contextual relevance, let's now work with this new set:
                        candidates = revised_candidates
                    # endif

                    # -- just as a safety measure, let's make sure that there are no ancestor units (i.e. already visited units) in our candidate set:
                    final_candidates = frozenset(set(candidates) - ancestors)

                    # NOTE: functional unit pruning:
                    if objects_to_keep:
                        for C in final_candidates:
                            # -- we will use a mapping between the functional unit to a pruned unit:
                            temp_FU, count = FOON.FunctionalUnit(), 0

                            # -- first, set a motion node that is the same as the original functional unit's motion node:
                            temp_FU.setMotionNode(FOON.Motion(motionID=searchFOON[C].getMotion(
                            ).getMotionType(), motionLabel=searchFOON[C].getMotion().getMotionLabel()))

                            # -- main idea here: remove any input or output nodes in all functional units that are not relevant to what we need:
                            for _input in searchFOON[C].getInputNodes():
                                if _input.getObjectLabel() in objects_to_keep:
                                    temp_FU.addObjectNode(
                                        _input, True, searchFOON[C].getMotionDescriptor(count, is_input=True))
                                elif _input.hasIngredients():
                                    ingredients = list(
                                        set(objects_to_keep) | set(_input.getIngredients()))
                                    if len(ingredients) > 0:
                                        _input.setIngredients(ingredients)
                                        temp_FU.addObjectNode(
                                            _input, True, FU.getMotionDescriptor(count, is_input=True))
                                # endif
                                count += 1
                            # endfor
                            count = 0
                            for _output in searchFOON[C].getOutputNodes():
                                if _output.getObjectLabel() in objects_to_keep:
                                    temp_FU.addObjectNode(
                                        _output, False, searchFOON[C].getMotionDescriptor(count, is_input=False))
                                count += 1
                            # endfor

                            if C not in mapping_to_pruned_units:
                                mapping_to_pruned_units[C] = temp_FU
                        # endfor
                    # endif

                    if verbose:
                        input()
                        print(
                            ' -- current list of unseen functional units: ' + str(final_candidates))

                    # -- only add the final candidate list if it is not empty:
                    if final_candidates:
                        permutations.add(final_candidates)

                    # endfor
                # endfor
            # endfor

            # NOTE: we must remove all empty sets that cause incorrect calculations!
            # -- this happens when we come across a 'leaf' FU with NO functional units
            # 	that create the inputs for this tree node.
            permutations = list(filter(None, permutations))
            if not permutations:
                if end_early:
                    break
                continue

            # --  we use a set() to keep UNIQUE copies of children:
            new_children = set()

            # -- NOTE: itertools.product() is a function that is used for calculating all sets
            # 	(the number of ways of selecting items in order) of a given set of items.
            # -- For example: given the sets [1, 2] and [2, 3], the total number of Cartesian sets would be:
            #		[1, 2], [1, 3], [2, 2], [2, 3]
            for P in IT.product(*permutations):
                # Interesting note: set((1,2)) == set((2,1)) will be True, so this is why we don't have to worry!

                # -- add the tuples to the set, as it will only keep unique Cartesian sets:
                new_children.add(frozenset(P))
            # endfor

            new_children = list(new_children)

            # -- we can sort the children from smallest to largest - the length will determine how many units are needed to fulfill head's requirements:
            new_children.sort(key=len, reverse=True)

            # -- greedy version of the path tree search will only allow a certain number of children per tree node;
            #	we may not want to restrict it if we want optimal solutions:
            new_children = list(
                set(new_children[:min(max_children, len(new_children))]))

            # -- once again trim more (in case of really large FOON):
            #new_children = new_children[:int(0.05 * len(new_children)) + 1]

            for C in new_children:
                # -- flag reflecting if tree node has been explored before (i.e. it has been created) or not:
                is_new_node = False

                # NOTE: C will be a frozen set object, making it immutable:
                if C not in tree_set:
                    tree_set[C] = FOON.TreeNode()
                    is_new_node = True
                else:
                    if verbose:
                        print('this combination already explored')
                # end

                # -- with this child, we need to set its attributes and parents accordingly:
                child = tree_set[C]

                # -- get the units already contained in this tree node (if any exist)
                # 	and merge them with the new set C:
                child.setUnitsToTreeNode(
                    list(set(child.getUnitsInTreeNode()) | C))

                # -- there may be some outputs to these functional units that have already been
                #	seen or solved; maybe we should add them to the list of items seen:
                for unit in C:
                    for _output in searchFOON[unit].getOutputNodes():
                        # -- get the object's index and add to list of items seen:
                        temp_object_id = searchNodes.index(_output)
                        items_seen.add(temp_object_id)

                child.setItemsSeen(child.getItemsSeen() | items_seen)

                # -- merge child's ancestors (if any) with ancestors from its parent:
                child.setAncestors(ancestors | child.getAncestors())

                # -- set parents and children here; check if we won't be making duplicates here:
                head.addChild(child)
                child.addParent(head)

                # -- only change this child's level if it did not exist before (i.e. has default value of 0):
                if child.getLevel() == 0:
                    child.setLevel(int(head.getLevel()) + 1)

                if verbose:
                    child.printTreeNode(searchFOON)

                # -- add new TreeNode to the deque if we just added a completely new tree node:
                if is_new_node:
                    tree_stack.append(child)

            # endfor

            if verbose:
                input("Press ENTER to continue...")

        # endwhile

        end_time = time.time()

        print('   -- Time taken to explore root #' + str(root_nodes.index(_root)
                                                         ) + ' : ' + (str(end_time - start_time)) + ' s..')
        print('   -- Total objects seen in exploration : ' +
              str(len(items_seen)) + " / " + str(len(searchMap)) + '\n')

        if verbose:
            print(' -- PATH_TREE: Objects seen from exploring root #' +
                  str(root_nodes.index(_root)) + ' are as follows:')
            for I in items_seen:
                searchNodes[I].print_functions[hierarchy_level-1]()
            # endfor
        # endif
    # endfor

    if verbose:
        print(' -- PATH_TREE: Printing trace of path trees:\n')
        for R in root_nodes:
            print('root #' + str(root_nodes.index(R)))
            FOON.TreeNode.printPathTree(R)
            print()

    # Step 3: Save all paths from all possible root nodes as found in Step 1 to a list.
    for R in root_nodes:
        for path in FOON.TreeNode.makeList(R):
            # -- everything will be saved as reverse order, so switch it around.
            path.reverse()

            # -- save all task trees found by algorithm to this list:
            if not objects_to_keep:
                task_tree = [searchFOON[FU] for FU in path]
            else:
                # -- consider the pruned versions of functional units:
                task_tree = [mapping_to_pruned_units[FU].copyFunctionalUnit()
                             for FU in path]

                # -- now, we need to remove the following:
                #	1. remove ALL instances of ingredient labels that are contained within remaining objects;
                #	2. remove all objects that have anything to do with irrelevant ingredients or objects.
                for FU in task_tree:
                    inputs_to_remove = []
                    for _input in FU.getInputNodes():
                        if not set(_input.getIngredients()) & set(objects_to_keep) and _input.getIngredients():
                            inputs_to_remove.append(_input)

                        ingredients_to_remove = []
                        for I in _input.getIngredients():
                            if I not in objects_to_keep:
                                ingredients_to_remove.append(I)

                        for I in ingredients_to_remove:
                            _input.objectIngredients.remove(I)

                    # endfor

                    for N in inputs_to_remove:
                        FU.inputNodes.remove(N)

                    outputs_to_remove = []
                    for _output in FU.getOutputNodes():
                        if not set(_output.getIngredients()) & set(objects_to_keep) and _output.getIngredients():
                            outputs_to_remove.append(_output)

                        ingredients_to_remove = []
                        for I in _output.getIngredients():
                            if I not in objects_to_keep:
                                ingredients_to_remove.append(I)

                        for I in ingredients_to_remove:
                            _output.objectIngredients.remove(I)

                    # endfor

                    for N in outputs_to_remove:
                        FU.outputNodes.remove(N)
                # endfor
            # endif

            all_path_trees.append(task_tree)

        # endfor
    # endfor

    print('\n -- PATH_TREE: Total number of task trees found : ' +
          str(len(all_path_trees)) + '\n')

    return all_path_trees
# enddef


###############################################################################################################################

# NOTE: The following are methods for generalization of FOON (from ICRA 2018).

def _findObjectSubstitute(O, method="WordNet", threshold=0.89):
    # -- instead of going into the expansion (which may take a long time), just find direct substitutes for
    #	objects (typically for task tree retrieval's inputs and add them to the kitchen)

    # -- list of objects and their similarity values w.r.t. other objects:
    global object_similarity_index, FOON_objectLabels

    if not object_similarity_index:
        import json
        try:
            # -- keep both similarity index files separate in case:
            if method == "WordNet":
                _file = open('object_similarity_index-WN.json', 'r')
            else:
                _file = open('object_similarity_index-CN.json', 'r')
            object_similarity_index = json.load(_file)
            _file.close()  # -- Don't forget to close the file once we are done!

        except FileNotFoundError:
            print(" -- WARNING: No object similarity dictionary found in memory!")
            print("\t-- Building object similarity index..")
            object_similarity_index = _buildObjectSimilarity(method)
            # -- keep both similarity index files separate in case:
            if method == "WordNet":
                json.dump(object_similarity_index, open(
                    'object_similarity_index-WN.json', 'w'))
            else:
                json.dump(object_similarity_index, open(
                    'object_similarity_index-CN.json', 'w'))
            # end
        # end
    # endif

    # -- now that we have loaded the object similarity index, then we can go ahead and populate
    #	a list of objects that are similar to this object:
    similar_objects = []

    object_name = O.getObjectLabel()

    for key in object_similarity_index:
        if key.startswith(object_name):
            # -- all similarity values are pre-computed and are found in the object_similarity_index dict:
            similarity_value = float(object_similarity_index[key])

            if similarity_value >= threshold:
                # -- this is an object that is considered similar based on the evaluation metric:
                sim_object_name = key.split(',')[1]

                # -- assign the similar object's name and ID to this newly created object:
                new_object = FOON.Object(
                    objectID=FOON_objectLabels[sim_object_name], objectLabel=sim_object_name)

                # -- copy over the attributes of the object in question (i.e. O) over to the similar objects:
                for S in O.getStatesList():
                    new_object.addNewState(list(S))

    return similar_objects
# enddef

# \begin{FOON-EXP}


def _prepareExpansion():
    # NOTE: this function is used to gather details about the expansion method:
    print(' -- [FOON-EXP] : Please provide the following details that are needed for the expansion process:')

    # -- we can either use WordNet or Concept-Net to measure similarity values (note, however, that Wordnet is MUCH faster)
    expansion_method = "Concept-Net" if input(
        "\ta) Perform expansion using: 1) WordNet or 2) Concept-Net? (default: 1 - WordNet) [1/2] > ") == "2" else "WordNet"

    # -- NLTK's word distance metrics return a value that reflects similarity depending on the function used;
    #	we use the Wu-Palmer metric, which will return a value from 0 to 1.0, where 1.0 means that two words are synonymous
    threshold_value = float(input(
        "\tb) Provide a threshold value for object similarity (between 0.0 and 1.0) > ")) or 0.89

    # -- instead of attempting to expand a FOON based on all possible objects found in the object index of FOON,
    #	we can instead permit a customized subset of objects for expansion.
    custom_object_list = None
    use_custom_list = input(
        "  -- Use all objects for semantic similarity file? [Y/N] (default:Y) > ") or "y"

    if use_custom_list.lower() == 'n':
        # -- NOTE: you will need to create a new file that has subset of object labels for comparison:
        _file = open(
            input("  -- Please type in PATH and NAME to custom object index: > "), 'r')
        for L in _file.readlines():
            if L.startswith("//"):
                continue

            _parts = L.split("\t")
            _object = _parts[1]
            if len(L.split("\t")) == 2:
                custom_object_list.append([_object, 1])
            else:
                custom_object_list.append([_object, _parts[2]])
            # endif
        # endfor
        print('  -- Added a total of ' +
              str(len(custom_object_list)) + ' items for expansion!')
    # endif

    # -- idea: use Concept-Net to decide on states being similar to one another (still needs testing):
    _response = input(
        '-- Use Concept-Net for state suggestion for better node checking? (default:N) [Y/N] > ')
    state_suggestion = True if _response.lower() == "y" else False

    return [expansion_method, threshold_value, custom_object_list, state_suggestion]
# enddef


def _buildObjectSimilarity(method="WordNet"):
    # -- To perform expansion, we need to calculate object similarity using WordNet or Concept-Net.
    global verbose, object_senses

    similarity_map = {}

    if method == "WordNet":
        # NOTE: READ MORE ABOUT WORDNET + NLTK HERE: http://www.nltk.org/howto/wordnet.html
        try:
            from nltk.corpus import wordnet as wn
            from nltk.corpus.reader.wordnet import WordNetError

        except ImportError:
            print(
                " -- ERROR: Missing NLTK library and/or WordNet corpus! Cannot build object similarity index!")
            print(
                "\t-- Please install it using 'pip install nltk' and then use 'nltk.download()'!")
            print("\t-- Refer to https://www.nltk.org/data.html for more details!")
            return

        for X in tqdm.tqdm(FOON_objectLabels):
            for Y in FOON_objectLabels:
                sim_value = 0.0

                # -- if two words have the same object ID (due to being synonyms, aliases, instances, etc.),
                #	then we can say that they are exact matches
                if FOON_objectLabels[X] == FOON_objectLabels[Y]:
                    sim_value = 1.0
                else:
                    try:
                        if verbose:
                            print(" -- Comparing '" + str(X) +
                                  "' with '" + str(Y) + "'...")

                        # -- creating word objects with the name of the items using NLTK
                        # NOTE: here, we need to directly indicate WHICH synset definition (sense) should be used for certain words,
                        # 	as the first sense is not always food-related. These are manually identified and placed in FOON-object_index.txt file.
                        word_1 = wn.synset(
                            str(X.replace(' ', '_')) + '.n.0' + str(object_senses[X]))
                        word_2 = wn.synset(
                            str(Y.replace(' ', '_')) + '.n.0' + str(object_senses[Y]))

                        # -- calculate Wu-Palmer metric for the words (for ALL senses);
                        #	Wu-Palmer is typically used, but other metrics can be used, which are described here: https://www.nltk.org/howto/wordnet.html
                        sim_value = word_1.wup_similarity(word_2)

                    except WordNetError:
                        # NOTE: WordNetError means that the object is not found in WordNet
                        pass

                    # endtry
                # endif

                similarity_map[str(X[0] + ',' + Y[0])] = sim_value
            # endfor
        # endfor

    elif method == "Concept-Net":

        method = int(
            input(' -- Use Concept-Net Web API or word2vec model? [1/2] (Default: 2) > '))

        if method == 2:
            # -- use word2vec model for Concept-Net; download here: https://github.com/commonsense/Concept-Net-numberbatch
            global path_to_ConceptNet

            # -- check if a path has already been specified during run-time:
            if not path_to_ConceptNet:
                path_to_ConceptNet = input(
                    '  -- Enter the full path and name to the Concept-Net model file: > ')

            w2v_concept = None

            try:
                from gensim.models import KeyedVectors
            except ImportError:
                print(' -- ERROR: Missing gensim library!')
                print(
                    "\t-- Please install gensim using 'pip install gensim' in terminal.")
                print(
                    "\t-- Refer to https://radimrehurek.com/gensim/install.html for more details!")
                return

            # -- load word embeddings for Concept-Net:
            try:
                w2v_concept = KeyedVectors.load_word2vec_format(
                    path_to_ConceptNet, binary=True)
            except:
                print(' -- ERROR: Problem loading word2vec model file!')
                print(
                    '\t-- Please download it here and try again: https://github.com/commonsense/Concept-Net-numberbatch')
                return
            # endtry

            for X in tqdm.tqdm(FOON_objectLabels):
                for Y in FOON_objectLabels:
                    sim_value = 0.0

                    # -- if two words have the same object ID (due to being synonyms, aliases, instances, etc.),
                    #	then we can say that they are exact matches
                    if FOON_objectLabels[X] == FOON_objectLabels[Y]:
                        sim_value = 1.0
                    else:
                        try:
                            if verbose:
                                print(" -- Comparing '" + str(X) +
                                      "' with '" + str(Y) + "'...")

                            # -- need to replace whitespaces with underscores:
                            word_1 = X.replace(' ', '_')
                            word_2 = Y.replace(' ', '_')

                            # NOTE: use gensim's similarity method (in KeyedVectors class) to calculate relatedness:
                            sim_value = w2v_concept.similarity(
                                str(word_1), str(word_2))

                        except KeyError:
                            # NOTE: this means that one of the words or expressions was not in Word2Vec:
                            pass
                    # endif

                    similarity_map[str(X[0] + ',' + Y[0])] = float(sim_value)
                # endfor
            # endfor

        else:
            # NOTE: this method uses Concept-Net REST API for calculating similarity (as relatedness)
            #	-- you can read more about it here: https://github.com/commonsense/Concept-Net5/wiki/API
            import requests
            import json

            for X in tqdm.tqdm(FOON_objectLabels):
                for Y in FOON_objectLabels:
                    sim_value = 0.0

                    # -- if two words have the same object ID (due to being synonyms, aliases, instances, etc.),
                    #	then we can say that they are exact matches
                    if FOON_objectLabels[X] == FOON_objectLabels[Y]:
                        sim_value = 1.0
                    else:
                        # -- need to replace whitespaces with underscores:
                        word_1 = X.replace(' ', '_')
                        word_2 = Y.replace(' ', '_')

                        # -- creating query for Concept-Net API (based on tutorial here: https://github.com/commonsense/Concept-Net5/wiki/API):
                        query = ('http://api.Concept-Net.io/relatedness?node1=/c/en/' +
                                 word_1 + '&node2=/c/en/' + word_2)

                        # NOTE: although they say to time requests every 1 sec, we can just keep trying and trying..
                        while True:
                            try:
                                answer = requests.get(query).json()
                            except json.decoder.JSONDecodeError:
                                # -- we keep checking thru a loop until we get an answer..
                                continue
                            else:
                                if verbose:
                                    print('<' + word_1 + ',' + word_2 +
                                          '> : ' + str(answer['value']))
                                sim_value = float(answer['value'])
                                break
                            # endtry
                        # endwhile
                    # endif

                    similarity_map[str(X + ',' + Y)] = sim_value
                # endfor
            # endfor
        # endif
    # endif

    print('  -- Finished building similarity map!')

    return similarity_map
# enddef


def _expandNetwork(obj_sim, method="WordNet", threshold=0.89, state_suggestion=False, custom_list=None):
    global path_to_ConceptNet

    # -- check if a path has already been specified during run-time:
    if not path_to_ConceptNet:
        path_to_ConceptNet = input(
            '  -- Enter the full path and name to the Concept-Net model file: > ')

    # -- use Concept-Net to suggest states that may make sense for objects:
    if state_suggestion:
        try:
            from gensim.models import KeyedVectors
        except ImportError:
            print(' -- WARNING: Missing gensim library!')
            print("\t-- Please install gensim using 'pip install gensim' in terminal.")
            return
        # endtry

        # -- load word embeddings for Concept-Net:
        try:
            w2v_concept = KeyedVectors.load_word2vec_format(
                path_to_ConceptNet, binary=True)
        except Exception:
            return

    FOON_EXP = set()
    nodes_EXP = []

    if not obj_sim:
        import json
        try:
            # -- keep both similarity index files separate in case:
            if method == "WordNet":
                _file = open('object_similarity_index-WN.json', 'r')
            else:
                _file = open('object_similarity_index-CN.json', 'r')
            object_similarity_index = json.load(_file)
            _file.close()  # -- Don't forget to close the file once we are done!

        except FileNotFoundError:
            print(" -- WARNING: No object similarity dictionary found in memory!")
            print("\t-- Building object similarity index..")
            object_similarity_index = _buildObjectSimilarity(method)
            # -- keep both similarity index files separate in case:
            if method == "WordNet":
                json.dump(object_similarity_index, open(
                    'object_similarity_index-WN.json', 'w'))
            else:
                json.dump(object_similarity_index, open(
                    'object_similarity_index-CN.json', 'w'))

    # NOTE: first, we start off with the base set of nodes and functional units from level 3:
    global nodes_lvl3, FOON_lvl3

    for N in nodes_lvl3:
        nodes_EXP.append(N)

    for FU in FOON_lvl3:
        FOON_EXP.add(FU)

    print('\n -- FOON_EXP: Beginning expansion..')

    for key in tqdm.tqdm(object_similarity_index):
        # NOTE: X - object we want to add via expansion; Y - object that already exists in FOON that is considered close to X

        # NOTE: Key(OBJ_1,OBJ_2) -> VALUE(int(similarity))
        # -- keys have whitespace replaced by underscore, so we have to replace that character where spotted:
        X = key.split(',')[0].replace("_", " ")
        Y = key.split(',')[1].replace("_", " ")

        # -- no sense in expanding for units with the same exact label:
        if X == Y:
            continue

        if custom_list:
            if X not in custom_list:
                continue

        # -- FIRST, we need to see if the similarity of objects X and Y are equal to or higher than the threshold:
        if object_similarity_index[key] >= threshold:
            if verbose:
                print(X + ' ' + Y)

            for N in nodes_lvl3:
                # -- then, we look for objects that already exist in FOON that are equal to Y.
                #	 They will be used as reference to copy new objects by swapping their ID/labels:
                if isinstance(N, FOON.Object) and N.getObjectLabel() == Y and N.getObjectType() == FOON_objectLabels[Y]:
                    # -- copied object which has exact same states as the reference object Y:
                    copiedObject = FOON.Object(
                        objectID=FOON_objectLabels[X], objectLabel=X)
                    copiedObject.setStatesList(N.getStatesList())

                    if state_suggestion:
                        # IDEA: find the average similarity between all states to determine whether copied objects suit it
                        average_similarity = 0
                        count = 0
                        object_label = str(X).replace(' ', '_')
                        for S in N.getStatesList():
                            state_label = S[1].split(' ')
                            if len(state_label) == 1:
                                # -- usually single-worded state labels will be better suited for measurement
                                #	and we will avoid states like 'ingredients inside' or 'in bowl'
                                try:
                                    value = float(w2v_concept.similarity(
                                        object_label, state_label))
                                except KeyError:
                                    pass
                                else:
                                    average_similarity += value
                                    count += 1
                            # endif
                        # endfor
                        if count == 0 or average_similarity == 0:
                            continue
                        average_similarity = average_similarity / count * 1.0
                        if average_similarity < threshold:
                            continue
                    # endif

                    # -- We then make sure that this object does not exist in FOON (specifically, the list of nodes) already:
                    index = -1
                    for M in nodes_EXP:
                        if not isinstance(M, FOON.Object):
                            continue
                        if copiedObject.equals_lvl3(M):
                            index = nodes_EXP.index(M)
                            break
                    # endfor
                    if index == -1:
                        index = len(nodes_EXP)
                        nodes_EXP.append(copiedObject)

                        # NOTE: verbose stuff...
                        if verbose:
                            print("added:")
                            nodes_EXP[index].printObject_lvl3()
                            print('to replace:')
                            N.printObject_lvl3()
                            print('-----------')
                        # endif
                    # endif

                    # -- now that we have made sure the node is there, we then get the appropriate mapping to the copied object as object X:
                    copiedObject = nodes_EXP[index]

                    # NEXT, we look at all instances of functional units present in FOON, which we will then be using to copy objects over.
                    # -- we will look for instances of object Y and then replace with instances of object X.
                    for FU in FOON_objectsToUnits_lvl3[N]:

                        # -- blank functional unit object to keep track of copy
                        newFU = FOON.FunctionalUnit()
                        changed = 0				# -- this variable will tell us if there were any changes made to the functional unit!

                        # -- we begin by looking at the reference unit's OUTPUT objects...
                        for M in FU.getOutputNodes():

                            # NOTE: There are certain cases we need to consider:

                            # 1) We found the EXACT instance of Y that we want to replace with the copied object of type X.
                            if N.equals_lvl3(M):
                                # -- no need to check or adjust the object; we just directly add the newly copied object to the functional unit:
                                newFU.addObjectNode(copiedObject, is_input=False, objectInMotion=FU.getOutputDescriptor()[
                                                    FU.getOutputList().index(M)])
                                changed += 1

                            # 2) We find an object instance of Y but in a different state than what we are looking for in X.
                            # 	However, in this case, we are still changing this object from Y to X.
                            elif N.getObjectType() == M.getObjectType() or N.getObjectLabel() == M.getObjectLabel():
                                # -- get the right ID, states and label for the new object X and assign it to the adjusted object:
                                adjustedObject = FOON.Object(objectID=copiedObject.getObjectType(
                                ), objectLabel=copiedObject.getObjectLabel())
                                adjustedObject.setStatesList(M.getStatesList())

                                # -- add the object as we normally would to the functional unit:
                                newFU.addObjectNode(adjustedObject, is_input=False, objectInMotion=FU.getOutputDescriptor()[
                                                    FU.getOutputList().index(M)])
                                changed += 1

                            # 3) We found an object that is not of type Y but that may or may not refer to Y such that we need to adjust it to refer to X.
                            # -- e.g. : when dealing with containers that hold ingredient Y, it should now hold ingredient X:
                            else:

                                # -- keep note of the original (remember, non-Y) object's type and label:
                                adjustedObject = FOON.Object(
                                    objectID=M.getObjectType(), objectLabel=M.getObjectLabel())

                                # -- set the states of the object to the exact states seen for the original object:
                                adjustedObject.setStatesList(M.getStatesList())

                                found_ref_to_Y = False

                                # 3a) Check the ingredients that are contained in an object for any references to the original object Y:
                                temp_ingredients = []
                                for y in M.getIngredients():
                                    if y == Y:
                                        temp_ingredients.append(X)
                                        found_ref_to_Y = True  # -- swap old references of Y with X
                                    else:
                                        temp_ingredients.append(y)
                                # endfor

                                if found_ref_to_Y:
                                    # -- this means that we did find Y in this object:

                                    # -- set the new object's ingredients to the adjusted list:
                                    adjustedObject.setIngredients(
                                        temp_ingredients)

                                # 3b) Maybe the object does not CONTAIN Y, but it may have some relationship to Y, which is reflected by related object value:
                                for x in range(len(adjustedObject.getStatesList())):
                                    if Y == adjustedObject.getRelatedObject(x):
                                        # -- set the related object X to where Y used to be:
                                        adjustedObject.setRelatedObject(
                                            x, relatedObj=X)
                                        found_ref_to_Y = True
                                    # endif
                                # endfor

                                if found_ref_to_Y:
                                    # -- check to see if this object exists already in FOON:
                                    index = -1
                                    for J in nodes_EXP:
                                        if isinstance(J, FOON.Object) and adjustedObject.equals_lvl3(J):
                                            index = nodes_EXP.index(J)
                                            break
                                        # endif
                                    # endfor

                                    if index == -1:
                                        index = len(nodes_EXP)
                                        nodes_EXP.append(adjustedObject)
                                    # endif

                                    # -- add the (potentially new) object to the functional unit as normal:
                                    newFU.addObjectNode(nodes_EXP[index], is_input=False, objectInMotion=FU.getOutputDescriptor()[
                                                        FU.getOutputList().index(M)])
                                    changed += 1

                                else:
                                    # 3) c. This object has not one reference of Y, so we just add this as normal.
                                    newFU.addObjectNode(M, is_input=False, objectInMotion=FU.getOutputDescriptor()[
                                                        FU.getOutputList().index(M)])
                                # endif
                            # endif
                        # endfor

                        # -- we just need to create a new Motion node of the exact same type and add it to this functional unit:
                        tempMotion = FOON.Motion(motionID=FU.getMotion(
                        ).getMotionType(), motionLabel=FU.getMotion().getMotionLabel())
                        newFU.setTimes(FU.getStartTime(), FU.getEndTime())
                        newFU.setSuccessRate(FU.getSuccessRate())
                        newFU.setIndication(FU.getIndication())
                        newFU.setMotion(tempMotion)

                        # -- finally, we take a look at the reference unit's INPUT objects...
                        for M in FU.getInputNodes():

                            # NOTE: There are certain cases we need to consider:

                            # 1) We found the EXACT instance of Y that we want to replace with the copied object of type X.
                            if N.equals_lvl3(M):
                                # -- no need to check or adjust the object; we just directly add the newly copied object to the functional unit:
                                newFU.addObjectNode(copiedObject, is_input=True, objectInMotion=FU.getInputDescriptor()[
                                                    FU.getInputList().index(M)])
                                changed += 1

                            # 2) We find an object instance of Y but in a different state than what we are looking for in X.
                            # 	However, in this case, we are still changing this object from Y to X.
                            elif N.getObjectType() == M.getObjectType() or N.getObjectLabel() == M.getObjectLabel():
                                # -- get the right ID, states and label for the new object X and assign it to the adjusted object:
                                adjustedObject = FOON.Object(objectID=copiedObject.getObjectType(
                                ), objectLabel=copiedObject.getObjectLabel())
                                adjustedObject.setStatesList(M.getStatesList())

                                # -- add the object as we normally would to the functional unit:
                                newFU.addObjectNode(adjustedObject, is_input=True, objectInMotion=FU.getInputDescriptor()[
                                                    FU.getInputList().index(M)])
                                changed += 1

                            # 3) We found an object that is not of type Y but that may or may not refer to Y such that we need to adjust it to refer to X.
                            # -- e.g. : when dealing with containers that hold ingredient Y, it should now hold ingredient X:
                            else:

                                # -- keep note of the original (remember, non-Y) object's type and label:
                                adjustedObject = FOON.Object(
                                    objectID=M.getObjectType(), objectLabel=M.getObjectLabel())

                                # -- set the states of the object to the exact states seen for the original object:
                                adjustedObject.setStatesList(M.getStatesList())

                                found_ref_to_Y = False

                                # 3a) Check the ingredients that are contained in an object for any references to the original object Y:
                                temp_ingredients = []
                                for y in M.getIngredients():
                                    if y == Y:
                                        temp_ingredients.append(X)
                                        found_ref_to_Y = True  # -- swap old references of Y with X
                                    else:
                                        temp_ingredients.append(y)
                                # endfor

                                if found_ref_to_Y:
                                    # -- this means that we did find Y in this object:

                                    # -- set the new object's ingredients to the adjusted list:
                                    adjustedObject.setIngredients(
                                        temp_ingredients)

                                # 3b) Maybe the object does not CONTAIN Y, but it may have some relationship to Y, which is reflected by related object value:
                                for x in range(len(adjustedObject.getStatesList())):
                                    if Y == adjustedObject.getRelatedObject(x):
                                        # -- set the related object X to where Y used to be:
                                        adjustedObject.setRelatedObject(
                                            x, relatedObj=X)
                                        found_ref_to_Y = True
                                    # endif
                                # endfor

                                if found_ref_to_Y:
                                    # -- check to see if this object exists already in FOON:
                                    index = -1
                                    for J in nodes_EXP:
                                        if isinstance(J, FOON.Object) and adjustedObject.equals_lvl3(J):
                                            index = nodes_EXP.index(J)
                                            break
                                        # endif
                                    # endfor

                                    if index == -1:
                                        index = len(nodes_EXP)
                                        nodes_EXP.append(adjustedObject)
                                    # endif

                                    # -- add the (potentially new) object to the functional unit as normal:
                                    newFU.addObjectNode(nodes_EXP[index], is_input=True, objectInMotion=FU.getInputDescriptor()[
                                                        FU.getInputList().index(M)])
                                    changed += 1

                                else:
                                    # 3) c. This object has not one reference of Y, so we just add this as normal.
                                    newFU.addObjectNode(M, is_input=True, objectInMotion=FU.getInputDescriptor()[
                                                        FU.getInputList().index(M)])
                                # endif
                            # endif
                        # endfor

                        # FINALLY, we have completed copying the functional unit.
                        # -- what we do now is add it to the list of candidate units that we want to add to FOON-EXP:
                        if changed > 0:
                            nodes_EXP.append(tempMotion)
                            FOON_EXP.add(newFU)
                            if verbose:
                                print('Original:')
                                FU.printFunctionalUnit_lvl3()
                                print('Expanded:')
                                newFU.printFunctionalUnit_lvl3()
                                input()

    print(' -- Total number of expanded functional units: ' + str(len(FOON_EXP)))

    # The last step is important: to make sure we have unique functional units before writing:
    FOON_final = []
    for U in tqdm.tqdm(list(FOON_EXP)):
        found = False
        for F in FOON_final:
            if F.equals_lvl3(U):
                found = True
                break
        if not found:
            FOON_final.append(U)

    global file_name
    _file = open(os.path.splitext(file_name)[
                 0] + "_EXP-" + str(threshold) + ".txt", 'w')
    for FU in tqdm.tqdm(FOON_final):
        _file.write(FU.getFunctionalUnitText())

    print(' -- FOON-EXP written to ' + (os.path.splitext(file_name)
          [0] + "_EXP-" + str(threshold) + ".txt") + "' (using threshold=" + str(threshold) + ")!")
    _file.close()

    # TODO: test this updated function...

    global flag_EXP_complete
    flag_EXP_complete = True

    return (os.path.splitext(file_name)[0] + "_EXP-" + str(threshold) + ".txt")
# enddef


def _expandNetwork_TextBased(obj_sim, method="WordNet", threshold=0.89, state_suggestion=False, custom_list=None):
    # -- use Concept-Net to suggest states that may make sense for objects:
    global path_to_ConceptNet

    # -- check if a path has already been specified during run-time:
    if not path_to_ConceptNet:
        path_to_ConceptNet = input(
            '  -- Enter the full path and name to the Concept-Net model file: > ')

    w2v_concept = None

    if state_suggestion:
        try:
            from gensim.models import KeyedVectors
        except ImportError:
            print(' -- WARNING: Missing gensim library!')
            print("\t-- Please install gensim using 'pip install gensim' in terminal.")
            return
        # endtry

        # -- load word embeddings for Concept-Net:
        w2v_concept = KeyedVectors.load_word2vec_format(
            path_to_ConceptNet, binary=True)

    global object_similarity_index

    if not obj_sim:
        import json
        try:
            # -- keep both similarity index files separate in case:
            if method == "WordNet":
                _file = open('object_similarity_index-WN.json', 'r')
            else:
                _file = open('object_similarity_index-CN.json', 'r')
            object_similarity_index = json.load(_file)
            _file.close()  # -- Don't forget to close the file once we are done!

        except FileNotFoundError:
            print(" -- WARNING: No object similarity dictionary found in memory!")
            print("\t-- Building object similarity index..")
            object_similarity_index = _buildObjectSimilarity(method)
            # -- keep both similarity index files separate in case:
            if method == "WordNet":
                json.dump(object_similarity_index, open(
                    'object_similarity_index-WN.json', 'w'))
            else:
                json.dump(object_similarity_index, open(
                    'object_similarity_index-CN.json', 'w'))

    # NOTE: We use a set structure to preserve uniqueness among strings of functional units:
    new_units = set()

    for FU in FOON_lvl3:
        # -- this way, we note what we already have in our present, regular universal FOON:
        new_units.add(FU.getFunctionalUnitText())

    for key in tqdm.tqdm(object_similarity_index):
        # NOTE: Key(OBJ_1,OBJ_2) -> VALUE(int(similarity))
        # -- keys have whitespace replaced by underscore, so we have to replace that character where spotted:
        X = key.split(',')[0].replace("_", " ")
        Y = key.split(',')[1].replace("_", " ")

        # -- no sense in expanding for units with the same exact label:
        if X == Y:
            continue

        # -- in the case of a condensed object list, we need to check if we are meeting that criteria:
        if custom_list:
            found = False
            for L in custom_list:
                # -- 'custom_list' is a list of lists.. where each list within it contains the name of each label:
                if X in L:
                    found = True
                    break
            if found == False:
                continue

        if object_similarity_index[key] >= threshold:
            for N in nodes_lvl3:
                if isinstance(N, FOON.Motion):
                    # NOTE: nodes_lvl3 contains both Object and Motion nodes.
                    continue

                for FU in FOON_objectsToUnits_lvl3[N]:
                    # -- we go through all functional units associated with a given object N:

                    # NOTE: this is how this section will work:
                    #	1. Iterate through every input object's text and make changes (while possibly noting relativity of states to objects)
                    #	2. Interage through every output object's text with same criteria
                    #	3. Append text for inputs, outputs, and motion to one single string called 'copied_unit'.
                    #	4. Append single string from previous step to a set of strings referring to expanded units.
                    copied_unit = ""
                    skip = False
                    found = False

                    for x in range(FU.getNumberOfInputs()):
                        # -- first, we start with INPUT nodes:
                        object_text = str(FU.getInputNodeText(x))
                        if Y in object_text:  # NOTE: checking if substring exists in string
                            # -- this means we found something similar to X that we will be substituting via text:
                            items = object_text.split('\n')

                            # NOTE: the following variables will ONLY be used to compute "associativity" of states to the new object:
                            average_similarity = 0
                            count_similarity = 0
                            substitution = False
                            found = True

                            for y in range(len(items)):
                                line = items[y]
                                # NOTE: there are two main parts to check when substituting objects:
                                #	1. Check the object label and ID
                                #	2. Check the ingredients for each object
                                if line.startswith("O"):
                                    # -- parsing through the object labels:
                                    objectParts = line.split("O")
                                    objectParts = objectParts[1].split("\t")
                                    if objectParts[1] == Y:
                                        # -- if we found instance of Y, we substitute with X:
                                        objectParts[0] = "O" + \
                                            str(FOON_objectLabels[X])
                                        objectParts[1] = X
                                        substitution = True
                                    else:
                                        objectParts[0] = "O" + \
                                            str(objectParts[0])
                                    # -- combining new line together:
                                    items[y] = '\t'.join(objectParts)
                                elif line.startswith("S"):
                                    # -- parsing through the state labels, specifically for INGREDIENTS:
                                    stateParts = line.split("\t")
                                    stateParts = list(filter(None, stateParts))
                                    if substitution and state_suggestion:
                                        # -- this means that we have swapped an object for another, so we should not care about ingredients:
                                        state_text = str(
                                            stateParts[1]).split(' ')
                                        if len(state_text) < 2:
                                            # -- we only care about single-worded state labels which are more likely to be found in Concept-Net:
                                            try:
                                                value = float(w2v_concept.similarity(
                                                    str(X.replace(' ', '_')), state_text[0]))
                                            except KeyError:
                                                pass
                                            else:
                                                average_similarity += value
                                                count_similarity += 1
                                    else:
                                        if len(stateParts) > 2:
                                            revised = '{'
                                            ingredients = [stateParts[2]]
                                            ingredients = ingredients[0].split(
                                                "{")
                                            ingredients = ingredients[1].split(
                                                "}")
                                            # -- we then need to make sure that there are ingredients to be read:
                                            if len(ingredients) > 0:
                                                ingredients = ingredients[0].split(
                                                    ",")
                                                for i in range(len(ingredients)):
                                                    if ingredients[i] == Y:
                                                        ingredients[i] = X
                                                    revised += ingredients[i]
                                                    if i < (len(ingredients) - 1):
                                                        revised += ','
                                            revised += '}'
                                            stateParts[2] = revised
                                    items[y] = '\t'.join(stateParts)

                            if state_suggestion:
                                if count_similarity == 0:
                                    copied_unit += '\n'.join(items)
                                else:
                                    average_similarity = average_similarity / count_similarity * 1.0
                                    if average_similarity >= 0.3:
                                        # -- this means that the states are somewhat related to the new object:
                                        copied_unit += '\n'.join(items)
                                    else:
                                        # -- if it is below threshold, then we cannot safely substitute the object, so we should skip adding this functional unit:
                                        skip = True
                            else:
                                copied_unit += '\n'.join(items)
                            # endfor
                        else:
                            # -- this means the object has no trace of the substituted object:
                            copied_unit += object_text
                        # endif
                    # endfor

                    if skip or copied_unit == '':
                        continue

                    copied_unit += FU.getMotionForFile()

                    for x in range(FU.getNumberOfOutputs()):
                        # -- next, we look at OUPUT nodes:
                        object_text = str(FU.getOutputNodeText(x))
                        if Y in object_text:  # NOTE: checking if substring exists in string
                            # -- this means we found something similar to X that we will be substituting via text:
                            items = object_text.split('\n')

                            # NOTE: the following variables will ONLY be used to compute "associativity" of states to the new object:
                            average_similarity = 0
                            count_similarity = 0
                            substitution = False

                            for y in range(len(items)):
                                line = items[y]
                                # NOTE: there are two main parts to check when substituting objects:
                                #	1. Check the object label and ID
                                #	2. Check the ingredients for each object
                                if line.startswith("O"):
                                    # -- parsing through the object labels:
                                    objectParts = line.split("O")
                                    objectParts = objectParts[1].split("\t")
                                    if objectParts[1] == Y:
                                        # -- if we found instance of Y, we substitute with X:
                                        objectParts[0] = "O" + \
                                            str(FOON_objectLabels[X])
                                        objectParts[1] = X
                                        substitution = True
                                    else:
                                        objectParts[0] = "O" + \
                                            str(objectParts[0])
                                    # -- combining new line together:
                                    items[y] = '\t'.join(objectParts)
                                elif line.startswith("S"):
                                    # -- parsing through the state labels, specifically for INGREDIENTS:
                                    stateParts = line.split("\t")
                                    stateParts = list(filter(None, stateParts))
                                    if substitution and state_suggestion:
                                        # -- this means that we have swapped an object for another, so we should not care about ingredients:
                                        state_text = str(
                                            stateParts[1]).split(' ')
                                        if len(state_text) < 2:
                                            # -- we only care about single-worded state labels which are more likely to be found in Concept-Net:
                                            try:
                                                value = float(w2v_concept.similarity(
                                                    str(X.replace(' ', '_')), state_text[0]))
                                            except KeyError:
                                                pass
                                            else:
                                                average_similarity += value
                                                count_similarity += 1
                                    else:
                                        if len(stateParts) > 2:
                                            revised = '{'
                                            ingredients = [stateParts[2]]
                                            ingredients = ingredients[0].split(
                                                "{")
                                            ingredients = ingredients[1].split(
                                                "}")
                                            # -- we then need to make sure that there are ingredients to be read:
                                            if len(ingredients) > 0:
                                                ingredients = ingredients[0].split(
                                                    ",")
                                                for i in range(len(ingredients)):
                                                    if ingredients[i] == Y:
                                                        ingredients[i] = X
                                                    revised += ingredients[i]
                                                    if i < (len(ingredients) - 1):
                                                        revised += ','
                                            revised += '}'
                                            stateParts[2] = revised
                                    items[y] = '\t'.join(stateParts)
                            if state_suggestion:
                                if count_similarity == 0:
                                    copied_unit += '\n'.join(items)
                                else:
                                    average_similarity = average_similarity / count_similarity * 1.0
                                    if average_similarity >= 0.3:
                                        # -- this means that the states are somewhat related to the new object:
                                        copied_unit += '\n'.join(items)
                                    else:
                                        # -- if it is below threshold, then we cannot safely substitute the object, so we should skip adding this functional unit:
                                        skip = True
                            else:
                                copied_unit += '\n'.join(items)
                            # endfor
                        else:
                            # -- this means the object has no trace of the substituted object:
                            copied_unit += object_text
                        # endif
                    # endfor

                    if skip or copied_unit.rstrip() == "":
                        continue

                    if found and verbose:
                        print(FU.getFunctionalUnitText())
                        print(Y + ' --> ' + X)
                        print(copied_unit)

                    new_units.add(copied_unit)
                # endfor
            # endif
        # endfor

    trailing = ''
    if custom_list:
        trailing = 'refined'

    # -- having expanded everything as text, we can then proceed to writing all units to a new file:
    global file_name
    expanded_file_name = os.path.splitext(
        file_name)[0] + "_EXP-" + str(threshold) + "-" + method + "-" + trailing + ".txt"
    _file = open(expanded_file_name, 'w')
    for FU in tqdm.tqdm(new_units):
        _file.write(FU)
        _file.write('//\n')
    # endfor
    _file.close()

    print(' -- FOON-EXP written to ' + (expanded_file_name) +
          "' (using threshold=" + str(threshold) + ")!")

    global flag_EXP_complete
    flag_EXP_complete = True

    return expanded_file_name
# \end{FOON-EXP}

# \begin{FOON-GEN}


def _populateCategoryList():
    try:
        import json
        global category_index, object_categories
        category_index = json.load(open('FOON_categories.json', 'r'))
        object_categories = category_index['object_categories']

    except FileNotFoundError:
        print(" -- WARNING: File 'object_categories.txt' not found in current directory!")
        exit(2)
# enddef


def _constructFOON_GEN():
    global FOON_GEN, file_name, verbose

    if not object_categories:
        print(" -- [FOON-GEN] : Loading predefined object categories index..")
        _populateCategoryList()

    print(" -- [FOON-GEN] : Creating abstracted FOON (FOON-GEN)..")

    for FU in FOON_lvl3:
        tempFU = FOON.FunctionalUnit()
        tempObject = None

        # -- go through all input/output object nodes and make them generalized concepts.
        for T in FU.getInputList():
            count = 0
            flag = False
            for key in object_categories:
                for S in object_categories[key]:
                    if S == T.getObjectLabel():
                        # -- get the object and create new Category object based on it:
                        tempObject = FOON.Object(
                            objectID=key.getID(), objectLabel=key.getLabel())
                        tempObject.setStatesList(T.getStatesList())
                        flag = True
                        found = False
                        for U in tempFU.getInputList():
                            if U.equals_functions[1](tempObject):
                                found = True
                        # endfor
                        if found is False:
                            index = -1
                            for N in nodes_GEN:
                                if isinstance(N, FOON.Object):
                                    if N.equals_functions[1](tempObject):
                                        index = nodes_GEN.index(N)
                            # endfor
                            if index == -1:
                                tempFU.addObjectNode(
                                    tempObject, 1, FU.getOutputDescriptor()[count])
                                nodes_GEN.append(tempObject)
                            else:
                                tempFU.addObjectNode(
                                    nodes_GEN[index], 1, FU.getOutputDescriptor()[count])
                        # endif
                # endfor
            # endfor
            if flag is False:
                index = -1
                for N in nodes_GEN:
                    if isinstance(N, FOON.Object):
                        if N.equals_functions[1](tempObject):
                            index = nodes_GEN.index(N)
                # endfor
                if index == -1:
                    tempFU.addObjectNode(
                        tempObject, 1, FU.getOutputDescriptor()[count])
                    nodes_GEN.append(tempObject)
                else:
                    tempFU.addObjectNode(
                        nodes_GEN[index], 1, FU.getOutputDescriptor()[count])
            # endif
            count += 1
        # endfor

        # -- motions are simply copied over:
        tempFU.setMotion(FU.getMotion())
        tempFU.setTimes(FU.getStartTime(), FU.getEndTime())

        for T in FU.getOutputList():
            count = 0
            flag = False
            for key in object_categories:
                for S in object_categories[key]:
                    if S == T.getObjectLabel():
                        # -- get the object and create new Category object based on it:
                        tempObject = FOON.Object(
                            objectID=key.getID(), objectLabel=key.getLabel())
                        tempObject.setStatesList(T.getStatesList())
                        flag = True
                        found = False
                        for U in tempFU.getOutputList():
                            if U.equals_functions[1](tempObject):
                                found = True
                                break
                        # endfor
                        if found is False:
                            index = -1
                            for N in nodes_GEN:
                                if isinstance(N, FOON.Object):
                                    if N.equals_functions[1](tempObject):
                                        index = nodes_GEN.index(N)
                            # endfor
                            if index == -1:
                                tempFU.addObjectNode(
                                    tempObject, 2, FU.getOutputDescriptor()[count])
                                nodes_GEN.append(tempObject)
                            else:
                                tempFU.addObjectNode(
                                    nodes_GEN[index], 2, FU.getOutputDescriptor()[count])
                        # endif
                    # endif
                # endfor
            # endfor
            if flag is False:
                index = -1
                for N in nodes_GEN:
                    if isinstance(N, FOON.Object):
                        if N.equals_functions[1](tempObject):
                            index = nodes_GEN.index(N)
                # endfor
                if index == -1:
                    tempFU.addObjectNode(
                        tempObject, 2, FU.getOutputDescriptor()[count])
                    nodes_GEN.append(tempObject)
                else:
                    tempFU.addObjectNode(
                        nodes_GEN[index], 2, FU.getOutputDescriptor()[count])
            # endif
            count += 1
        # endfor

        if verbose:
            tempFU.printFunctionalUnit_lvl3()
            input("testing...")

        # -- now that we have created generalized object nodes in a new functional unit,
        #		we check to see if we have the generalized unit already in FOON-GEN:
        found = False
        for U in FOON_GEN:
            if U.equals_lvl3(tempFU):
                found = True
                break
        # endfor
        if found is False:
            FOON_GEN.append(tempFU)
    # endfor

    _file = open(os.path.splitext(file_name)[0] + "_GEN.txt", 'w')
    _unit = ''

    for FU in FOON_GEN:
        _unit += FU.getInputsForFile() + FU.getMotionForFile() + \
            FU.getOutputsForFile() + "//\n"
    # endfor

    _file.write(_unit)
    _file.close()

    global flag_GEN_complete
    flag_GEN_complete = True

    print("  -- Number of functional units in FOON-GEN: " + str(len(FOON_GEN)))
    print("  -- Number of nodes in FOON-GEN: " + str(len(nodes_GEN)))
    _buildOutputsToUnitMap_GEN()
# enddef


def _buildOutputsToUnitMap_GEN():
    global FOON_GEN, FOON_object_map_GEN
    for _input in nodes_GEN:
        if isinstance(_input, FOON.Object):
            procedures = []
            for _U in FOON_GEN:
                for _output in _U.getOutputList():
                    if _input.equals_functions[2](_output):
                        procedures.append(_U)
                        break
                    # endif
                # endfor
            # endfor
            FOON_object_map_GEN[_input] = procedures
        # endif
    # endfor
# enddef


def _taskTreeRetrieval_GEN(goalType, goalState, environment=None):
    # NOTE: task tree retrieval for FOON-GEN.
    global FOON_object_map_GEN

    hierarchy_level = 3
    search_level = 2
    searchMap = FOON_object_map_GEN

    # -- sometimes we may define a specific kitchen; other times, we just stick to default:
    if not environment:
        file = input(
            '  -- Please provide the name of the file with KITCHEN ITEMS (press ENTER ONLY to use default): > ')
        if file == '':
            # -- use default 'FOON-input_only_nodes.txt'
            environment = _identifyKitchenItems()
        else:
            # -- use a different text file with kitchen items
            environment = _identifyKitchenItems(file)
        # endif
    # endif

    goalNode = FOON.Object(objectID=int(goalType), objectLabel=list(
        FOON_objectLabels.keys())[list(FOON_objectLabels.values()).index(int(goalType))])
    for x in goalState:
        goalNode.addNewState([int(x), list(FOON_stateLabels.keys())[
                             list(FOON_stateLabels.values()).index(int(x))], []])
    # endif

    # -- we will identify different target goal nodes that have the same object/state combination:
    goals = []

    # FIRST, we need to identify possible GENERALIZED goal nodes:
    index = -1
    for G in nodes_GEN:
        if isinstance(G, FOON.Object):
            if G.equals_functions[search_level-1](goalNode):
                # -- if we find the exact goal in FOON-GEN's nodes, then we use that as the target:
                index = nodes_GEN.index(G)
                goals.append(nodes_GEN[index])
            else:
                # -- else, this means that we need to find other goal nodes that are generalized and possibly in FOON-GEN:
                for key in object_categories:
                    for S in object_categories[key]:
                        if S == goalNode.getObjectLabel():
                            tempGoal = FOON.Object(
                                objectID=key.getID(), objectLabel=key.getLabel())
                            tempGoal.setStatesList(goalNode.getStatesList())
                            for N in nodes_GEN:
                                if N.equals_functions[hierarchy_level-1](tempGoal):
                                    index = nodes_GEN.index(N)
                                    goals.append(nodes_GEN[index])
                                # endif
                            # endfor
                        # endif
                    # endfor
                # endfor
            # endif
    # endfor

    if index == -1:
        print("Goal " + goalNode.getObjectKey(hierarchy_level) +
              " has not been found in network!")
        return False
    # endif

    if verbose:
        print("\n-- Candidate goal nodes are as follows:")
        for G in goals:
            G.print_functions[hierarchy_level-1]()

    print("\n-- Proceeding with task tree retrieval for " +
          goalNode.getObjectKey(hierarchy_level) + "...")
    goalNode.print_functions[hierarchy_level-1]()
    print()

    kitchen = []
    for T in environment:
        index = -1
        for G in nodes_GEN:
            if isinstance(G, FOON.Object):
                if T.equals_functions[hierarchy_level-1](G):
                    index = nodes_GEN.index(G)
                    kitchen.append(nodes_GEN[index])
                else:
                    for key in object_categories:
                        for S in object_categories[key]:
                            if S == G.getObjectLabel():
                                kitchen_item = FOON.Object(
                                    objectID=key.getID(), objectLabel=key.getLabel())
                                kitchen_item.setStatesList(G.getStatesList())
                                if isinstance(G, FOON.Object) and G.equals_functions[hierarchy_level-1](kitchen_item):
                                    index = nodes_GEN.index(G)
                                    kitchen.append(nodes_GEN[index])
                            # endfor
                        # endfor
                    # endfor
                # endif
            # endif
        # endfor
    # endfor

    kitchen = list(set(kitchen))

    for G in goals:
        print(" -- Attempting to search for object:")
        G.print_functions[hierarchy_level-1]()
        print(" -----------------------------------")

        # What structures do we need in record keeping?
        #	-- a FIFO list of all nodes we need to search (a queue)
        #	-- a list that keeps track of what we have seen
        #	-- a list of all items we have/know how to make in the present case (i.e. the kitchen list)
        itemsToSearch = collections.deque()
        startNodes = []

        goalNode = G  # this is the actual goal node which is in the network.

        # -- Add the object we wish to search for to the two lists created above:
        itemsToSearch.append(goalNode)

        candidates = [] 	# -- structure to keep track of all candidate functional units in FOON
        taskTree = []		# -- tree with all functional units needed to create the goal based on the kitchen items
        # -- list of ALL possible functional units that can be used for making the item.
        all_path_trees = []

        if verbose:
            print("\n-- Generalized starting nodes are as follows:")
            for K in kitchen:
                K.printObject_lvl3()

        # -- maximum number of times you can "see" the original goal node.
        global depth
        max_iterations = 0
        # -- flag that is used to stop searching at goal node G that satisfies retrieval.
        endSearch = False

        while itemsToSearch:
            # -- Remove the item we are trying to make from the queue of items we need to learn how to make
            tempObject = itemsToSearch.popleft()

            if tempObject in startNodes:
                continue

            # -- sort-of a time-out for the search if it does not produce an answer dictated by the amount of time it takes.
            if tempObject.equals_functions[hierarchy_level-1](goalNode):
                max_iterations += 1

            # just the worst possible way of doing this, but will do for now.
            if max_iterations > depth:
                if verbose:
                    print("-- Objects that were found to be missing:")
                    for S in startNodes:
                        S.print_functions[hierarchy_level-1]()
                endSearch = True
                break

            if goalNode in kitchen:
                break

            if tempObject in kitchen:
                # -- just proceed to next iteration, as we already know how to make current item!
                continue

            num_candidates = 0
            candidates = list(searchMap.get(tempObject))
            num_candidates = len(candidates)
            all_path_trees += candidates

            if candidates == False:
                # -- this means that there is NO solution to making an object,
                #		and so we just need to add it as something we still need to learn how to make.
                startNodes.append(tempObject)
                continue

            numObjectsAdded = 0

            while candidates:
                # -- remove the first candidate functional unit we found
                candidate_FU = candidates.pop()
                count = 0	 				# -- variable to count the number of inputs that we have in the kitchen
                for T in candidate_FU.getInputList():
                    if verbose:
                        T.print_functions[hierarchy_level-1]()
                        input()

                    flag = False
                    if T in kitchen:
                        flag = True

                    if flag == False:
                        # -- if an item is not in the "objects found" list, then we add it to the list of items we then need to explore and find out how to make.
                        if T in itemsToSearch:
                            flag = True

                        if flag == False:
                            itemsToSearch.append(T)
                            numObjectsAdded += 1
                    else:
                        # -- since this is in the kitchen, we need to account for this:
                        count += 1

                num_candidates -= 1  # -- one less candidate to consider..

                if count == candidate_FU.getNumberOfInputs() and count != 0:
                    # We will have all items needed to make something;
                    #	add that item to the "kitchen", as we consider it already made.
                    found = False
                    if tempObject in kitchen:
                        found = True

                    if found == False:
                        kitchen.append(tempObject)

                    found = False
                    if candidate_FU in taskTree:
                        found = True

                    if not found:
                        taskTree.append(candidate_FU)

                    # -- since we found a suitable FU, we must remove all other traces of other candidates:
                    for x in range(numObjectsAdded):
                        # -- remove the last items that were added to queue that no longer should be considered!
                        itemsToSearch.pop()

                    for x in range(num_candidates):
                        # -- remove all functional units that can make an item - we take the first one that works!
                        candidates.pop()
                else:
                    # -- if a solution has not been found yet, add the object back to queue.
                    found = False
                    if tempObject in itemsToSearch:
                        found = True

                    if not found:
                        itemsToSearch.append(tempObject)

        if endSearch:
            print(" -- Task tree could not be found!")
            return False

        if not taskTree:
            print(" -- No functional units were found! Search has timed out!")

        else:
            # -- saving task tree sequence to file..
            file_name = "FOON-GEN_task-tree-for-" + \
                goalNode.getObjectKey(hierarchy_level) + \
                "_lvl" + str(hierarchy_level) + ".txt"
            _file = open(file_name, 'w')

            print("-- Length of task tree found:" + str(len(taskTree)))
            for FU in taskTree:
                # -- just write all functional units that were put into the list:
                _file.write(FU.getInputsForFile())
                _file.write(FU.getMotionForFile())
                _file.write(FU.getOutputsForFile())
                _file.write("//\n")
                if verbose:
                    FU.print_functions[hierarchy_level-1]()
                    print("//")
            # endfor
            _file.close()
            print(" -- Task tree sequence has been saved in file : " + file_name)
            return taskTree

    # -- after exhausting through all possible goal nodes, we just end the search:
    return False
# enddef
# \end{FOON-GEN}

# \begin{ICRA-18}


def _randomSearchExperiment(percent=0.60, n_trials=2, n_objects=5, hierarchy_level=3, expanded_ConceptNet=None, expanded_WordNet=None):
    print(' -- Starting random search experiment...')

    global verbose
    if verbose:
        print('  -- trials = ' + str(n_trials))
        print('  -- n_objects = ' + str(n_objects))
        print('  -- subset = ' + str(percent))

    # First, create random subsets of kitchen items available to the algorithm:
    file = input(
        '  -- Please provide the name of the file with KITCHEN ITEMS (press ENTER ONLY to use default): > ')
    if file == '':
        # -- use default 'FOON-input_only_nodes.txt'
        environment = _identifyKitchenItems()
    else:
        # -- use a different text file with kitchen items
        environment = _identifyKitchenItems(file)
    # endif

    if verbose:
        print(' -- Kitchen file provided with ' +
              str(len(environment)) + ' objects!')

    # -- making use of native Python random library to derive random subsets of objects (both GOALS and KITCHEN):
    import random

    # -- create kitchens for each trial of the searching experiment:
    trial_kitchens = []
    # -- size of the subset of ingredients for a particular trial
    subset_size = int(percent * len(environment))
    print('  -- Kitchen file provided with ' +
          str(len(environment)) + ' objects!')

    for K in range(n_trials):
        # -- use random.sample() to generate random subsets of a given length:
        # Source: https://www.geeksforgeeks.org/python-random-sample-function/
        #	This is being used to generate a random list of goal nodes for the experiment:
        if verbose:
            print(' -- Random sample ' + str(K) + '/' + str(n_trials))
        trial_kitchens.append(random.sample(environment, subset_size))

    # -- use random.sample() to generate random subsets of a given length:
    #	This is being used to generate a random list of goal nodes for the experiment:
    goal_nodes = random.sample(_findNodes_OutputOnly(), n_objects)

    # -- now that we have the random subset of goal nodes and the random kitchen subsets, we can proceed
    #	with the experiments:
    avg_fails_REG = 0
    avg_fails_EXP_W = 0
    avg_fails_EXP_C = 0
    avg_fails_GEN = 0  # -- counting the number of failures from search

    print('\n -- Performing random search on FOON-REG..')
    # -- when conducting the search, we need to note the amount of time taken on average..
    time_taken = 0
    for T in range(n_trials):
        n_fails_REG = 0
        for O in range(n_objects):
            # -- get one of the random goal nodes from the list..
            temp_goal = goal_nodes[O]
            start_time = time.time()
            # -- perform search and note whether it is success or not:
            if not _taskTreeRetrieval_greedy(temp_goal.getObjectType(), temp_goal.getStateIDs(), hierarchy_level, environment=trial_kitchens[T]):
                n_fails_REG += 1
            # endif
            end_time = time.time()
            # -- add the time taken to variable:
            time_taken += (end_time - start_time)
        # endfor
        print('  -- trial #' + str(T+1) + ' : ' +
              str(n_objects - n_fails_REG) + '/' + str(n_objects))
        avg_fails_REG += n_fails_REG
    # endfor

    avg_time_REG = time_taken / (n_trials * n_objects)

    # -- we will compare both WordNet and Concept-Net approaches:
    print('\n -- Performing random search on FOON-EXP (WordNet)..')
    global object_similarity_index

    if not expanded_WordNet:
        # NOTE: we can do this to avoid having to wait to provide input..
        expanded_WordNet = input(
            "  -- Please enter the name of the EXPANDED FOON you wish to use: (press ENTER when done)> ")
    if os.path.exists(expanded_WordNet) is False:
        # -- executing pre-expansion methods:
        params = _prepareExpansion()
        expanded_WordNet = _expandNetwork_TextBased(
            object_similarity_index, method=params[0], threshold=params[1], custom_list=params[2])
    # -- using the newly expanded file, we read it as regular FOON file:
    _constructFOON(file=expanded_WordNet)
    _buildInternalMaps()

    # -- when conducting the search, we need to note the amount of time taken on average..
    time_taken = 0
    for T in range(n_trials):
        n_fails_EXP = 0
        for O in range(n_objects):
            # -- get one of the random goal nodes from the list..
            temp_goal = goal_nodes[O]
            start_time = time.time()
            # -- perform search and note whether it is success or not:
            if not _taskTreeRetrieval_greedy(temp_goal.getObjectType(), temp_goal.getStateIDs(), hierarchy_level, environment=trial_kitchens[T]):
                n_fails_EXP += 1
            # endif
            end_time = time.time()
            # -- add the time taken to variable:
            time_taken += (end_time - start_time)
        # endfor
        print('  -- trial #' + str(T+1) + ' : ' +
              str(n_objects - n_fails_EXP) + '/' + str(n_objects))
        avg_fails_EXP_W += n_fails_EXP
    # endfor

    avg_time_EXP_W = time_taken / (n_trials * n_objects)

    _resetFOON(reload=True)

    print('\n -- Performing random search on FOON-EXP (Concept-Net)..')

    if not expanded_ConceptNet:
        # NOTE: we can do this to avoid having to wait to provide input..
        expanded_ConceptNet = input(
            "  -- Please enter the name of the EXPANDED FOON you wish to use: (press ENTER when done)> ")
    if os.path.exists(expanded_ConceptNet) is False:
        # -- executing pre-expansion methods:
        params = _prepareExpansion()
        expanded_ConceptNet = _expandNetwork_TextBased(
            object_similarity_index, method=params[0], threshold=params[1], custom_list=params[2])
    # -- using the newly expanded file, we read it as regular FOON file:
    _constructFOON(file=expanded_ConceptNet)
    _buildInternalMaps()

    # -- when conducting the search, we need to note the amount of time taken on average..
    time_taken = 0
    for T in range(n_trials):
        n_fails_EXP = 0
        for O in range(n_objects):
            # -- get one of the random goal nodes from the list..
            temp_goal = goal_nodes[O]
            start_time = time.time()
            # -- perform search and note whether it is success or not:
            if not _taskTreeRetrieval_greedy(temp_goal.getObjectType(), temp_goal.getStateIDs(), hierarchy_level, environment=trial_kitchens[T]):
                n_fails_EXP += 1
            # endif
            end_time = time.time()
            # -- add the time taken to variable:
            time_taken += (end_time - start_time)
        # endfor
        print('  -- trial #' + str(T+1) + ' : ' +
              str(n_objects - n_fails_EXP) + '/' + str(n_objects))
        avg_fails_EXP_C += n_fails_EXP
    # endfor

    avg_time_EXP_C = time_taken / (n_trials * n_objects)

    _resetFOON(reload=True)

    print('\n -- Performing random search on FOON-GEN..')
    # NOTE: first check if FOON-GEN and FOON-GEN have been constructed:
    if not FOON_GEN:
        _constructFOON_GEN()

    # -- when conducting the search, we need to note the amount of time taken on average..
    time_taken = 0
    for T in range(n_trials):
        n_fails_GEN = 0
        for O in range(n_objects):
            # -- get one of the random goal nodes from the list..
            temp_goal = goal_nodes[O]
            start_time = time.time()
            # -- perform search and note whether it is success or not:
            if not _taskTreeRetrieval_GEN(temp_goal.getObjectType(), temp_goal.getStateIDs(), environment=trial_kitchens[T]):
                n_fails_GEN += 1
            # endif
            end_time = time.time()
            # -- add the time taken to variable:
            time_taken += (end_time - start_time)
        # endfor
        print('  -- trial #' + str(T+1) + ' : ' +
              str(n_objects - n_fails_GEN) + '/' + str(n_objects))
        avg_fails_GEN += n_fails_GEN
    # endfor

    avg_time_GEN = time_taken / (n_trials * n_objects)

    print('\n -- Results of experiment are as follows:')

    print(' ---- AVERAGE PERFORMANCE (SUCCESSES) ----')
    print('\t\tFOON-REG\t\t- ' + str((n_objects * n_trials) - avg_fails_REG))
    print('\t\tFOON-EXP (WordNet)\t- ' +
          str((n_objects * n_trials) - avg_fails_EXP_C))
    print('\t\tFOON-EXP (Concept-Net)\t- ' +
          str((n_objects * n_trials) - avg_fails_EXP_C))
    print('\t\tFOON-GEN\t\t- ' + str((n_objects * n_trials) - avg_fails_GEN))

    print(' ---- AVERAGE PERFORMANCE (TIME) ----')
    print('\t\tFOON-REG\t\t- ' + str(avg_time_REG))
    print('\t\tFOON-EXP (WordNet)\t- ' + str(avg_time_EXP_W))
    print('\t\tFOON-EXP (Concept-Net)\t- ' + str(avg_time_EXP_C))
    print('\t\tFOON-GEN\t\t- ' + str(avg_time_GEN))

    input('  -- Press ENTER to continue...')
# enddef

# \end{ICRA-18}

###############################################################################################################################

# NOTE: universal FOON merging and creation functions:


def _createUniversalFOON():

    global verbose

    # -- first, give the directory for merging:
    directory = None
    if config.sections():
        # -- check if there is a config file that contains details for FOON functionality:
        directory = config['Paths']['data_source']
    else:
        directory = input(
            " -- [FOON_MERGE] : Please enter the DIRECTORY with files to be merged with the current FOON: > ")

    for root, _, files in os.walk(os.path.abspath(directory)):
        for file in files:
            _file = os.path.join(root, file)
            print("  -- merging: '" + str(_file) + "' ...")
            _constructFOON(_file)
            motions = [F for F in nodes_lvl3 if isinstance(F, FOON.Motion)]
            if verbose:
                print(str(len(nodes_lvl3) - len(motions)) +
                      "," + str(len(motions)))
    print(' -- [FOON_MERGE] : Total sum of nodes: ' +
          str(FOON_node_count) + '\n')
# enddef


def _writeFOONtoFile(file_name=None, post_merge=True, skip_PKL=False):
    global verbose, config, FOON_lvl3, FOON_video_source

    # -- check config file for possible name for universal FOON or use default name:
    if config.sections():
        merged_file = config['Paths']['universal_FOON']
    elif file_name:
        merged_file = file_name
    else:
        merged_file = 'universal_FOON.txt'

    _file = open(merged_file, 'w')

    print(" -- [FOON_MERGE] : Saving new universal FOON as file '" +
          (merged_file) + "' ...")
    if verbose:
        print("\n -- [FOON_MERGE] : Merged FOON functional units as follows:")

    if not post_merge:
        # -- write the source link for the FOON subgraph (if available):
        for _source in FOON_video_source:
            _file.write('# Source:\t' + str(_source) + '\n')

    # -- write timestamp of when file was created for record-keeping:
    from datetime import datetime

    _file.write('# Date created:\t' +
                str(datetime.today().strftime('%d.%m.%Y')) + '\n')
    _file.write('//\n')

    # -- append strings for each functional unit together and then write to a file after:
    _unit = str()
    for FU in FOON_lvl3:
        if verbose:
            FU.print_functions[2]()
            print("//")
        _unit += FU.getFunctionalUnitText()
    _file.write(_unit)
    _file.close()

    if not skip_PKL:
        _saveFOON_pkl(merged_file)

    # -- return the name of the merged file:
    return merged_file
# enddef

###############################################################################################################################


def _loadFOON_pkl(file=None):
    import pickle

    global FOON_functionalUnits, FOON_lvl1, FOON_lvl2, FOON_lvl3
    global FOON_nodes, nodes_lvl1, nodes_lvl2, nodes_lvl3

    print(
        ' -- [FOON_PKL] : Loading FOON (nodes and functional units) from .PKL file...')

    if not file:
        file = 'FOON.pkl'

    with open(file, 'rb') as F:
        FOON_nodes, FOON_functionalUnits = pickle.load(F)

    # -- load the main lists that are needed for FOON to operate:
    nodes_lvl1, nodes_lvl2, nodes_lvl3 = FOON_nodes[0], FOON_nodes[1], FOON_nodes[2]
    FOON_lvl1, FOON_lvl2, FOON_lvl3 = FOON_functionalUnits[
        0], FOON_functionalUnits[1], FOON_functionalUnits[2]

    print(' -- [FOON_PKL] : Completed loading of .PKL file!')
    print('\n -- Now running internal map building function...')

    # -- best to run the following function now to build all required dictionaries:
    _buildInternalMaps()

    return
# enddef


def _saveFOON_pkl(file=None):
    try:
        import pickle
    except ImportError:
        print(' -- ERROR: Missing pickle library!')
        return

    print(
        ' -- [FOON_PKL] : Saving FOON (nodes and functional units) as .PKL file...')

    global FOON_nodes, FOON_functionalUnits, file_name

    if not file:
        file = os.path.splitext(file_name)[0] + '.pkl'
    elif file.endswith('.txt'):
        file = os.path.splitext(file)[0] + '.pkl'
    # endif

    # -- we can probably pickle both functional unit lists and node lists separately:
    while True:
        try:
            with open(file, 'wb') as F:
                pickle.dump((FOON_nodes, FOON_functionalUnits), F)

            print(" -- [FOON_PKL] : Pickle file for FOON stored as 'FOON.pkl'!")
            break

        except RecursionError:
            # -- prompt user if they mind increasing recursion limit to save this universal FOON:
            response = input(' 	-- ERROR: Python requires higher recursion limit to save FOON objects (default - 1000; current - ' + str(sys.getrecursionlimit()) + ')!' +
                             '\n  -- Would you like to try raising the recursion limit? [Y/N] (Default: Y) > ')
            if response == 'N':
                print(
                    '  -- [FOON_PKL] : File not saved; terminating pickle file dumping process...')
                break

            # -- conservatively increase the recursion limit..
            sys.setrecursionlimit(sys.getrecursionlimit() + 200)
            print('\033[A\033[A')
        # end
    # endwhile

    return
# enddef


def _saveFOON_JS():
    # -- first, read the current file and then split it into separate lines:
    global file_name
    _file = open(file_name, 'r')
    items = _file.read().splitlines()

    # -- now, go ahead and open the new file containing the Javascript version of the FOON:
    _file = open(os.path.splitext(file_name)[0] + '.js', 'w')
    _file.write("var FOON_graph = [")

    for line in items:
        _file.write("\"" + line + "\"")
        _file.write(",\n")
    # endfor

    _file.write("];")
    _file.close()
# enddef


def _saveFOON_JSON():
    import json

    global FOON_video_source, FOON_lvl3, file_name

    # -- create a dictionary to store each functional unit:
    subgraph_units = {}
    subgraph_units['functional_units'] = []

    for FU in FOON_lvl3:
        # -- use function already defined in FOON_classes.py file:
        subgraph_units['functional_units'].append(FU.getFunctionalUnitJSON())
    if FOON_video_source:
        subgraph_units['source'] = FOON_video_source

    json_file_name = os.path.splitext(file_name)[0] + '.json'

    # -- dump all the contents in dictionary as .JSON:
    json.dump(subgraph_units, open(json_file_name, 'w'), indent=7)
# enddef

###############################################################################################################################

# NOTE: miscellaneous operations:


def _printSummary_FOON():
    print(" -> Level 3: # of UNITS - " + str(len(FOON_lvl3)))
    print(" -> Level 2: # of UNITS - " + str(len(FOON_lvl2)))
    print(" -> Level 1: # of UNITS - " + str(len(FOON_lvl1)))
# enddef


def _printSummary_nodes():
    print(" -> TOTAL NUMBER OF NODES : " + str(len(nodes_lvl3)))
    print(" -> Level 3: # of OBJECT - " + str(len(FOON_outputsToUnits_lvl3)) +
          "; # of MOTION - " + str(len(nodes_lvl3) - len(FOON_outputsToUnits_lvl3)))
    print(" -> Level 2: # of OBJECT - " + str(len(FOON_outputsToUnits_lvl2)) +
          "; # of MOTION - " + str(len(nodes_lvl2) - len(FOON_outputsToUnits_lvl2)))
    print(" -> Level 1: # of OBJECT - " + str(len(FOON_outputsToUnits_lvl1)) +
          "; # of MOTION - " + str(len(nodes_lvl1) - len(FOON_outputsToUnits_lvl1)))
# enddef


def _printSummary_edges():
    total = 0
    for N in nodes_lvl3:
        total += len(N.getNeighbourList())
    # endfor
    print(" -> Level 3: # of EDGES - " + str(total))
    total = 0
    for N in nodes_lvl2:
        total += len(N.getNeighbourList())
    # endfor
    print(" -> Level 2: # of EDGES - " + str(total))
    total = 0
    for N in nodes_lvl1:
        total += len(N.getNeighbourList())
    # endfor
    print(" -> Level 1: # of EDGES - " + str(total))
# endfor


def _printAnyNode(X, hierarchy_level=3):
    if hierarchy_level == 1:
        node = nodes_lvl1[X]
    elif hierarchy_level == 2:
        node = nodes_lvl2[X]
    else:
        node = nodes_lvl3[X]
    if isinstance(node, FOON.Motion):
        node.printMotion()
        return
    node.print_functions[hierarchy_level-1]()
# enddef


def _printAnyFunctionalUnit(X, hierarchy_level=3):
    if hierarchy_level == 1:
        node = FOON_lvl1[X]
    elif hierarchy_level == 2:
        node = FOON_lvl2[X]
    else:
        node = FOON_lvl3[X]
    node.print_functions[hierarchy_level-1]()
# enddef


def _objectFrequencyReport():
    try:
        _file = open('FOON-object_index.txt', 'r')
    except FileNotFoundError:
        print(" -- WARNING: File 'FOON-object_index.txt' not found in current directory!")
        return
    items = _file.read().splitlines()
    frequency = [0] * len(items)

    for FU in FOON_lvl3:
        for _O in FU.getInputList():
            # -- get the motion node for each functional unit and just tally them up:
            frequency[_O.getObjectType()] += 1
    # endfor

    global file_name
    _file = open(os.path.splitext(file_name)[
                 0] + '_FOON_object_frequency_report.txt', 'w')

    for x in range(len(items)):
        line = items[x].split("\t")
        # -- write everything to the file..
        _file.write("O" + str(line[0]) + " : " +
                    str(line[1]) + "\t" + str(frequency[x]) + "\n")
    # endfor

    print(" -- Object frequency file has been saved as '" +
          (os.path.splitext(file_name)[0] + '_FOON_object_frequency_report.txt') + "'.")
    _file.close()

    _file = open(os.path.splitext(file_name)[
                 0] + '_FOON_existent_FOON_objectLabels.txt', 'w')
    for x in range(len(items)):
        line = items[x].split("\t")
        # -- write everything to the file only if there is at least one instance!
        if frequency[x] > 0:
            _file.write("O" + str(line[0]) + " : " +
                        str(line[1]) + "\t" + str(frequency[x]) + "\n")
        # endif
    # endfor
    _file.close()

    _file = open(os.path.splitext(file_name)[
                 0] + '_FOON_existent_object-states.txt', 'w')
    for x in range(len(nodes_lvl3)):
        # -- write everything to the file only if there is at least one instance!
        if isinstance(nodes_lvl3[x], FOON.Object):
            _file.write(nodes_lvl3[x].getObjectText() + "\n" + "//" + "\n")
        # endif
    # endfor
    _file.close()
# enddef


def _motionFrequencyReport():
    try:
        _file = open('FOON-motion_index.txt', 'r')
    except FileNotFoundError:
        print(" -- WARNING: File 'FOON-motion_index.txt' not found in current directory!")
        return
    items = _file.read().splitlines()
    frequency = [0] * len(items)

    for FU in FOON_lvl3:
        # -- get the motion node for each functional unit and just tally them up:
        frequency[FU.getMotion().getMotionType()] += 1
    # endfor

    global file_name
    _file = open(os.path.splitext(file_name)[
                 0] + '_FOON_motion_frequency_report.txt', 'w')

    for x in range(len(items)):
        line = items[x].split("\t")
        # -- write everything to the file..
        _file.write("M" + str(line[0]) + " : " +
                    str(line[1]) + "\t" + str(frequency[x]) + "\n")
    # endfor

    print(" -- Motion frequency file has been saved as '" +
          (os.path.splitext(file_name)[0] + '_FOON_motion_frequency_report.txt') + "'.")
    _file.close()
# enddef


def _stateFrequencyReport():
    try:
        _file = open('FOON-state_index.txt', 'r')
    except FileNotFoundError:
        print(" -- WARNING: File 'FOON-state_index.txt' not found in current directory!")
        return

    items = _file.read().splitlines()
    frequency = [0] * len(items)

    for N in nodes_lvl3:
        if isinstance(N, FOON.Object):
            for x in range(len(N.getStatesList())):
                frequency[int(N.getStateType(x))] += 1
    # endfor

    global file_name
    _file = open(os.path.splitext(file_name)[
                 0] + '_FOON_state_frequency_report.txt', 'w')

    for x in range(len(items)):
        line = items[x].split("\t")
        # -- write everything to the file..
        _file.write("S" + str(line[0]) + " : " +
                    str(line[1]) + "\t" + str(frequency[x]) + "\n")
    # endfor

    print(" -- State frequency file has been saved as '" +
          (os.path.splitext(file_name)[0] + '_FOON_state_frequency_report.txt') + "'.")
    _file.close()
# enddef


def _startFOONview():
    try:
        import webbrowser
        webbrowser.open('http://foonets.com/FOON_view/visualizer.html')
    except ImportError:
        print(" -- WARNING: Missing 'webbrowser' module for this function!")
        return
    # endtry
# enddef


def _parseStateClustering(file):
    try:
        cluster_file = open(file, 'r')
    except:
        print(' -- ERROR: there is an issue opening the file!')
    else:
        state_clusters = {}

        lines = cluster_file.readlines()
        for L in lines:
            if L.startswith('#'):
                # -- this means that this line refers to a comment:
                continue

            line_parts = list(filter(None, L.split('\t')))

            # -- the first part will have the cluster name, while the second part has the state labels that fall under it:
            states = []
            for S in list(filter(None, line_parts[1].split(','))):
                states.append(S)

            state_clusters[line_parts[0].rstrip()] = states
        # endfor

        return state_clusters
    # end
    return
# enddef

###############################################################################################################################

# NOTE: functions used to translate functional units into sentences:
# -- sentences are <input_label_1> ... <input_label_k> <motion_label> <output_label_1> ... <output_label_k>


def _FOONtoSentence():
    # -- this function is to generate sentences which can be used for training Word2Vec:
    global file_name
    _file = open(os.path.splitext(file_name)[
                 0] + '_word2vec_sentences.txt', 'w')
    for _FU in FOON_lvl3:
        # -- all this simply does is makes sentences in the form of <input_object_1> ... <input_object_n> <motion> <output_object_1> ... <output_object_n>:
        _file.write(_FU.getWord2VecSentence() + '\n')
    _file.close()
# enddef


def _toLSTMSentences():
    # -- this function is to generate sentences which can be used for LSTM experiment:
    global file_name
    _file = open(os.path.splitext(file_name)[0] + '_to_sentences.txt', 'w')
    for _FU in FOON_lvl3:
        inputs = ''
        for I in _FU.getInputList():
            inputs += I.getObjectLabel()
            inputs += ','
        inputs = inputs[:-1]
        _file.write(inputs + '\t' + _FU.getMotion().getMotionLabel() + '\n')
    _file.close()
# enddef

###############################################################################################################################


def _printArgumentsUsage():
    print("ERROR: Unrecognized arguments given to program! Please use from one of the following:")
    print(" --help\t\t\t:- gives an overview of all the flags that work with the program")
    print(" --file='X.txt'\t\t:- open FOON file given as 'X.txt', where X can be any given name")
    print(" --verbose (or --v)\t:- this flag turns on verbose mode, which will result in MANY print-outs for debugging the program")
    print(" --object=X\t\t:- this flag indicates the object type (for searching algorithms)")
    print(" --state=[X, ...]\t:- this flag indicates the object's state(s) (for searching algorithms), provided as a list of numbers")
# enddef


def _displayMenu():
    global last_updated
    print("< fga: FOON Graph Analyzer - (last updated " + str(last_updated) + " ) >")
    global file_name
    print(("   --file:'" + str(file_name) + "'") if file_name else '')
    print("\tWhat would you like to do?")
    print("\t\t1.\tMerge multiple subgraphs into universal FOON;")
    print("\t\t2.\tSearch for task tree in FOON;")
    print("\t\t3.\tPrint FOON details (nodes, functional units, motions);")
    print("\t\t4.\tPerform generalization tasks (with FOON-EXP / FOON-GEN);")
    print("\t\t5.\tPerform network centrality on universal FOON;")
    print("\t\t6.\tUse FOON_parser (parsing script for FOON subgraphs);")
    print("\t\t7.\tUse FOON_view (FOON graph visualization tool);")
    print("\t\t8.\tSave universal FOON as other format (.PKL, .JS, .JSON, word2vec);")
    print()
    response = input(
        " -- Please enter your option or press ENTER to exit : [1-8] > ")
    return response
# enddef


def _start():
    # -- command-line argument parsing to put switches:
    try:
        opts, _ = getopt.getopt(sys.argv[1:], 'v:fi:ob:st', [
                                'verbose', 'file=', "object=", "state=", 'help'])
    except getopt.GetoptError:
        _printArgumentsUsage()
        exit(2)
    # end

    global verbose, file_name, flag_EXP_complete, flag_GEN_complete

    # -- you can either define the search object or state here or in the program's arguments:
    searchObject, searchState = None, None

    for opt, arg in opts:
        if opt in ('-v', '--verbose') or opt in ('-v', '--v'):
            print(" -- selected verbose option...")
            verbose = True
        elif opt in ('-fi', '--file'):
            print(" -- provided FOON graph as file '" + str(arg) + "'")
            file_name = arg
        elif opt in ('-ob', '--object'):
            print(" -- given object type '" + str(arg) + "'")
            searchObject = arg
        elif opt in ('-st', '--state'):
            print(" -- given object state '" + str(arg) + "'")
            searchState = ast.literal_eval(arg)
        else:
            _printArgumentsUsage()
            exit(2)
        # end
    # endfor

    # -- start by reading the text file:
    start_time = time.time()
    # -- NOTE: replace text file here with any subgraph/FOON graph file.
    _constructFOON(file_name)
    end_time = time.time()
    print('  -- Time taken: ' + (str(end_time - start_time)) + ' s..')

    # -- build internal dictionaries for faster references:
    start_time = time.time()
    _buildInternalMaps()
    end_time = time.time()

    print('  -- Time taken: ' + (str(end_time - start_time)) + ' s..')

    # -- load index files for objects, motions, and states:
    _readIndexFiles(FOON_objectLabels, FOON_motionLabels, FOON_stateLabels)

    while True:
        print()
        option = _displayMenu()
        print()

        # -- based on selection given, perform the following function calls:
        if option == "1":
            print("-- [FOON_MERGE] : Initiating universal FOON merging operation..")
            _createUniversalFOON()
            new_file_name = _writeFOONtoFile()
            if verbose:
                _printSummary_FOON()

            # -- now that we have merged the subgraphs together, we can either work with the new universal FOON or just the loaded file:
            _response = input(
                ' -- Load newly merged FOON or keep using subgraph? (Default: 1) [1/2] > ')
            if _response != '2':
                file_name = new_file_name
            _resetFOON(reload=True)
        # end

        elif option == "2":
            print("-- [TASK_TREE] : Initiating task tree retrieval..")
            if searchObject == None or searchState == None:
                _response = input(
                    ' -- Would you like to look up objects and states by name? [Y/N] > ')
                if _response.lower() == 'y':
                    searchObject, searchState = _prepareTaskTreeRetrieval(1)
                else:
                    searchObject, searchState = _prepareTaskTreeRetrieval(2)
                # end
            _response = input(
                " -- Would you like to perform: 1) greedy or 2) optimal retrieval? [1/2] > ")
            hierarchy_level = int(
                input(" -- At what level is the search being done? [1/2/3] > "))
            if _response == 2:
                _pathTreeRetrieval(searchObject, searchState, hierarchy_level)
            else:
                _taskTreeRetrieval_greedy(
                    searchObject, searchState, hierarchy_level)
        # end

        elif option == "3":
            print(" -- What kind of details do you want to see?")
            print("\t\t1.\tPrint FOON node summary (display total number of object and motion nodes at each level);")
            print(
                "\t\t2.\tPrint FOON functional unit summary (display total number of units at each level);")
            print(
                "\t\t3.\tGenerate label frequency reports (on objects, motions, and states);")

            _response = input(
                "  -- Please enter your option or press ENTER to exit : [1-3] > ")
            if _response == "1":
                print("\n -- Printing details about FOON nodes...")
                # -- call upon functions to print summary of number of nodes/edges:
                _printSummary_nodes()
                print()
                _printSummary_edges()
                print()

                _response = input(
                    " -- Do you want to print the list of nodes? [Y/N] (default:N) > ")
                if _response.lower() == "y":
                    _response = input(" -- At which level? [1/2/3] > ")
                    hierarchy_level = 1 if _response == "1" else 2 if _response == "2" else 3
                    for N in FOON_nodes[hierarchy_level-1]:
                        if isinstance(N, FOON.Object):
                            N.print_functions[hierarchy_level-1]()
                            print("----------------------")
                        # end
                    # end
                # end
            elif _response == "2":
                print("\n -- Printing details about FOON units...")
                # -- call upon functions to print summary of number of functional units in loaded universal FOON:
                _printSummary_FOON()
                print()

                _response = input(
                    " -- Do you want to print all functional units? [Y/N] (default:N) > ")
                if _response == "Y" or _response == "y":
                    _response = input(" -- At which level? [1/2/3] > ")
                    hierarchy_level = 1 if _response == "1" else 2 if _response == "2" else 3
                    for N in FOON_functionalUnits[hierarchy_level-1]:
                        N.print_functions[hierarchy_level-1]()
                    # end
                # end
            elif _response == "3":
                print("-- Initiating universal FOON report function..")
                _objectFrequencyReport()
                _motionFrequencyReport()
                _stateFrequencyReport()
            # end
        # end

        elif option == "4":
            print(" -- What kind of generalization tasks do ou want to do?")
            print("\t\t1.\tPerform network expansion (create a FOON-EXP);")
            print("\t\t2.\tPerform network abstraction (create a FOON-GEN);")
            print("\t\t3.\tPerform task tree retrieval using FOON-EXP;")
            print("\t\t4.\tPerform task tree retrieval using FOON-GEN;")
            _response = input(
                "  -- Please enter your option or press ENTER to exit : [1-4] > ")

            if _response == "1":
                if not flag_EXP_complete:
                    # -- prepare for expansion by prompting user for parameters to go by (viz. threshold value, method of expansion, etc.)
                    params = _prepareExpansion()

                    _response = input(
                        ' -- [FOON-EXP] : Expand using 1) regular method or 2) text-based method? [1/2] (default:2) > ')
                    if _response == "1":
                        expanded_file = _expandNetwork(
                            object_similarity_index,
                            method=params[0],
                            threshold=params[1],
                            custom_list=params[2],
                            state_suggestion=params[3]
                        )
                    else:
                        expanded_file = _expandNetwork_TextBased(
                            object_similarity_index,
                            method=params[0],
                            threshold=params[1],
                            custom_list=params[2],
                            state_suggestion=params[3]
                        )

                    # -- using the newly expanded file, we read it as regular FOON file:
                    _constructFOON(expanded_file)
                    _buildInternalMaps()

                    print('\n  -- after expansion with threshold=' +
                          str(params[1]) + ' using ' + str(params[0]) + ', we obtained:')
                    _printSummary_nodes()
                    print()
                    _printSummary_edges()
                    print()
                    input(' -- Press ENTER to continue...')
                # endif

            elif _response == "2":
                if not flag_GEN_complete:
                    _constructFOON_GEN()

            elif _response == "3":
                if searchObject == None or searchState == None:
                    _response = input(
                        ' -- Would you like to look up objects and states by name? [Y/N] > ')
                    if _response.lower() == 'y':
                        searchObject, searchState = _prepareTaskTreeRetrieval(
                            1)
                    else:
                        searchObject, searchState = _prepareTaskTreeRetrieval(
                            2)

                # -- check if expansion has already been done before:
                if not flag_EXP_complete:
                    # -- prepare for expansion by prompting user for parameters to go by (viz. threshold value, method of expansion, etc.)
                    params = _prepareExpansion()

                    _response = input(
                        ' -- Expand using 1) regular method or 2) text-based method? [1/2] (default:2) > ')
                    if _response == "1":
                        expanded_file = _expandNetwork(
                            object_similarity_index,
                            method=params[0],
                            threshold=params[1],
                            custom_list=params[2],
                            state_suggestion=params[3]
                        )
                    else:
                        expanded_file = _expandNetwork_TextBased(
                            object_similarity_index,
                            method=params[0],
                            threshold=params[1],
                            custom_list=params[2],
                            state_suggestion=params[3]
                        )

                # -- using the newly expanded file, we read it as regular FOON file:
                _constructFOON(expanded_file)
                _buildInternalMaps()

                _response = input(
                    " -- Would you like to perform: 1) greedy or 2) optimal retrieval? [1/2] > ")
                hierarchy_level = int(
                    input(" -- At what level is the search being done? [1/2/3] > "))
                if _response == "1":
                    _taskTreeRetrieval_greedy(
                        searchObject, searchState, hierarchy_level=hierarchy_level)
                elif _response == "2":
                    _pathTreeRetrieval(
                        searchObject, searchState, hierarchy_level=hierarchy_level)
                else:
                    pass

                # -- we then restore the FOON to the unexpanded version:
                _resetFOON(reload=True)

            elif _response == "4":
                if not flag_GEN_complete:
                    _constructFOON_GEN()

                # -- perform task tree retrieval using the abstracted (FOON-GEN) FOON:
                _taskTreeRetrieval_GEN(searchObject, searchState)
            # endif
        # end

        elif option == "5":
            print("-- [NET-CENT] : Initiating FOON centrality calculation...")
            _response = input(" -- At which level? [1/2/3] > ")
            hierarchy_level = 1 if _response == "1" else 2 if _response == "2" else 3
            _calculateCentrality(hierarchy_level)
        # end

        elif option == '6':
            try:
                import FOON_parser
            except ImportError:
                print(
                    " -- ERROR: Missing 'FOON_parser.py' script! Please check if it is located in the same directory!")
                print(
                    "\t-- Download here: https://bitbucket.org/davidpaulius/foon_api/src/master/")
            else:
                print(' -- [FOON_parser] : Running FOON parsing script...')
                FOON_parser._run_parser()
            # endtry
        # end

        elif option == '7':
            # -- open the user's browser and opens the 'FOON_view' interface:
            print(' -- [FOON_view] : Running FOON graph visualization tool...')
            _startFOONview()
        # end

        elif option == "8":
            print(" -- What kind of file do you want to output?")
            print("\t\t1.\tCreate a .PKL file of the loaded universal FOON;")
            print("\t\t2.\tCreate a .JSON file of the loaded universal FOON;")
            print("\t\t3.\tCreate a .JS (JavaScript) file of the loaded universal FOON;")
            print("\t\t4.\tCreate a .TXT file with FOON word2vec sentences;")

            _response = input(
                "  -- Please enter your option or press ENTER to exit : [1-4] > ")

            if _response == "1":
                _saveFOON_pkl()

            elif _response == "2":
                print("-- [FOON_JSON] : Writing FOON graph as .JSON file...")
                _saveFOON_JSON()

            elif _response == "3":
                print("-- [FOON_JS] : Writing FOON graph as JavaScript file...")
                _saveFOON_JS()

            elif _response == "4":
                print("-- Writing FOON functional units as Word2Vec sentences...")
                _FOONtoSentence()
                # _toLSTMSentences()
            # endif
        # end

        # NOTE: Tread ye not below these waters..
        # -- these are debugging or experimental functions that were just there for testing.

        elif option == '13':
            # -- simple function to print any node existing in FOON:
            _object = int(input(
                ' -- Please enter a number corresponding to the node\'s position in list: > '))
            _level = input(' -- At which hierarchy level?: [1/2/3] > ')
            if _level == '':
                _printAnyNode(_object)
            else:
                _printAnyNode(_object, hierarchy_level=int(_level))
        # end

        elif option == '14':
            # -- simple function to print a functional unit existing in FOON:
            _object = int(input(
                ' -- Please enter a number corresponding to the functional unit\'s position in list: > '))
            _level = input(' -- At which hierarchy level?: [1/2/3] > ')
            if _level == '':
                _printAnyFunctionalUnit(_object)
            else:
                _printAnyFunctionalUnit(_object, hierarchy_level=int(_level))
        # end

        elif option == '55':
            # -- simple function to merge two kitchen files together:
            kitchen_1 = _identifyKitchenItems(
                input(" -- Enter PATH and FILE NAME of kitchen file #1: > "))
            print("  -- size of kitchen #1 : " + str(len(kitchen_1)))
            kitchen_2 = _identifyKitchenItems(
                input(" -- Enter PATH and FILE NAME of kitchen file #2: > "))
            print("  -- size of kitchen #2 : " + str(len(kitchen_2)))
            new_kitchen = list(set(kitchen_1 + kitchen_2))
            _file = open('kitchen_merged.txt', 'w')
            for O in new_kitchen:
                text = O.getObjectText()
                if len(text.split('\n')) < 2:
                    print(text)
                _file.write(text)
                _file.write("\n//\n")
            # endfor
            _file.close()
        # end

        elif option == '77':
            # -- perform random searching experiment as done in Paulius et al. 2018:
            # 	This function has been modified to compare FOON-EXP from WordNet and Concept-Net.
            _randomSearchExperiment(
                n_trials=2,
                n_objects=5,
                hierarchy_level=3,
                expanded_ConceptNet='universal_FOON_27.10.2019_EXP-0.75-Concept-Net-refined.txt',
                expanded_WordNet='universal_FOON_27.10.2019_EXP-0.87-WordNet-refined.txt'
            )
        # end

        elif option == '':
            print("-- Exiting program", end='')
            for _ in range(5):
                print('.', end='')
                time.sleep(0.3)
            print('\n')
            break
        # end

        else:
            pass
# enddef


if __name__ == '__main__':
    _start()
