import os, sys, json, tqdm

# NOTE: this is a function that will be used to load each FOON file (as .TXT) and convert each of them into .JSON files.
# -- this requires the functions defined in FOON_graph_analyzer.py to work!

def convert_to_JSON(directory=None):
	try:
		import FOON_graph_analyzer as fga
		fga.FOON.print_old_style = True
	except ImportError:
		print("ERROR: Missing 'FOON_graph_analyzer.py' file!")
		exit()

	if not directory:
		# -- this means we did not specify the directory before run-time:
		directory = input("-- Please enter the DIRECTORY with files to be converted to .JSON format: > ")

	print("-- Provided directory '" + str(directory) + "'...")
	for root, _, files in os.walk(os.path.abspath(directory)):

		files = [ fi for fi in files if fi.endswith('.txt') ]

		if files:
			if not os.path.exists(os.path.join(os.path.abspath(directory), 'JSON')):
			    os.makedirs(os.path.join(os.path.abspath(directory), 'JSON'))
			input('check!')

		for file in tqdm.tqdm(files):
			# -- go through each file in the provided directory with FOON subgraph files:
			file_name =  os.path.join(root, file)

			fga._resetFOON()

			# -- use _constructFOON() function as already defined in the graph analyzer code:
			fga._constructFOON(file_name)

			# -- create a dictionary to store each functional unit:
			subgraph_units = {}
			subgraph_units['functional_units'] = []

			for FU in fga.FOON_lvl3:
				# -- use function already defined in FOON_classes.py file:
				subgraph_units['functional_units'].append( FU.getFunctionalUnitJSON() )

			if fga.FOON_video_source:
				subgraph_units['source'] = fga.FOON_video_source

			json_file_name = os.path.splitext(file)[0] + '.json'

			# -- dump all the contents in dictionary as .JSON:
			json.dump(subgraph_units, open(os.path.join(os.path.abspath(directory), 'JSON', json_file_name), 'w'), indent=7)
		#end
	#end
#enddef

# -- call the function to go through all of the subgraphs and conver them to JSON:
#	(adding a call to this from the FOON_parser.py script for convenience as well)
if __name__ == '__main__':
	convert_to_JSON()
