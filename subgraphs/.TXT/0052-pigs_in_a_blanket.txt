O320	oven	0
S91	off
M123	turn on	0:28	0:31
O320	oven	0
S94	on
//
O456	tin can	1
S24	contains	{dough}
O152	dough	0
S118	rolled
S73	in	[tin can]
M64	open	0:37	0:41
O456	tin can	1
S24	contains	{dough}
S96	opened
//
O146	cutting board	0
S47	empty
O456	tin can	1
S24	contains	{dough}
S96	opened
O152	dough	0
S118	rolled
S73	in	[tin can]
M66	pick-and-place	0:41	0:44
O146	cutting board	0
S24	contains	{dough}
O152	dough	0
S118	rolled
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{dough}
O152	dough	0
S118	rolled
S94	on	[cutting board]
O382	rolling pin	1
M86	roll	0:48	1:03
O152	dough	0
S52	flattened
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{dough}
O152	dough	0
S52	flattened
S94	on	[cutting board]
O358	plate	0
S24	contains	{cheese}
O78	cheese	1
S137	sliced
S94	on	[plate]
M66	pick-and-place	1:04	1:08
O152	dough	0
S24	contains	{cheese}
S94	on	[cutting board]
O78	cheese	1
S137	sliced
S94	on	[dough]
//
O146	cutting board	0
S24	contains	{dough}
O152	dough	0
S24	contains	{cheese}
S94	on	[cutting board]
O358	plate	1
S24	contains	{sausage}
O396	sausage	1
S25	cooked
S94	on	[plate]
M66	pick-and-place	1:09	1:13
O152	dough	0
S24	contains	{cheese,sausage}
S94	on	[cutting board]
O396	sausage	1
S25	cooked
S94	on	[dough]
//
O152	dough	0
S24	contains	{cheese,sausage}
S94	on	[cutting board]
M86	roll	1:14	1:22
O349	pigs in a blanket	0
S155	uncooked
S24	contains	{cheese,sausage}
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{dough}
O13	baking tray	0
S47	empty
O349	pigs in a blanket	1
S155	uncooked
S24	contains	{cheese,sausage}
S94	on	[cutting board]
M66	pick-and-place	1:24	1:32
O13	baking tray	0
S24	contains	{pigs in a blanket}
O349	pigs in a blanket	1
S155	uncooked
S24	contains	{cheese,sausage}
S73	in	[baking tray]
O146	cutting board	0
//
O320	oven	0
S94	on
O13	baking tray	1
S24	contains	{pigs in a blanket}
M81	put inside	1:38:50	1:43
O13	baking tray	1
S24	contains	{pigs in a blanket}
S73	in	[oven]
O320	oven	0
S94	on
S168	with	[baking tray]
//
O13	baking tray	0
S24	contains	{pigs in a blanket}
S73	in	[oven]
O349	pigs in a blanket	1
S155	uncooked
S24	contains	{cheese,sausage}
S73	in	[baking tray]
O320	oven	0
S94	on
S168	with	[baking tray]
M2	bake	Assumed	Assumed
O349	pigs in a blanket	1
S25	cooked
S24	contains	{cheese,sausage}
S73	in	[baking tray]
O13	baking tray	0
S24	contains	{pigs in a blanket}
S73	in	[oven]
//
O320	oven	0
S94	on
S168	with	[baking tray]
O349	pigs in a blanket	1
S25	cooked
S24	contains	{cheese,sausage}
S73	in	[baking tray]
O13	baking tray	1
S24	contains	{pigs in a blanket}
S73	in	[oven]
M82	remove	Assumed	Assumed
O320	oven	0
S91	off
O13	baking tray	1
S24	contains	{pigs in a blanket}
//
O349	pigs in a blanket	1
S25	cooked
S24	contains	{cheese,sausage}
S73	in	[baking tray]
O13	baking tray	0
S24	contains	{pigs in a blanket}
O358	plate	0
S47	empty
O463	tongs	1
M66	pick-and-place	Assumed	Assumed
O358	plate	0
S24	contains	{pigs in a blanket}
O349	pigs in a blanket	1	29
S25	cooked
S24	contains	{dough,cheese,sausage}
S94	on	[plate]
//
