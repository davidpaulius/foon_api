O435	stove	0
S94	on
O360	pot	1
S47	empty
M66	pick-and-place	Assumed	Assumed
O360	pot	1
S70	heated
S94	on	[stove]
O435	stove	0
S94	on
S168	with	[pot]
//
O172	faucet	0
S94	on
O484	water	0
S60	from faucet
O282	measuring cup	1
S47	empty
M39	fill	Assumed	Assumed
O282	measuring cup	1
S24	contains	{water}
O484	water	1
S73	in	[measuring cup]
//
O360	pot	0
S70	heated
S94	on	[stove]
O282	measuring cup	1
S24	contains	{water}
O484	water	1
S73	in	[measuring cup]
M70	pour	Assumed	Assumed
O360	pot	0
S24	contains	{water}
S94	on	[stove]
O484	water	1
S73	in	[pot]
//
O360	pot	0
S24	contains	{water}
S94	on	[stove]
O409	shaker	1
S24	contains	{salt}
O390	salt	1
S64	granulated
S73	in	[shaker]
M106	sprinkle	Assumed	Assumed
O360	pot	0
S24	contains	{water,salt}
S94	on	[stove]
O390	salt	1
S64	granulated
S73	in	[pot]
//
O360	pot	0
S24	contains	{water,salt}
S94	on	[stove]
O390	salt	1
S64	granulated
S73	in	[pot]
O484	water	1
S73	in	[pot]
O427	spoon	1
M109	stir	Assumed	Assumed
O360	pot	0
S24	contains	{saltwater}
S94	on	[stove]
O391	saltwater	1
S73	in	[pot]
//
O360	pot	0
S24	contains	{saltwater}
S94	on	[stove]
O268	linguine	1
S155	uncooked
S94	on	[plate]
M66	pick-and-place	Assumed	Assumed
O360	pot	0
S24	contains	{saltwater,linguine}
S94	on	[stove]
O268	linguine	1
S155	uncooked
S73	in	[pot]
//
O435	stove	0
S94	on
S168	with	[pot]
O360	pot	0
S24	contains	{saltwater,linguine}
S94	on	[stove]
O268	linguine	1
S155	uncooked
S73	in	[pot]
O427	spoon	1
M20	cook and stir	Assumed	Assumed
O360	pot	0
S24	contains	{saltwater,linguine}
S94	on	[stove]
O268	linguine	1
S25	cooked
S73	in	[pot]
//
O360	pot	0
S24	contains	{saltwater,linguine}
S94	on	[stove]
O268	linguine	1
S25	cooked
S73	in	[pot]
O436	strainer	1
S98	over sink
M70	pour	Assumed	Assumed
O436	strainer	1
S24	contains	{linguine}
O268	linguine	1
S25	cooked
S73	in	[strainer]
O360	pot	0
//
O360	pot	0
S47	empty
O436	strainer	1
S24	contains	{linguine}
O268	linguine	1
S25	cooked
S73	in	[strainer]
M70	pour	Assumed	Assumed
O360	pot	0
S24	contains	{linguine}
O268	linguine	1
S25	cooked
S73	in	[pot]
O436	strainer	1
//
O37	blender jar	0
S47	empty
S1	attached to	[blender]
O45	bowl	1
S24	contains	{basil}
O17	basil	1
S162	washed
S73	in	[bowl]
M70	pour	Assumed	Assumed
O45	bowl	1
O37	blender jar	0
S24	contains	{basil}
S1	attached to	[blender]
O17	basil	1
S162	washed
S73	in	[blender jar]
//
O37	blender jar	0
S24	contains	{basil}
S1	attached to	[blender]
O45	bowl	1
S24	contains	{pine nut}
O350	pine nut	1
S153	toasted
S73	in	[bowl]
M70	pour	1:40	1:41
O37	blender jar	0
S24	contains	{basil,pine nut}
S1	attached to	[blender]
O45	bowl	1
O350	pine nut	1
S153	toasted
S73	in	[blender jar]
//
O37	blender jar	0
S24	contains	{basil,pine nut}
S1	attached to	[blender]
O194	garlic	1
S104	peeled
S94	on	[surface]
M66	pick-and-place	1:44	1:45
O37	blender jar	0
S24	contains	{basil,pine nut,garlic}
S1	attached to	[blender]
O194	garlic	1
S104	peeled
S73	in	[blender jar]
//
O37	blender jar	0
S24	contains	{basil,pine nut,garlic}
S1	attached to	[blender]
O222	grinder	1
S24	contains	{black pepper}
O33	black pepper	1
S73	in	[grinder]
M49	grind	1:48	1:50
O37	blender jar	0
S24	contains	{basil,pine nut,garlic,black pepper}
S1	attached to	[blender]
O33	black pepper	1
S67	ground
S73	in	[blender jar]
//
O37	blender jar	0
S24	contains	{basil,pine nut,garlic,black pepper}
S1	attached to	[blender]
O118	container	1
S24	contains	{salt}
O390	salt	1
S64	granulated
S73	in	[container]
M106	sprinkle	1:52	1:54
O37	blender jar	0
S24	contains	{basil,pine nut,garlic,black pepper,salt}
S1	attached to	[blender]
O390	salt	1
S64	granulated
S73	in	[blender jar]
//
O37	blender jar	0
S24	contains	{basil,pine nut,garlic,black pepper}
S1	attached to	[blender]
O118	container	1
S24	contains	{salt}
O390	salt	1
S64	granulated
S73	in	[container]
M106	sprinkle	1:52	1:54
O37	blender jar	0
S24	contains	{basil,pine nut,garlic,black pepper,salt}
S1	attached to	[blender]
O390	salt	1
S64	granulated
S73	in	[blender jar]
//
O207	grater	1
O255	lemon	1
S167	whole
O37	blender jar	0
S24	contains	{basil,pine nut,garlic,black pepper,salt}
S1	attached to	[blender]
M48	grate	1:57	2:02
O37	blender jar	0
S24	contains	{basil,pine nut,garlic,black pepper,salt,lemon zest}
S1	attached to	[blender]
O259	lemon zest	1
S73	in	[blender jar]
O255	lemon	1
S170	zested
S94	on	[surface]
//
O255	lemon	0
S170	zested
S94	on	[surface]
O251	knife	1
M25	cut	2:08	2:09
O255	lemon	0
S68	halved
S94	on	[surface]
//
O255	lemon	0
S68	halved
O37	blender jar	0
S24	contains	{basil,pine nut,garlic,black pepper,salt,lemon zest}
S1	attached to	[blender]
M107	squeeze	2:10	2:14
O37	blender jar	0
S24	contains	{basil,pine nut,garlic,black pepper,salt,lemon zest,lemon juice}
S1	attached to	[blender]
O258	lemon juice	1
S73	in	[blender jar]
O255	lemon	1
S144	squeezed
//
O37	blender jar	0
S24	contains	{basil,pine nut,garlic,black pepper,salt,lemon zest}
S1	attached to	[blender]
O38	blender lid	1
M23	cover	2:17	2:18
O38	blender lid	1
S94	on	[blender]
O37	blender jar	0
S27	covered	{blender lid}
S24	contains	{basil,pine nut,garlic,black pepper,salt,lemon zest}
S1	attached to	[blender]
//
O34	blender	0
S168	with	[blender jar]
O37	blender jar	0
S27	covered	{blender lid}
S24	contains	{basil,pine nut,garlic,black pepper,salt,lemon zest}
S1	attached to	[blender]
O17	basil	1
S162	washed
S73	in	[blender jar]
O350	pine nut	1
S153	toasted
S73	in	[blender jar]
O194	garlic	1
S104	peeled
S73	in	[blender jar]
O33	black pepper	1
S67	ground
S73	in	[blender jar]
O390	salt	1
S64	granulated
S73	in	[blender jar]
O259	lemon zest	1
S73	in	[blender jar]
O258	lemon juice	1
S73	in	[blender jar]
M4	blend	2:25	2:26
O37	blender jar	0
S27	covered	{blender lid}
S24	contains	{basil mixture}
S1	attached to	[blender]
O18	basil mixture	0
S24	contains	{basil,pine nut,garlic,black pepper,salt,lemon zest}
S73	in	[blender jar]
//
O37	blender jar	0
S27	covered	{blender lid}
S24	contains	{basil mixture}
S1	attached to	[blender]
O18	basil mixture	0
S24	contains	{basil,pine nut,garlic,black pepper,salt,lemon zest}
S73	in	[blender jar]
O44	bottle	1
S24	contains	{olive oil}
O310	olive oil	1
S73	in	[bottle]
M71	pour and blend	2:49	2:51
O18	basil mixture	0
S24	contains	{basil,pine nut,garlic,black pepper,salt,lemon zest,olive oil}
S73	in	[blender jar]
//
O37	blender jar	0
S27	covered	{blender lid}
S24	contains	{basil mixture}
S1	attached to	[blender]
O38	blender lid	1
S94	on	[blender]
M124	uncover	2:52	2:53
O38	blender lid	1
O37	blender jar	0
S24	contains	{basil mixture}
S1	attached to	[blender]
//
O37	blender jar	0
S24	contains	{basil mixture}
S1	attached to	[blender]
O34	blender	0
S168	with	[blender jar]
M32	detach	2:57	2:58
O37	blender jar	0
S24	contains	{basil mixture}
O34	blender	0
S91	off
//
O37	blender jar	1
S24	contains	{basil mixture}
O18	basil mixture	1
S24	contains	{basil,pine nut,garlic,black pepper,salt,lemon zest,olive oil}
S73	in	[blender jar]
O45	bowl	0
S47	empty
M70	pour	2:59	3:04
O45	bowl	0
S24	contains	{basil mixture}
O18	basil mixture	0
S24	contains	{basil,pine nut,garlic,black pepper,salt,lemon zest,olive oil}
S73	in	[bowl]
O37	blender jar	1
//
O207	grater	1
O330	parmesan	1
S9	block
O45	bowl	0
S24	contains	{basil mixture}
M48	grate	3:07	3:19
O45	bowl	0
S24	contains	{basil mixture,parmesan}
O330	parmesan	1
S65	grated
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{basil mixture,parmesan}
O330	parmesan	1
S65	grated
S73	in	[bowl]
O18	basil mixture	0
S24	contains	{basil,pine nut,garlic,black pepper,salt,lemon zest,olive oil}
S73	in	[bowl]
O424	spatula	1
M109	stir	3:23	3:33
O45	bowl	0
S24	contains	{pesto}
O346	pesto	0
S24	contains	{basil,pine nut,garlic,black pepper,salt,lemon zest,olive oil,parmesan}
S73	in	[bowl]
O424	spatula	1
//
O45	bowl	1
S24	contains	{pesto}
O346	pesto	1
S24	contains	{basil,pine nut,garlic,black pepper,salt,lemon zest,olive oil,parmesan}
S73	in	[bowl]
O360	pot	0
S24	contains	{linguine}
O424	spatula	1
M93	scrape	3:40	3:43
O360	pot	0
S24	contains	{linguine,pesto}
O424	spatula	1
O45	bowl	1
O346	pesto	1
S24	contains	{basil,pine nut,garlic,black pepper,salt,lemon zest,olive oil,parmesan}
S73	in	[pot]
//
O300	mug	1
S24	contains	{saltwater}
O391	saltwater	1
S73	in	[mug]
O360	pot	0
S24	contains	{linguine,pesto}
M70	pour	3:49	3:52
O360	pot	0
S24	contains	{linguine,pesto,saltwater}
O391	saltwater	1
S73	in	[pot]
//
O360	pot	0
S24	contains	{linguine,pesto,saltwater}
O391	saltwater	1
S73	in	[pot]
O346	pesto	1
S24	contains	{basil,pine nut,garlic,black pepper,salt,lemon zest,olive oil,parmesan}
S73	in	[pot]
O268	linguine	0
S25	cooked
S73	in	[pot]
O463	tongs	1
M61	mix	3:54	3:57
O360	pot	0
S24	contains	{pesto linguine}
O347	pesto linguine	1
S24	contains	{pesto,linguine}
S73	in	[pot]
//
O207	grater	1
O330	parmesan	1
S9	block
O360	pot	0
S24	contains	{pesto linguine}
O347	pesto linguine	0
S24	contains	{pesto,linguine}
S73	in	[pot]
M48	grate	3:59	4:04
O347	pesto linguine	0
S24	contains	{pesto,linguine,parmesan}
S73	in	[pot]
O330	parmesan	1
S65	grated
S73	in	[pot]
//
O360	pot	0
S24	contains	{pesto linguine}
O347	pesto linguine	0
S24	contains	{pesto,linguine,parmesan}
S73	in	[pot]
O222	grinder	1
S24	contains	{black pepper}
O33	black pepper	1
S73	in	[grinder]
M49	grind	4:07	4:09
O347	pesto linguine	0
S24	contains	{pesto,linguine,parmesan,black pepper}
S73	in	[pot]
O33	black pepper	1
S73	in	[pot]
//
O360	pot	0
S24	contains	{pesto linguine}
O347	pesto linguine	1
S24	contains	{pesto,linguine,parmesan,black pepper}
S73	in	[pot]
O463	tongs	1
M61	mix	4:11	4:20
O347	pesto linguine	1
S24	contains	{pesto,linguine,parmesan,black pepper}
S73	in	[pot]
O463	tongs	1
//
O45	bowl	0
S47	empty
O360	pot	0
S24	contains	{pesto linguine}
O347	pesto linguine	1
S24	contains	{pesto,linguine,parmesan,black pepper}
S73	in	[pot]
O463	tongs	1
M66	pick-and-place	4:23	4:30
O347	pesto linguine	1	13
S24	contains	{pesto,linguine,parmesan,black pepper}
S73	in	[bowl]
O45	bowl	1
S24	contains	{pesto linguine}
O463	tongs	1
//
