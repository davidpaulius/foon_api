# Source:	http://foonets.com/foon_subgraphs/subgraphs/EPIC-P20_04-cooked_salmon.MP4
//
O322	packet	1
S24	contains	{salmon}
S123	sealed
O401	scissors	1
M29	cut open	<0:35,0:36>
O322	packet	0
S96	opened
S24	contains	{salmon}
//
O322	packet	0
S96	opened
S24	contains	{salmon}
O389	salmon	1
S114	raw
S73	in	[packet]
S61	frozen
O45	bowl	0
S47	empty
M66	pick-and-place	<0:51,1:12>
O45	bowl	0
S24	contains	{salmon}
O389	salmon	0
S114	raw
S73	in	[bowl]
S61	frozen
//
O45	bowl	1
S24	contains	{salmon}
O284	microwave	0
S48	empty (ready)
M81	put inside	<1:16,1:20>
O45	bowl	1
S24	contains	{salmon}
S73	in	[microwave]
O284	microwave	0
S24	contains	{bowl}
//
O45	bowl	1
S24	contains	{salmon}
S73	in	[microwave]
O284	microwave	0
S168	with	[bowl]
O389	salmon	0
S114	raw
S73	in	[bowl]
S61	frozen
M31	defrost	<1:44,2:40>
O389	salmon	0
S114	raw
S73	in	[bowl]
S39	defrosted
//
O45	bowl	1
S24	contains	{salmon}
S73	in	[microwave]
O284	microwave	0
S168	with	[bowl]
M82	remove	<2:50,2:54>
O45	bowl	1
S24	contains	{salmon}
O284	microwave	0
S47	empty
//
O146	cutting board	0
S47	empty
O45	bowl	0
S24	contains	{salmon}
O389	salmon	1
S114	raw
S73	in	[bowl]
S39	defrosted
M66	pick-and-place	<3:35,3:39>
O146	cutting board	0
S24	contains	{salmon}
O389	salmon	0
S114	raw
S94	on	[cutting board]
S39	defrosted
O45	bowl	1
S47	empty
//
O389	salmon	1
S114	raw
S94	on	[cutting board]
S39	defrosted
O251	knife	1
M65	peel	<3:40,5:30>
O389	salmon	0
S114	raw
S94	on	[cutting board]
S136	skinned
//
O172	faucet	1
S94	on
O484	water	0
S82	liquid
S60	from faucet
O389	salmon	1
S114	raw
S94	on	[cutting board]
S136	skinned
M126	wash	<5:55,6:04>
O389	salmon	0
S114	raw
S136	skinned
//
O146	cutting board	0
S47	empty
O389	salmon	0
S114	raw
S136	skinned
M66	pick-and-place	<6:35,6:38>
O146	cutting board	0
S24	contains	{salmon}
O389	salmon	0
S114	raw
S94	on	[cutting board]
S136	skinned
//
O389	salmon	1
S114	raw
S94	on	[cutting board]
S136	skinned
O251	knife	1
M33	dice	<6:40,8:14>
O389	salmon	0
S114	raw
S94	on	[cutting board]
S43	diced
//
O146	cutting board	0
S24	contains	{salmon}
O389	salmon	1
S114	raw
S94	on	[cutting board]
S43	diced
O45	bowl	0
S47	empty
M66	pick-and-place	<8:18,8:30>
O45	bowl	0
S24	contains	{salmon}
O146	cutting board	0
S47	empty
O389	salmon	0
S114	raw
S73	in	[bowl]
S43	diced
//
O45	bowl	0
S24	contains	{salmon}
O44	bottle	1
S96	opened
S24	contains	{sesame oil}
O407	sesame oil	1
S82	liquid
S73	in	[bottle]
M70	pour	<8:54,8:58>
O45	bowl	0
S24	contains	{salmon,sesame oil}
O407	sesame oil	0
S82	liquid
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{salmon,sesame oil}
O222	grinder	1
S24	contains	{black pepper}
O33	black pepper	1
S73	in	[grinder]
S167	whole
M50	grind and pour	<9:08,9:15>
O45	bowl	0
S24	contains	{black pepper,salmon,sesame oil}
O33	black pepper	0
S73	in	[bowl]
S67	ground
//
O45	bowl	0
S24	contains	{black pepper,salmon,sesame oil}
O33	black pepper	1
S73	in	[bowl]
S67	ground
O389	salmon	1
S114	raw
S73	in	[bowl]
S43	diced
O407	sesame oil	1
S82	liquid
S73	in	[bowl]
M61	mix	<9:18,9:44>
O45	bowl	0
S24	contains	{black pepper,salmon,sesame oil}
O389	salmon	1
S93	oiled
S73	in	[bowl]
S114	raw
S24	contains	{black pepper,sesame oil}
//
O45	bowl	0
S47	empty
O44	bottle	1
S96	opened
S24	contains	{sesame oil}
O407	sesame oil	1
S82	liquid
S73	in	[bottle]
M70	pour	<10:19,10:22>
O45	bowl	0
S24	contains	{sesame oil}
O407	sesame oil	0
S82	liquid
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{sesame oil}
O244	jar	0
S24	contains	{salt}
O390	salt	1
S73	in	[jar]
S64	granulated
O427	spoon	1
S47	empty
M106	sprinkle	<10:37,10:43>
O45	bowl	0
S24	contains	{salt,sesame oil}
O390	salt	0
S73	in	[bowl]
S64	granulated
//
O45	bowl	0
S24	contains	{salt,sesame oil}
O222	grinder	1
S24	contains	{black pepper}
O33	black pepper	1
S167	whole
S73	in	[grinder]
M50	grind and pour	<10:50,10:54>
O45	bowl	0
S24	contains	{black pepper,salt,sesame oil}
O33	black pepper	0
S73	in	[bowl]
S67	ground
//
O202	ginger	1
S19	chunk
O251	knife	1
M65	peel	<15:22,16:13>
O202	ginger	0
S104	peeled
S19	chunk
//
O45	bowl	0
S24	contains	{black pepper,salt,sesame oil}
O207	grater	1
O202	ginger	1
S104	peeled
S19	chunk
M48	grate	<16:30,17:45>
O45	bowl	0
S24	contains	{black pepper,ginger,salt,sesame oil}
O202	ginger	0
S73	in	[bowl]
S65	grated
//
O45	bowl	0
S24	contains	{black pepper,ginger,salt,sesame oil}
O44	bottle	1
S24	contains	{vegetable oil}
O478	vegetable oil	1
S82	liquid
S73	in	[bottle]
M70	pour	<18:18,19:00>
O45	bowl	0
S24	contains	{black pepper,ginger,salt,sesame oil,vegetable oil}
O478	vegetable oil	0
S82	liquid
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{black pepper,ginger,salt,sesame oil,vegetable oil}
O407	sesame oil	1
S82	liquid
S73	in	[bowl]
O202	ginger	1
S73	in	[bowl]
S65	grated
O33	black pepper	1
S73	in	[bowl]
S67	ground
O390	salt	1
S73	in	[bowl]
S64	granulated
O478	vegetable oil	1
S82	liquid
S73	in	[bowl]
M109	stir	<Assumed>
O309	oil mixture	0
S82	liquid
S88	mixed
S73	in	[bowl]
S24	contains	{black pepper,ginger,salt,sesame oil,vegetable oil}
//
O45	bowl	0
S24	contains	{black pepper,ginger,salt,sesame oil,vegetable oil}
O309	oil mixture	0
S82	liquid
S88	mixed
S73	in	[bowl]
S24	contains	{black pepper,ginger,salt,sesame oil,vegetable oil}
O45	bowl	1
S24	contains	{black pepper,salmon,sesame oil}
M70	pour	<Assumed>
O45	bowl	0
S24	contains	{black pepper,ginger,salmon,salt,sesame oil,vegetable oil}
//
O45	bowl	0
S24	contains	{black pepper,ginger,salmon,salt,sesame oil,vegetable oil}
O309	oil mixture	1
S82	liquid
S88	mixed
S73	in	[bowl]
S24	contains	{black pepper,ginger,salt,sesame oil,vegetable oil}
O389	salmon	1
S93	oiled
S73	in	[bowl]
S114	raw
S24	contains	{black pepper,sesame oil}
M61	mix	<19:17,19:37>
O389	salmon	1
S93	oiled
S73	in	[bowl]
S24	contains	{black pepper,ginger,salt,sesame oil,vegetable oil}
S114	raw
//
O435	stove	0
S92	off (ready)
M123	turn on	<Assumed>
O435	stove	0
S94	on
//
O120	cooking pan	1
S47	empty
O435	stove	0
S94	on
M66	pick-and-place	<20:40,20:44>
O120	cooking pan	1
S47	empty
S94	on	[stove]
O435	stove	0
S168	with	[cooking pan]
S94	on
//
O120	cooking pan	1
S47	empty
S94	on	[stove]
O435	stove	0
S168	with	[cooking pan]
S94	on
M77	preheat	<Assumed>
O120	cooking pan	1
S72	hot
S47	empty
S94	on	[stove]
//
O120	cooking pan	0
S72	hot
S47	empty
S94	on	[stove]
O44	bottle	1
S96	opened
S24	contains	{vegetable oil}
O478	vegetable oil	1
S82	liquid
S73	in	[bottle]
M70	pour	<Assumed>
O120	cooking pan	0
S72	hot
S24	contains	{vegetable oil}
S94	on	[stove]
O478	vegetable oil	0
S82	liquid
S73	in	[cooking pan]
//
O45	bowl	1
S24	contains	{black pepper,oil mixture,salmon,sesame oil}
O389	salmon	1
S93	oiled
S73	in	[bowl]
S24	contains	{black pepper,ginger,salt,sesame oil,vegetable oil}
S114	raw
O120	cooking pan	0
S72	hot
S24	contains	{vegetable oil}
S94	on	[stove]
M70	pour	<Assumed>
O120	cooking pan	0
S72	hot
S24	contains	{salmon,vegetable oil}
S94	on	[stove]
O389	salmon	1
S93	oiled
S73	in	[cooking pan]
S24	contains	{black pepper,ginger,salt,sesame oil,vegetable oil}
S114	raw
O45	bowl	0
S47	empty
//
O435	stove	0
S168	with	[cooking pan]
S94	on
O120	cooking pan	0
S72	hot
S24	contains	{salmon,vegetable oil}
S94	on	[stove]
O389	salmon	1
S93	oiled
S73	in	[cooking pan]
S24	contains	{black pepper,ginger,salt,sesame oil,vegetable oil}
S114	raw
O474	turner	1
M20	cook and stir	<20:44,31:42>
O389	salmon	1
S25	cooked
S73	in	[cooking pan]
S24	contains	{black pepper,ginger,salt,sesame oil,vegetable oil}
//
O120	cooking pan	0
S72	hot
S24	contains	{salmon,vegetable oil}
S94	on	[stove]
O389	salmon	1
S25	cooked
S73	in	[cooking pan]
S24	contains	{black pepper,ginger,salt,sesame oil,vegetable oil}
O474	turner	1
O358	plate	0
S47	empty
M91	scoop and pour	<Assumed>
O358	plate	0
S24	contains	{salmon}
O389	salmon	1	17
S25	cooked
S94	on	[plate]
S24	contains	{black pepper,ginger,salt,sesame oil,vegetable oil}
//
