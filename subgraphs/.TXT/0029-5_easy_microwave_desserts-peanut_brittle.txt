O282	measuring cup	1
S24	contains	{corn syrup}
O128	corn syrup	1
S73	in	[measuring cup]
O45	bowl	0
S47	empty
M70	pour	Assumed	Assumed
O45	bowl	0
S24	contains	{corn syrup}
O128	corn syrup	1
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{corn syrup}
O282	measuring cup	1
S24	contains	{sugar}
O438	sugar	1
S64	granulated
S73	in	[measuring cup]
M70	pour	Assumed	Assumed
O45	bowl	0
S24	contains	{corn syrup,sugar}
O438	sugar	1
S64	granulated
S73	in	[bowl]
//
O45	bowl	1
S24	contains	{corn syrup,sugar}
O284	microwave	0
S47	empty
M81	put inside	Assumed	Assumed
O284	microwave	0
S168	with	[bowl]
O45	bowl	1
S24	contains	{corn syrup,sugar}
S73	in	[microwave]
//
O284	microwave	0
S168	with	[bowl]
O45	bowl	1
S24	contains	{corn syrup,sugar}
S73	in	[microwave]
M52	heat	Assumed	Assumed
O45	bowl	0
S24	contains	{sugar mixture}
S73	in	[microwave]
O439	sugar mixture	1
S86	melted
S73	in	[bowl]
//
O45	bowl	1
S24	contains	{sugar mixture}
S73	in	[microwave]
O284	microwave	0
S168	with	[bowl]
M82	remove	Assumed	Assumed
O45	bowl	1
S24	contains	{sugar mixture}
O284	microwave	0
S47	empty
//
O45	bowl	0
S24	contains	{sugar mixture}
O45	bowl	1
S24	contains	{butter}
O61	butter	1
S35	cubed
S73	in	[bowl]
M70	pour	Assumed	Assumed
O45	bowl	0
S24	contains	{sugar mixture,butter}
O61	butter	1
S86	melted
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{sugar mixture,butter}
O45	bowl	1
S24	contains	{vanilla extract}
O475	vanilla extract	1
S73	in	[bowl]
M70	pour	Assumed	Assumed
O45	bowl	0
S24	contains	{sugar mixture,butter,vanilla extract}
//
O45	bowl	0
S24	contains	{sugar mixture,butter,vanilla extract}
O282	measuring cup	1
S24	contains	{peanut}
O336	peanut	1
S73	in	[measuring cup]
M70	pour	Assumed	Assumed
O45	bowl	0
S24	contains	{sugar mixture,butter,vanilla extract,peanut}
O336	peanut	1
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{sugar mixture,butter,vanilla extract,peanut}
O183	fork	1
M109	stir	Assumed	Assumed
O45	bowl	0
S24	contains	{peanut mixture}
O340	peanut mixture	1
S24	contains	{sugar mixture,butter,vanilla extract,peanut}
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{peanut mixture}
O340	peanut mixture	1
S24	contains	{sugar mixture,butter,vanilla extract,peanut}
S73	in	[bowl]
O284	microwave	0
S91	off
M66	pick-and-place	Assumed	Assumed
O284	microwave	0
S168	with	[bowl]
O45	bowl	0
S24	contains	{peanut mixture}
S73	in	[microwave]
//
O284	microwave	0
S168	with	[bowl]
O340	peanut mixture	1
S24	contains	{sugar mixture,butter,vanilla extract,peanut}
S73	in	[bowl]
O45	bowl	0
S24	contains	{peanut mixture}
S73	in	[microwave]
M52	heat	Assumed	Assumed
O45	bowl	0
S24	contains	{peanut mixture}
S73	in	[microwave]
O340	peanut mixture	1
S70	heated
S24	contains	{sugar mixture,butter,vanilla extract,peanut}
S73	in	[bowl]
//
O284	microwave	0
S168	with	[bowl]
O45	bowl	1
S24	contains	{peanut mixture}
S73	in	[microwave]
M82	remove	Assumed	Assumed
O284	microwave	0
S91	off
O45	bowl	1
S24	contains	{peanut mixture}
//
O45	bowl	1
S24	contains	{baking soda}
O12	baking soda	1
S73	in	[bowl]
O45	bowl	0
S24	contains	{peanut mixture}
M70	pour	Assumed	Assumed
O45	bowl	0
S24	contains	{peanut mixture,baking soda}
//
O45	bowl	0
S24	contains	{peanut mixture,baking soda}
O340	peanut mixture	0
S70	heated
S24	contains	{sugar mixture,butter,vanilla extract,peanut}
S73	in	[bowl]
O183	fork	1
M109	stir	Assumed	Assumed
O45	bowl	0
S24	contains	{peanut mixture}
O340	peanut mixture	0
S70	heated
S24	contains	{sugar mixture,butter,vanilla extract,peanut,baking soda}
S73	in	[bowl]
//
O45	bowl	1
S24	contains	{peanut mixture}
O340	peanut mixture	0
S70	heated
S24	contains	{sugar mixture,butter,vanilla extract,peanut,baking soda}
S73	in	[bowl]
O329	parchment paper	0
S47	empty
M70	pour	Assumed	Assumed
O329	parchment paper	0
S24	contains	{peanut mixture}
O340	peanut mixture	1
S70	heated
S24	contains	{sugar mixture,butter,vanilla extract,peanut,baking soda}
S94	on	[sheet]
//
O340	peanut mixture	0
S70	heated
S24	contains	{sugar mixture,butter,vanilla extract,peanut,baking soda}
S94	on	[sheet]
O329	parchment paper	0
S24	contains	{peanut mixture}
M100	sit	Assumed	Assumed
O329	parchment paper	0
S24	contains	{peanut brittle}
O337	peanut brittle	0	10
S24	contains	{sugar mixture,butter,vanilla extract,peanut,baking soda}
S94	on	[sheet]
//
