# Source:	https://www.youtube.com/watch?v=gPBEsV6qNM0
# Source:	http://foonets.com/foon_subgraphs/subgraphs/ACT-0034-greek_salad.mp4
//
O146	cutting board	0
S47	empty
O459	tomato	1
S167	whole
M66	pick-and-place	<Assumed>
O146	cutting board	0
S24	contains	{tomato}
O459	tomato	1
S167	whole
S94	on	[cutting board]
//
O459	tomato	0
S167	whole
S94	on	[cutting board]
O251	knife	1
M102	slice	<1:07-1:14>
O459	tomato	0
S137	sliced
S94	on	[cutting board]
//
O459	tomato	0
S137	sliced
S94	on	[cutting board]
O251	knife	1
M33	dice	<1:17-1:21>
O459	tomato	0
S35	cubed
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{tomato}
O459	tomato	1
S35	cubed
S94	on	[cutting board]
O290	mixing bowl	0
S47	empty
M66	pick-and-place	<1:28-1:31>
O290	mixing bowl	0
S24	contains	{tomato}
O459	tomato	1
S35	cubed
S73	in	[mixing bowl]
//
O313	onion	0
S167	whole
S104	peeled
S94	on	[cutting board]
O251	knife	1
M102	slice	<1:32-1:40>
O313	onion	0
S137	sliced
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{onion}
O313	onion	1
S137	sliced
S94	on	[cutting board]
O290	mixing bowl	0
S24	contains	{tomato}
O459	tomato	1
S35	cubed
S73	in	[mixing bowl]
M66	pick-and-place	<1:41-1:43>
O290	mixing bowl	0
S24	contains	{tomato,onion}
O313	onion	1
S137	sliced
S73	in	[mixing bowl]
//
O140	cucumber	1
S167	whole
O341	peeler	1
M65	peel	<1:50-1:58>
O140	cucumber	1
S167	whole
S104	peeled
//
O146	cutting board	0
S47	empty
O140	cucumber	1
S167	whole
S104	peeled
M66	pick-and-place	<Assumed>
O146	cutting board	0
S24	contains	{cucumber}
O140	cucumber	1
S167	whole
S104	peeled
S94	on	[cutting board]
//
O140	cucumber	0
S167	whole
S104	peeled
S94	on	[cutting board]
O251	knife	1
M33	dice	<1:59-2:09>
O140	cucumber	0
S43	diced
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{cucumber}
O140	cucumber	1
S94	on	[cutting board]
S43	diced
O290	mixing bowl	0
S24	contains	{tomato,onion}
M66	pick-and-place	<2:10-2:13>
O290	mixing bowl	0
S24	contains	{tomato,onion,cucumber}
O140	cucumber	1
S43	diced
S73	in	[mixing bowl]
//
O146	cutting board	0
S47	empty
O442	sweet pepper	1
S167	whole
M66	pick-and-place	<Assumed>
O146	cutting board	0
S24	contains	{sweet pepper}
O442	sweet pepper	1
S167	whole
S94	on	[cutting board]
//
O442	sweet pepper	0
S167	whole
S94	on	[cutting board]
O251	knife	1
M22	core	<2:18-2:24>
O442	sweet pepper	0
S26	cored
S94	on	[cutting board]
//
O442	sweet pepper	0
S26	cored
S94	on	[cutting board]
O251	knife	1
M33	dice	<2:26-2:31>
O442	sweet pepper	0
S35	cubed
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{sweet pepper}
O442	sweet pepper	0
S94	on	[cutting board]
S137	sliced
O251	knife	1
M25	cut	<2:27-2:31>
O442	sweet pepper	0
S43	diced
S94	on	[cutting board]
//
O146	cutting board	0
S47	empty
O216	green pepper	1
S167	whole
M66	pick-and-place	<Assumed>
O146	cutting board	0
S24	contains	{green pepper}
O216	green pepper	1
S167	whole
S94	on	[cutting board]
//
O216	green pepper	0
S167	whole
S94	on	[cutting board]
O251	knife	1
M22	core	<2:33-2:36>
O216	green pepper	0
S26	cored
S94	on	[cutting board]
//
O216	green pepper	0
S26	cored
S94	on	[cutting board]
O251	knife	1
M33	dice	<Assumed>
O216	green pepper	0
S35	cubed
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{sweet pepper}
O442	sweet pepper	1
S94	on	[cutting board]
S35	cubed
O146	cutting board	0
S24	contains	{green pepper}
O216	green pepper	1
S94	on	[cutting board]
S35	cubed
O290	mixing bowl	0
S24	contains	{tomato,onion,cucumber}
M66	pick-and-place	<2:38-2:43>
O290	mixing bowl	0
S24	contains	{tomato,onion,cucumber,sweet pepper,green pepper}
O442	sweet pepper	1
S35	cubed
S73	in	[mixing bowl]
O216	green pepper	1
S35	cubed
S73	in	[mixing bowl]
//
O45	bowl	1
S24	contains	{oregano}
O318	oregano	1
S45	dried
S73	in	[bowl]
O290	mixing bowl	0
S24	contains	{tomato,onion,cucumber,sweet pepper,green pepper}
M70	pour	<2:47-2:48>
O290	mixing bowl	0
S24	contains	{tomato,onion,cucumber,sweet pepper,green pepper,oregano}
O318	oregano	1
S45	dried
S73	in	[mixing bowl]
//
O45	bowl	1
S24	contains	{black olive}
O32	black olive	1
S167	whole
S73	in	[bowl]
O290	mixing bowl	0
S24	contains	{tomato,onion,cucumber,sweet pepper,green pepper,oregano}
M70	pour	<2:51-2:52>
O290	mixing bowl	0
S24	contains	{tomato,onion,cucumber,sweet pepper,green pepper,oregano,black olive}
O32	black olive	1
S167	whole
S73	in	[mixing bowl]
//
O290	mixing bowl	0
S24	contains	{tomato,onion,cucumber,sweet pepper,green pepper,oregano,black olive}
O222	grinder	1
S24	contains	{salt}
O390	salt	1
S106	pieces
S73	in	[grinder]
M49	grind	<2:53-2:57>
O290	mixing bowl	0
S24	contains	{tomato,onion,cucumber,sweet pepper,green pepper,oregano,black olive,salt}
O390	salt	1
S64	granulated
S73	in	[mixing bowl]
//
O290	mixing bowl	0
S24	contains	{tomato,onion,cucumber,sweet pepper,green pepper,oregano,black olive,salt}
O255	lemon	1
S68	halved
O183	fork	1
M107	squeeze	<2:59-3:03>
O290	mixing bowl	0
S24	contains	{tomato,onion,cucumber,sweet pepper,green pepper,oregano,black olive,salt,lemon}
O255	lemon	1
S75	juice
S73	in	[mixing bowl]
//
O45	bowl	1
S24	contains	{olive oil}
O310	olive oil	1
S82	liquid
S73	in	[bowl]
O290	mixing bowl	0
S24	contains	{tomato,onion,cucumber,sweet pepper,green pepper,oregano,black olive,salt,lemon}
M70	pour	<3:06-3:08>
O290	mixing bowl	0
S24	contains	{tomato,onion,cucumber,sweet pepper,green pepper,oregano,black olive,salt,lemon,olive oil}
O310	olive oil	1
S82	liquid
S73	in	[mixing bowl]
//
O290	mixing bowl	0
S24	contains	{tomato,onion,cucumber,sweet pepper,green pepper,oregano,black olive,salt,lemon,olive oil}
O310	olive oil	1
S82	liquid
S73	in	[mixing bowl]
O255	lemon	1
S75	juice
S73	in	[mixing bowl]
O390	salt	1
S64	granulated
S73	in	[mixing bowl]
O32	black olive	1
S167	whole
S73	in	[mixing bowl]
O318	oregano	1
S45	dried
S73	in	[mixing bowl]
O442	sweet pepper	1
S35	cubed
S73	in	[mixing bowl]
O216	green pepper	1
S35	cubed
S73	in	[mixing bowl]
O140	cucumber	1
S43	diced
S73	in	[mixing bowl]
O313	onion	1
S137	sliced
S73	in	[mixing bowl]
O459	tomato	1
S35	cubed
S73	in	[mixing bowl]
O427	spoon	1
S47	empty
M61	mix	<3:09-3:13>
O385	salad	0
S88	mixed
S24	contains	{tomato,onion,cucumber,sweet pepper,green pepper,oregano,black olive,salt,lemon,olive oil}
S73	in	[mixing bowl]
//
O118	container	0
S24	contains	{feta cheese}
O173	feta cheese	0
S35	cubed
S73	in	[container]
O427	spoon	1
S47	empty
M89	scoop	<Assumed>
O427	spoon	1
S24	contains	{feta cheese}
O173	feta cheese	1
S35	cubed
S73	in	[spoon]
//
O290	mixing bowl	0
S24	contains	{tomato,onion,cucumber,sweet pepper,green pepper,oregano,black olive,salt,lemon,olive oil}
O385	salad	0
S88	mixed
S24	contains	{tomato,onion,cucumber,sweet pepper,green pepper,oregano,black olive,salt,lemon,olive oil}
S73	in	[mixing bowl]
O427	spoon	1
S24	contains	{feta cheese}
O173	feta cheese	1
S35	cubed
S73	in	[spoon]
M70	pour	<3:13-3:19>
O290	mixing bowl	0
S24	contains	{salad}
O208	greek salad	0	15
S88	mixed
S73	in	[mixing bowl]
S24	contains	{tomato,onion,cucumber,sweet pepper,green pepper,oregano,black olive,salt,lemon,olive oil,feta cheese}
O385	salad	0
S88	mixed
S73	in	[mixing bowl]
S24	contains	{tomato,onion,cucumber,sweet pepper,green pepper,oregano,black olive,salt,lemon,olive oil,feta cheese}
//
