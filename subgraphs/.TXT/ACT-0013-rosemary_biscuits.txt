O46	box	1
S24	contains	{pancake mix}
O327	pancake mix	1
S73	in	[box]
O282	measuring cup	0
S47	empty
M70	pour	Assumed	Assumed
O282	measuring cup	1
S24	contains	{pancake mix}
O327	pancake mix	1
S73	in	[measuring cup]
//
O282	measuring cup	1
S24	contains	{pancake mix}
O327	pancake mix	1
S73	in	[measuring cup]
O45	bowl	0
S47	empty
M70	pour	0:28	0:30
O45	bowl	0
S24	contains	{pancake mix}
O327	pancake mix	1
S73	in	[bowl]
O282	measuring cup	1
//
O282	measuring cup	1
S24	contains	{sour cream}
O421	sour cream	1
S73	in	[measuring cup]
O45	bowl	0
S24	contains	{pancake mix}
O424	spatula	1
M91	scoop and pour	0:33	0:35
O45	bowl	0
S24	contains	{pancake mix,sour cream}
O421	sour cream	1
S73	in	[bowl]
O282	measuring cup	1
//
O45	bowl	1
S24	contains	{butter}
O61	butter	1
S86	melted
S73	in	[bowl]
O45	bowl	0
S24	contains	{pancake mix,sour cream}
M70	pour	0:37	0:40
O45	bowl	0
S24	contains	{pancake mix,sour cream,butter}
O45	bowl	1
//
O45	bowl	0
S24	contains	{pancake mix,sour cream,butter}
O45	bowl	1
S24	contains	{rosemary}
O383	rosemary	1
S45	dried
S73	in	[bowl]
M70	pour	0:45	0:47
O45	bowl	0
S24	contains	{pancake mix,sour cream,butter,rosemary}
O45	bowl	1
//
O45	bowl	0
S24	contains	{pancake mix,sour cream,butter,rosemary}
O383	rosemary	1
S45	dried
S73	in	[bowl]
O61	butter	1
S86	melted
S73	in	[bowl]
O421	sour cream	1
S73	in	[bowl]
O427	spoon	1
O327	pancake mix	1
S73	in	[bowl]
M61	mix	0:49	0:51
O45	bowl	0
S24	contains	{biscuit dough}
O30	biscuit dough	1
S24	contains	{pancake mix,sour cream,butter,rosemary}
S73	in	[bowl]
//
O121	cooking spray	1
S24	contains	{vegetable oil}
O478	vegetable oil	1
S73	in	[spray bottle]
O299	muffin pan	0
S47	empty
M104	spray	Assumed	Assumed
O299	muffin pan	0
S66	greased	{vegetable oil}
O478	vegetable oil	1
S73	in	[muff]
//
O299	muffin pan	0
S66	greased	{vegetable oil}
O45	bowl	0
S24	contains	{biscuit dough}
O30	biscuit dough	1
S24	contains	{pancake mix,sour cream,butter,rosemary}
S73	in	[bowl]
O402	scoop	1
M91	scoop and pour	0:56	0:58
O299	muffin pan	0
S24	contains	{biscuit dough}
O30	biscuit dough	1
S24	contains	{pancake mix,sour cream,butter,rosemary}
S73	in	[muff]
//
O299	muffin pan	1
S24	contains	{biscuit dough}
O320	oven	0
S94	on
M81	put inside	Assumed	Assumed
O299	muffin pan	1
S24	contains	{biscuit dough}
S73	in	[oven]
O320	oven	0
S94	on
S168	with	[pan]
//
O299	muffin pan	0
S24	contains	{biscuit dough}
S73	in	[oven]
O320	oven	0
S94	on
S168	with	[pan]
O30	biscuit dough	1
S24	contains	{pancake mix,sour cream,butter,rosemary}
S73	in	[muff]
M2	bake	Assumed	Assumed
O299	muffin pan	1
S24	contains	{biscuit}
O29	biscuit	1	0
S2	baked
S24	contains	{pancake mix,sour cream,butter,rosemary}
S73	in	[muff]
//
