# Source:	https://www.youtube.com/watch?v=II2gnuhe0NY
//
O3	apple	1
S167	whole
O115	colander	0
S47	empty
M66	pick-and-place	Assumed	Assumed
O3	apple	1
S167	whole
S73	in	[colander]
O115	colander	0
S24	contains	{apple}
//
O172	faucet	1
S91	off
M123	turn on	Assumed	Assumed
O172	faucet	1
S94	on
O484	water	1
S60	from faucet
//
O172	faucet	0
S94	on
O484	water	0
S59	from	[faucet]
O115	colander	1
S24	contains	{apple}
O3	apple	1
S167	whole
S73	in	[colander]
M126	wash	Assumed	Assumed
O3	apple	1
S167	whole
S162	washed
S73	in	[colander]
//
O172	faucet	1
S94	on
O484	water	1
S60	from faucet
M122	turn off	Assumed	Assumed
O172	faucet	1
S91	off
//
O45	bowl	0
S47	empty
O115	colander	0
S24	contains	{apple}
O3	apple	1
S167	whole
S162	washed
S73	in	[colander]
M66	pick-and-place	Assumed	Assumed
O45	bowl	0
S24	contains	{apple}
O3	apple	1
S167	whole
S73	in	[bowl]
O115	colander	0
//
O45	bowl	0
S24	contains	{apple}
O3	apple	1
S167	whole
S73	in	[bowl]
O146	cutting board	0
S47	empty
S94	on	[surface]
M66	pick-and-place	Assumed	Assumed
O146	cutting board	0
S24	contains	{apple}
S94	on	[surface]
O3	apple	1
S167	whole
S94	on	[cutting board]
//
O3	apple	1
S167	whole
S94	on	[cutting board]
O146	cutting board	0
S24	contains	{apple}
S94	on	[surface]
O341	peeler	1
M65	peel	0:29	0:44
O3	apple	1
S167	whole
S104	peeled
O341	peeler	1
O146	cutting board	0
S94	on	[surface]
//
O3	apple	1
S167	whole
S104	peeled
O146	cutting board	0
S47	empty
S94	on	[surface]
M66	pick-and-place	Assumed	Assumed
O3	apple	1
S167	whole
S104	peeled
S94	on	[cutting board]
O146	cutting board	0
S24	contains	{apple}
S94	on	[surface]
//
O146	cutting board	0
S24	contains	{apple}
O3	apple	0
S104	peeled
S94	on	[cutting board]
O251	knife	1
M25	cut	0:46	0:50
O3	apple	0
S37	cut in pieces
S94	on	[cutting board]
//
O3	apple	0
S37	cut in pieces
S94	on	[cutting board]
O146	cutting board	0
S24	contains	{apple}
O251	knife	1
M25	cut	0:59	1:01
O3	apple	0
S37	cut in pieces
S126	seed removed
S94	on	[cutting board]
//
O3	apple	0
S37	cut in pieces
S94	on	[cutting board]
O146	cutting board	0
S24	contains	{apple}
O251	knife	1
M12	chop	1:05	1:12
O3	apple	0
S17	chopped
S94	on	[cutting board]
O251	knife	1
//
O146	cutting board	0
S24	contains	{apple}
O3	apple	1
S17	chopped
S94	on	[cutting board]
O45	bowl	0
S47	empty
M66	pick-and-place	Assumed	Assumed
O45	bowl	0
S24	contains	{apple}
O3	apple	0
S17	chopped
S73	in	[bowl]
O146	cutting board	0
S47	empty
//
O120	cooking pan	0
S70	heated
S94	on	[stove]
O45	bowl	0
S24	contains	{butter}
O61	butter	1
S148	stick
S73	in	[bowl]
M91	scoop and pour	Assumed	Assumed
O120	cooking pan	0
S24	contains	{butter}
S94	on	[stove]
O61	butter	1
S35	cubed
S73	in	[cooking pan]
//
O120	cooking pan	0
S24	contains	{butter}
S94	on	[stove]
O424	spatula	1
O61	butter	1
S35	cubed
S73	in	[pan]
M52	heat	Assumed	Assumed
O120	cooking pan	0
S24	contains	{butter}
S94	on	[stove]
O61	butter	1
S86	melted
S73	in	[pan]
//
O45	bowl	0
S24	contains	{apple}
O3	apple	0
S17	chopped
S73	in	[bowl]
O120	cooking pan	0
S24	contains	{butter}
S94	on	[stove]
O61	butter	1
S86	melted
S73	in	[pan]
M70	pour	1:27	1:28
O120	cooking pan	0
S24	contains	{butter,apple}
S94	on	[stove]
O3	apple	0
S17	chopped
S73	in	[cooking pan]
//
O45	bowl	0
S24	contains	{salt}
O390	salt	1
S64	granulated
S73	in	[bowl]
O120	cooking pan	0
S24	contains	{butter,apple}
S94	on	[stove]
M106	sprinkle	1:29	1:30
O120	cooking pan	0
S24	contains	{butter,apple,salt}
S94	on	[stove]
O390	salt	1
S64	granulated
S73	in	[cooking pan]
//
O120	cooking pan	0
S24	contains	{butter,apple,salt}
S94	on	[stove]
O390	salt	1
S64	granulated
S73	in	[cooking pan]
O424	spatula	1
M109	stir	1:31	1:35
O390	salt	1
S86	melted
S73	in	[cooking pan]
//
O45	bowl	1
S24	contains	{sugar}
O438	sugar	1
S166	white
S64	granulated
S73	in	[bowl]
O45	bowl	1
S24	contains	{brown sugar}
O53	brown sugar	1
S64	granulated
S73	in	[bowl]
O120	cooking pan	0
S24	contains	{butter,apple,salt}
S94	on	[stove]
M70	pour	1:36	1:40
O120	cooking pan	0
S24	contains	{butter,apple,salt,sugar,brown sugar}
S94	on	[stove]
O438	sugar	1
S166	white
S64	granulated
S73	in	[cooking pan]
O53	brown sugar	1
S64	granulated
S73	in	[cooking pan]
//
O120	cooking pan	0
S24	contains	{butter,apple,salt,sugar,brown sugar}
S94	on	[stove]
O424	spatula	1
M109	stir	1:41	2:00
O120	cooking pan	0
S24	contains	{butter,apple,salt,sugar,brown sugar}
S94	on	[stove]
//
O45	bowl	1
S24	contains	{cinnamon}
O105	cinnamon	1
S73	in	[bowl]
O120	cooking pan	0
S24	contains	{butter,apple,salt,sugar,brown sugar}
S94	on	[stove]
M75	pour and stir	2:01	2:14
O120	cooking pan	0
S24	contains	{butter,apple,salt,sugar,brown sugar,cinnamon}
S94	on	[stove]
O105	cinnamon	1
S73	in	[cooking pan]
//
O120	cooking pan	0
S24	contains	{butter,apple,salt,sugar,brown sugar,cinnamon}
S94	on	[stove]
O45	bowl	1
S24	contains	{water}
O484	water	1
S73	in	[bowl]
M75	pour and stir	2:15	2:36
O120	cooking pan	0
S24	contains	{filling}
S94	on	[stove]
O174	filling	0
S24	contains	{butter,apple,salt,sugar,brown sugar,cinnamon,water}
O484	water	1
S73	in	[cooking pan]
//
O358	plate	0
S47	empty
O120	cooking pan	1
S24	contains	{filling}
S94	on	[stove]
O174	filling	1
S24	contains	{butter,apple,salt,sugar,brown sugar,cinnamon,water}
S73	in	[cooking pan]
M70	pour	2:37	2:28
O358	plate	0
S24	contains	{filling}
O174	filling	1
S24	contains	{butter,apple,salt,sugar,brown sugar,cinnamon,water}
S73	in	[plate]
O120	cooking pan	1
//
O456	tin can	1
S24	contains	{dough}
O152	dough	0
S118	rolled
S73	in	[tin can]
M64	open	Assumed	Assumed
O456	tin can	1
S24	contains	{dough}
S96	opened
//
O447	table	0
S47	empty
O177	flour	1
S73	in	[bowl]
O45	bowl	0
S24	contains	{flour}
M106	sprinkle	Assumed	Assumed
O447	table	0
S24	contains	{flour}
//
O447	table	0
S24	contains	{flour}
O456	tin can	1
S24	contains	{dough}
S96	opened
O152	dough	0
S118	rolled
S73	in	[tin can]
M66	pick-and-place	Assumed	Assumed
O447	table	0
S24	contains	{flour,dough}
O152	dough	0
S118	rolled
S94	on	[table]
//
O447	table	0
S24	contains	{flour,dough}
O152	dough	0
S118	rolled
S94	on	[table]
O382	rolling pin	1
M86	roll	3:00	3:10
O152	dough	0
S52	flattened
S94	on	[table]
//
O427	spoon	1
S47	empty
O358	plate	0
S24	contains	{filling}
O174	filling	1
S24	contains	{butter,apple,salt,sugar,brown sugar,cinnamon,water}
S73	in	[plate]
M89	scoop	Assumed	Assumed
O427	spoon	1
S24	contains	{filling}
O174	filling	1
S24	contains	{butter,apple,salt,sugar,brown sugar,cinnamon,water}
S94	on	[spoon]
//
O447	table	0
S24	contains	{flour,dough}
O152	dough	0
S52	flattened
S94	on	[table]
O427	spoon	1
S24	contains	{filling}
O174	filling	1
S24	contains	{butter,apple,salt,sugar,brown sugar,cinnamon,water}
S94	on	[spoon]
M70	pour	3:17	3:21
O152	dough	0
S52	flattened
S24	contains	{filling}
S94	on	[table]
O174	filling	1
S24	contains	{butter,apple,salt,sugar,brown sugar,cinnamon,water}
S94	on	{dough}
//
O152	dough	1
S52	flattened
S24	contains	{filling}
S94	on	[table]
M44	fold and pinch	3:23	3:34
O152	dough	1
S52	flattened
S24	contains	{filling}
S57	folded
S94	on	[table]
//
O152	dough	1
S52	flattened
S24	contains	{filling}
S57	folded
S94	on	[table]
M43	fold	3:35	4:14
O5	apple pie	1
S155	uncooked
S24	contains	{dough,filling}
S94	on	[table]
//
O5	apple pie	0
S155	uncooked
S24	contains	{dough,filling}
S94	on	[table]
O13	baking tray	0
S47	empty
O404	scraper	1
M66	pick-and-place	4:17	4:18
O13	baking tray	0
S24	contains	{apple pie}
O5	apple pie	0
S155	uncooked
S24	contains	{dough,filling}
S94	on	[baking tray]
O404	scraper	1
//
O159	egg	1
S156	uncracked
O45	bowl	0
S47	empty
M24	crack	Assumed	Assumed
O45	bowl	0
S24	contains	{egg white and yolk}
O166	egg white and yolk	1
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{egg white and yolk}
O166	egg white and yolk	1
S73	in	[bowl]
O183	fork	1
M61	mix	Assumed	Assumed
O45	bowl	0
S24	contains	{egg white and yolk}
O166	egg white and yolk	1
S6	beaten
S73	in	[bowl]
//
O247	jug	1
S24	contains	{milk}
O285	milk	1
S73	in	[jug]
O45	bowl	0
S24	contains	{egg white and yolk}
O166	egg white and yolk	1
S6	beaten
S73	in	[bowl]
M70	pour	Assumed	Assumed
O45	bowl	0
S24	contains	{egg wash}
O164	egg wash	0
S24	contains	{egg white and yolk}
S73	in	[bowl]
O285	milk	1
S73	in	[bowl]
//
O56	brush	1
O45	bowl	0
S24	contains	{egg wash}
O164	egg wash	0
S24	contains	{egg white and yolk}
S73	in	[bowl]
M34	dip	Assumed	Assumed
O56	brush	1
S24	contains	{egg wash}
O164	egg wash	0
S24	contains	{egg white and yolk}
S94	on	[brush]
//
O56	brush	1
S24	contains	{egg wash}
O164	egg wash	0
S24	contains	{egg white and yolk}
S94	on	[brush]
O5	apple pie	0
S155	uncooked
S24	contains	{dough,filling}
S94	on	[baking tray]
O13	baking tray	0
S24	contains	{apple pie}
M10	brush	4:19	4:26
O164	egg wash	0
S24	contains	{egg white and yolk}
S94	on	[apple pie]
O5	apple pie	0
S155	uncooked
S24	contains	{dough,filling,egg wash}
S94	on	[baking tray]
//
O5	apple pie	0
S155	uncooked
S24	contains	{dough,filling,egg wash}
S94	on	[baking try]
O13	baking tray	0
S24	contains	{apple pie}
O45	bowl	1
S24	contains	{sugar}
O438	sugar	1
S166	white
S64	granulated
S73	in	[bowl]
M106	sprinkle	4:28	4:33
O5	apple pie	0
S155	uncooked
S24	contains	{dough,filling,egg wash,sugar}
S94	on	[baking tray]
O438	sugar	1
S166	white
S64	granulated
S94	on	[apple pie]
//
O251	knife	1
O5	apple pie	0
S155	uncooked
S24	contains	{dough,filling,egg wash,sugar}
S94	on	[baking tray]
O13	baking tray	0
S24	contains	{apple pie}
M69	poke	4:35	4:49
O5	apple pie	0
S155	uncooked
S24	contains	{dough,filling,egg wash,sugar}
S108	poked
S94	on	[baking tray]
O251	knife	1
//
O320	oven	1
S91	off
M123	turn on	Assumed	Assumed
O320	oven	1
S94	on
//
O320	oven	1
S94	on
M77	preheat	Assumed	Assumed
O320	oven	1
S94	on
S111	preheated
//
O320	oven	0
S94	on
S111	preheated
O5	apple pie	0
S155	uncooked
S24	contains	{dough,filling,egg wash,sugar}
S108	poked
S94	on	[baking tray]
O13	baking tray	0
S24	contains	{apple pie}
M66	pick-and-place	Assumed	Assumed
O320	oven	0
S94	on
S111	preheated
S24	contains	{baking tray}
O13	baking tray	0
S24	contains	{apple pie}
S73	in	[oven]
//
O320	oven	0
S94	on
S111	preheated
S24	contains	{baking tray}
O13	baking tray	0
S24	contains	{apple pie}
S73	in	[oven]
O5	apple pie	0
S155	uncooked
S24	contains	{dough,filling,egg wash,sugar}
S108	poked
S94	on	[baking tray]
M2	bake	Assumed	Assumed
O5	apple pie	0
S25	cooked
S94	on	[baking tray]
O320	oven	0
S94	on
S24	contains	{baking tray}
//
O320	oven	0
S94	on
S24	contains	{apple pie}
O5	apple pie	0
S25	cooked
S94	on	[baking tray]
O13	baking tray	0
S24	contains	{apple pie}
S73	in	[oven]
M82	remove	Assumed	Assumed
O320	oven	0
S91	off
O13	baking tray	0
S24	contains	{apple pie}
//
O5	apple pie	0
S25	cooked
S94	on	[baking tray]
O13	baking tray	0
S24	contains	{apple pie}
O358	plate	0
S47	empty
O463	tongs	1
M66	pick-and-place	Assumed	Assumed
O358	plate	0
S24	contains	{apple pie}
O5	apple pie	0	6
S25	cooked
S24	contains	{butter,apple,salt,sugar,brown sugar,cinnamon,water,dough,egg wash}
S94	on	[plate]
//
