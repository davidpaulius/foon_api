O146	cutting board	0
S24	contains	{onion}
O251	knife	1
O313	onion	0
S104	peeled
S94	on	[cutting board]
M102	slice	0:33	0:36
O313	onion	0
S137	sliced
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{sweet pepper}
O251	knife	1
O442	sweet pepper	0
S167	whole
S94	on	[cutting board]
M102	slice	0:39	0:46
O442	sweet pepper	0
S137	sliced
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{sweet pepper}
O442	sweet pepper	1
S137	sliced
S94	on	[cutting board]
O45	bowl	0
S47	empty
M66	pick-and-place	Assumed	Assumed
O146	cutting board	0
O45	bowl	0
S24	contains	{sweet pepper}
O442	sweet pepper	1
S137	sliced
S73	in	[bowl]
//
O146	cutting board	0
S24	contains	{garlic}
O251	knife	1
O194	garlic	1
S104	peeled
S94	on	[cutting board]
M12	chop	0:47	0:50
O194	garlic	0
S17	chopped
S94	on	[cutting board]
//
O463	tongs	1
O120	cooking pan	0
S70	heated
S94	on	[stove]
O396	sausage	1
S155	uncooked
S73	in	[packet]
M66	pick-and-place	0:51	0:58
O120	cooking pan	0
S24	contains	{sausage}
S94	on	[stove]
O396	sausage	1
S155	uncooked
S73	in	[pan]
//
O120	cooking pan	0
S24	contains	{sausage}
S94	on	[stove]
O463	tongs	1
O396	sausage	1
S155	uncooked
S73	in	[pan]
M40	flip	1:00	1:03
O396	sausage	1
S99	partly cooked
S73	in	[pan]
//
O120	cooking pan	0
S24	contains	{sausage}
S94	on	[stove]
O463	tongs	1
O396	sausage	1
S99	partly cooked
S73	in	[pan]
O146	cutting board	0
S47	empty
M66	pick-and-place	Assumed	Assumed
O120	cooking pan	0
S124	seasoned
S94	on	[stove]
O146	cutting board	0
S24	contains	{sausage}
O396	sausage	1
S99	partly cooked
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{sausage}
O396	sausage	0
S99	partly cooked
S94	on	[cutting board]
O251	knife	1
M25	cut	1:04	1:10
O396	sausage	0
S17	chopped
S99	partly cooked
S94	on	[cutting board]
//
O120	cooking pan	0
S124	seasoned
S94	on	[stove]
O45	bowl	1
S24	contains	{butter}
O61	butter	1
S35	cubed
S73	in	[bowl]
M70	pour	1:15	1:17
O120	cooking pan	0
S66	greased
S24	contains	{butter}
S94	on	[stove]
O61	butter	1
S35	cubed
S73	in	[pan]
//
O120	cooking pan	0
S66	greased
S24	contains	{butter}
S94	on	[stove]
O146	cutting board	1
S24	contains	{onion}
O427	spoon	1
O313	onion	1
S137	sliced
S94	on	[cutting board]
M70	pour	1:19	1:25
O120	cooking pan	0
S24	contains	{butter,onion}
S94	on	[stove]
O313	onion	1
S137	sliced
S73	in	[pan]
O146	cutting board	1
//
O120	cooking pan	0
S24	contains	{butter,onion}
S94	on	[stove]
O146	cutting board	1
S24	contains	{garlic}
O194	garlic	1
S17	chopped
S94	on	[cutting board]
M70	pour	1:26	1:28
O120	cooking pan	0
S24	contains	{butter,onion,garlic}
S94	on	[stove]
O146	cutting board	1
O194	garlic	1
S17	chopped
S73	in	[pan]
//
O120	cooking pan	0
S24	contains	{butter,onion,garlic}
S94	on	[stove]
O427	spoon	1
O194	garlic	1
S17	chopped
S73	in	[pan]
O313	onion	1
S137	sliced
S73	in	[pan]
M20	cook and stir	1:29	1:33
O120	cooking pan	0
S24	contains	{butter,onion,garlic}
S94	on	[stove]
S99	partly cooked
O194	garlic	1
S58	fried
S73	in	[pan]
O313	onion	1
S58	fried
S73	in	[pan]
//
O120	cooking pan	0
S24	contains	{butter,onion,garlic}
S94	on	[stove]
S99	partly cooked
O45	bowl	1
S24	contains	{sweet pepper}
O442	sweet pepper	1
S137	sliced
S73	in	[bowl]
M70	pour	1:34	1:42
O120	cooking pan	0
S24	contains	{butter,onion,garlic,sweet pepper}
S94	on	[stove]
O442	sweet pepper	1
S137	sliced
S73	in	[pan]
O45	bowl	1
//
O120	cooking pan	0
S24	contains	{butter,onion,garlic,sweet pepper}
S94	on	[stove]
O442	sweet pepper	1
S137	sliced
S73	in	[pan]
O427	spoon	1
M109	stir	1:42	1:45
O120	cooking pan	0
S24	contains	{butter,onion,garlic,sweet pepper}
S94	on	[stove]
O442	sweet pepper	1
S58	fried
S73	in	[pan]
//
O45	bowl	1
S24	contains	{basil}
O17	basil	1
S45	dried
S73	in	[bowl]
O120	cooking pan	0
S24	contains	{butter,onion,garlic,sweet pepper}
S94	on	[stove]
M106	sprinkle	1:47	1:50
O120	cooking pan	0
S24	contains	{butter,onion,garlic,sweet pepper,basil}
S94	on	[stove]
//
O45	bowl	1
S24	contains	{oregano}
O318	oregano	1
S45	dried
S73	in	[bowl]
O120	cooking pan	0
S24	contains	{butter,onion,garlic,sweet pepper,basil}
S94	on	[stove]
M106	sprinkle	1:47	1:50
O120	cooking pan	0
S24	contains	{butter,onion,garlic,sweet pepper,basil,oregano}
S94	on	[stove]
O318	oregano	1
S45	dried
S73	in	[pan]
//
O120	cooking pan	0
S24	contains	{butter,onion,garlic,sweet pepper,basil,oregano}
S94	on	[stove]
O427	spoon	1
M109	stir	1:53:50	1:56
O120	cooking pan	0
S24	contains	{butter,onion,garlic,sweet pepper,basil,oregano}
S94	on	[stove]
//
O282	measuring cup	0
S47	empty
O44	bottle	1
S24	contains	{white wine}
O496	white wine	1
S73	in	[bottle]
M70	pour	Assumed	Assumed
O282	measuring cup	0
S24	contains	{white wine}
O496	white wine	1
S73	in	[measuring cup]
//
O120	cooking pan	0
S24	contains	{butter,onion,garlic,sweet pepper,basil,oregano}
S94	on	[stove]
O282	measuring cup	1
S24	contains	{white wine}
O496	white wine	1
S73	in	[measuring cup]
M70	pour	1:59	2:02
O120	cooking pan	0
S24	contains	{butter,onion,garlic,sweet pepper,basil,oregano,white wine}
S94	on	[stove]
//
O120	cooking pan	0
S24	contains	{butter,onion,garlic,sweet pepper,basil,oregano,white wine}
S94	on	[stove]
O427	spoon	1
M109	stir	2:03	2:09
O120	cooking pan	0
S24	contains	{butter,onion,garlic,sweet pepper,basil,oregano,white wine}
S94	on	[stove]
//
O120	cooking pan	0
S24	contains	{butter,onion,garlic,sweet pepper,basil,oregano,white wine}
S94	on	[stove]
O146	cutting board	1
S24	contains	{sausage}
O396	sausage	1
S99	partly cooked
S17	chopped
S94	on	[cutting board]
O427	spoon	1
M93	scrape	2:10	2:14
O120	cooking pan	0
S24	contains	{butter,onion,garlic,sweet pepper,basil,oregano,white wine,sausage}
S94	on	[stove]
O396	sausage	1
S17	chopped
S99	partly cooked
S73	in	[pan]
O146	cutting board	1
//
O120	cooking pan	0
S24	contains	{butter,onion,garlic,sweet pepper,basil,oregano,white wine,sausage}
S94	on	[stove]
O427	spoon	1
O396	sausage	1
S17	chopped
S99	partly cooked
S73	in	[pan]
M20	cook and stir	2:17	2:19
O120	cooking pan	0
S24	contains	{italian sausage}
S94	on	[stove]
O241	italian sausage	1
S25	cooked
S24	contains	{butter,onion,garlic,sweet pepper,basil,oregano,white wine,sausage}
S73	in	[pan]
//
O120	cooking pan	0
S24	contains	{italian sausage}
S94	on	[stove]
O241	italian sausage	1
S25	cooked
S24	contains	{butter,onion,garlic,sweet pepper,basil,oregano,white wine,sausage}
S73	in	[pan]
O358	plate	0
S47	empty
O427	spoon	1
M91	scoop and pour	Assumed	Assumed
O358	plate	0
S24	contains	{italian sausage}
O241	italian sausage	1	29
S25	cooked
S24	contains	{sweet pepper,basil,oregano,white wine,sausage}
S94	on	[plate]
//
