# Source:	https://www.youtube.com/watch?v=l89y2jme4Ls
# Source:	http://foonets.com/foon_subgraphs/subgraphs/0063-plain_omelette_mahalo.mp4
//
O435	stove	0
S91	off
S157	under	[frying pan]
M123	turn on	<0:23-0:25>
O435	stove	0
S94	on
S157	under	[frying pan]
//
O190	frying pan	0
S47	empty
S94	on	[stove]
O435	stove	0
S91	off
S157	under	[frying pan]
M77	preheat	<Assumed>
O190	frying pan	0
S72	hot
S47	empty
S94	on	[stove]
//
O190	frying pan	0
S72	hot
S47	empty
S94	on	[stove]
O45	bowl	1
S24	contains	{olive oil}
O310	olive oil	1
S82	liquid
S73	in	[bowl]
M70	pour	<0:33-0:36>
O190	frying pan	0
S72	hot
S24	contains	{olive oil}
S94	on	[stove]
O310	olive oil	1
S82	liquid
S73	in	[frying pan]
//
O190	frying pan	0
S72	hot
S24	contains	{olive oil}
S94	on	[stove]
O45	bowl	0
S24	contains	{butter}
O61	butter	1
S140	softened
S159	unmelted
S35	cubed
S73	in	[bowl]
M70	pour	<0:44-0:46>
O190	frying pan	0
S72	hot
S24	contains	{olive oil,butter}
S94	on	[stove]
O61	butter	1
S140	softened
S159	unmelted
S35	cubed
S73	in	[frying pan]
//
O45	bowl	0
S47	empty
O159	egg	1
S167	whole
M24	crack	<Assumed>
O45	bowl	0
S24	contains	{egg white,egg yolk}
O165	egg white	0
S155	uncooked
S73	in	[bowl]
O167	egg yolk	0
S155	uncooked
S73	in	[bowl]
//
O290	mixing bowl	0
S47	empty
O45	bowl	0
S24	contains	{egg white,egg yolk}
O165	egg white	0
S155	uncooked
S73	in	[bowl]
O167	egg yolk	0
S155	uncooked
S73	in	[bowl]
M70	pour	<0:50-0:52>
O290	mixing bowl	0
S24	contains	{egg white,egg yolk}
O165	egg white	0
S155	uncooked
S73	in	[mixing bowl]
O167	egg yolk	0
S155	uncooked
S73	in	[mixing bowl]
//
O290	mixing bowl	0
S24	contains	{egg white,egg yolk}
O45	bowl	0
S24	contains	{salt}
O390	salt	1
S64	granulated
S73	in	[bowl]
M106	sprinkle	<0:58-1:00>
O290	mixing bowl	0
S24	contains	{egg white,egg yolk,salt}
O390	salt	1
S64	granulated
S73	in	[mixing bowl]
//
O290	mixing bowl	0
S24	contains	{egg white,egg yolk,salt}
O45	bowl	0
S24	contains	{black pepper}
O33	black pepper	1
S67	ground
S73	in	[bowl]
M106	sprinkle	<1:01-1:04>
O290	mixing bowl	0
S24	contains	{egg white,egg yolk,salt,black pepper}
O33	black pepper	1
S67	ground
S73	in	[mixing bowl]
//
O290	mixing bowl	0
S24	contains	{egg white,egg yolk,salt,black pepper}
O390	salt	1
S64	granulated
S73	in	[mixing bowl]
O33	black pepper	1
S67	ground
S73	in	[mixing bowl]
O165	egg white	0
S155	uncooked
S73	in	[mixing bowl]
O167	egg yolk	0
S155	uncooked
S73	in	[mixing bowl]
O491	whisk	1
M127	whisk	<1:06-1:25>
O290	mixing bowl	0
S24	contains	{egg,salt,black pepper}
O159	egg	0
S6	beaten
S155	uncooked
S24	contains	{black pepper,salt}
S73	in	[mixing bowl]
//
O190	frying pan	0
S72	hot
S24	contains	{olive oil,butter}
S94	on	[stove]
O61	butter	1
S140	softened
S159	unmelted
S35	cubed
S73	in	[frying pan]
M105	spread	<1:29-1:48>
O61	butter	1
S86	melted
S82	liquid
S73	in	[frying pan]
//
O290	mixing bowl	1
S24	contains	{egg,salt,black pepper}
O159	egg	1
S6	beaten
S155	uncooked
S24	contains	{black pepper,salt}
S73	in	[mixing bowl]
O190	frying pan	0
S72	hot
S24	contains	{olive oil,butter}
S94	on	[stove]
O310	olive oil	0
S82	liquid
S73	in	[frying pan]
O61	butter	1
S86	melted
S82	liquid
S73	in	[frying pan]
M70	pour	<1:54-1:57>
O190	frying pan	0
S72	hot
S24	contains	{olive oil,butter,egg}
S94	on	[stove]
O159	egg	1
S6	beaten
S155	uncooked
S24	contains	{black pepper,salt}
S73	in	[frying pan]
//
O435	stove	0
S94	on
S157	under	[frying pan]
O190	frying pan	0
S72	hot
S24	contains	{olive oil,butter,egg}
S94	on	[stove]
O159	egg	0
S6	beaten
S155	uncooked
S24	contains	{black pepper,salt}
S73	in	[frying pan]
O424	spatula	1
M20	cook and stir	<2:09-2:12,2:13-2:20,2:21-2:28>
O311	omelette	0
S88	mixed
S99	partly cooked
S24	contains	{egg,black pepper,salt}
S73	in	[frying pan]
//
O190	frying pan	0
S72	hot
S24	contains	{olive oil,butter,egg}
S94	on	[stove]
O311	omelette	0
S88	mixed
S99	partly cooked
S24	contains	{egg,black pepper,salt}
S73	in	[frying pan]
O474	turner	1
M43	fold	<3:34-3:36>
O311	omelette	0
S57	folded
S99	partly cooked
S24	contains	{egg,black pepper,salt}
S73	in	[frying pan]
//
O435	stove	0
S94	on
S157	under	[frying pan]
O190	frying pan	0
S72	hot
S24	contains	{olive oil,butter,egg}
S94	on	[stove]
O311	omelette	0
S57	folded
S99	partly cooked
S24	contains	{egg,black pepper,salt}
S73	in	[frying pan]
M16	cook	<Assumed>
O190	frying pan	0
S72	hot
S24	contains	{omelette}
S94	on	[stove]
O311	omelette	0
S57	folded
S25	cooked
S24	contains	{egg,black pepper,salt}
S73	in	[frying pan]
//
O190	frying pan	0
S72	hot
S24	contains	{omelette}
S94	on	[stove]
O311	omelette	1
S57	folded
S25	cooked
S24	contains	{egg,black pepper,salt}
S73	in	[frying pan]
O358	plate	0
S47	empty
O474	turner	1
M66	pick-and-place	<4:13-4:16>
O358	plate	0
S24	contains	{omelette}
O311	omelette	1	23
S57	folded
S25	cooked
S24	contains	{egg,black pepper,salt}
S94	on	[plate]
//
