# Source:	https://www.youtube.com/watch?v=CMoxSgxWIIg
# Source:	http://foonets.com/foon_subgraphs/subgraphs/0015-garlic_bread-student-mealz.mp4
//
O47	bread	1
S167	whole
S83	loaf
O146	cutting board	0
M66	pick-and-place	<Assumed>
O146	cutting board	0
S24	contains	{bread}
O47	bread	1
S167	whole
S83	loaf
S94	on	[cutting board]
//
O47	bread	1
S167	whole
S83	loaf
S94	on	[cutting board]
O251	knife	1
M102	slice	<0:42,0:48>
O47	bread	0
S94	on	[cutting board]
S137	sliced
//
O146	cutting board	0
S24	contains	{bread}
O47	bread	1
S94	on	[cutting board]
S137	sliced
O358	plate	0
S47	empty
M66	pick-and-place	<0:48,0:51>
O358	plate	0
S24	contains	{bread}
O47	bread	1
S94	on	[plate]
S137	sliced
O146	cutting board	0
S47	empty
//
O146	cutting board	0
S47	empty
O331	parsley	1
S146	stem
M66	pick-and-place	<Assumed>
O146	cutting board	0
S24	contains	{parsley}
O331	parsley	1
S94	on	[cutting board]
S146	stem
//
O331	parsley	0
S94	on	[cutting board]
S146	stem
O251	knife	1
M12	chop	<0:54,1:02>
O331	parsley	0
S17	chopped
S94	on	[cutting board]
//
O146	cutting board	1
S24	contains	{parsley}
O331	parsley	1
S17	chopped
S94	on	[cutting board]
O45	bowl	0
S24	contains	{butter}
M73	pour and scrape	<1:08,1:12>
O45	bowl	0
S24	contains	{butter,parsley}
O331	parsley	1
S17	chopped
S73	in	[bowl]
O146	cutting board	1
S47	empty
//
O198	garlic press	1
S47	empty
S20	closed
M64	open	<Assumed>
O198	garlic press	1
S47	empty
S96	opened
//
O198	garlic press	0
S47	empty
S96	opened
O194	garlic	1
S21	clove
S104	peeled
M53	insert	<Assumed>
O198	garlic press	0
S96	opened
S24	contains	{garlic}
O194	garlic	1
S21	clove
S73	in	[garlic press]
S104	peeled
//
O45	bowl	0
S24	contains	{butter,parsley}
O198	garlic press	1
S24	contains	{garlic}
S96	opened
O194	garlic	1
S21	clove
S73	in	[garlic press]
S104	peeled
O251	knife	1
M108	squeeze and scrape	<Assumed>
O45	bowl	0
S24	contains	{butter,garlic,parsley}
O194	garlic	1
S73	in	[mixing bowl]
S87	minced
O198	garlic press	1
S20	closed
//
O45	bowl	0
S24	contains	{butter,garlic,parsley}
O409	shaker	1
S24	contains	{salt}
O390	salt	1
S73	in	[shaker]
S64	granulated
M106	sprinkle	<1:27,1:30>
O45	bowl	0
S24	contains	{butter,garlic,parsley,salt}
O390	salt	1
S73	in	[bowl]
S64	granulated
//
O45	bowl	0
S24	contains	{butter,garlic,parsley,salt}
O344	pepper mill	1
S24	contains	{black pepper}
O33	black pepper	1
S167	whole
S73	in	[pepper mill]
S105	peppercorn
M49	grind	<1:31,1:34>
O45	bowl	0
S24	contains	{black pepper,butter,garlic,parsley,salt}
O33	black pepper	1
S73	in	[bowl]
S67	ground
//
O45	bowl	0
S24	contains	{black pepper,butter,garlic,parsley,salt}
O61	butter	0
S140	softened
S148	stick
S73	in	[bowl]
O331	parsley	1
S17	chopped
S73	in	[bowl]
O194	garlic	1
S87	minced
S73	in	[bowl]
O390	salt	1
S73	in	[bowl]
S64	granulated
O33	black pepper	1
S73	in	[bowl]
S67	ground
O427	spoon	1
M61	mix	<1:35,1:38:50>
O196	garlic butter	0
S88	mixed
S73	in	[bowl]
S24	contains	{black pepper,butter,garlic,parsley,salt}
//
O46	box	1
S24	contains	{parchment paper}
O329	parchment paper	1
S131	sheet
S73	in	[box]
O13	baking tray	0
S47	empty
M117	tear and place	<Assumed>
O13	baking tray	0
S47	empty
S168	with	[parchment paper]
O329	parchment paper	1
S131	sheet
S94	on	[baking tray]
//
O45	bowl	0
S24	contains	{black pepper,butter,garlic,parsley,salt}
O196	garlic butter	1
S88	mixed
S73	in	[bowl]
S24	contains	{black pepper,butter,garlic,parsley,salt}
O358	plate	0
S24	contains	{bread}
O47	bread	1
S94	on	[plate]
S137	sliced
O251	knife	1
M105	spread	<1:43,1:48>
O47	bread	0
S24	contains	{garlic butter}
S137	sliced
O196	garlic butter	1
S88	mixed
S94	on	[bread]
S24	contains	{black pepper,butter,garlic,parsley,salt}
//
O13	baking tray	0
S47	empty
S168	with	[parchment paper]
O47	bread	0
S24	contains	{garlic butter}
S137	sliced
M66	pick-and-place	<1:48,1:50>
O13	baking tray	0
S168	with	[parchment paper]
S24	contains	{bread}
O47	bread	0
S24	contains	{garlic butter}
S94	on	[baking tray]
S137	sliced
//
O320	oven	0
S47	empty
S94	on
O13	baking tray	0
S168	with	[parchment paper]
S24	contains	{bread}
M81	put inside	<2:06:50,2:10:50>
O320	oven	0
S24	contains	{baking tray}
S94	on
O13	baking tray	0
S168	with	[parchment paper]
S73	in	[oven]
S24	contains	{bread}
//
O320	oven	0
S24	contains	{baking tray}
S94	on
O13	baking tray	0
S168	with	[parchment paper]
S73	in	[oven]
S24	contains	{bread}
O47	bread	0
S24	contains	{garlic butter}
S94	on	[baking tray]
S137	sliced
O196	garlic butter	1
S88	mixed
S94	on	[bread]
S24	contains	{black pepper,butter,garlic,parsley,salt}
M2	bake	<Assumed>
O195	garlic bread	0
S2	baked
S24	contains	{bread,black pepper,butter,garlic,parsley,salt}
S94	on	[baking tray]
//
O195	garlic bread	1
S2	baked
S24	contains	{bread,black pepper,butter,garlic,parsley,salt}
S94	on	[baking tray]
O358	plate	0
S47	empty
O463	tongs	1
M66	pick-and-place	<Assumed>
O13	baking tray	0
S47	empty
O195	garlic bread	1	0
S2	baked
S24	contains	{bread,black pepper,butter,garlic,parsley,salt}
S94	on	[plate]
S115	ready
//
