# Source:	https://www.youtube.com/watch?v=DkxxITuXZoE
# Source:	http://foonets.com/foon_subgraphs/subgraphs/0034-mushroom_soup.mp4
//
O360	pot	0
S72	hot
S47	empty
S94	on	[stove]
O358	plate	1
S24	contains	{butter}
O61	butter	1
S140	softened
S159	unmelted
S34	cube
S94	on	[plate]
M66	pick-and-place	<1:00-1:03>
O360	pot	0
S94	on	[stove]
S72	hot
S24	contains	{butter}
O61	butter	1
S140	softened
S159	unmelted
S34	cube
S73	in	[pot]
//
O360	pot	0
S72	hot
S24	contains	{butter}
S94	on	[stove]
O61	butter	0
S140	softened
S159	unmelted
S34	cube
S73	in	[pot]
O427	spoon	1
M105	spread	<1:05-1:10>
O61	butter	0
S82	liquid
S86	melted
S73	in	[pot]
//
O322	packet	0
S24	contains	{mushroom}
O301	mushroom	1
S167	whole
S73	in	[packet]
O146	cutting board	0
S47	empty
M66	pick-and-place	<Assumed
O301	mushroom	1
S167	whole
S94	on	[cutting board]
O146	cutting board	0
S24	contains	{mushroom}
//
O301	mushroom	0
S167	whole
S94	on	[cutting board]
O251	knife	1
M102	slice	<Assumed>
O301	mushroom	0
S137	sliced
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{mushroom}
O301	mushroom	1
S137	sliced
S94	on	[cutting board]
O45	bowl	0
S47	empty
M66	pick-and-place	<Assumed>
O146	cutting board	0
S47	empty
O45	bowl	0
S24	contains	{mushroom}
O301	mushroom	1
S137	sliced
S73	in	[bowl]
//
O360	pot	0
S72	hot
S24	contains	{butter}
S94	on	[stove]
O45	bowl	0
S24	contains	{onion}
O313	onion	1
S17	chopped
S73	in	[bowl]
O427	spoon	1
M93	scrape	<1:11-1:15>
O45	bowl	1
S47	empty
O313	onion	1
S17	chopped
S73	in	[pot]
O360	pot	0
S72	hot
S24	contains	{butter,onion}
S94	on	[stove]
//
O435	stove	0
S94	on
S157	under	[pot]
O360	pot	0
S72	hot
S24	contains	{butter,onion}
S94	on	[stove]
O313	onion	1
S17	chopped
S73	in	[pot]
O61	butter	0
S82	liquid
S86	melted
S73	in	[pot]
O427	spoon	1
M20	cook and stir	<1:15-1:19:50>
O313	onion	1
S17	chopped
S122	sauteed
S73	in	[pot]
//
O360	pot	0
S72	hot
S24	contains	{butter,onion}
S94	on	[stove]
O45	bowl	1
S24	contains	{mushroom}
O301	mushroom	1
S73	in	[bowl]
S137	sliced
M70	pour	<1:20-1:22>
O360	pot	0
S72	hot
S24	contains	{butter,mushroom,onion}
S94	on	[stove]
O301	mushroom	1
S137	sliced
S73	in	[pot]
//
O360	pot	0
S72	hot
S24	contains	{mushroom,onion}
S94	on	[stove]
O61	butter	0
S82	liquid
S86	melted
S73	in	[pot]
O301	mushroom	1
S137	sliced
S73	in	[pot]
O427	spoon	1
M20	cook and stir	<1:25-1:32>
O301	mushroom	1
S137	sliced
S122	sauteed
S73	in	[pot]
//
O73	carton	1
S24	contains	{chicken broth}
O84	chicken broth	1
S82	liquid
S73	in	[carton]
O282	measuring cup	0
S47	empty
M70	pour	<Assumed>
O282	measuring cup	0
S24	contains	{chicken broth}
O84	chicken broth	1
S82	liquid
S73	in	[measuring cup]
//
O282	measuring cup	0
S24	contains	{chicken broth}
O45	bowl	1
S24	contains	{flour}
O177	flour	1
S110	powder
S73	in	[bowl]
M70	pour	<1:41-1:43>
O282	measuring cup	0
S24	contains	{chicken broth,flour}
O177	flour	1
S110	powder
S73	in	[measuring cup]
//
O282	measuring cup	0
S24	contains	{chicken broth,flour}
O177	flour	0
S110	powder
S73	in	[measuring cup]
O84	chicken broth	0
S82	liquid
S73	in	[measuring cup]
O491	whisk	1
M61	mix	<1:43-1:50>
O52	broth	0
S89	mixture
S82	liquid
S24	contains	{chicken broth,flour}
S73	in	[measuring cup]
//
O360	pot	0
S72	hot
S24	contains	{butter,onion,mushroom}
S94	on	[stove]
O282	measuring cup	1
S24	contains	{chicken broth,flour}
O52	broth	0
S88	mixed
S82	liquid
S24	contains	{chicken broth,flour}
S73	in	[measuring cup]
M70	pour	<1:55-1:58>
O360	pot	0
S72	hot
S24	contains	{butter,onion,mushroom,chicken broth,flour}
S94	on	[stove]
O52	broth	0
S88	mixed
S82	liquid
S24	contains	{chicken broth,flour}
S73	in	[pot]
O282	measuring cup	1
S47	empty
//
O360	pot	0
S72	hot
S24	contains	{butter,onion,mushroom,chicken broth,flour}
S94	on	[stove]
O52	broth	0
S88	mixed
S82	liquid
S24	contains	{chicken broth,flour}
S73	in	[pot]
O313	onion	0
S17	chopped
S122	sauteed
S73	in	[pot]
O301	mushroom	0
S137	sliced
S122	sauteed
S73	in	[pot]
O427	spoon	1
M20	cook and stir	<2:01-2:08>
O418	soup	0
S152	thick liquid
S24	contains	{butter,onion,mushroom,chicken broth,flour}
S73	in	[pot]
//
O360	pot	0
S72	hot
S24	contains	{butter,onion,mushroom,chicken broth,flour}
S94	on	[stove]
O282	measuring cup	1
S24	contains	{half-and-half}
O228	half-and-half	1
S82	liquid
S73	in	[measuring cup]
M70	pour	<2:21-2:23>
O360	pot	0
S72	hot
S24	contains	{butter,onion,mushroom,chicken broth,flour,half-and-half}
S94	on	[stove]
O282	measuring cup	1
S47	empty
O228	half-and-half	1
S82	liquid
S73	in	[pot]
//
O360	pot	0
S72	hot
S24	contains	{butter,onion,mushroom,chicken broth,flour,half-and-half}
S94	on	[stove]
O45	bowl	1
S24	contains	{salt}
O390	salt	1
S64	granulated
S73	in	[bowl]
M70	pour	<2:26-2:28>
O360	pot	0
S72	hot
S24	contains	{butter,onion,mushroom,chicken broth,flour,half-and-half,salt}
S94	on	[stove]
O390	salt	1
S64	granulated
S73	in	[pot]
//
O360	pot	0
S72	hot
S24	contains	{butter,onion,mushroom,chicken broth,flour,half-and-half,salt}
S94	on	[stove]
O45	bowl	1
S24	contains	{black pepper}
O33	black pepper	1
S67	ground
S73	in	[bowl]
M70	pour	<2:26-2:28>
O360	pot	0
S72	hot
S24	contains	{butter,onion,mushroom,chicken broth,flour,half-and-half,salt,black pepper}
S94	on	[stove]
O33	black pepper	1
S67	ground
S73	in	[pot]
//
O435	stove	0
S94	on
S157	under	[pot]
O360	pot	0
S72	hot
S24	contains	{butter,onion,mushroom,chicken broth,flour,half-and-half,salt,black pepper}
S94	on	[stove]
O418	soup	0
S152	thick liquid
S24	contains	{butter,onion,mushroom,chicken broth,flour}
S73	in	[pot]
O228	half-and-half	1
S82	liquid
S73	in	[pot]
O390	salt	1
S64	granulated
S73	in	[pot]
O33	black pepper	1
S67	ground
S73	in	[pot]
O427	spoon	1
M20	cook and stir	<2:29-2:35>
O360	pot	0
S72	hot
S24	contains	{soup}
S94	on	[stove]
O418	soup	0
S152	thick liquid
S24	contains	{butter,onion,mushroom,chicken broth,flour,half-and-half,salt,black pepper}
S73	in	[pot]
O302	mushroom soup	0	1
S152	thick liquid
S24	contains	{butter,onion,mushroom,chicken broth,flour,half-and-half,salt,black pepper}
S73	in	[pot]
//
O360	pot	0
S72	hot
S24	contains	{soup}
S94	on	[stove]
O302	mushroom soup	1
S152	thick liquid
S24	contains	{butter,onion,mushroom,chicken broth,flour,half-and-half,salt,black pepper}
S73	in	[pot]
O45	bowl	0
S47	empty
O254	ladle	1
M91	scoop and pour	<2:50-2:52,2:53-2:57>
O45	bowl	0
S24	contains	{soup}
O302	mushroom soup	1	1
S152	thick liquid
S24	contains	{butter,onion,mushroom,chicken broth,flour,half-and-half,salt,black pepper}
S73	in	[bowl]
//
