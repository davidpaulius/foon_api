O322	packet	1
S24	contains	{cranberry}
O131	cranberry	1
S45	dried
S73	in	[packet]
O282	measuring cup	0
S47	empty
M70	pour	Assumed	Assumed
O282	measuring cup	0
S24	contains	{cranberry}
O131	cranberry	1
S45	dried
S73	in	[measuring cup]
//
O282	measuring cup	0
S24	contains	{cranberry}
O44	bottle	1
S24	contains	{maple syrup}
O273	maple syrup	1
S73	in	[bottle]
M70	pour	assumed	assumed
O273	maple syrup	0
S73	in	[measuring cup]
O282	measuring cup	0
S24	contains	{cranberry,maple syrup}
//
O282	measuring cup	0
S24	contains	{cranberry,maple syrup}
O273	maple syrup	0
S73	in	[measuring cup]
O131	cranberry	0
S45	dried
S73	in	[measuring cup]
M103	soak	assumed	assumed
O131	cranberry	0
S17	chopped
S139	soaked	{maple syrup}
S73	in	[measuring cup]
//
O45	bowl	0
S47	empty
O130	cornmeal	0
S67	ground
S73	in	[bowl]
O45	bowl	0
S24	contains	{cornmeal}
M70	pour	2:03	2:07
O45	bowl	0
S24	contains	{cornmeal}
//
O45	bowl	0
S24	contains	{cornmeal}
O177	flour	0
S110	powder
S73	in	[bowl]
O45	bowl	0
S24	contains	{flour}
M70	pour	2:08	2:12
O45	bowl	0
S24	contains	{cornmeal,flour}
//
O45	bowl	0
S24	contains	{cornmeal,flour}
O12	baking soda	0
S110	powder
S73	in	[box]
O46	box	1
S24	contains	{baking soda}
O427	spoon	1
M91	scoop and pour	2:17	2:21
O45	bowl	0
S24	contains	{cornmeal,flour,baking soda}
O12	baking soda	0
S110	powder
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{cornmeal,flour,baking soda}
O11	baking powder	0
S110	powder
S73	in	[tin can]
O456	tin can	0
S24	contains	{baking powder}
O427	spoon	1
M91	scoop and pour	2:22	2:25
O45	bowl	0
S24	contains	{cornmeal,flour,baking soda,baking powder}
//
O118	container	1
S24	contains	{salt}
O390	salt	1
S64	granulated
S73	in	[container]
O141	cup	0
S47	empty
M70	pour	Assumed	Assumed
O141	cup	0
S24	contains	{salt}
O390	salt	1
S64	granulated
S73	in	[cup]
//
O45	bowl	0
S24	contains	{cornmeal,flour,baking soda,baking powder}
O141	cup	0
S24	contains	{salt}
O390	salt	0
S64	granulated
S73	in	[cup]
O427	spoon	1
M91	scoop and pour	2:26	2:28
O45	bowl	0
S24	contains	{cornmeal,flour,baking soda,baking powder,salt}
O390	salt	0
S64	granulated
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{cornmeal,flour,baking soda,baking powder,salt}
O390	salt	1
S64	granulated
S73	in	[bowl]
O12	baking soda	1
S110	powder
S73	in	[bowl]
O11	baking powder	1
S110	powder
S73	in	[bowl]
O177	flour	1
S110	powder
S73	in	[bowl]
O130	cornmeal	0
S67	ground
S73	in	[bowl]
O424	spatula	0
M61	mix	2:29	2:32
O45	bowl	0
S24	contains	{flour mixture}
O179	flour mixture	1
S24	contains	{cornmeal,flour,baking soda,baking powder,salt}
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{flour mixture}
O179	flour mixture	1
S24	contains	{cornmeal,flour,baking soda,baking powder,salt}
S73	in	[bowl]
O282	measuring cup	1
S24	contains	{yogurt}
O502	yogurt	1
S73	in	[measuring cup]
M70	pour	2:34	2:37
O45	bowl	0
S24	contains	{flour mixture,yogurt}
O502	yogurt	0
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{flour mixture,yogurt}
O179	flour mixture	1
S24	contains	{cornmeal,flour,baking soda,baking powder,salt}
S73	in	[bowl]
O502	yogurt	1
S73	in	[bowl]
O424	spatula	1
M61	mix	2:38	2:41
O45	bowl	0
S24	contains	{pancake batter}
O326	pancake batter	1
S24	contains	{flour mixture,yogurt}
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{pancake batter}
O282	measuring cup	1
S24	contains	{cranberry,maple syrup}
O131	cranberry	1
S17	chopped
S139	soaked	{maple syrup}
S73	in	[measuring cup]
O427	spoon	0
M93	scrape	2:45	2:54
O45	bowl	0
S24	contains	{pancake batter,cranberry,maple syrup}
O131	cranberry	0
S17	chopped
S139	soaked	{maple syrup}
S73	in	[bowl]
O273	maple syrup	0
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{pancake batter,cranberry,maple syrup}
O483	walnut	0
S17	chopped
S73	in	[bowl]
O45	bowl	0
S24	contains	{walnut}
M70	pour	2:55	2:57
O45	bowl	0
S24	contains	{pancake batter,cranberry,maple syrup,walnut}
//
O45	bowl	0
S24	contains	{pancake batter,cranberry,maple syrup,walnut}
O326	pancake batter	1
S24	contains	{flour mixture,yogurt}
S73	in	[bowl]
O131	cranberry	1
S17	chopped
S139	soaked	{maple syrup}
S73	in	[bowl]
O273	maple syrup	1
S73	in	[bowl]
O483	walnut	1
S17	chopped
S73	in	[bowl]
O424	spatula	1
M61	mix	2:58	3:05
O45	bowl	0
S24	contains	{pancake batter}
O326	pancake batter	1
S24	contains	{flour mixture,yogurt,cranberry,maple syrup,walnut}
S73	in	[bowl]
//
O322	packet	0
S24	contains	{dark chocolate}
O148	dark chocolate	0
S3	bar
S73	in	[packet]
O146	cutting board	0
S47	empty
M66	pick-and-place	assumed	assumed
O146	cutting board	0
S24	contains	{dark chocolate}
O148	dark chocolate	0
S3	bar
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{dark chocolate}
O148	dark chocolate	0
S3	bar
S94	on	[cutting board]
O251	knife	1
M12	chop	assumed	assumed
O148	dark chocolate	0
S17	chopped
S94	on	[cutting board]
//
O45	bowl	0
S47	empty
O148	dark chocolate	1
S17	chopped
S94	on	[cutting board]
M66	pick-and-place	Assumed	Assumed
O45	bowl	0
S24	contains	{dark chocolate}
O148	dark chocolate	1
S17	chopped
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{pancake batter}
O45	bowl	0
S24	contains	{dark chocolate}
O148	dark chocolate	0
S17	chopped
S73	in	[bowl]
M70	pour	3:07	3:09
O45	bowl	0
S24	contains	{pancake batter,chocolate}
//
O45	bowl	0
S24	contains	{pancake batter,chocolate}
O326	pancake batter	1
S24	contains	{flour mixture,yogurt,cranberry,maple syrup,walnut}
S73	in	[bowl]
O148	dark chocolate	1
S17	chopped
S73	in	[bowl]
O424	spatula	1
M61	mix	3:10	3:20
O45	bowl	0
S24	contains	{pancake batter}
O326	pancake batter	1
S24	contains	{flour mixture,yogurt,cranberry,maple syrup,walnut,dark chocolate}
S73	in	[bowl]
//
O120	cooking pan	0
S70	heated
S94	on	[stove]
O456	tin can	1
S24	contains	{canola oil}
O70	canola oil	1
S73	in	[tin can]
M104	spray	assumed	assumed
O120	cooking pan	0
S66	greased	{canola oil}
S94	on	[stove]
O70	canola oil	0
S73	in	[pan]
//
O45	bowl	0
S24	contains	{pancake batter}
O326	pancake batter	1
S24	contains	{flour mixture,yogurt,cranberry,maple syrup,walnut,dark chocolate}
S73	in	[bowl]
O120	cooking pan	0
S66	greased	{canola oil}
S94	on	[stove]
O254	ladle	1
M91	scoop and pour	3:35	3:43
O254	ladle	0
O120	cooking pan	0
S24	contains	{pancake batter}
S94	on	[stove]
O326	pancake batter	1
S24	contains	{flour mixture,yogurt,cranberry,maple syrup,walnut,dark chocolate}
S73	in	[pan]
//
O435	stove	0
S94	on
S168	with	[pan]
O120	cooking pan	0
S24	contains	{pancake batter}
S94	on	[stove]
O326	pancake batter	1
S24	contains	{flour mixture,yogurt,cranberry,maple syrup,walnut,dark chocolate}
S73	in	[pan]
O474	turner	1
M40	flip	4:39	4:42
O474	turner	0
O325	pancake	1
S99	partly cooked
S24	contains	{flour mixture,yogurt,cranberry,maple syrup,walnut,dark chocolate}
S73	in	[pan]
//
O435	stove	0
S94	on
S168	with	[pan]
O120	cooking pan	0
S24	contains	{pancake batter}
S94	on	[stove]
O325	pancake	1
S99	partly cooked
S24	contains	{flour mixture,yogurt,cranberry,maple syrup,walnut,dark chocolate}
S73	in	[pan]
O474	turner	1
M40	flip	4:45	4:48
O120	cooking pan	0
S24	contains	{pancake}
S94	on	[stove]
O325	pancake	1
S25	cooked
S24	contains	{flour mixture,yogurt,cranberry,maple syrup,walnut,dark chocolate}
S73	in	[pan]
O474	turner	0
//
O120	cooking pan	0
S24	contains	{pancake}
S94	on	[stove]
O325	pancake	1
S25	cooked
S24	contains	{flour mixture,yogurt,cranberry,maple syrup,walnut,dark chocolate}
S73	in	[pan]
O474	turner	1
O358	plate	0
S47	empty
M66	pick-and-place	4:46	4:55
O358	plate	0
S24	contains	{pancake}
O132	cranberry-walnut pancake	1
S24	contains	{cornmeal,flour,baking soda,baking powder,salt,yogurt,cranberry,maple syrup,walnut,dark chocolate}
S94	on	[plate]
//
O358	plate	0
S24	contains	{pancake}
O490	whipped cream	0
S73	in	[bowl]
O45	bowl	0
S24	contains	{whipped cream}
O427	spoon	1
O132	cranberry-walnut pancake	1
S24	contains	{cornmeal,flour,baking soda,baking powder,salt,yogurt,cranberry,maple syrup,walnut,dark chocolate}
S94	on	[plate]
M91	scoop and pour	5:02	5:05
O132	cranberry-walnut pancake	1
S24	contains	{cornmeal,flour,baking soda,baking powder,salt,yogurt,cranberry,maple syrup,walnut,dark chocolate,whipped cream}
S94	on	[plate]
O490	whipped cream	0
S94	on	[pancake]
//
O358	plate	0
S24	contains	{pancake}
O132	cranberry-walnut pancake	1
S24	contains	{cornmeal,flour,baking soda,baking powder,salt,yogurt,cranberry,maple syrup,walnut,dark chocolate,whipped cream}
S94	on	[plate]
O141	cup	1
S24	contains	{powdered sugar}
O365	powdered sugar	0
S110	powder
S73	in	[cup]
M106	sprinkle	5:08	5:16
O132	cranberry-walnut pancake	1	5
S24	contains	{pancake batter,flour mixture,pancake,cornmeal,flour,baking soda,baking powder,salt,yogurt,cranberry,maple syrup,walnut,dark chocolate,whipped cream,powdered sugar}
S94	on	[plate]
O365	powdered sugar	0
S110	powder
S94	on	[pancake]
//
