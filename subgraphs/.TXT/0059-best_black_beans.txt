O313	onion	1
S104	peeled
O146	cutting board	0
S47	empty
M66	pick-and-place	Assumed	Assumed
O146	cutting board	0
S24	contains	{onion}
O313	onion	1
S104	peeled
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{onion}
O313	onion	0
S104	peeled
S94	on	[cutting board]
O251	knife	1
M12	chop	0:20	0:22
O313	onion	0
S17	chopped
S94	on	[cutting board]
//
O146	cutting board	1
S24	contains	{onion}
O313	onion	1
S17	chopped
S94	on	[cutting board]
O45	bowl	0
S47	empty
M70	pour	Assumed	Assumed
O45	bowl	0
S24	contains	{onion}
O313	onion	1
S17	chopped
S73	in	[bowl]
//
O146	cutting board	0
S47	empty
O194	garlic	1
S104	peeled
M66	pick-and-place	Assumed	Assumed
O146	cutting board	0
S24	contains	{garlic}
O194	garlic	1
S104	peeled
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{garlic}
O194	garlic	1
S104	peeled
S94	on	[cutting board]
O251	knife	1
M12	chop	0:23	0:27
O194	garlic	0
S17	chopped
S94	on	[cutting board]
//
O146	cutting board	1
S24	contains	{garlic}
O194	garlic	1
S17	chopped
S94	on	[cutting board]
O45	bowl	0
S47	empty
M70	pour	Assumed	Assumed
O45	bowl	0
S24	contains	{garlic}
O194	garlic	1
S17	chopped
S73	in	[bowl]
O146	cutting board	1
//
O103	cilantro	1
S167	whole
O146	cutting board	0
S47	empty
M66	pick-and-place	Assumed	Assumed
O146	cutting board	0
S24	contains	{cilantro}
O103	cilantro	1
S167	whole
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{cilantro}
O103	cilantro	1
S167	whole
S94	on	[cutting board]
O251	knife	1
M25	cut	0:28	0:33
O103	cilantro	0
S17	chopped
S94	on	[cutting board]
//
O146	cutting board	1
S24	contains	{cilantro}
O103	cilantro	1
S17	chopped
S94	on	[cutting board]
O45	bowl	0
S47	empty
M70	pour	Assumed	Assumed
O45	bowl	0
S24	contains	{cilantro}
O103	cilantro	1
S17	chopped
S73	in	[bowl]
O146	cutting board	1
//
O435	stove	0
S94	on
O360	pot	0
S47	empty
M66	pick-and-place	Assumed	Assumed
O435	stove	0
S94	on
S168	with	[pot]
O360	pot	0
S70	heated
S94	on	[stove]
//
O360	pot	0
S70	heated
S94	on	[stove]
O456	tin can	1
S24	contains	{black bean}
O31	black bean	1
S73	in	[tin can]
M70	pour	0:34	0:38
O360	pot	0
S24	contains	{black bean}
S94	on	[stove]
O31	black bean	1
S73	in	[pot]
//
O360	pot	0
S24	contains	{black bean}
S94	on	[stove]
O45	bowl	1
S24	contains	{onion}
O313	onion	1
S17	chopped
S73	in	[bowl]
M70	pour	0:40	0:42
O360	pot	0
S24	contains	{black bean,onion}
S94	on	[stove]
O313	onion	1
S17	chopped
S73	in	[pot]
//
O360	pot	0
S24	contains	{black bean,onion}
S94	on	[stove]
O45	bowl	1
S24	contains	{garlic}
O194	garlic	1
S17	chopped
S73	in	[bowl]
M70	pour	0:42	0:44
O360	pot	0
S24	contains	{black bean,onion,garlic}
S94	on	[stove]
O194	garlic	1
S17	chopped
S73	in	[pot]
//
O360	pot	0
S24	contains	{black bean,onion,garlic}
S94	on	[stove]
O194	garlic	1
S17	chopped
S73	in	[pot]
O313	onion	1
S17	chopped
S73	in	[pot]
O427	spoon	1
O31	black bean	1
S73	in	[pot]
M20	cook and stir	0:45	0:49
O360	pot	0
S24	contains	{black bean,onion,garlic}
S94	on	[stove]
O194	garlic	1
S58	fried
S73	in	[pot]
O313	onion	1
S58	fried
S73	in	[pot]
O31	black bean	1
S99	partly cooked
S24	contains	{onion,garlic}
S73	in	[pot]
//
O360	pot	0
S24	contains	{black bean,onion,garlic}
S94	on	[stove]
O45	bowl	1
S24	contains	{cilantro}
O103	cilantro	1
S17	chopped
S73	in	[bowl]
M70	pour	0:50	0:52
O360	pot	0
S24	contains	{black bean,onion,garlic,cilantro}
S94	on	[stove]
O103	cilantro	1
S17	chopped
S73	in	[pot]
//
O360	pot	0
S24	contains	{black bean,onion,garlic,cilantro}
S94	on	[stove]
O45	bowl	1
S24	contains	{cayenne pepper}
O74	cayenne pepper	1
S67	ground
S73	in	[bowl]
M106	sprinkle	0:53	0:55
O360	pot	0
S24	contains	{black bean,onion,garlic,cilantro,cayenne pepper}
S94	on	[stove]
O74	cayenne pepper	1
S67	ground
S73	in	[pot]
//
O360	pot	0
S24	contains	{black bean,onion,garlic,cilantro}
S94	on	[stove]
O390	salt	1
S64	granulated
M106	sprinkle	0:56	0:59
O360	pot	0
S24	contains	{black bean,onion,garlic,cilantro,cayenne pepper,salt}
S94	on	[stove]
O390	salt	1
S64	granulated
S73	in	[pot]
//
O360	pot	0
S24	contains	{black bean,onion,garlic,cilantro,cayenne pepper,salt}
S94	on	[stove]
O103	cilantro	1
S17	chopped
S73	in	[pot]
O74	cayenne pepper	1
S67	ground
S73	in	[pot]
O390	salt	1
S64	granulated
S73	in	[pot]
O427	spoon	1
O31	black bean	1
S99	partly cooked
S24	contains	{onion,garlic}
S73	in	[pot]
M109	stir	1:00	1:05
O360	pot	0
S24	contains	{black bean,onion,garlic,cilantro,cayenne pepper,salt}
S94	on	[stove]
O31	black bean	1
S99	partly cooked
S24	contains	{onion,garlic,cilantro,cayenne pepper,salt}
S73	in	[pot]
//
O360	pot	0
S24	contains	{black bean,onion,garlic,cilantro,cayenne pepper,salt}
S94	on	[stove]
O31	black bean	1
S99	partly cooked
S24	contains	{onion,garlic,cilantro,cayenne pepper,salt}
S73	in	[pot]
M16	cook	Assumed	Assumed
O360	pot	0
S24	contains	{black bean}
S94	on	[stove]
O31	black bean	1
S25	cooked
S24	contains	{onion,garlic,cilantro,cayenne pepper,salt}
S73	in	[pot]
//
O360	pot	0
S24	contains	{black bean}
S94	on	[stove]
O31	black bean	1
S25	cooked
S24	contains	{onion,garlic,cilantro,cayenne pepper,salt}
S73	in	[pot]
O254	ladle	1
O45	bowl	1
S47	empty
M91	scoop and pour	Assumed	Assumed
O31	black bean	1	1
S25	cooked
S24	contains	{onion,garlic,cilantro,cayenne pepper,salt}
S73	in	[bowl]
O45	bowl	1
S24	contains	{black bean}
//
