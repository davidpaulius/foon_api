# Source:	https://www.youtube.com/watch?v=S_1_ZSMxRfg
# Source:	http://foonets.com/foon_subgraphs/subgraphs/0061-club_sandwich.mp4
//
O120	cooking pan	1
S47	empty
O435	stove	0
S92	off (ready)
M66	pick-and-place	<Assumed>
O120	cooking pan	1
S47	empty
S94	on	[stove]
O435	stove	0
S92	off (ready)
S157	under	[cooking pan]
//
O120	cooking pan	0
S94	on	[stove]
S47	empty
O435	stove	0
S94	on
S157	under	[cooking pan]
M52	heat	<Assumed>
O120	cooking pan	0
S47	empty
S72	hot
S94	on	[stove]
//
O120	cooking pan	0
S47	empty
S72	hot
S94	on	[stove]
O8	bacon	1
S114	raw
S137	sliced
M66	pick-and-place	<0:15-0:25>
O120	cooking pan	0
S72	hot
S24	contains	{bacon}
S94	on	[stove]
O8	bacon	1
S114	raw
S137	sliced
S73	in	[cooking pan]
//
O435	stove	0
S94	on
S157	under	[cooking pan]
O120	cooking pan	0
S72	hot
S24	contains	{bacon}
S94	on	[stove]
O8	bacon	0
S114	raw
S137	sliced
S73	in	[cooking pan]
M16	cook	<Assumed>
O8	bacon	1
S25	cooked
S137	sliced
S73	in	[cooking pan]
//
O120	cooking pan	0
S72	hot
S24	contains	{bacon}
S94	on	[stove]
O8	bacon	0
S25	cooked
S137	sliced
S73	in	[cooking pan]
O358	plate	0
S47	empty
O183	fork	1
M66	pick-and-place	<0:26-0:33>
O120	cooking pan	0
S47	empty
S94	on	[stove]
O358	plate	0
S24	contains	{bacon}
O8	bacon	0
S25	cooked
S137	sliced
S94	on	[plate]
//
O146	cutting board	0
S47	empty
O459	tomato	1
S167	whole
M66	pick-and-place	<Assumed>
O146	cutting board	0
S24	contains	{tomato}
O459	tomato	1
S167	whole
S94	on	[cutting board]
//
O459	tomato	0
S167	whole
S94	on	[cutting board]
O251	knife	1
M22	core	<0:35-0:39>
O459	tomato	0
S26	cored
S167	whole
S94	on	[cutting board]
//
O459	tomato	0
S167	whole
S26	cored
S94	on	[cutting board]
O251	knife	1
M102	slice	<0:40-0:52>
O459	tomato	0
S137	sliced
S94	on	[cutting board]
//
O47	bread	1
S137	sliced
S73	in	[toaster]
O457	toaster	0
S48	empty (ready)
M66	pick-and-place	<Assumed>
O457	toaster	0
S24	contains	{bread}
O47	bread	1
S137	sliced
S73	in	[toaster]
//
O457	toaster	0
S24	contains	{bread}
O47	bread	1
S137	sliced
S73	in	[toaster]
M120	toast	<Assumed>
O47	bread	1
S137	sliced
S153	toasted
S73	in	[toaster]
//
O457	toaster	0
S24	contains	{bread}
O146	cutting board	0
S47	empty
O47	bread	1
S137	sliced
S153	toasted
S73	in	[toaster]
M66	pick-and-place	<Assumed>
O457	toaster	0
S47	empty
O146	cutting board	0
S24	contains	{bread}
O47	bread	1
S137	sliced
S153	toasted
S94	on	[cutting board]
//
O244	jar	0
S24	contains	{mayonnaise}
O281	mayonnaise	0
S30	creamy
S73	in	[jar]
O47	bread	0
S137	sliced
S153	toasted
S94	on	[cutting board]
O251	knife	1
M105	spread	<0:58-1:03,1:40-1:50,2:08-2:14>
O47	bread	0
S137	sliced
S24	contains	{mayonnaise}
S94	on	[cutting board]
O281	mayonnaise	1
S30	creamy
S94	on	[bread]
//
O358	plate	0
S24	contains	{ham}
O47	bread	0
S137	sliced
S24	contains	{mayonnaise}
S94	on	[cutting board]
O229	ham	1
S25	cooked
S137	sliced
S94	on	[plate]
M66	pick-and-place	<1:05-1:16>
O47	bread	0
S137	sliced
S24	contains	{mayonnaise,ham}
S94	on	[cutting board]
O229	ham	1
S25	cooked
S137	sliced
S94	on	[bread]
//
O358	plate	0
S24	contains	{turkey}
O47	bread	0
S137	sliced
S24	contains	{mayonnaise,ham}
S94	on	[cutting board]
O472	turkey	1
S25	cooked
S137	sliced
S94	on	[plate]
M66	pick-and-place	<1:17-1:22>
O47	bread	0
S137	sliced
S24	contains	{mayonnaise,ham,turkey}
S94	on	[cutting board]
O472	turkey	1
S137	sliced
S25	cooked
S94	on	[bread]
//
O358	plate	0
S24	contains	{cheese}
O47	bread	0
S137	sliced
S24	contains	{mayonnaise,ham,turkey}
S94	on	[cutting board]
O78	cheese	1
S137	sliced
S94	on	[plate]
M66	pick-and-place	<1:22-1:25>
O47	bread	0
S137	sliced
S24	contains	{mayonnaise,ham,turkey,cheese}
S94	on	[cutting board]
O78	cheese	1
S137	sliced
S94	on	[bread]
//
O47	bread	0
S137	sliced
S24	contains	{mayonnaise,ham,turkey,cheese}
S94	on	[cutting board]
O47	bread	1
S137	sliced
S24	contains	{mayonnaise}
S94	on	[cutting board]
M66	pick-and-place	<1:36-1:39>
O47	bread	1
S137	sliced
S24	contains	{mayonnaise}
S94	on	[bread]
O47	bread	0
S137	sliced
S24	contains	{mayonnaise,ham,turkey,cheese}
S94	on	[cutting board]
S157	under	[bread]
O392	sandwich	0
S24	contains	{mayonnaise,ham,turkey,cheese}
S94	on	[cutting board]
//
O358	plate	0
S24	contains	{bacon}
O8	bacon	1
S94	on	[plate]
S58	fried
S137	sliced
O47	bread	0
S137	sliced
S24	contains	{mayonnaise}
S94	on	[bread]
M66	pick-and-place	<1:51-1:55>
O47	bread	0
S137	sliced
S24	contains	{mayonnaise,bacon}
S94	on	[bread]
O8	bacon	1
S58	fried
S137	sliced
S94	on	[bread]
//
O358	plate	0
S24	contains	{lettuce}
O264	lettuce	1
S79	leaf
S94	on	[plate]
O47	bread	0
S137	sliced
S24	contains	{mayonnaise,bacon}
S94	on	[bread]
M66	pick-and-place	<1:56-2:00>
O47	bread	0
S137	sliced
S24	contains	{mayonnaise,bacon,lettuce}
S94	on	[bread]
O264	lettuce	1
S79	leaf
S94	on	[bread]
//
O358	plate	0
S24	contains	{tomato}
O459	tomato	1
S137	sliced
S94	on	[cutting board]
O47	bread	0
S137	sliced
S24	contains	{mayonnaise,bacon,lettuce}
S94	on	[bread]
M66	pick-and-place	<2:02-2:05>
O47	bread	0
S137	sliced
S24	contains	{mayonnaise,bacon,lettuce,tomato}
S94	on	[bread]
O459	tomato	1
S137	sliced
S94	on	[bread]
//
O146	cutting board	0
S24	contains	{bread}
O47	bread	1
S137	sliced
S24	contains	{mayonnaise}
S94	on	[cutting board]
O47	bread	0
S137	sliced
S24	contains	{mayonnaise,bacon,lettuce,tomato}
S94	on	[bread]
O47	bread	0
S137	sliced
S24	contains	{mayonnaise,ham,turkey,cheese}
S94	on	[cutting board]
M66	pick-and-place	<2:15-2:19>
O146	cutting board	0
S24	contains	{sandwich}
O47	bread	1
S137	sliced
S24	contains	{mayonnaise}
S94	on	[bread]
O392	sandwich	0
S167	whole
S24	contains	{bacon,bread,cheese,ham,lettuce,mayonnaise,tomato,turkey}
S94	on	[cutting board]
O107	club sandwich	0	4
S167	whole
S24	contains	{bacon,bread,cheese,ham,lettuce,mayonnaise,tomato,turkey}
S94	on	[cutting board]
//
O107	club sandwich	0	4
S167	whole
S24	contains	{bacon,bread,cheese,ham,lettuce,mayonnaise,tomato,turkey}
S94	on	[cutting board]
O251	knife	1
M35	divide	<2:40-2:49>
O107	club sandwich	0	4
S109	portioned
S24	contains	{bacon,bread,cheese,ham,lettuce,mayonnaise,tomato,turkey}
S94	on	[cutting board]
//
O358	plate	0
S47	empty
O45	bowl	1
S24	contains	{fries}
O187	fries	1
S25	cooked
S73	in	[bowl]
M70	pour	<2:51-2:56>
O358	plate	0
S24	contains	{fries}
O187	fries	1
S25	cooked
S94	on	[plate]
O45	bowl	1
S47	empty
//
O187	fries	1
S25	cooked
S94	on	[plate]
O146	cutting board	0
S24	contains	{sandwich}
O358	plate	0
S24	contains	{fries}
O107	club sandwich	1
S109	portioned
S24	contains	{bacon,bread,cheese,ham,lettuce,mayonnaise,tomato,turkey}
S94	on	[cutting board]
M66	pick-and-place	<2:58-3:07>
O358	plate	0
S24	contains	{club sandwich,fries}
O107	club sandwich	1	4
S109	portioned
S24	contains	{bacon,bread,cheese,ham,lettuce,mayonnaise,tomato,turkey}
S94	on	[plate]
S115	ready
O146	cutting board	0
S47	empty
//
