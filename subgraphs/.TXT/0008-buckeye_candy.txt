# Source:	http://foonets.com/foon_subgraphs/subgraphs/0008-buckeye_candy-allrecipes.mp4 
# Source:	https://www.youtube.com/watch?v=ej8EFvrOuQE
//
O45	bowl	0
S47	empty
O244	jar	0
S24	contains	{peanut butter}
O338	peanut butter	1
S30	creamy
S73	in	[jar]
O251	knife	1
M89	scoop	<Assumed>
O45	bowl	0
S24	contains	{peanut butter}
O338	peanut butter	0
S30	creamy
S73	in	[bowl]
//
O290	mixing bowl	0
S47	empty
O45	bowl	0
S24	contains	{peanut butter}
O338	peanut butter	0
S30	creamy
S73	in	[bowl]
O424	spatula	1
M73	pour and scrape	<Assumed>
O290	mixing bowl	0
S24	contains	{peanut butter}
O338	peanut butter	1
S30	creamy
S73	in	[mixing bowl]
O45	bowl	1
S47	empty
//
O290	mixing bowl	0
S24	contains	{peanut butter}
O322	packet	1
S24	contains	{butter}
O61	butter	1
S140	softened
S159	unmelted
S73	in	[packet]
S148	stick
M70	pour	<0:30,0:32>
O290	mixing bowl	0
S24	contains	{butter,peanut butter}
O61	butter	1
S140	softened
S159	unmelted
S73	in	[mixing bowl]
S148	stick
O322	packet	1
S47	empty
//
O44	bottle	1
S24	contains	{vanilla extract}
O475	vanilla extract	1
S82	liquid
S73	in	[bottle]
O427	spoon	0
M70	pour	<Assumed>
O427	spoon	1
S24	contains	{vanilla extract}
O475	vanilla extract	1
S82	liquid
S73	in	[spoon]
//
O290	mixing bowl	0
S24	contains	{butter,peanut butter}
O427	spoon	1
S24	contains	{vanilla extract}
O475	vanilla extract	1
S73	in	[spoon]
S82	liquid
M70	pour	<0:34,0:35>
O290	mixing bowl	0
S24	contains	{butter,peanut butter,vanilla extract}
O475	vanilla extract	1
S82	liquid
S73	in	[mixing bowl]
O427	spoon	1
//
O233	hand mixer	1
S92	off (ready)
O290	mixing bowl	0
S24	contains	{butter,peanut butter,vanilla extract}
M66	pick-and-place	<0:36,0:37>
O233	hand mixer	0
S73	in	[mixing bowl]
S92	off (ready)
O290	mixing bowl	0
S168	with	[hand mixer]
S24	contains	{butter,peanut butter,vanilla extract}
//
O233	hand mixer	1
S73	in	[mixing bowl]
S92	off (ready)
M123	turn on	<0:37,0:38>
O233	hand mixer	1
S73	in	[mixing bowl]
S94	on
//
O233	hand mixer	1
S73	in	[mixing bowl]
S94	on
O290	mixing bowl	0
S168	with	[hand mixer]
S24	contains	{butter,peanut butter,vanilla extract}
O338	peanut butter	0
S30	creamy
S73	in	[mixing bowl]
O61	butter	0
S140	softened
S159	unmelted
S73	in	[mixing bowl]
S148	stick
O475	vanilla extract	0
S82	liquid
S73	in	[mixing bowl]
M4	blend	<0:38,0:46>
O233	hand mixer	1
S73	in	[mixing bowl]
S91	off
O339	peanut butter mixture	0
S30	creamy
S88	mixed
S73	in	[mixing bowl]
S24	contains	{butter,peanut butter,vanilla extract}
//
O322	packet	1
S24	contains	{powdered sugar}
O365	powdered sugar	1
S110	powder
S73	in	[packet]
O282	measuring cup	0
S47	empty
M70	pour	<Assumed>
O282	measuring cup	0
S24	contains	{powdered sugar}
O365	powdered sugar	1
S110	powder
S73	in	[measuring cup]
//
O282	measuring cup	1
S24	contains	{powdered sugar}
O365	powdered sugar	1
S110	powder
S73	in	[measuring cup]
O290	mixing bowl	0
S24	contains	{butter,peanut butter,vanilla extract}
M70	pour	<0:49,0:55>
O290	mixing bowl	0
S24	contains	{butter,peanut butter,powdered sugar,vanilla extract}
O365	powdered sugar	1
S110	powder
S73	in	[mixing bowl]
O282	measuring cup	1
S47	empty
//
O233	hand mixer	1
S92	off (ready)
O290	mixing bowl	0
S24	contains	{butter,peanut butter,powdered sugar,vanilla extract}
M66	pick-and-place	<Assumed>
O233	hand mixer	0
S73	in	[mixing bowl]
S92	off (ready)
O290	mixing bowl	0
S168	with	[hand mixer]
S24	contains	{butter,peanut butter,powdered sugar,vanilla extract}
//
O233	hand mixer	1
S73	in	[mixing bowl]
S94	on
O290	mixing bowl	0
S24	contains	{butter,peanut butter,powdered sugar,vanilla extract}
O365	powdered sugar	1
S110	powder
S73	in	[mixing bowl]
O339	peanut butter mixture	1
S30	creamy
S88	mixed
S73	in	[mixing bowl]
S24	contains	{butter,peanut butter,vanilla extract}
M4	blend	<0:58,1:09>
O339	peanut butter mixture	0
S84	lumpy
S88	mixed
S73	in	[mixing bowl]
S24	contains	{butter,peanut butter,powdered sugar,vanilla extract}
//
O46	box	1
S24	contains	{parchment paper}
O329	parchment paper	1
S131	sheet
S73	in	[box]
O13	baking tray	0
S47	empty
M66	pick-and-place	<0:59,1:04>
O13	baking tray	0
S47	empty
S168	with	[parchment paper]
O329	parchment paper	1
S131	sheet
S94	on	[baking tray]
//
O290	mixing bowl	0
S24	contains	{butter,peanut butter,powdered sugar,vanilla extract}
O339	peanut butter mixture	1
S84	lumpy
S88	mixed
S73	in	[mixing bowl]
S24	contains	{butter,peanut butter,powdered sugar,vanilla extract}
M87	roll and shape	<1:05,1:09>
O59	buckeye candy	1
S24	contains	{butter,peanut butter,powdered sugar,vanilla extract}
S118	rolled
//
O13	baking tray	0
S47	empty
S168	with	[parchment paper]
O59	buckeye candy	1
S24	contains	{butter,peanut butter,powdered sugar,vanilla extract}
S118	rolled
M67	place	<1:11,1:13>
O13	baking tray	0
S24	contains	{buckeye candy}
O59	buckeye candy	1
S94	on	[baking tray]
S24	contains	{butter,peanut butter,powdered sugar,vanilla extract}
S118	rolled
//
O464	toothpick	1
O59	buckeye candy	0
S94	on	[baking tray]
S24	contains	{butter,peanut butter,powdered sugar,vanilla extract}
S118	rolled
M53	insert	<1:28,1:32>
O464	toothpick	0
S73	in	[buckeye candy]
O59	buckeye candy	1
S94	on	[baking tray]
S168	with	[toothpick]
S24	contains	{butter,peanut butter,powdered sugar,vanilla extract}
S118	rolled
//
O45	bowl	0
S47	empty
O322	packet	1
S24	contains	{chocolate chip}
O97	chocolate chip	1
S106	pieces
S73	in	[packet]
M70	pour	<Assumed>
O45	bowl	0
S24	contains	{chocolate chip}
O97	chocolate chip	1
S106	pieces
S73	in	[bowl]
//
O360	pot	1
S47	empty
O435	stove	0
S92	off (ready)
M66	pick-and-place	<Assumed>
O360	pot	1
S94	on	[stove]
S47	empty
O435	stove	0
S92	off (ready)
S168	with	[pot]
//
O360	pot	0
S94	on	[stove]
S47	empty
O282	measuring cup	1
S24	contains	{water}
O484	water	1
S73	in	[measuring cup]
S82	liquid
M70	pour	<Assumed>
O360	pot	0
S94	on	[stove]
S24	contains	{water}
O484	water	1
S73	in	[pot]
S82	liquid
O282	measuring cup	1
S47	empty
//
O435	stove	0
S91	off
S168	with	[pot]
M123	turn on	<Assumed>
O435	stove	0
S94	on
S168	with	[pot]
//
O360	pot	0
S94	on	[stove]
S24	contains	{water}
O435	stove	0
S94	on
S168	with	[pot]
O484	water	1
S73	in	[pot]
S82	liquid
M5	boil	<Assumed>
O360	pot	0
S94	on	[stove]
S72	hot
S24	contains	{water}
O484	water	1
S11	boiling
S73	in	[pot]
S82	liquid
//
O360	pot	0
S94	on	[stove]
S72	hot
S24	contains	{water}
O151	double boiler	1
S47	empty
M66	pick-and-place	<1:37,1:39>
O360	pot	0
S94	on	[stove]
S72	hot
S168	with	[double boiler]
S24	contains	{water}
O151	double boiler	1
S47	empty
S73	in	[pot]
//
O360	pot	0
S94	on	[stove]
S72	hot
S168	with	[double boiler]
S24	contains	{water}
O151	double boiler	1
S47	empty
S73	in	[pot]
O45	bowl	0
S24	contains	{chocolate chip}
O97	chocolate chip	1
S106	pieces
S73	in	[bowl]
M70	pour	<1:41,1:45>
O151	double boiler	1
S24	contains	{chocolate chip}
S73	in	[pot]
O97	chocolate chip	1
S106	pieces
S73	in	[double boiler]
//
O360	pot	0
S94	on	[stove]
S72	hot
S168	with	[double boiler]
S24	contains	{water}
O151	double boiler	1
S24	contains	{chocolate chip}
S73	in	[pot]
O97	chocolate chip	1
S106	pieces
S73	in	[double boiler]
O251	knife	1
M111	stir and melt	<1:47,1:56>
O151	double boiler	1
S24	contains	{chocolate chip}
S73	in	[pot]
O97	chocolate chip	1
S86	melted
S152	thick liquid
S73	in	[double boiler]
//
O151	double boiler	1
S24	contains	{chocolate chip}
S73	in	[pot]
O360	pot	0
S94	on	[stove]
S72	hot
S168	with	[double boiler]
S24	contains	{water}
M82	remove	<Assumed>
O360	pot	0
S94	on	[stove]
S72	hot
S24	contains	{water}
O151	double boiler	1
S24	contains	{chocolate chip}
//
O151	double boiler	1
S24	contains	{chocolate chip}
O97	chocolate chip	1
S86	melted
S152	thick liquid
S73	in	[double boiler]
O45	bowl	0
S47	empty
M70	pour	<Assumed>
O45	bowl	0
S24	contains	{chocolate chip}
O97	chocolate chip	1
S86	melted
S152	thick liquid
S73	in	[bowl]
O151	double boiler	1
S47	empty
//
O59	buckeye candy	1
S94	on	[baking tray]
S168	with	[toothpick]
S24	contains	{butter,peanut butter,powdered sugar,vanilla extract}
S118	rolled
O151	double boiler	0
S24	contains	{chocolate chip}
O97	chocolate chip	0
S86	melted
S152	thick liquid
S73	in	[bowl]
M34	dip	<2:05,2:10>
O59	buckeye candy	1
S94	on	[baking tray]
S168	with	[toothpick]
S24	contains	{butter,chocolate chip,peanut butter,powdered sugar,vanilla extract}
S118	rolled
O97	chocolate chip	0
S86	melted
S94	on	[buckeye candy]
S152	thick liquid
//
O13	baking tray	1
S24	contains	{buckeye candy}
O59	buckeye candy	0
S94	on	[baking tray]
S168	with	[toothpick]
S24	contains	{butter,chocolate chip,peanut butter,powdered sugar,vanilla extract}
S118	rolled
O373	refrigerator	1
S94	on
M11	chill	<Assumed>
O59	buckeye candy	0
S16	chilled
S94	on	[baking tray]
S168	with	[toothpick]
S24	contains	{butter,chocolate chip,peanut butter,powdered sugar,vanilla extract}
S118	rolled
//
O13	baking tray	0
S24	contains	{buckeye candy}
O59	buckeye candy	0
S16	chilled
S94	on	[baking tray]
S168	with	[toothpick]
S24	contains	{butter,chocolate chip,peanut butter,powdered sugar,vanilla extract}
S118	rolled
O464	toothpick	0
S73	in	[buckeye candy]
M82	remove	<Assumed>
O59	buckeye candy	0	8
S94	on	[baking tray]
S24	contains	{butter,chocolate chip,peanut butter,powdered sugar,vanilla extract}
S118	rolled
O464	toothpick	0
//
