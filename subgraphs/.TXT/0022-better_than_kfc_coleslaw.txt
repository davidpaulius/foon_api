# Source:	https://www.youtube.com/watch?v=_DRIfPEM6qw
# Source:	http://foonets.com/foon_subgraphs/subgraphs/0022-better_than_kfc_coleslaw.mp4
//
O290	mixing bowl	0
S47	empty
O244	jar	0
S24	contains	{mayonnaise}
O281	mayonnaise	0
S30	creamy
S73	in	[bowl]
O427	spoon	1
M91	scoop and pour	<Assumed>
O290	mixing bowl	0
S24	contains	{mayonnaise}
O281	mayonnaise	0
S30	creamy
S73	in	[mixing bowl]
//
O45	bowl	1
S24	contains	{sugar}
O438	sugar	1
S64	granulated
S73	in	[bowl]
O290	mixing bowl	0
S24	contains	{mayonnaise}
M70	pour	<1:00-1:02>
O290	mixing bowl	0
S24	contains	{mayonnaise,sugar}
O438	sugar	1
S64	granulated
S73	in	[mixing bowl]
O45	bowl	1
S47	empty
//
O290	mixing bowl	0
S24	contains	{mayonnaise,sugar}
O45	bowl	1
S24	contains	{vinegar}
O481	vinegar	1
S82	liquid
S73	in	[bowl]
M70	pour	<1:04-1:06>
O290	mixing bowl	0
S24	contains	{mayonnaise,sugar,vinegar}
O481	vinegar	1
S82	liquid
S73	in	[mixing bowl]
O45	bowl	1
S47	empty
//
O290	mixing bowl	0
S24	contains	{mayonnaise,sugar,vinegar}
O45	bowl	1
S24	contains	{vegetable oil}
O478	vegetable oil	1
S82	liquid
S73	in	[bowl]
M70	pour	<1:08-1:10>
O290	mixing bowl	0
S24	contains	{mayonnaise,sugar,vinegar,vegetable oil}
O478	vegetable oil	1
S82	liquid
S73	in	[mixing bowl]
O45	bowl	1
S47	empty
//
O290	mixing bowl	0
S24	contains	{mayonnaise,sugar,vegetable oil,vinegar}
O438	sugar	0
S64	granulated
S73	in	[mixing bowl]
O281	mayonnaise	0
S30	creamy
S73	in	[mixing bowl]
O481	vinegar	0
S82	liquid
S73	in	[mixing bowl]
O478	vegetable oil	0
S82	liquid
S73	in	[mixing bowl]
O424	spatula	1
M61	mix	<1:14-1:28>
O290	mixing bowl	0
S24	contains	{salad dressing}
O387	salad dressing	0
S30	creamy
S24	contains	{mayonnaise,sugar,vegetable oil,vinegar}
S73	in	[mixing bowl]
S88	mixed
//
O146	cutting board	0
S47	empty
O63	cabbage	1
S167	whole
M66	pick-and-place	<Assumed>
O146	cutting board	0
S24	contains	{cabbage}
O63	cabbage	1
S167	whole
S94	on	[cutting board]
//
O146	cutting board	0
S47	empty
O313	onion	1
S167	whole
S104	peeled
M66	pick-and-place	<Assumed>
O146	cutting board	0
S24	contains	{onion}
O313	onion	1
S94	on	[cutting board]
S167	whole
S104	peeled
//
O71	carrot	0
S167	whole
S160	unpeeled
O341	peeler	1
M65	peel	<Assumed>
O71	carrot	1
S104	peeled
S167	whole
//
O146	cutting board	0
S47	empty
O71	carrot	1
S104	peeled
S167	whole
M66	pick-and-place	<Assumed>
O146	cutting board	0
S24	contains	{carrot}
O71	carrot	1
S167	whole
S104	peeled
S94	on	[cutting board]
//
O71	carrot	0
S167	whole
S104	peeled
S94	on	[cutting board]
O251	knife	1
M12	chop	<1:44-1:57>
O71	carrot	0
S17	chopped
S94	on	[cutting board]
//
O386	salad bowl	0
S47	empty
O146	cutting board	0
S24	contains	{carrot}
O71	carrot	1
S94	on	[cutting board]
S17	chopped
M91	scoop and pour	<1:58-2:00>
O386	salad bowl	0
S24	contains	{carrot}
O71	carrot	1
S17	chopped
S73	in	[salad bowl]
O146	cutting board	0
S47	empty
//
O313	onion	0
S94	on	[cutting board]
S104	peeled
O251	knife	1
M12	chop	<2:03-2:38>
O313	onion	0
S17	chopped
S94	on	[cutting board]
//
O386	salad bowl	0
S24	contains	{carrot}
O313	onion	1
S17	chopped
S94	on	[cutting board]
O146	cutting board	0
S24	contains	{onion}
M91	scoop and pour	<Assumed>
O386	salad bowl	0
S24	contains	{carrot,onion}
O313	onion	1
S17	chopped
S73	in	[salad bowl]
O146	cutting board	0
S47	empty
//
O63	cabbage	0
S94	on	[cutting board]
S167	whole
O251	knife	1
M12	chop	<2:39-2:54>
O63	cabbage	0
S94	on	[cutting board]
S17	chopped
//
O386	salad bowl	0
S24	contains	{carrot,onion}
O146	cutting board	0
S24	contains	{cabbage}
O63	cabbage	1
S17	chopped
S94	on	[cutting board]
M91	scoop and pour	<2:55-2:57>
O386	salad bowl	0
S24	contains	{carrot,onion,cabbage}
O63	cabbage	1
S17	chopped
S73	in	[salad bowl]
O146	cutting board	0
S47	empty
//
O386	salad bowl	0
S24	contains	{cabbage,carrot,onion}
O290	mixing bowl	1
S24	contains	{salad dressing}
O387	salad dressing	0
S30	creamy
S24	contains	{mayonnaise,sugar,vegetable oil,vinegar}
S73	in	[mixing bowl]
S88	mixed
O424	spatula	1
M73	pour and scrape	<3:03-3:06>
O386	salad bowl	0
S24	contains	{cabbage,carrot,onion,salad dressing}
O387	salad dressing	1
S30	creamy
S24	contains	{mayonnaise,sugar,vegetable oil,vinegar}
S73	in	[salad bowl]
//
O386	salad bowl	0
S24	contains	{cabbage,carrot,onion,salad dressing}
O387	salad dressing	1
S30	creamy
S24	contains	{mayonnaise,sugar,vegetable oil,vinegar}
S73	in	[salad bowl]
O313	onion	1
S17	chopped
S73	in	[salad bowl]
O71	carrot	0
S17	chopped
S73	in	[salad bowl]
O63	cabbage	1
S17	chopped
S73	in	[salad bowl]
O424	spatula	1
M61	mix	<3:08-3:20>
O386	salad bowl	0
S24	contains	{coleslaw}
O385	salad	0	15
S88	mixed
S24	contains	{cabbage,carrot,mayonnaise,onion,sugar,vegetable oil,vinegar}
S73	in	[salad bowl]
O116	coleslaw	0	15
S88	mixed
S24	contains	{cabbage,carrot,mayonnaise,onion,sugar,vegetable oil,vinegar}
S73	in	[salad bowl]
//
