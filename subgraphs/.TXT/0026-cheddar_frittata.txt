# Source:	http://foonets.com/foon_subgraphs/subgraphs/0026-cheddar_frittata.mp4
# Source:	https://www.youtube.com/watch?v=pInOF0S6uxg
//
O146	cutting board	0
S47	empty
O442	sweet pepper	1
S167	whole
M66	pick-and-place	<Assumed>
O146	cutting board	0
S24	contains	{sweet pepper}
O442	sweet pepper	1
S167	whole
S94	on	[cutting board]
//
O251	knife	1
O442	sweet pepper	0
S167	whole
S94	on	[cutting board]
M12	chop	<0:25-0:41>
O442	sweet pepper	0
S17	chopped
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{sweet pepper}
O442	sweet pepper	1
S17	chopped
S94	on	[cutting board]
O45	bowl	0
S47	empty
M66	pick-and-place	<0:41-0:46>
O45	bowl	0
S24	contains	{sweet pepper}
O442	sweet pepper	1
S17	chopped
S73	in	[bowl]
O146	cutting board	0
S47	empty
//
O146	cutting board	0
S47	empty
O301	mushroom	1
S167	whole
M66	pick-and-place	<Assumed>
O146	cutting board	0
S24	contains	{mushroom}
O301	mushroom	1
S94	on	[cutting board]
S167	whole
//
O301	mushroom	0
S167	whole
S94	on	[cutting board]
O251	knife	1
M12	chop	<0:54-1:01>
O301	mushroom	0
S17	chopped
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{mushroom}
O301	mushroom	1
S17	chopped
S94	on	[cutting board]
O45	bowl	0
S47	empty
M66	pick-and-place	<1:10-1:13>
O45	bowl	0
S24	contains	{mushroom}
O301	mushroom	1
S17	chopped
S73	in	[bowl]
O146	cutting board	0
S47	empty
//
O313	onion	1
S167	whole
S160	unpeeled
M65	peel	<Assumed>
O313	onion	1
S167	whole
S104	peeled
//
O146	cutting board	0
S47	empty
O313	onion	1
S167	whole
S104	peeled
M66	pick-and-place	<Assumed>
O146	cutting board	0
S24	contains	{onion}
O313	onion	1
S167	whole
S104	peeled
S94	on	[cutting board]
//
O313	onion	0
S167	whole
S104	peeled
S94	on	[cutting board]
O251	knife	1
M12	chop	<Assumed>
O313	onion	0
S17	chopped
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{onion}
O313	onion	1
S17	chopped
S94	on	[cutting board]
O45	bowl	0
S47	empty
M66	pick-and-place	<Assumed>
O45	bowl	0
S24	contains	{onion}
O313	onion	1
S17	chopped
S73	in	[bowl]
O146	cutting board	0
//
O120	cooking pan	1
S47	empty
O435	stove	0
S92	off (ready)
M66	pick-and-place	<Assumed>
O435	stove	0
S92	off (ready)
S158	under [cooking pan]
O120	cooking pan	1
S94	on	[stove]
S47	empty
//
O435	stove	1
S92	off (ready)
S158	under [cooking pan]
M123	turn on	<Assumed>
O435	stove	1
S94	on
S158	under [cooking pan]
//
O120	cooking pan	0
S47	empty
S94	on	[stove]
O435	stove	0
S94	on
S158	under [cooking pan]
M77	preheat	<Assumed>
O120	cooking pan	0
S94	on	[stove]
S72	hot
S47	empty
//
O120	cooking pan	0
S94	on	[stove]
S72	hot
S47	empty
O44	bottle	1
S24	contains	{olive oil}
O310	olive oil	1
S82	liquid
S73	in	[bottle]
M70	pour	<1:28-1:34>
O120	cooking pan	0
S72	hot
S94	on	[stove]
S24	contains	{olive oil}
O310	olive oil	1
S82	liquid
S73	in	[cooking pan]
//
O310	olive oil	1
S82	liquid
S73	in	[cooking pan]
O120	cooking pan	0
S72	hot
S94	on	[stove]
S24	contains	{olive oil}
O45	bowl	1
S24	contains	{onion}
O313	onion	1
S17	chopped
S73	in	[bowl]
O474	turner	1
M73	pour and scrape	<1:36-1:41>
O120	cooking pan	0
S72	hot
S94	on	[stove]
S24	contains	{olive oil,onion}
O313	onion	1
S17	chopped
S73	in	[cooking pan]
//
O120	cooking pan	0
S72	hot
S94	on	[stove]
S24	contains	{olive oil,onion}
O45	bowl	1
S24	contains	{mushroom}
O301	mushroom	1
S17	chopped
S73	in	[bowl]
O474	turner	1
M73	pour and scrape	<1:42-1:45>
O120	cooking pan	0
S72	hot
S94	on	[stove]
S24	contains	{olive oil,onion,mushroom}
O301	mushroom	1
S17	chopped
S73	in	[cooking pan]
//
O120	cooking pan	0
S72	hot
S94	on	[stove]
S24	contains	{olive oil,onion,mushroom}
O45	bowl	1
S24	contains	{green pepper}
O216	green pepper	1
S17	chopped
S73	in	[bowl]
O474	turner	1
M73	pour and scrape	<1:46-1:48>
O120	cooking pan	0
S72	hot
S94	on	[stove]
S24	contains	{olive oil,onion,mushroom,green pepper}
O216	green pepper	1
S17	chopped
S73	in	[cooking pan]
//
O120	cooking pan	0
S72	hot
S94	on	[stove]
S24	contains	{olive oil,onion,mushroom,green pepper}
O45	bowl	1
S24	contains	{sweet pepper}
O442	sweet pepper	1
S17	chopped
S73	in	[bowl]
O474	turner	1
M73	pour and scrape	<1:49-1:51>
O120	cooking pan	0
S72	hot
S94	on	[stove]
S24	contains	{olive oil,onion,mushroom,green pepper,sweet pepper}
O442	sweet pepper	1
S17	chopped
S73	in	[cooking pan]
//
O120	cooking pan	0
S72	hot
S94	on	[stove]
S24	contains	{olive oil,onion,mushroom,green pepper,sweet pepper}
O313	onion	0
S17	chopped
S73	in	[cooking pan]
O301	mushroom	0
S17	chopped
S73	in	[cooking pan]
O442	sweet pepper	0
S17	chopped
S73	in	[cooking pan]
O216	green pepper	0
S17	chopped
S73	in	[cooking pan]
O474	turner	1
M20	cook and stir	<1:52-1:59>
O313	onion	0
S17	chopped
S122	sauteed
S73	in	[cooking pan]
O301	mushroom	0
S17	chopped
S122	sauteed
S73	in	[cooking pan]
O442	sweet pepper	0
S17	chopped
S122	sauteed
S73	in	[cooking pan]
O216	green pepper	0
S17	chopped
S122	sauteed
S73	in	[cooking pan]
//
O120	cooking pan	0
S72	hot
S94	on	[stove]
S24	contains	{olive oil,onion,mushroom,green pepper,sweet pepper}
O344	pepper mill	1
S24	contains	{black pepper}
O33	black pepper	1
S105	peppercorn
S167	whole
S73	in	[pepper mill]
M49	grind	<2:01-2:02>
O120	cooking pan	0
S72	hot
S94	on	[stove]
S24	contains	{olive oil,onion,mushroom,green pepper,sweet pepper,black pepper}
O33	black pepper	1
S67	ground
S73	in	[cooking pan]
//
O120	cooking pan	0
S72	hot
S94	on	[stove]
S24	contains	{olive oil,onion,mushroom,green pepper,sweet pepper,black pepper}
O244	jar	1
S24	contains	{salt}
O390	salt	1
S64	granulated
S73	in	[jar]
M106	sprinkle	<2:02-2:04>
O120	cooking pan	0
S72	hot
S94	on	[stove]
S24	contains	{olive oil,onion,mushroom,green pepper,sweet pepper,black pepper,salt}
O390	salt	1
S64	granulated
S73	in	[cooking pan]
//
O290	mixing bowl	0
S47	empty
O45	bowl	0
S24	contains	{egg}
O159	egg	1
S167	whole
S73	in	[bowl]
M24	crack	<2:12-2:24>
O290	mixing bowl	0
S24	contains	{egg white,egg yolk}
O165	egg white	0
S155	uncooked
S73	in	[mixing bowl]
O167	egg yolk	0
S155	uncooked
S73	in	[mixing bowl]
//
O290	mixing bowl	0
S24	contains	{egg white,egg yolk}
O165	egg white	0
S155	uncooked
S73	in	[mixing bowl]
O167	egg yolk	0
S155	uncooked
S73	in	[mixing bowl]
O491	whisk	1
M3	beat	<2:25-2:34>
O290	mixing bowl	0
S24	contains	{egg}
O159	egg	1
S6	beaten
S155	uncooked
S73	in	[mixing bowl]
//
O290	mixing bowl	0
S24	contains	{egg}
O244	jar	1
S24	contains	{salt}
O390	salt	1
S64	granulated
S73	in	[jar]
M106	sprinkle	<2:36-2:42>
O290	mixing bowl	0
S24	contains	{egg,salt}
O390	salt	1
S64	granulated
S73	in	[mixing bowl]
//
O290	mixing bowl	0
S24	contains	{egg,salt}
O344	pepper mill	1
S24	contains	{black pepper}
O33	black pepper	1
S105	peppercorn
S73	in	[pepper mill]
M49	grind	<2:40-2:42>
O290	mixing bowl	0
S24	contains	{egg,salt,black pepper}
O33	black pepper	1
S67	ground
S73	in	[mixing bowl]
//
O146	cutting board	0
S47	empty
O77	cheddar	1
S9	block
O207	grater	0
S97	over	[cutting board]
M48	grate	<2:44-2:46>
O146	cutting board	0
S24	contains	{cheddar}
O77	cheddar	1
S65	grated
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{cheddar}
O45	bowl	0
S47	empty
O77	cheddar	1
S65	grated
S94	on	[cutting board]
M66	pick-and-place	<Assumed>
O45	bowl	0
S24	contains	{cheddar}
O77	cheddar	1
S65	grated
S73	in	[bowl]
O146	cutting board	0
S47	empty
//
O290	mixing bowl	0
S24	contains	{egg,salt,black pepper}
O45	bowl	1
S24	contains	{cheddar}
O77	cheddar	1
S65	grated
S73	in	[bowl]
M70	pour	<2:46-2:50>
O290	mixing bowl	0
S24	contains	{egg,salt,black pepper,cheddar}
O77	cheddar	1
S65	grated
S73	in	[mixing bowl]
//
O290	mixing bowl	0
S24	contains	{egg,salt,black pepper,cheddar}
O390	salt	1
S64	granulated
S73	in	[mixing bowl]
O33	black pepper	1
S67	ground
S73	in	[mixing bowl]
O77	cheddar	1
S65	grated
S73	in	[mixing bowl]
O159	egg	1
S6	beaten
S155	uncooked
S73	in	[mixing bowl]
O491	whisk	1
M3	beat	<2:49-2:51>
O159	egg	0
S6	beaten
S155	uncooked
S24	contains	{black pepper,cheddar,salt}
S73	in	[mixing bowl]
//
O120	cooking pan	0
S72	hot
S24	contains	{olive oil,onion,mushroom,green pepper,sweet pepper,black pepper,salt}
S94	on	[stove]
O290	mixing bowl	0
S24	contains	{egg,salt,black pepper,cheddar}
O159	egg	0
S6	beaten
S155	uncooked
S24	contains	{black pepper,cheddar,salt}
S73	in	[mixing bowl]
M70	pour	<2:54-2:57>
O120	cooking pan	0
S72	hot
S24	contains	{olive oil,onion,mushroom,green pepper,sweet pepper,black pepper,salt,egg}
S94	on	[stove]
O159	egg	0
S6	beaten
S155	uncooked
S24	contains	{black pepper,cheddar,salt}
S73	in	[cooking pan]
//
O120	cooking pan	0
S72	hot
S24	contains	{olive oil,onion,mushroom,green pepper,sweet pepper,black pepper,salt,egg}
S94	on	[stove]
O159	egg	0
S6	beaten
S155	uncooked
S24	contains	{black pepper,cheddar,salt}
S73	in	[cooking pan]
O313	onion	0
S17	chopped
S122	sauteed
S73	in	[cooking pan]
O301	mushroom	0
S17	chopped
S122	sauteed
S73	in	[cooking pan]
O442	sweet pepper	0
S17	chopped
S122	sauteed
S73	in	[cooking pan]
O216	green pepper	0
S17	chopped
S122	sauteed
S73	in	[cooking pan]
M16	cook	<Assumed>
O188	frittata	0
S99	partly cooked
S24	contains	{egg,onion,mushroom,green pepper,sweet pepper,black pepper,salt}
S73	in	[cooking pan]
//
O320	oven	1
S47	empty
S92	off (ready)
M123	turn on	<Assumed>
O320	oven	1
S47	empty
S94	on
//
O120	cooking pan	0
S72	hot
S24	contains	{olive oil,onion,mushroom,green pepper,sweet pepper,black pepper,salt,egg}
S94	on	[stove]
O320	oven	0
S47	empty
S94	on
O435	stove	0
S94	on
S158	under [cooking pan]
M53	insert	<3:31-3:37>
O320	oven	0
S94	on
S24	contains	{cooking pan}
O120	cooking pan	0
S72	hot
S24	contains	{olive oil,onion,mushroom,green pepper,sweet pepper,black pepper,salt,egg}
S73	in	[oven]
O435	stove	0
S94	on
//
O435	stove	0
S94	on
M122	turn off	<Assumed>
O435	stove	0
S91	off
//
O320	oven	0
S94	on
S24	contains	{cooking pan}
O120	cooking pan	0
S72	hot
S24	contains	{olive oil,onion,mushroom,green pepper,sweet pepper,black pepper,salt,egg}
S73	in	[oven]
O188	frittata	0
S99	partly cooked
S24	contains	{egg,onion,mushroom,green pepper,sweet pepper,black pepper,salt}
S73	in	[cooking pan]
M2	bake	<Assumed>
O120	cooking pan	0
S72	hot
S24	contains	{frittata}
S73	in	[oven]
O188	frittata	0
S25	cooked
S24	contains	{olive oil,egg,onion,mushroom,green pepper,sweet pepper,black pepper,salt}
S73	in	[cooking pan]
//
O320	oven	0
S94	on
S24	contains	{cooking pan}
M122	turn off	<Assumed>
O320	oven	0
S91	off
S24	contains	{cooking pan}
//
O320	oven	0
S91	off
S24	contains	{cooking pan}
O120	cooking pan	1
S72	hot
S24	contains	{frittata}
S73	in	[oven]
M82	remove	<3:45-3:55>
O320	oven	0
S91	off
S47	empty
O120	cooking pan	1
S72	hot
S24	contains	{frittata}
S73	in	[hand]
//
O120	cooking pan	1
S72	hot
S24	contains	{frittata}
S73	in	[hand]
O146	cutting board	0
S47	empty
M67	place	<3:55-3:56>
O146	cutting board	0
S47	empty
S157	under	[cooking pan]
O120	cooking pan	1
S24	contains	{frittata}
S94	on	[cutting board]
//
O188	frittata	0
S25	cooked
S24	contains	{olive oil,egg,onion,mushroom,green pepper,sweet pepper,black pepper,salt}
S73	in	[cooking pan]
O251	knife	1
M102	slice	<4:09-4:12>
O188	frittata	0
S25	cooked
S137	sliced
S24	contains	{olive oil,egg,onion,mushroom,green pepper,sweet pepper,black pepper,salt}
S73	in	[cooking pan]
//
O120	cooking pan	0
S24	contains	{frittata}
S94	on	[cutting board]
O188	frittata	0
S25	cooked
S137	sliced
S24	contains	{olive oil,egg,onion,mushroom,green pepper,sweet pepper,black pepper,salt}
S73	in	[cooking pan]
O358	plate	0
S47	empty
O474	turner	1
M66	pick-and-place	<4:13-4:19>
O358	plate	0
S24	contains	{frittata}
O188	frittata	0	23
S25	cooked
S137	sliced
S24	contains	{olive oil,egg,onion,mushroom,green pepper,sweet pepper,black pepper,salt}
S94	on	[plate]
S115	ready
//
