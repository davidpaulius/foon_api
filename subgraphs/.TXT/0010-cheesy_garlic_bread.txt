# Source:	http://foonets.com/foon_subgraphs/subgraphs/0010-cheesy_garlic_bread.mp4
# Source:	https://www.youtube.com/watch?v=lXgMX8wteNI
//
O320	oven	0
S91	off
S47	empty
M123	turn on	<Assumed>
O320	oven	0
S94	on
S47	empty
//
O194	garlic	1
S13	bulb
M113	take apart	<Assumed>
O194	garlic	1
S160	unpeeled
S21	clove
//
O194	garlic	1
S160	unpeeled
S21	clove
M65	peel	<Assumed>
O194	garlic	1
S21	clove
S104	peeled
//
O146	cutting board	0
S47	empty
O194	garlic	1
S21	clove
S104	peeled
M66	pick-and-place	<Assumed>
O146	cutting board	0
S24	contains	{garlic}
O194	garlic	1
S21	clove
S94	on	[cutting board]
S104	peeled
//
O180	food processor	0
S92	off (ready)
O181	food processor bowl	1
S47	empty
M1	attach	<Assumed>
O180	food processor	0
S92	off (ready)
S168	with	[food processor bowl]
O181	food processor bowl	1
S1	attached to	[food processor]
S47	empty
//
O181	food processor bowl	0
S1	attached to	[food processor]
S47	empty
O146	cutting board	0
S24	contains	{garlic}
O194	garlic	1
S21	clove
S94	on	[cutting board]
S104	peeled
M66	pick-and-place	<1:19,1:22>
O181	food processor bowl	0
S1	attached to	[food processor]
S24	contains	{garlic}
O194	garlic	1
S21	clove
S94	on	[food processor bowl]
S104	peeled
O146	cutting board	0
S47	empty
//
O181	food processor bowl	0
S1	attached to	[food processor]
S24	contains	{garlic}
O45	bowl	1
S24	contains	{parsley}
O331	parsley	1
S79	leaf
S17	chopped
S73	in	[bowl]
M70	pour	<1:24,1:25>
O181	food processor bowl	0
S1	attached to	[food processor]
S24	contains	{garlic,parsley}
O45	bowl	1
S47	empty
//
O181	food processor bowl	0
S1	attached to	[food processor]
S24	contains	{garlic,parsley}
O45	bowl	1
S24	contains	{olive oil}
O310	olive oil	1
S82	liquid
S73	in	[bowl]
M70	pour	<1:26,1:30>
O181	food processor bowl	0
S1	attached to	[food processor]
S24	contains	{garlic,olive oil,parsley}
O310	olive oil	1
S82	liquid
S73	in	[food processor bowl]
O45	bowl	1
S47	empty
//
O181	food processor bowl	1
S1	attached to	[food processor]
S24	contains	{garlic,olive oil,parsley}
O182	food processor lid	1
M1	attach	<Assumed>
O181	food processor bowl	1
S1	attached to	[food processor]
S27	covered	[food processor lid]
S24	contains	{garlic,olive oil,parsley}
O182	food processor lid	1
S94	on	[food processor bowl]
//
O180	food processor	0
S92	off (ready)
S168	with	[food processor bowl]
M123	turn on	<Assumed>
O180	food processor	0
S94	on
S168	with	[food processor bowl]
//
O180	food processor	0
S94	on
S168	with	[food processor bowl]
O181	food processor bowl	0
S1	attached to	[food processor]
S27	covered	[food processor lid]
S24	contains	{garlic,olive oil,parsley}
O194	garlic	0
S21	clove
S73	in	[food processor bowl]
S104	peeled
O310	olive oil	1
S82	liquid
S73	in	[food processor bowl]
M4	blend	<1:30,1:34>
O196	garlic butter	1
S88	mixed
S24	contains	{garlic,olive oil,parsley}
S73	in	[food processor bowl]
//
O180	food processor	0
S94	on
S168	with	[food processor bowl]
M122	turn off	<Assumed>
O180	food processor	0
S91	off
S168	with	[food processor bowl]
//
O180	food processor	0
S91	off
S168	with	[food processor bowl]
O181	food processor bowl	1
S1	attached to	[food processor]
S27	covered	[food processor lid]
S24	contains	{garlic,olive oil,parsley}
M32	detach	<Assumed>
O180	food processor	0
S91	off
O181	food processor bowl	1
S27	covered	[food processor lid]
S24	contains	{garlic,olive oil,parsley}
//
O182	food processor lid	1
S94	on	[food processor bowl]
O181	food processor bowl	0
S27	covered	[food processor lid]
S24	contains	{garlic,olive oil,parsley}
M32	detach	<Assumed>
O181	food processor bowl	0
S24	contains	{garlic,olive oil,parsley}
O182	food processor lid	1
//
O146	cutting board	0
S47	empty
O47	bread	1
S167	whole
S83	loaf
M66	pick-and-place	<1:37,1:38>
O146	cutting board	0
S24	contains	{bread}
O47	bread	1
S167	whole
S94	on	[cutting board]
S83	loaf
//
O47	bread	0
S167	whole
S94	on	[cutting board]
S83	loaf
O251	knife	1
M25	cut	<1:38,1:44>
O47	bread	0
S94	on	[cutting board]
S68	halved
//
O181	food processor bowl	1
S24	contains	{garlic,olive oil,parsley}
O45	bowl	0
S47	empty
O196	garlic butter	1
S88	mixed
S24	contains	{garlic,olive oil,parsley}
S73	in	[food processor bowl]
O427	spoon	1
M94	scrape and pour	<Assumed>
O45	bowl	0
S24	contains	{garlic,olive oil,parsley}
O196	garlic butter	1
S88	mixed
S24	contains	{garlic,olive oil,parsley}
S73	in	[bowl]
O181	food processor bowl	1
S47	empty
//
O45	bowl	0
S24	contains	{garlic,olive oil,parsley}
O47	bread	0
S94	on	[cutting board]
S68	halved
O196	garlic butter	0
S88	mixed
S24	contains	{garlic,olive oil,parsley}
S73	in	[bowl]
O427	spoon	1
M92	scoop and spread	<1:46,1:51>
O47	bread	0
S24	contains	{garlic butter}
S94	on	[cutting board]
S68	halved
O196	garlic butter	0
S88	mixed
S24	contains	{garlic,olive oil,parsley}
S94	on	[bread]
O427	spoon	1
//
O47	bread	0
S24	contains	{garlic butter}
S94	on	[cutting board]
S68	halved
O45	bowl	0
S24	contains	{mozzarella}
O294	mozzarella	1
S65	grated
S73	in	[bowl]
M106	sprinkle	<1:54,2:03>
O47	bread	0
S24	contains	{garlic butter,mozzarella}
S94	on	[cutting board]
S68	halved
O294	mozzarella	1
S65	grated
S94	on	[bread]
//
O46	box	0
S24	contains	{aluminium foil}
O1	aluminium foil	1
S131	sheet
S73	in	[box]
O13	baking tray	0
S47	empty
M117	tear and place	<2:06,2:10>
O1	aluminium foil	1
S131	sheet
S94	on	[baking tray]
O13	baking tray	0
S47	empty
S168	with	[aluminium foil]
//
O13	baking tray	0
S47	empty
S168	with	[aluminium foil]
O47	bread	1
S24	contains	{garlic butter,mozzarella}
S94	on	[cutting board]
S68	halved
M66	pick-and-place	<2:10,2:14>
O47	bread	1
S24	contains	{garlic butter,mozzarella}
S94	on	[baking tray]
S68	halved
O13	baking tray	0
S24	contains	{bread}
S168	with	[aluminium foil]
O146	cutting board	0
S47	empty
//
O13	baking tray	0
S24	contains	{bread}
S168	with	[aluminium foil]
O47	bread	1
S24	contains	{garlic butter,mozzarella}
S94	on	[baking tray]
S68	halved
O1	aluminium foil	1
S131	sheet
S94	on	[baking tray]
M128	wrap	<2:17,2:21>
O1	aluminium foil	1
S28	covering	[bread]
S94	on	[baking tray]
O47	bread	1
S24	contains	{garlic butter,mozzarella}
S27	covered	[aluminium foil]
S94	on	[baking tray]
S68	halved
//
O13	baking tray	1
S24	contains	{bread}
O320	oven	0
S94	on
M81	put inside	<Assumed>
O320	oven	0
S94	on
S168	with	[baking tray]
O13	baking tray	1
S73	in	[oven]
S24	contains	{bread}
//
O320	oven	0
S94	on
S168	with	[baking tray]
O13	baking tray	1
S73	in	[oven]
S24	contains	{bread}
O47	bread	0
S24	contains	{garlic butter,mozzarella}
S27	covered	[aluminium foil]
S94	on	[baking tray]
S68	halved
M2	bake	<Assumed>
O13	baking tray	1
S73	in	[oven]
S24	contains	{garlic bread}
O195	garlic bread	0
S2	baked
S24	contains	{garlic,mozzarella,olive oil,parsley}
S27	covered	[aluminium foil]
S94	on	[baking tray]
//
O320	oven	0
S94	on
S168	with	[baking tray]
M122	turn off	<Assumed>
O320	oven	0
S91	off
S168	with	[baking tray]
//
O13	baking tray	1
S73	in	[oven]
S24	contains	{garlic bread}
O195	garlic bread	0
S2	baked
S24	contains	{garlic,mozzarella,olive oil,parsley}
S27	covered	[aluminium foil]
S94	on	[baking tray]
O320	oven	0
S91	off
S168	with	[baking tray]
M82	remove	<Assumed>
O320	oven	0
S91	off
O13	baking tray	1
S24	contains	{garlic bread}
//
O13	baking tray	1
S24	contains	{garlic bread}
O195	garlic bread	0
S2	baked
S24	contains	{garlic,mozzarella,olive oil,parsley}
S27	covered	[aluminium foil]
S94	on	[baking tray]
O1	aluminium foil	1
S94	on	[baking tray]
S28	covering	[bread]
O251	knife	1
M125	unwrap	<2:31,2:38>
O195	garlic bread	0
S2	baked
S24	contains	{garlic,mozzarella,olive oil,parsley}
S94	on	[baking tray]
O1	aluminium foil	0
S24	contains	{garlic bread}
S94	on	[baking tray]
//
O195	garlic bread	0
S2	baked
S24	contains	{garlic,mozzarella,olive oil,parsley}
S94	on	[baking tray]
O330	parmesan	1
S9	block
S167	whole
O207	grater	0
M48	grate	<2:39,2:51>
O207	grater	0
O330	parmesan	1
S65	grated
S94	on	[bread]
O195	garlic bread	0
S24	contains	{garlic,mozzarella,olive oil,parmesan,parsley}
S94	on	[baking tray]
//
O13	baking tray	1
S24	contains	{garlic bread}
O195	garlic bread	0
S24	contains	{garlic,mozzarella,olive oil,parmesan,parsley}
S94	on	[baking tray]
O320	oven	0
S94	on
M81	put inside	<Assumed>
O13	baking tray	1
S73	in	[oven]
S24	contains	{garlic bread}
O320	oven	0
S94	on
S168	with	[baking tray]
//
O320	oven	0
S94	on
S168	with	[baking tray]
O13	baking tray	0
S73	in	[oven]
S24	contains	{garlic bread}
O195	garlic bread	0
S24	contains	{garlic,mozzarella,olive oil,parmesan,parsley}
S94	on	[baking tray]
M2	bake	<Assumed>
O195	garlic bread	0
S2	baked
S24	contains	{garlic,mozzarella,olive oil,parmesan,parsley}
S94	on	[baking tray]
//
O13	baking tray	0
S24	contains	{garlic bread}
O195	garlic bread	0
S2	baked
S24	contains	{garlic,mozzarella,olive oil,parmesan,parsley}
S94	on	[baking tray]
O146	cutting board	0
S47	empty
M66	pick-and-place	<2:55,2:58>
O146	cutting board	0
S24	contains	{garlic bread}
O195	garlic bread	0	0
S2	baked
S24	contains	{garlic,mozzarella,olive oil,parmesan,parsley}
S94	on	[cutting board]
O146	cutting board	0
S47	empty
//
O195	garlic bread	0
S2	baked
S24	contains	{garlic,mozzarella,olive oil,parmesan,parsley}
S94	on	[cutting board]
O251	knife	1
M102	slice	<2:58,3:02>
O195	garlic bread	0
S2	baked
S24	contains	{garlic,mozzarella,olive oil,parmesan,parsley}
S44	divided
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{garlic bread}
O195	garlic bread	0
S2	baked
S24	contains	{garlic,mozzarella,olive oil,parmesan,parsley}
S44	divided
S94	on	[cutting board]
O358	plate	0
S47	empty
O427	spoon	1
M66	pick-and-place	<Assumed>
O146	cutting board	0
S47	empty
S24	contains	{garlic bread}
O195	garlic bread	1	0
S24	contains	{black pepper,butter,garlic,parsley,salt}
S94	on	[plate]
S137	sliced
//
