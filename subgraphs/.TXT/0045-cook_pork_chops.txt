O358	plate	0
S47	empty
O359	porkchop	1
S155	uncooked
M66	pick-and-place	Assumed	Assumed
O358	plate	0
S24	contains	{porkchop}
O359	porkchop	1
S155	uncooked
S94	on	[plate]
//
O45	bowl	0
S24	contains	{salt,black pepper}
O33	black pepper	1
S67	ground
S73	in	[bowl]
O390	salt	1
S64	granulated
S73	in	[bowl]
O358	plate	0
S24	contains	{porkchop}
O359	porkchop	1
S155	uncooked
S94	on	[plate]
M106	sprinkle	0:22	0:24
O359	porkchop	0
S24	contains	{salt,black pepper}
S124	seasoned
S94	on	[plate]
//
O120	cooking pan	1
S47	empty
O435	stove	0
S94	on
M66	pick-and-place	Assumed	Assumed
O435	stove	1
S94	on
S168	with	[cooking pan]
O120	cooking pan	0
S70	heated
S94	on	[stove]
//
O120	cooking pan	0
S70	heated
S94	on	[stove]
O251	knife	1
O358	plate	0
S24	contains	{butter}
O61	butter	1
S148	stick
S94	on	[plate]
M91	scoop and pour	0:46	0:50
O120	cooking pan	0
S24	contains	{butter}
S94	on	[stove]
O61	butter	1
S35	cubed
S73	in	[cooking pan]
//
O120	cooking pan	0
S24	contains	{butter}
S94	on	[stove]
O44	bottle	1
S24	contains	{vegetable oil}
O478	vegetable oil	1
S73	in	[bottle]
M70	pour	0:54	0:57
O120	cooking pan	0
S24	contains	{butter,vegetable oil}
S94	on	[stove]
O478	vegetable oil	1
S73	in	[cooking pan]
//
O120	cooking pan	1
S24	contains	{butter,vegetable oil}
S94	on	[stove]
O61	butter	1
S35	cubed
S73	in	[cooking pan]
O478	vegetable oil	1
S73	in	[cooking pan]
M105	spread	1:01	1:06
O120	cooking pan	1
S66	greased
S24	contains	{butter,vegetable oil}
S94	on	[stove]
//
O120	cooking pan	1
S66	greased
S24	contains	{butter,vegetable oil}
S94	on	[stove]
O358	plate	0
S24	contains	{porkchop}
O359	porkchop	0
S24	contains	{salt,black pepper}
S124	seasoned
S94	on	[plate]
O463	tongs	1
M66	pick-and-place	1:42	1:48
O120	cooking pan	0
S24	contains	{porkchop,butter,vegetable oil}
S94	on	[stove]
O359	porkchop	0
S24	contains	{salt,black pepper,butter,vegetable oil}
S124	seasoned
S73	in	[cooking pan]
O358	plate	0
//
O120	cooking pan	0
S24	contains	{porkchop,butter,vegetable oil}
S94	on	[stove]
O359	porkchop	0
S24	contains	{salt,black pepper,butter,vegetable oil}
S124	seasoned
S73	in	[cooking pan]
O463	tongs	1
M40	flip	2:00	2:03
O359	porkchop	0
S24	contains	{salt,black pepper,butter,vegetable oil}
S124	seasoned
S99	partly cooked
S73	in	[cooking pan]
//
O120	cooking pan	0
S24	contains	{porkchop,butter,vegetable oil}
S94	on	[stove]
O13	baking tray	1
S47	empty
M23	cover	2:30	2:33
O120	cooking pan	0
S27	covered
S24	contains	{porkchop,butter,vegetable oil}
S94	on	[stove]
O13	baking tray	1
S94	on	[cooking pan]
//
O120	cooking pan	0
S27	covered
S24	contains	{porkchop,butter,vegetable oil}
S94	on	[stove]
O359	porkchop	0
S24	contains	{salt,black pepper,butter,vegetable oil}
S124	seasoned
S99	partly cooked
S73	in	[cooking pan]
M16	cook	Assumed	Assumed
O359	porkchop	0
S25	cooked
S24	contains	{salt,black pepper,butter,vegetable oil}
S73	in	[cooking pan]
//
O120	cooking pan	0
S27	covered
S24	contains	{porkchop,butter,vegetable oil}
S94	on	[stove]
O13	baking tray	1
S94	on	[cooking pan]
M124	uncover	Assumed	Assumed
O120	cooking pan	0
S24	contains	{porkchop,butter,vegetable oil}
S94	on	[stove]
O13	baking tray	1
//
O120	cooking pan	0
S24	contains	{porkchop,butter,vegetable oil}
S94	on	[stove]
O358	plate	0
S47	empty
O359	porkchop	0
S25	cooked
S24	contains	{salt,black pepper,butter,vegetable oil}
S73	in	[cooking pan]
O463	tongs	1
M66	pick-and-place	3:28	3:31
O120	cooking pan	0
O358	plate	0
S24	contains	{porkchop,butter,vegetable oil}
S94	on	[stove]
O359	porkchop	1	18
S25	cooked
S24	contains	{salt,black pepper,butter,vegetable oil}
S94	on	[plate]
//
