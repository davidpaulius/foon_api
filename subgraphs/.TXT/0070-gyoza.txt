O45	bowl	1
S24	contains	{cabbage}
O63	cabbage	1
S87	minced
S73	in	[bowl]
O424	spatula	1
O360	pot	0
S47	empty
M70	pour	1:24	1:27
O360	pot	0
S24	contains	{cabbage}
O63	cabbage	1
S87	minced
S73	in	[pot]
//
O360	pot	0
S24	contains	{cabbage}
O45	bowl	1
S24	contains	{ground pork}
O224	ground pork	1
S87	minced
S73	in	[bowl]
O424	spatula	1
M70	pour	1:27	1:28
O360	pot	0
S24	contains	{cabbage,ground pork}
O224	ground pork	1
S87	minced
S73	in	[pot]
//
O360	pot	0
S24	contains	{cabbage,ground pork}
O45	bowl	1
S24	contains	{scallion}
O400	scallion	1
S17	chopped
S73	in	[bowl]
O427	spoon	1
M70	pour	1:29	1:32
O427	spoon	1
O360	pot	0
S24	contains	{cabbage,ground pork,scallion}
O400	scallion	0
S17	chopped
S73	in	[pot]
//
O360	pot	0
S24	contains	{cabbage,ground pork,scallion}
O45	bowl	1
S24	contains	{soy sauce}
O422	soy sauce	1
S73	in	[bowl]
M70	pour	1:33	1:34
O360	pot	0
S24	contains	{cabbage,ground pork,scallion,soy sauce}
O422	soy sauce	0
S73	in	[pot]
//
O360	pot	0
S24	contains	{cabbage,ground pork,scallion,soy sauce}
O45	bowl	1
S24	contains	{rice beer}
O379	rice beer	1
S73	in	[bowl]
M70	pour	1:35	1:36
O360	pot	0
S24	contains	{cabbage,ground pork,scallion,soy sauce,rice beer}
O379	rice beer	0
S73	in	[pot]
//
O360	pot	0
S24	contains	{cabbage,ground pork,scallion,soy sauce,rice beer}
O45	bowl	1
S24	contains	{sesame oil}
O407	sesame oil	1
S73	in	[bowl]
M70	pour	1:37	1:39
O360	pot	0
S24	contains	{cabbage,ground pork,scallion,soy sauce,rice beer,sesame oil}
O407	sesame oil	1
S73	in	[pot]
//
O360	pot	0
S24	contains	{cabbage,ground pork,scallion,soy sauce,rice beer,sesame oil}
O45	bowl	1
S24	contains	{garlic}
O194	garlic	1
S87	minced
S73	in	[bowl]
M93	scrape	1:39	1:40
O360	pot	0
S24	contains	{cabbage,ground pork,scallion,soy sauce,sesame oil,garlic}
O194	garlic	0
S87	minced
S73	in	[pot]
//
O360	pot	0
S24	contains	{cabbage,ground pork,scallion,soy sauce,sesame oil,garlic}
O45	bowl	1
S24	contains	{corn starch}
O127	corn starch	1
S73	in	[bowl]
M70	pour	1:42	1:43
O360	pot	0
S24	contains	{cabbage,ground pork,scallion,soy sauce,sesame oil,garlic,corn starch}
O127	corn starch	0
S73	in	[pot]
//
O360	pot	0
S24	contains	{cabbage,ground pork,scallion,soy sauce,sesame oil,garlic,corn starch}
O424	spatula	1
O194	garlic	1
S87	minced
S73	in	[pot]
O224	ground pork	1
S87	minced
S73	in	[pot]
O400	scallion	1
S17	chopped
S73	in	[pot]
O422	soy sauce	1
S73	in	[pot]
O379	rice beer	1
S73	in	[pot]
O407	sesame oil	1
S73	in	[pot]
O127	corn starch	1
S73	in	[pot]
M61	mix	1:44	1:55
O360	pot	0
S24	contains	{gyoza filling}
O227	gyoza filling	1
S24	contains	{cabbage,ground pork,scallion,soy sauce,sesame oil,garlic,corn starch}
S73	in	[pot]
//
O360	pot	0
S24	contains	{gyoza filling}
O157	dumpling wrapper	1
S47	empty
S73	in	[hand]
O227	gyoza filling	1
S24	contains	{cabbage,ground pork,scallion,soy sauce,sesame oil,garlic,corn starch}
S73	in	[pot]
O251	knife	1
M91	scoop and pour	1:59	2:00
O251	knife	1
O157	dumpling wrapper	0
S24	contains	{gyoza filling}
S73	in	[hand]
O227	gyoza filling	1
S94	on	[wrapper]
S24	contains	{cabbage,ground pork,scallion,soy sauce,sesame oil,garlic,corn starch}
//
O157	dumpling wrapper	0
S24	contains	{gyoza filling}
S73	in	[hand]
O227	gyoza filling	1
S94	on	[wrapper]
M43	fold	2:04	2:14
O156	dumpling	0
S169	wrapped
S24	contains	{gyoza filling}
S73	in	[hand]
O227	gyoza filling	1
S73	in	[dumpling]
//
O156	dumpling	0
S169	wrapped
S24	contains	{gyoza filling}
S73	in	[hand]
M78	press	2:15	2:18
O156	dumpling	0
S155	uncooked
S24	contains	{gyoza filling}
S73	in	[hand]
//
O435	stove	1
S91	off
M123	turn on	Assumed	Assumed
O435	stove	0
S94	on
//
O435	stove	0
S94	on
S168	with	[pan]
O120	cooking pan	0
S47	empty
S94	on	[stove]
M52	heat	Assumed	Assumed
O120	cooking pan	0
S70	heated
S94	on	[stove]
//
O120	cooking pan	0
S70	heated
S94	on	[stove]
O45	bowl	1
S24	contains	{vegetable oil}
O478	vegetable oil	1
S73	in	[bowl]
M70	pour	2:40	2:42
O120	cooking pan	0
S24	contains	{vegetable oil}
S94	on	[stove]
O478	vegetable oil	0
S73	in	[pan]
//
O120	cooking pan	0
S24	contains	{vegetable oil}
S94	on	[stove]
O156	dumpling	0
S155	uncooked
S24	contains	{gyoza filling}
S73	in	[hand]
M66	pick-and-place	2:45	2:53
O120	cooking pan	0
S24	contains	{vegetable oil,dumpling}
S94	on	[stove]
O156	dumpling	0
S155	uncooked
S24	contains	{gyoza filling}
S73	in	[pan]
//
O120	cooking pan	0
S24	contains	{vegetable oil,dumpling}
S94	on	[stove]
O282	measuring cup	1
S24	contains	{water}
O484	water	1
S73	in	[measuring cup]
M70	pour	3:02	3:04
O120	cooking pan	0
S24	contains	{vegetable oil,dumpling,water}
S94	on	[stove]
O484	water	1
S73	in	[pan]
//
O120	cooking pan	0
S24	contains	{vegetable oil,dumpling,water}
S94	on	[stove]
O324	pan lid	1
M23	cover	3:07	3:09
O120	cooking pan	0
S27	covered
S24	contains	{vegetable oil,dumpling,water}
S94	on	[stove]
O324	pan lid	1
S94	on	[pan]
//
O120	cooking pan	0
S27	covered
S24	contains	{vegetable oil,dumpling,water}
S94	on	[stove]
O156	dumpling	0
S155	uncooked
S24	contains	{gyoza filling}
S73	in	[pan]
O227	gyoza filling	1
S94	on	[wrapper]
S24	contains	{cabbage,ground pork,scallion,soy sauce,sesame oil,garlic,corn starch}
M16	cook	Assumed	Assumed
O120	cooking pan	0
S27	covered
S24	contains	{gyoza}
S94	on	[stove]
O226	gyoza	0
S25	cooked
S24	contains	{gyoza filling}
S73	in	[pan]
//
O120	cooking pan	0
S27	covered
S24	contains	{gyoza}
S94	on	[stove]
O324	pan lid	0
S94	on	[pan]
M124	uncover	3:14	3:15
O120	cooking pan	0
S24	contains	{gyoza}
S94	on	[stove]
O324	pan lid	1
//
O358	plate	0
S47	empty
O120	cooking pan	1
S24	contains	{gyoza}
S94	on	[stove]
O226	gyoza	0
S25	cooked
S24	contains	{gyoza filling}
S73	in	[pan]
O474	turner	1
M91	scoop and pour	Assumed	Assumed
O358	plate	0
S24	contains	{gyoza}
O226	gyoza	0	26
S25	cooked
S24	contains	{dumpling wrapper,cabbage,ground pork,scallion,soy sauce,sesame oil,vegetable oil,garlic,corn starch,gyoza filling}
S94	on	[plate]
//
