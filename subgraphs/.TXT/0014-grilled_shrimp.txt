# Source:	http://foonets.com/foon_subgraphs/subgraphs/0014-grilled_shrimp.mp4
//
O45	bowl	0
S47	empty
O322	packet	1
S24	contains	{shrimp}
O412	shrimp	1
S167	whole
S114	raw
S73	in	[packet]
M70	pour	<Assumed>
O45	bowl	0
S24	contains	{shrimp}
O412	shrimp	1
S167	whole
S73	in	[bowl]
S114	raw
//
O172	faucet	1
S94	on
O484	water	0
S82	liquid
S59	from	[faucet]
O45	bowl	0
S24	contains	{shrimp}
O412	shrimp	1
S73	in	[bowl]
S114	raw
M84	rinse	<Assumed>
O412	shrimp	1
S167	whole
S73	in	[bowl]
S162	washed
S114	raw
//
O358	plate	0
S47	empty
O45	bowl	1
S24	contains	{shrimp}
O412	shrimp	1
S167	whole
S73	in	[bowl]
S162	washed
S114	raw
M70	pour	<Assumed>
O358	plate	0
S24	contains	{shrimp}
O412	shrimp	1
S167	whole
S94	on	[plate]
S114	raw
O45	bowl	1
S47	empty
//
O44	bottle	1
S24	contains	{olive oil}
O310	olive oil	1
S82	liquid
S73	in	[bottle]
O358	plate	0
S24	contains	{shrimp}
O412	shrimp	0
S167	whole
S94	on	[plate]
S114	raw
M70	pour	<0:02,0:05>
O412	shrimp	0
S24	contains	{olive oil}
S167	whole
S94	on	[plate]
S114	raw
O310	olive oil	1
S82	liquid
S94	on	[shrimp]
//
O358	plate	0
S24	contains	{shrimp}
O412	shrimp	0
S24	contains	{olive oil}
S167	whole
S94	on	[plate]
S114	raw
O222	grinder	1
S24	contains	{salt}
O390	salt	1
S64	granulated
S73	in	[grinder]
M51	grind and sprinkle	<0:07,0:12>
O412	shrimp	0
S24	contains	{olive oil,salt}
S167	whole
S94	on	[plate]
S114	raw
O390	salt	1
S64	granulated
S94	on	[shrimp]
//
O358	plate	0
S24	contains	{shrimp}
O412	shrimp	0
S24	contains	{olive oil,salt}
S167	whole
S94	on	[plate]
S114	raw
O222	grinder	1
S24	contains	{black pepper}
O33	black pepper	1
S105	peppercorn
S167	whole
S73	in	[grinder]
M49	grind	<0:12,0:15>
O412	shrimp	0
S24	contains	{black pepper,olive oil,salt}
S167	whole
S94	on	[plate]
S114	raw
O33	black pepper	1
S67	ground
S94	on	[shrimp]
//
O218	grill	1
S92	off (ready)
S47	empty
M55	light	<Assumed>
O218	grill	1
S94	on
S47	empty
//
O358	plate	0
S24	contains	{shrimp}
O412	shrimp	0
S24	contains	{black pepper,olive oil,salt}
S114	raw
S167	whole
S94	on	[plate]
O463	tongs	1
O218	grill	0
S94	on
S47	empty
M66	pick-and-place	<0:21,0:27>
O412	shrimp	0
S24	contains	{black pepper,olive oil,salt}
S114	raw
S167	whole
S94	on	[grill]
O218	grill	0
S94	on
S24	contains	{shrimp}
//
O218	grill	0
S94	on
S24	contains	{shrimp}
M13	close	<Assumed>
O218	grill	0
S94	on
S20	closed
S24	contains	{shrimp}
//
O218	grill	0
S20	closed
S94	on
S24	contains	{shrimp}
O412	shrimp	0
S24	contains	{black pepper,olive oil,salt}
S114	raw
S167	whole
S94	on	[grill]
M16	cook	<Assumed>
O412	shrimp	0
S167	whole
S94	on	[grill]
S25	cooked
//
O218	grill	0
S94	on
S20	closed
S24	contains	{shrimp}
M64	open	<Assumed>
O218	grill	0
S94	on
S24	contains	{shrimp}
//
O218	grill	0
S94	on
S24	contains	{shrimp}
M122	turn off	<Assumed>
O218	grill	0
S91	off
S24	contains	{shrimp}
//
O218	grill	0
S91	off
S24	contains	{shrimp}
O412	shrimp	1
S167	whole
S94	on	[grill]
S25	cooked
O358	plate	0
S47	empty
O463	tongs	1
M66	pick-and-place	<Assumed>
O218	grill	0
S91	off
S47	empty
O358	plate	0
S24	contains	{shrimp}
O221	grilled shrimp	1	17
S94	on	[plate]
S24	contains	{black pepper,olive oil,salt,shrimp}
S25	cooked
O412	shrimp	1
S167	whole
S94	on	[plate]
S25	cooked
//
