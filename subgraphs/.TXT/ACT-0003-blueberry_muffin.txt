O45	bowl	1
S24	contains	{sugar}
O438	sugar	1
S64	granulated
S73	in	[bowl]
O45	bowl	0
S47	empty
M70	pour	Assumed	Assumed
O45	bowl	0
S24	contains	{sugar}
//
O233	hand mixer	1
S91	off
M123	turn on	Assumed	Assumed
O233	hand mixer	1
S94	on
//
O45	bowl	0
S24	contains	{sugar}
O61	butter	1
S148	stick
S73	in	[packet]
M66	pick-and-place	Assumed	Assumed
O45	bowl	0
S24	contains	{sugar,butter}
O61	butter	1
S148	stick
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{sugar,butter}
O438	sugar	1
S64	granulated
S73	in	[bowl]
O61	butter	1
S148	stick
S73	in	[bowl]
O233	hand mixer	0
S94	on
M4	blend	0:16	0:34
O45	bowl	0
S24	contains	{sugar,butter}
//
O45	bowl	0
S24	contains	{sugar,butter}
O159	egg	1
S156	uncracked
M24	crack	0:35	0:35
O45	bowl	0
S24	contains	{sugar,butter,egg white and yolk}
S73	in	[mixer]
O166	egg white and yolk	1
S73	in	[bowl]
//
O233	hand mixer	0
S94	on
O45	bowl	0
S24	contains	{sugar,butter,egg white and yolk}
O166	egg white and yolk	1
S73	in	[bowl]
M4	blend	0:39	0:47
O45	bowl	0
S24	contains	{sugar,butter,egg white and yolk}
O166	egg white and yolk	1
S6	beaten
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{sugar,butter,egg white and yolk}
O45	bowl	1
S24	contains	{sour cream}
O421	sour cream	1
S73	in	[bowl]
O424	spatula	1
M93	scrape	0:48	0:50
O45	bowl	0
S24	contains	{sugar,butter,egg white and yolk,sour cream}
//
O247	jug	1
S24	contains	{milk}
O285	milk	1
S73	in	[jug]
O45	bowl	0
S24	contains	{sugar,butter,egg white and yolk,sour cream}
M70	pour	0:50	0:51
O45	bowl	0
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk}
O285	milk	1
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk}
O491	whisk	1
M3	beat	0:52	0:54
O45	bowl	0
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk}
//
O282	measuring cup	1
S24	contains	{lemon extract}
O256	lemon extract	1
S73	in	[measuring cup]
O45	bowl	0
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk}
M70	pour	0:56	0:58
O45	bowl	0
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk,lemon extract}
O256	lemon extract	1
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk,lemon extract}
O491	whisk	1
M3	beat	0:59	1:02
O45	bowl	0
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk,lemon extract}
//
O45	bowl	0
S24	contains	{flour}
O45	bowl	0
S24	contains	{baking powder}
O11	baking powder	1
S110	powder
S73	in	[bowl]
M70	pour	1:02	1:04
O45	bowl	0
S24	contains	{flour,baking powder}
O45	bowl	0
//
O45	bowl	1
S24	contains	{baking soda}
O45	bowl	0
S24	contains	{flour,baking powder}
O12	baking soda	1
S110	powder
S73	in	[bowl]
M70	pour	1:05	1:06
O45	bowl	0
S24	contains	{flour,baking powder,baking soda}
O45	bowl	0
//
O45	bowl	1
S24	contains	{salt}
O45	bowl	0
S24	contains	{flour,baking powder,baking soda}
O390	salt	1
S64	granulated
S73	in	[bowl]
M70	pour	1:09	1:10
O45	bowl	0
S24	contains	{flour,baking powder,baking soda,salt}
O45	bowl	0
//
O45	bowl	0
S24	contains	{flour,baking powder,baking soda,salt}
O491	whisk	1
O390	salt	1
S64	granulated
S73	in	[bowl]
O12	baking soda	1
S110	powder
S73	in	[bowl]
O11	baking powder	1
S110	powder
S73	in	[bowl]
O177	flour	1
S110	powder
S73	in	[bowl]
M61	mix	1:11	1:21
O45	bowl	0
S24	contains	{flour mixture}
O179	flour mixture	1
S24	contains	{flour,baking powder,baking soda,salt}
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk,lemon extract}
O45	bowl	1
S24	contains	{flour mixture}
O179	flour mixture	1
S24	contains	{flour,baking powder,baking soda,salt}
S73	in	[bowl]
M70	pour	1:21	1:23
O45	bowl	0
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk,lemon extract,flour mixture}
//
O179	flour mixture	1
S24	contains	{flour,baking powder,baking soda,salt}
S73	in	[bowl]
O424	spatula	1
O45	bowl	0
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk,lemon extract,flour mixture}
M61	mix	1:23	1:28
O45	bowl	0
S24	contains	{muffin batter}
O297	muffin batter	0
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk,lemon extract,flour,baking powder,baking soda,salt}
S73	in	[bowl]
//
O45	bowl	1
S24	contains	{muffin batter}
O45	bowl	0
S24	contains	{flour mixture}
M70	pour	1:28	1:30
O45	bowl	0
S24	contains	{muffin batter,flour mixture}
O297	muffin batter	0
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk,lemon extract,flour,baking powder,baking soda,salt}
S73	in	[bowl]
O45	bowl	1
//
O45	bowl	1
S24	contains	{muffin batter,flour mixture}
O42	blueberry	1
S162	washed
S73	in	[bowl]
O45	bowl	0
S24	contains	{muffin batter}
M70	pour	1:31	1:33
O45	bowl	0
S24	contains	{muffin batter,flour mixture,blueberry}
//
O424	spatula	1
O42	blueberry	1
S162	washed
S73	in	[bowl]
O45	bowl	0
S24	contains	{muffin batter,flour mixture,blueberry}
O297	muffin batter	0
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk,lemon extract,flour,baking powder,baking soda,salt}
S73	in	[bowl]
M61	mix	1:35	1:46
O297	muffin batter	0
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk,lemon extract,flour,baking powder,baking soda,salt,blueberry}
S73	in	[bowl]
//
O299	muffin pan	0
S47	empty
O298	muffin cup	1
S73	in	[packet]
M66	pick-and-place	Assumed	Assumed
O299	muffin pan	0
S168	with	[cup]
O298	muffin cup	0
S73	in	[pan]
//
O402	scoop	1
O297	muffin batter	0
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk,lemon extract,flour,baking powder,baking soda,salt,blueberry}
S73	in	[bowl]
O299	muffin pan	0
S168	with	[cup]
M70	pour	1:47	2:00
O299	muffin pan	0
S24	contains	{muffin batter}
O297	muffin batter	1
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk,lemon extract,flour,baking powder,baking soda,salt,blueberry}
S73	in	[muffin pan]
O402	scoop	0
//
O299	muffin pan	1
S24	contains	{muffin batter}
O297	muffin batter	1
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk,lemon extract,flour,baking powder,baking soda,salt,blueberry}
O320	oven	0
S94	on
M81	put inside	Assumed	Assumed
O299	muffin pan	1
S24	contains	{muffin batter}
S73	in	[oven]
O320	oven	0
S94	on
S168	with	[pan]
//
O299	muffin pan	0
S24	contains	{muffin batter}
S73	in	[oven]
O297	muffin batter	0
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk,lemon extract,flour,baking powder,baking soda,salt,blueberry}
S73	in	[muffin pan]
O320	oven	0
S94	on
S168	with	[pan]
M2	bake	Assumed	Assumed
O299	muffin pan	0
S24	contains	{blueberry muffin}
S73	in	[oven]
O43	blueberry muffin	0	5
S2	baked
S24	contains	{sugar,butter,egg white and yolk,sour cream,milk,lemon extract,flour,baking powder,baking soda,salt,blueberry}
S73	in	[muffin pan]
//
