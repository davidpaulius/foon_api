# Source:	http://foonets.com/foon_subgraphs/subgraphs/0006-blood_orange_salad.mp4 
# Source:	https://www.youtube.com/watch?v=26z5ErTwGEo
//
O322	packet	1
S24	contains	{salad greens}
O388	salad greens	1
S79	leaf
S73	in	[packet]
O45	bowl	0
S47	empty
M70	pour	<Assumed>
O45	bowl	0
S24	contains	{salad greens}
O388	salad greens	1
S79	leaf
S73	in	[bowl]
O322	packet	1
S47	empty
//
O172	faucet	1
S94	on
O484	water	0
S82	liquid
S59	from	[faucet]
O388	salad greens	1
S79	leaf
S73	in	[bowl]
O45	bowl	0
S24	contains	{salad greens}
M84	rinse	<Assumed>
O388	salad greens	1
S79	leaf
S73	in	[bowl]
S162	washed
//
O322	packet	1
S24	contains	{basil}
O17	basil	1
S79	leaf
S73	in	[packet]
O45	bowl	0
S47	empty
M70	pour	<Assumed>
O45	bowl	0
S24	contains	{basil}
O17	basil	1
S79	leaf
S73	in	[bowl]
O322	packet	1
S47	empty
//
O172	faucet	1
S94	on
O484	water	0
S82	liquid
S59	from	[faucet]
O17	basil	1
S79	leaf
S73	in	[bowl]
O45	bowl	0
S24	contains	{basil}
M84	rinse	<Assumed>
O17	basil	1
S79	leaf
S73	in	[bowl]
S162	washed
//
O146	cutting board	0
S47	empty
O7	avocado	1
S167	whole
M66	pick-and-place	Assumed	Assumed
O146	cutting board	0
S24	contains	{avocado}
O7	avocado	1
S167	whole
S94	on	[cutting board]
//
O7	avocado	1
S167	whole
S94	on	[cutting board]
O251	knife	1
M25	cut	1:00	1:04
O7	avocado	1
S138	slit
S94	on	[cutting board]
//
O7	avocado	1
S138	slit
S94	on	[cutting board]
M80	pull apart	1:04	1:07
O7	avocado	0
S68	halved
S94	on	[cutting board]
//
O7	avocado	1
S68	halved
S94	on	[cutting board]
M65	peel	1:08	1:25
O7	avocado	0
S68	halved
S104	peeled
S94	on	[cutting board]
//
O7	avocado	0
S94	on	[cutting board]
S68	halved
S104	peeled
O251	knife	1
M33	dice	<1:26-1:33>
O7	avocado	0
S94	on	[cutting board]
S43	diced
//
O146	cutting board	0
S24	contains	{avocado}
O45	bowl	0
S47	empty
O7	avocado	0
S94	on	[cutting board]
S43	diced
M66	pick-and-place	<Assumed>
O146	cutting board	0
S47	empty
O45	bowl	0
S24	contains	{avocado}
O7	avocado	1
S73	in	[bowl]
S43	diced
//
O146	cutting board	0
S47	empty
O39	blood orange	1
S167	whole
S160	unpeeled
M66	pick-and-place	<Assumed>
O146	cutting board	0
S24	contains	{blood orange}
O39	blood orange	1
S94	on	[cutting board]
S167	whole
S160	unpeeled
//
O39	blood orange	1
S94	on	[cutting board]
S167	whole
S160	unpeeled
M65	peel	<Assumed>
O39	blood orange	1
S94	on	[cutting board]
S104	peeled
S167	whole
//
O39	blood orange	1
S94	on	[cutting board]
S104	peeled
S167	whole
O251	knife	1
M33	dice	<Assumed>
O39	blood orange	1
S94	on	[cutting board]
S43	diced
//
O146	cutting board	0
S24	contains	{blood orange}
O45	bowl	0
S47	empty
O39	blood orange	1
S94	on	[cutting board]
S43	diced
M66	pick-and-place	<Assumed>
O146	cutting board	0
S47	empty
O45	bowl	0
S24	contains	{blood orange}
O39	blood orange	1
S73	in	[bowl]
S43	diced
//
O44	bottle	1
S24	contains	{olive oil}
O45	bowl	0
S47	empty
O310	olive oil	1
S73	in	[bottle]
S82	liquid
M70	pour	<Assumed>
O45	bowl	0
S24	contains	{olive oil}
O310	olive oil	1
S73	in	[bowl]
S82	liquid
//
O44	bottle	1
S24	contains	{lime juice}
O45	bowl	0
S47	empty
O266	lime juice	1
S73	in	[bottle]
M70	pour	<Assumed>
O45	bowl	0
S24	contains	{lime juice}
O266	lime juice	1
S73	in	[bowl]
//
O44	bottle	1
S24	contains	{lemon juice}
O45	bowl	0
S47	empty
O258	lemon juice	1
S73	in	[bottle]
M70	pour	<Assumed>
O45	bowl	0
S24	contains	{lemon juice}
O258	lemon juice	1
S73	in	[bowl]
S75	juice
//
O45	bowl	1
S24	contains	{salad greens}
O388	salad greens	1
S79	leaf
S73	in	[bowl]
S162	washed
O386	salad bowl	0
S47	empty
M73	pour and scrape	<1:14-1:17>
O386	salad bowl	0
S24	contains	{salad greens}
O388	salad greens	1
S79	leaf
S73	in	[salad bowl]
S162	washed
O45	bowl	1
S47	empty
//
O386	salad bowl	0
S24	contains	{salad greens}
O45	bowl	1
S24	contains	{basil}
O17	basil	1
S79	leaf
S73	in	[bowl]
S162	washed
M106	sprinkle	<1:21-1:23>
O386	salad bowl	0
S24	contains	{basil,salad greens}
O17	basil	1
S79	leaf
S73	in	[salad bowl]
S162	washed
O45	bowl	1
S47	empty
//
O386	salad bowl	0
S24	contains	{basil,salad greens}
O45	bowl	1
S24	contains	{avocado}
O7	avocado	1
S73	in	[bowl]
S43	diced
M73	pour and scrape	<1:26-1:28>
O386	salad bowl	0
S24	contains	{avocado,basil,salad greens}
O7	avocado	1
S73	in	[salad bowl]
S43	diced
O45	bowl	1
S47	empty
//
O386	salad bowl	0
S24	contains	{avocado,basil,salad greens}
O45	bowl	1
S24	contains	{blood orange}
O39	blood orange	1
S73	in	[bowl]
S43	diced
M73	pour and scrape	<1:31-1:33>
O386	salad bowl	0
S24	contains	{avocado,basil,blood orange,salad greens}
O39	blood orange	1
S73	in	[salad bowl]
S43	diced
O45	bowl	1
S47	empty
//
O386	salad bowl	0
S24	contains	{avocado,basil,blood orange,salad greens}
O45	bowl	1
S24	contains	{olive oil}
O310	olive oil	1
S73	in	[bowl]
S82	liquid
M70	pour	<1:39-1:42>
O386	salad bowl	0
S24	contains	{avocado,basil,blood orange,olive oil,salad greens}
O310	olive oil	1
S73	in	[salad bowl]
S82	liquid
O45	bowl	1
S47	empty
//
O386	salad bowl	0
S24	contains	{avocado,basil,blood orange,olive oil,salad greens}
O45	bowl	1
S24	contains	{lime juice}
O266	lime juice	1
S73	in	[bowl]
M70	pour	<1:44-1:45>
O386	salad bowl	0
S24	contains	{avocado,basil,blood orange,lime juice,olive oil,salad greens}
O310	olive oil	1
S73	in	[salad bowl]
S82	liquid
O45	bowl	1
S47	empty
//
O386	salad bowl	0
S24	contains	{avocado,basil,blood orange,lime juice,olive oil,salad greens}
O45	bowl	1
S24	contains	{lemon juice}
O258	lemon juice	1
S73	in	[bowl]
M70	pour	<1:47-1:48>
O386	salad bowl	0
S24	contains	{avocado,basil,blood orange,lemon juice,lime juice,olive oil,salad greens}
O258	lemon juice	1
S73	in	[salad bowl]
O45	bowl	1
S47	empty
//
O386	salad bowl	0
S24	contains	{avocado,basil,blood orange,lemon juice,lime juice,olive oil,salad greens}
O45	bowl	1
S24	contains	{salt}
O390	salt	1
S73	in	[bowl]
S64	granulated
M106	sprinkle	<1:52-1:53>
O386	salad bowl	0
S24	contains	{avocado,basil,blood orange,lemon juice,lime juice,olive oil,salad greens,salt}
O390	salt	1
S73	in	[salad bowl]
S64	granulated
//
O386	salad bowl	0
S24	contains	{avocado,basil,blood orange,lemon juice,lime juice,olive oil,salad greens,salt}
O45	bowl	1
S24	contains	{black pepper}
O33	black pepper	1
S73	in	[bowl]
S67	ground
M106	sprinkle	<1:56-1:57>
O386	salad bowl	0
S24	contains	{avocado,basil,black pepper,blood orange,lemon juice,lime juice,olive oil,salad greens,salt}
O33	black pepper	1
S73	in	[salad bowl]
S67	ground
//
O386	salad bowl	0
S24	contains	{avocado,basil,black pepper,blood orange,lemon juice,lime juice,olive oil,salad greens,salt}
O390	salt	0
S73	in	[salad bowl]
S64	granulated
O33	black pepper	0
S73	in	[salad bowl]
S67	ground
O388	salad greens	0
S79	leaf
S73	in	[salad bowl]
S162	washed
O17	basil	0
S79	leaf
S73	in	[salad bowl]
S162	washed
O310	olive oil	0
S73	in	[salad bowl]
S82	liquid
O39	blood orange	0
S73	in	[salad bowl]
S43	diced
O7	avocado	0
S73	in	[salad bowl]
S43	diced
O266	lime juice	0
S73	in	[salad bowl]
O258	lemon juice	0
S73	in	[salad bowl]
O427	spoon	1
S47	empty
M61	mix	<1:59-2:16>
O385	salad	0	15
S73	in	[salad bowl]
S88	mixed
O40	blood orange salad	0	15
S24	contains	{avocado,basil,black pepper,blood orange,lemon juice,lime juice,olive oil,salad greens,salt}
S73	in	[salad bowl]
S88	mixed
//
