O435	stove	0
S91	off
M123	turn on	Assumed	Assumed
O435	stove	0
S94	on
//
O282	measuring cup	0
S47	empty
O159	egg	1
S156	uncracked
M24	crack	Assumed	Assumed
O282	measuring cup	0
S24	contains	{egg}
O159	egg	1
S29	cracked
S73	in	[measuring cup]
//
O159	egg	1
S29	cracked
S73	in	[measuring cup]
O45	bowl	0
S47	empty
M70	pour	1:18	1:21
O45	bowl	0
S24	contains	{egg}
O159	egg	1
S29	cracked
S73	in	[bowl]
//
O45	bowl	1
S24	contains	{milk}
O285	milk	1
S73	in	[bowl]
O159	egg	1
S29	cracked
S73	in	[bowl]
M70	pour	1:22	1:24
O45	bowl	0
S24	contains	{egg,milk}
//
O45	bowl	0
S24	contains	{egg,milk}
O183	fork	1
M61	mix	1:24	1:38:50
O45	bowl	0
S24	contains	{egg mixture}
O161	egg mixture	0
S73	in	[bowl]
S24	contains	{egg,milk}
//
O415	skillet	1
S47	empty
O435	stove	0
S94	on
M52	heat	1:40	1:42
O415	skillet	0
S70	heated
S94	on	[stove]
//
O45	bowl	1
S24	contains	{butter}
O61	butter	1
S35	cubed
S73	in	[bowl]
O415	skillet	0
S70	heated
S94	on	[stove]
M70	pour	1:57	2:00
O61	butter	0
S35	cubed
S73	in	[pan]
O415	skillet	0
S24	contains	{butter}
S94	on	[stove]
//
O435	stove	0
S94	on
S168	with	[pan]
O61	butter	0
S35	cubed
S73	in	[pan]
O415	skillet	0
S24	contains	{butter}
S94	on	[stove]
M52	heat	Assumed	Assumed
O61	butter	0
S86	melted
S73	in	[pan]
O415	skillet	0
S24	contains	{butter}
S94	on	[stove]
//
O47	bread	1
S137	sliced
O45	bowl	0
S24	contains	{egg mixture}
M66	pick-and-place	2:01:50	2:04
O45	bowl	0
S24	contains	{egg,milk,bread}
O47	bread	0
S24	contains	{egg mixture}
S73	in	[bowl]
//
O47	bread	1
S24	contains	{egg mixture}
S73	in	[bowl]
O45	bowl	0
S24	contains	{egg,milk,bread}
M40	flip	2:07	2:10
O47	bread	1
S101	partly soaked
S24	contains	{egg mixture}
S73	in	[bowl]
//
O47	bread	1
S101	partly soaked
S24	contains	{egg mixture}
S73	in	[bowl]
O45	bowl	0
S24	contains	{egg,milk,bread}
M40	flip	2:12	2:15
O47	bread	1
S139	soaked
S24	contains	{egg mixture}
S73	in	[bowl]
//
O47	bread	1
S139	soaked
S24	contains	{egg mixture}
S73	in	[bowl]
O415	skillet	0
S23	contain	{butter}
S94	on	[stove]
M66	pick-and-place	2:37:50	2:41
O415	skillet	0
S24	contains	{bread}
S94	on	[stove]
O47	bread	1
S139	soaked
S24	contains	{egg mixture}
S73	in	[pan]
//
O47	bread	1
S139	soaked
S24	contains	{egg mixture}
S73	in	[pan]
O415	skillet	0
S24	contains	{bread}
S94	on	[stove]
O474	turner	1
M40	flip	Assumed	Assumed
O184	french toast	1
S99	partly cooked
S73	in	[pan]
S24	contains	{bread,egg mixture,egg,milk,butter}
//
O435	stove	0
S94	on
S168	with	[pan]
O415	skillet	0
S24	contains	{bread}
S94	on	[stove]
O184	french toast	1
S99	partly cooked
S73	in	[pan]
S24	contains	{bread,egg mixture,egg,milk,butter}
M16	cook	Assumed	Assumed
O415	skillet	0
S24	contains	{french toast}
S94	on	[stove]
O184	french toast	1
S25	cooked
S73	in	[pan]
S24	contains	{bread,egg mixture,egg,milk,butter}
//
O358	plate	0
S47	empty
O184	french toast	1
S25	cooked
S73	in	[pan]
S24	contains	{bread,egg mixture,egg,milk,butter}
O415	skillet	0
S24	contains	{french toast}
S94	on	[stove]
M66	pick-and-place	2:52	3:00:50
O358	plate	0
S24	contains	{french toast}
O184	french toast	1
S25	cooked
S94	on	[plate]
S24	contains	{bread,egg mixture,egg,milk,butter}
//
O358	plate	0
S24	contains	{french toast}
O184	french toast	1
S25	cooked
S94	on	[plate]
S24	contains	{bread,egg mixture,egg,milk,butter}
O45	bowl	1
S24	contains	{maple syrup}
O273	maple syrup	1
S73	in	[bowl]
M70	pour*	3:08	3:13
O184	french toast	1
S94	on	[plate]
S24	contains	{bread,egg mixture,egg,butter,maple syrup}
//
O146	cutting board	0
S47	empty
O437	strawberry	0
S167	whole
M66	pick-and-place	Assumed	Assumed
O146	cutting board	0
S24	contains	{strawberry}
O437	strawberry	0
S167	whole
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{strawberry}
O437	strawberry	0
S167	whole
S94	on	[cutting board]
M12	chop	Assumed	Assumed
O437	strawberry	0
S17	chopped
S94	on	[cutting board]
//
O146	cutting board	0
S24	contains	{strawberry}
O437	strawberry	0
S17	chopped
S94	on	[cutting board]
O45	bowl	0
S47	empty
M70	pour	Assumed	Assumed
O45	bowl	0
S24	contains	{strawberry}
O437	strawberry	0
S17	chopped
S73	in	[bowl]
//
O358	plate	0
S24	contains	{french toast}
O184	french toast	1
S25	cooked
S94	on	[plate]
S24	contains	{bread,egg mixture,egg,milk,butter,maple syrup}
O45	bowl	1
S24	contains	{strawberry}
O437	strawberry	1
S17	chopped
S73	in	[bowl]
M66	pick-and-place	3:14	3:22
O184	french toast	1	0
S24	contains	{bread,egg mixture,egg,butter,milk,maple syrup,strawberry}
S94	on	[plate]
//
