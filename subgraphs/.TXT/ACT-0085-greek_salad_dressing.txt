# Source:	https://www.youtube.com/watch?v=9Bl7dbwT2ZM
# Source:	http://foonets.com/foon_subgraphs/subgraphs/ACT-0085-greek_salad_dressing.mp4
//
O282	measuring cup	0
S47	empty
O45	bowl	1
S24	contains	{lemon juice}
O258	lemon juice	1
S73	in	[bowl]
S75	juice
M70	pour	<0:31-0:37>
O282	measuring cup	0
S24	contains	{lemon juice}
O258	lemon juice	1
S73	in	[measuring cup]
//
O45	bowl	0
S47	empty
O409	shaker	1
S24	contains	{oregano}
O318	oregano	1
S45	dried
S73	in	[shaker]
M95	shake	<Assumed>
O45	bowl	0
S24	contains	{oregano}
O318	oregano	1
S45	dried
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{oregano}
O409	shaker	1
S24	contains	{basil}
O17	basil	1
S45	dried
S73	in	[shaker]
M95	shake	<Assumed>
O45	bowl	0
S24	contains	{oregano,basil}
//
O45	bowl	0
S24	contains	{oregano,basil}
O409	shaker	1
S24	contains	{thyme}
O455	thyme	1
S45	dried
S73	in	[shaker]
M95	shake	<Assumed>
O45	bowl	0
S24	contains	{oregano,basil,thyme}
O455	thyme	1
S45	dried
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{oregano,basil,thyme}
O409	shaker	1
S24	contains	{rosemary}
O383	rosemary	1
S45	dried
S73	in	[shaker]
M95	shake	<Assumed>
O45	bowl	0
S24	contains	{oregano,basil,thyme,rosemary}
//
O45	bowl	0
S24	contains	{oregano,basil,thyme,rosemary}
O409	shaker	1
S24	contains	{marjoram}
O275	marjoram	1
S45	dried
S73	in	[shaker]
M95	shake	<Assumed>
O45	bowl	0
S24	contains	{oregano,basil,thyme,rosemary,marjoram}
O275	marjoram	1
S45	dried
S73	in	[bowl]
//
O45	bowl	0
S24	contains	{oregano,basil,thyme,rosemary,marjoram}
O318	oregano	0
S45	dried
S73	in	[bowl]
O275	marjoram	0
S45	dried
S73	in	[bowl]
O383	rosemary	0
S45	dried
S73	in	[bowl]
O455	thyme	0
S45	dried
S73	in	[bowl]
O427	spoon	1
S47	empty
M61	mix	<Assumed>
O45	bowl	0
S24	contains	{italian seasoning}
O242	italian seasoning	0
S88	mixed
S45	dried
S73	in	[bowl]
//
O282	measuring cup	0
S24	contains	{lemon juice}
O45	bowl	1
S24	contains	{italian seasoning}
O242	italian seasoning	1
S88	mixed
S45	dried
S73	in	[bowl]
M70	pour	<0:38-0:42>
O282	measuring cup	0
S24	contains	{italian seasoning,lemon juice}
O242	italian seasoning	0
S45	dried
S88	mixed
S73	in	[measuring cup]
//
O282	measuring cup	0
S24	contains	{italian seasoning,lemon juice}
O45	bowl	1
S24	contains	{garlic}
O194	garlic	1
S87	minced
S73	in	[bowl]
M70	pour	<0:42-0:50>
O282	measuring cup	0
S24	contains	{garlic,italian seasoning,lemon juice}
O194	garlic	1
S73	in	[measuring cup]
S87	minced
//
O282	measuring cup	0
S24	contains	{garlic,italian seasoning,lemon juice}
O344	pepper mill	1
S24	contains	{black pepper}
O33	black pepper	0
S105	peppercorn
S73	in	[pepper mill]
S167	whole
M49	grind	<0:50-1:00>
O282	measuring cup	0
S24	contains	{garlic,italian seasoning,lemon juice,pepper}
O33	black pepper	0
S73	in	[measuring cup]
S67	ground
//
O282	measuring cup	0
S24	contains	{garlic,italian seasoning,lemon juice,pepper}
O222	grinder	1
S24	contains	{salt}
O390	salt	1
S64	granulated
S106	pieces
S73	in	[grinder]
M51	grind and sprinkle	<1:00-1:06>
O282	measuring cup	0
S24	contains	{garlic,italian seasoning,lemon juice,pepper,salt}
O390	salt	1
S73	in	[measuring cup]
S64	granulated
//
O282	measuring cup	0
S24	contains	{garlic,italian seasoning,lemon juice,pepper,salt}
O45	bowl	1
S24	contains	{olive oil}
O310	olive oil	1
S82	liquid
S73	in	[bowl]
O491	whisk	1
M75	pour and stir	<1:07-2:27>
O282	measuring cup	0
S24	contains	{salad dressing}
O387	salad dressing	0	7
S30	creamy
S88	mixed
S24	contains	{garlic,italian seasoning,lemon juice,olive oil,pepper,salt}
S73	in	[bowl]
O209	greek salad dressing	0	7
S30	creamy
S24	contains	{garlic,italian seasoning,lemon juice,olive oil,pepper,salt}
S73	in	[bowl]
//
