###################################################################################################
# 					    FOON: Parser (FOON_parser.py) 		 			#
# 			--------------------------------------------------------------------		#
# 	Written by David Paulius (davidpaulius@usf.edu) of RPAL (University of South Florida) 	#
#	     (with special thanks to William David Buchanan for initial version of parser)		#
###################################################################################################

# NOTE: this script is used for rebuilding index files used for FOON as well as making
#	labels consistent among all subgraph files within a given directory.
# -- Input to this function is simply a path to the folder containing the old, unclean subgraph files.
# -- You may include older index files ('objectIndex.txt', 'motionIndex.txt', and 'stateIndex.txt')
# 	to use as base list to be added on.

from __future__ import print_function
import os
import sys
import json
import getopt
import requests
from configparser import ConfigParser
from datetime import datetime


# -- added NLTK lemmatization process to remove unnecessarily plural words:
lemmatizer = None
try:
    from nltk.stem import WordNetLemmatizer
    lemmatizer = WordNetLemmatizer()

    from nltk.corpus import wordnet

except ImportError:
    print(' -- ERROR: NLTK is not downloaded and available for use!')
    print("\t-- Please install it using 'pip install nltk' and then use 'nltk.download()' from a Python terminal.")
    print("\t-- Refer to <https://www.nltk.org/data.html> for more details.")
    sys.exit()


config_file = 'config.ini'
config = ConfigParser()
config.read(config_file)

last_updated = '14th October, 2020'

source_dir = None


def _check_args():
    global source_dir
    try:
        opts, _ = getopt.getopt(sys.argv[1:], 'src:h', ['source_dir=', 'help'])

        for opt, arg in opts:
            if opt in ('-src', '--source_dir'):
                print("  -- Unparsed files will be found in '" + str(arg) + "'")
                source_dir = arg
            else:
                _usage()
    except getopt.GetoptError:
        _usage()
        sys.exit(2)


def _usage():
    print("ERROR: incorrect arguments given to program!")
    print(" --source_dir=""XXX""\t-\tparse files found in path/directory 'XXX'.")


def update_index_files(file_list, objectIndex, object_senses, motionIndex, motion_categories, stateIndex):
    for F in file_list:
        if not F.endswith('.txt'):
            continue
        unparsed_file = open(os.path.join(source_dir, F), "r")

        # Iterate through FOON files to find objects, states, identifiers, and motions.
        for line in unparsed_file:
            if line.startswith("/"):
                continue

            label = line.split("\t")
            if len(label) < 2:
                continue

            label[1] = label[1].lower().rstrip()

            if line.startswith("O"):
                if len(line.split('\t')) < 3:
                    print('ERROR: there is a mistake with this file:')
                    print('line ' + str(unparsed_file.index(line)) + '\t-\t' + line)
                    sys.exit(0)

                else:
                    objectIndex.append(label[1])

            if line.startswith("S"):
                stateIndex.append(label[1])

            if line.startswith("M"):
                if len(line.split('\t')) < 4 and '<' not in line:
                    print('ERROR: there is a mistake with this file:')
                    #print('line ' + str(unparsed_file.index(line)) + '\t-\t' + line)
                    sys.exit(0)

                elif len(line.split('\t')) < 3:
                    print('ERROR: there is a mistake with this file:')
                    #print('line ' + str(unparsed_file.index(line)) + '\t-\t' + line)
                    sys.exit(0)

                # -- remove asterisk signifying a composite functional unit:
                else:
                    label[1] = label[1].replace('*', '')
                    motionIndex.append(label[1])

        unparsed_file.close()

    # # NOTE: make sure that everything is unique and sorted before beginning!
    objectIndex = sorted(list(set(objectIndex)))
    motionIndex = sorted(list(set(motionIndex)))
    stateIndex = sorted(list(set(stateIndex)))

    # NOTE: try using WordNet to guess the appropriate sense of a word, so that the newly updated index
    #	will reflect these new additions. This should only be done if NLTK and WordNet are available.

    print('\n-- Revising object label senses using WordNet and Concept-Net...\n')

    # -- this can be done to alleviate the need to manually assign all senses

    for O in objectIndex:
        # -- if a word already has a sense assigned to it, then we just skip it:
        if O in object_senses:
            continue

        # -- prepare the object label for analysis with WordNet; change whitespaces to underscores:
        object_name = O.replace(' ', '_')
        object_synsets = wordnet.synsets(object_name)

        # -- by default, use the first sense:
        object_sense = 1

        sense_found = False
        # -- first, check to see if there is a 'food' definition for the object:
        for synset in object_synsets:
            if 'food' in synset.lexname():
                object_sense = object_synsets.index(synset) + 1
                sense_found = True
                break

        if sense_found:
            object_senses[O] = int(object_sense)
            continue

        # -- if there is no food definition, then we can try to see if we can find a 'substance' meaning
        #	(for certain ingredients or liquids):
        for synset in object_synsets:
            if 'substance' in synset.lexname():
                object_sense = object_synsets.index(synset) + 1
                sense_found = True
                break

        if sense_found:
            object_senses[O] = int(object_sense)
            continue

        # -- if there is no food definition, then we can try to see if we can find an 'artifact' meaning
        #	(for utensils, tools or appliances):
        for synset in object_synsets:
            if 'artifact' in synset.lexname():
                object_sense = object_synsets.index(synset) + 1
                sense_found = True
                break

        if sense_found:
            object_senses[O] = int(object_sense)
            continue

        # --  check to see if a word or phrase exists as a concept in Concept-Net:
        # try:
        # 	query_concept = requests.get('http://api.conceptnet.io/c/en/' + object_name).json()
        # 	if 'edges' in query_concept:
        # 		# -- even if we cannot find it in WordNet, we could use Concept-Net to find similarity:
        # 		object_senses[O] = 'C'
        # except ConnectionError:
        # 	continue

        # --  if a word was not found, we will not give it a sense number

    # endfor

    # NOTE: (current idea) create a combined index file that contains objects, motions and states:
    # -- this file will be a .JSON file..
    index_file = {}
    index_file['date_created'] = str(datetime.today().strftime('%d.%m.%Y'))
    index_file['motions'] = {}
    index_file['objects'] = {}
    index_file['states'] = {}

    # -- writing object labels to files..
    parsed_file_str = ''
    for i, O in enumerate(objectIndex):
        # -- writing to .TXT index file:
        if O in object_senses:
            parsed_file_str += str(i) + "\t" + str(O) + \
                "\t" + str(object_senses[O]) + "\n"
        else:
            parsed_file_str += str(i) + "\t" + str(O) + "\n"

        # -- writing to .JSON index file:
        index_file['objects'][O] = {}
        index_file['objects'][O]['id'] = i
        if O in object_senses:
            index_file['objects'][O]['sense'] = object_senses[O]
    # end

    object_file = open(config['Index']['object_index'], 'w')
    object_file.truncate(0)
    object_file.write(parsed_file_str)
    object_file.close()

    # -- writing motion labels to files..
    parsed_file_str = ''
    for i, M in enumerate(motionIndex):
        # -- writing to .TXT index file:
        parsed_file_str += str(i) + "\t" + str(M) + (str("\t" + str(
            motion_categories[M])) if M in motion_categories else str()) + "\n"

        # -- writing to .JSON index file:
        index_file['motions'][M] = {}
        index_file['motions'][M]['id'] = i
        if M in motion_categories:
            index_file['motions'][M]['motion_type'] = motion_categories[M]
    # end
    motion_file = open(config['Index']['motion_index'], 'w')
    motion_file.truncate(0)
    motion_file.write(parsed_file_str)
    motion_file.close()

    # -- writing state labels to files..
    parsed_file_str = ''
    for i, S in enumerate(stateIndex):
        parsed_file_str += str(i) + "\t" + str(S) + "\n"
        index_file['states'][S] = {}
        index_file['states'][S]['id'] = i
    # end
    state_file = open(config['Index']['state_index'], 'w')
    state_file.truncate(0)
    state_file.write(parsed_file_str)
    state_file.close()

    # NOTE: this is the combined index file:
    json.dump(index_file, open(
        config['Index']['FOON_index'], 'w'), indent=7, sort_keys=True)

    return objectIndex, motionIndex, stateIndex


def update_unparsed_files(file_list, objectIndex, motionIndex, stateIndex):

    # -- now that we have revised the index files, we will now adjust the labels in the old files:
    for F in file_list:
        if not F.endswith('.txt'):
            continue

        unparsed_filepath = os.path.join(source_dir, F)
        parsed_filepath = os.path.join(source_dir, 'parsed.txt')
        unparsed_file = open(unparsed_filepath, "r")
        parsed_file = open(parsed_filepath, "w")

        print('  -- Saving ' + unparsed_filepath + '...')

        for line in unparsed_file:
            if line.startswith("# Source:"):
                # -- new addition: note the source of the annotated video:
                parsed_file.write(line)
                continue

            if line.startswith("/"):
                # -- functional unit separators:
                parsed_file.write('//\n')
                continue

            label = line.split("\t")
            if len(label) < 2:
                # -- this is to make sure that we skip incorrect lines
                print(
                    ' -- WARNING: there is a line that is possibly incorrect : ' + str(label))
                continue

            parsed_line = ''

            label[1] = label[1].lower().rstrip()

            if line.startswith("O"):
                # len = 3 for goal node
                if len(label) > 3:
                    parsed_line = 'O' + str(objectIndex.index(
                        label[1])) + '\t' + label[1] + '\t' + label[2] + '\t' + str(label[3].rstrip()) + '\n'

                else:
                    parsed_line = 'O' + \
                        str(objectIndex.index(
                            label[1])) + '\t' + label[1] + '\t' + str(label[2].rstrip()) + '\n'

            if line.startswith("S"):
                if len(label) > 2:
                    parsed_line = 'S' + \
                        str(stateIndex.index(label[1])) + '\t' + \
                        label[1] + '\t' + label[2].rstrip() + '\n'

                else:
                    parsed_line = 'S' + \
                        str(stateIndex.index(label[1])
                            ) + '\t' + label[1] + '\n'

            if line.startswith("M"):
                if '<' in line:
                    parsed_line = 'M' + str(motionIndex.index(label[1].replace(
                        '*', ''))) + '\t' + label[1] + '\t' + label[2].rstrip() + '\n'
                else:
                    parsed_line = 'M' + str(motionIndex.index(label[1].replace(
                        '*', ''))) + '\t' + label[1] + '\t' + label[2] + '\t' + label[3].rstrip() + '\n'
            if 'clean' in parsed_line or 'dirty' in parsed_line:
                continue
            else:
                parsed_file.write(parsed_line)
        # endfor

        parsed_file.close()
        unparsed_file.close()

        # replace the unparsed file with the parsed file
        os.rename(parsed_filepath, unparsed_filepath)

    # endfor


def foon_categorization(motion_categories):
    # ######################################################################################
    # ########################	2)	FOON CATEGORIZATION		#######################
    # ######################################################################################

    # NOTE: this has to do with categories of objects and motions that are used for some of FOON's operations.
    #	however, these are not used for common tasks but rather for things such as FOON task tree retrieval
    #		using concepts of expansion / compression.
    # -- please refer to Paulius et al. 2018 (ICRA 2018) for further explanation on these operations.
    # -- if you do not care about these, you can skip this part and set 'skip_categorization' flag to True.

    if config['Parser']['skip_categorization'] == 'False':
        _response = input(
            '\n -- Create the FOON categories index file? [Y/N] > ')

        if _response == 'y' or _response == 'Y':
            category_index = {}

            category_index['date_created'] = str(
                datetime.today().strftime('%d.%m.%Y'))

            category_index['object_categories'] = {}

            _response = input(
                '  -- Re-use object categories from previous version (if not, it will create index from legacy file)? [Y/N] > ')

            if _response == 'y' or _response == 'Y':
                # -- try to use existing 'FOON_categories.json' file and build upon it:
                try:
                    _file = open('FOON_categories.json', 'r')
                    categories_file = json.load(_file)
                    category_index['object_categories'] = categories_file['object_categories'] if 'object_categories' in categories_file else {
                    }

                except FileNotFoundError:
                    pass

            else:
                # -- if there is no 'FOON_categories.json' file, then just assume that we have to build from legacy version (.TXT file):

                try:
                    _file = open('object_categories.txt', 'r')

                except FileNotFoundError:
                    pass

                items = _file.read().splitlines()

                for line in items:
                    temp = line.split(":")
                    new_category = temp[0]
                    category_index['object_categories'][new_category] = []
                    temp = temp[1].split(",")
                    for S in temp:
                        if bool(S):
                            category_index['object_categories'][new_category].append(
                                S)
                        # endif
                    # endfor
                # endfor
            # end

            category_index['motion_categories'] = {
                'location_critical': [], 'state_critical': [], 'time_critical': []}

            for _item in motion_categories:
                # NOTE: L - location critical, T - time critical, S - state critical:
                if motion_categories[_item] == 'L':
                    category_index['motion_conditions']['location_critical'].append(
                        _item)
                elif motion_categories[_item] == 'S':
                    category_index['motion_conditions']['state_critical'].append(
                        _item)
                elif motion_categories[_item] == 'T':
                    category_index['motion_conditions']['time_critical'].append(
                        _item)
                else:
                    print(
                        " -- WARNING: incorrect motion criterion flag for motion '" + str(_item) + "'")
                # endif
            # endfor

            category_index['state_taxonomy'] = {}

            _response = input(
                '  -- Re-use state taxonomy from previous version (if not, it will create index from legacy file)? [Y/N] > ')

            if _response == 'y' or _response == 'Y':
                # -- try to use existing 'FOON_categories.json' file and build upon it:
                try:
                    _file = open('FOON_categories.json', 'r')
                    categories_file = json.load(_file)
                    category_index['state_taxonomy'] = categories_file['state_taxonomy'] if 'state_taxonomy' in categories_file else {
                    }

                except FileNotFoundError:
                    pass

            else:
                # -- if there is no 'FOON_categories.json' file, then just assume that we have to build from legacy version (.TXT file):

                try:
                    _file = open('state_taxonomy.txt', 'r')

                except FileNotFoundError:
                    pass

                items = _file.read().splitlines()

                for line in items:
                    temp = line.split(":")
                    new_category = temp[0]
                    category_index['state_taxonomy'][new_category] = []
                    temp = temp[1].split(",")
                    for S in temp:
                        if bool(S):
                            category_index['state_taxonomy'][new_category].append(
                                S)
                        # endif
                    # endfor
                # endfor
            # end

            json.dump(category_index, open(
                'FOON_categories.json', 'w'), indent=7)

        # endif
    # endif


def _run_parser():
    # -- use the defined variables as on the higher scope:
    global source_dir

    print("< FOON_parser.py (updated " + str(last_updated) + " ) >\n")

    if not source_dir:
        source_dir = config['Paths']['data_source']

    # -- First, list all files within a certain folder:
    file_list = os.listdir(source_dir)

    objectIndex = []
    motionIndex = []
    stateIndex = []

    # NOTE: these structures are used to hold meaning behind the objects or
    # -- we note the object label's sense (or meaning) based on WordNet (if it exists in WordNet, i.e.).
    # -- we also note the motion's categorization based on three types: 1) location-critical motions, 2) state-critical, and 3) time-critical.
    object_senses = {}
    motion_categories = {}

    # NOTE: to make it easier to add new objects, motions, and states, the parser script will attempt to pre-load existing index files.
    # -- the reason for this is that the parsing will create index files based SOLELY on the provided un-parsed files if no file is provided
    #	or found in the directory of this script.

    if not config['Parser']['skip_JSON_conversion']:
        import FOON_convert_to_JSON as fjc

    ######################################################################################
    ########################	1)	FOON SUBGRAPH PARSING	#######################
    ######################################################################################

    objectIndex, motionIndex, stateIndex = update_index_files(
        file_list, objectIndex, object_senses, motionIndex, motion_categories, stateIndex)
    # end

    update_unparsed_files(file_list, objectIndex, motionIndex, stateIndex)

    print('\nSUMMARY OF CHANGES:')
    print('  -- new total of OBJECTS : ' + str(len(objectIndex)))
    print('  -- new total of STATES : ' + str(len(stateIndex)))
    print('  -- new total of MOTIONS : ' + str(len(motionIndex)))

    if config['Parser']['skip_JSON_conversion'] == 'False':
        import FOON_convert_to_JSON as fjc

        print('\n-- Converting all parsed files to .JSON format...')
        fjc.convert_to_JSON(source_dir)

    foon_categorization(motion_categories)

    print('\n-- FOON_parser.py complete!')


# def report_motion_frequency():
# 	source_dir = config['Paths']['data_source']
# 	file_list = os.listdir(source_dir)
# 	freq = {}
# 	for file in file_list:
# 		F = open(os.path.join(source_dir, file), "r")

# 		for line in F:
# 			if line.startswith('M'):
# 				freq[line.split('\t')[1]] = freq.get(line.split('\t')[1], 0) + 1

# 	for w in sorted(freq, key = freq.get, reverse=True):
# 		print(w.replace('*', '') + ','  + str(freq[w]))


if __name__ == '__main__':
    _check_args()
    _run_parser()
